# MH-Builder

MH-Builder is a C++ framework for designing adaptive metaheuristics for single and multi-objective optimization.

Note that this code repository also includes the code of the [`MOCA-I project`](https://software-orkad.univ-lille.fr/MOCA-I.html)

[:memo: Documentation](https://mh-builder-orkad.univ-lille.fr)
[:globe_with_meridians: ORKAD team web site](https://orkad.univ-lille.fr)


## Prerequisites

### Required Elements

* `C++ compiler` (version 20 or higher)
* `Cmake` (version 3.10 or higher)
* `Git`

### Additional elements

* [`Doxygen`](https://doxygen.nl) in order to obtain a detailed documentation of the source code.
During the documentation generation, `doxygen` uses different graphical tools to provide visualization tools (such as UML-like diagrams): [`dia`](http://dia-installer.de), [`plantuml`](https://plantuml.com), dot tool from [`graphhviz`](http://www.graphviz.org)
* `Google test Framework`, note that the compilation process automatically download this framework.
* [`matplotlib`](https://matplotlib.org) Python library for creating interactive visualizations. `Python` (version 3 or higher) is required
* [hypervolume indicator](https://lopez-ibanez.eu/hypervolume) : an implementation of the hypervolume computation (works with a number of objectives > 2)


## Quick installation
Default installation and compilation can be summarized as follows:

```bash
git clone https://gitlab.cristal.univ-lille.fr/orkad-public/mh-builder.git
cd mh-builder
mkdir build
cd build
cmake ..
make
```

## Running code
Once compiled, executables are stored in `build/bin`

Go to this folder and execute one of these executables, (analyse code source to discover required parameters).

## Folder organisation

- **src** the folder that contains code sources
- **src-bin** the folder that contains the code source of executables
- **doxygen/documentation** generated documentation available either in html or latex format.
- **build** generated binary code
- **external** external libraries are stored in this directory. The GIT project already includes 
  an implementation of the hypervolume computation (https://lopez-ibanez.eu/hypervolume)
- **tests** unit tests
- **instances** different datasets are provided for different kind of problems.


## Information

### Authors
See [Authors](./Authors)

### Licenses

MH-Builder is *dual licensed* under the following licenses. You can use the software according to the terms of your chosen license.

1. [GNU General Public License version 3 (GPLv3)](./LICENSE) GPL refers to the GNU General Public License as published by the Free Software Foundation;
   either version 3 of the License, or (at your option) any later version.
2. Possibility of proprietary licence : if, for any particular reason, you are interested with a no-copyleft licence, please contact the [ORKAD team](http://orkad.univ-lille.fr).

### Documentation

A more detailed documentation (work in progress) will be available [here](https://mh-builder-orkad.univ-lille.fr)

