/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERATED_ALGORITHM_H
#define MH_BUILDER_ITERATED_ALGORITHM_H

#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "core/criterion/CriterionAnd.h"
#include <functional>

namespace opt {
    /**
     * Class representing an iterated Algorithm
     * @tparam IN
     */
    template<typename IN>
    class IteratedAlgorithm : public core::Algorithm<IN> {
    public:
        using core::Algorithm<IN>::operator();

        /**
         * Constructor of a Iterated Algorithm
         * @param _perturbation perturbation criterion
         * @param _algorithm algorithm which need to be iterated
         * @param _criterion stop criterion
         * @param _update_func update function of output
         * @param _copy_func update function of output
         * @param _tmp_func update function of output
         */
        IteratedAlgorithm(
                core::Algorithm<IN> &_perturbation,
                core::Algorithm<IN> &_algorithm,
                core::Criterion<IN> &_criterion,
                std::function<void(IN &_in, IN &_current, IN &_tmp)> _update_func = [](IN &_in, IN &_current,
                                                                                       IN &_tmp __attribute__((unused))) {
                    if (_current > _in)_in = _current;
                },
                std::function<void(IN &_in, IN &_current)> _copy_func = [](IN &_in, IN &_current) { _current = _in; },
                std::function<void(IN &_current, IN &_tmp)> _tmp_func = [](IN &_current __attribute__((unused)),
                                                                           IN &_tmp __attribute__((unused))) { /* do nothing */ })
                :
                perturbation(&_perturbation), algorithm(&_algorithm), update_func(_update_func), tmp_func(_tmp_func),
                copy_func(_copy_func) {
            this->criterion = &_criterion;
        }

        void init([[maybe_unused]] const IN &_in) override {
            this->criterion->init();
        }

        /**
         * Iterated Algorithm
         * @param _in input
         */
        void operator()(IN &_in, core::Criterion<IN> &_criterion) override {
            init(_in);
            core::criterion::CriterionAnd<IN> criterionComb(*this->criterion, _criterion);
            IN current;
            copy_func(_in, current);
            IN tmp;
            tmp_func(current, tmp);
            (*algorithm)(current, criterionComb);
            this->checkpoint->operator()(current);
            update_func(_in, current, tmp);
            while (criterionComb()) {
                tmp_func(current, tmp);
                (*perturbation)(current);
                this->checkpoint->operator()(current);
                (*algorithm)(current, criterionComb);
                this->checkpoint->operator()(current);
                update_func(_in, current, tmp);
                criterionComb.update();
            }
        }

        /**
        * Iterated Algorithm
        * @param _in input
        */
        void operator()(IN &_in) override {
            init(_in);
            IN current;
            copy_func(_in, current);
            IN tmp;
            tmp_func(current, tmp);
            (*algorithm)(current, *this->criterion);
            this->checkpoint->operator()(current);
            update_func(_in, current, tmp);
            while ((*this->criterion)()) {
                tmp_func(current, tmp);
                (*perturbation)(current);
                this->checkpoint->operator()(current);
                (*algorithm)(current);
                this->checkpoint->operator()(current);
                update_func(_in, current, tmp);
                this->criterion->update();
            }
        }

        /**
         * Set perturbation
         * @param _perturbation
         */
        void setPerturbation(core::Algorithm<IN> &_perturbation) {
            perturbation = &_perturbation;
        }

        /**
         * Set algorithm
         * @param _algorithm
         */
        void setAlgorithm(core::Algorithm<IN> &_algorithm) {
            algorithm = &_algorithm;
        }
        /**
         * get algorithm
         */
        core::Algorithm<IN> *getAlgorithm() {
            return algorithm ;
        }

    protected:
        core::Algorithm<IN> *perturbation;
        core::Algorithm<IN> *algorithm;
        std::function<void(IN &_in, IN &_current, IN &_tmp)> update_func;
        std::function<void(IN &_current, IN &_tmp)> tmp_func;
        std::function<void(IN &_current, IN &_tmp)> copy_func;
    };
}

#endif //MH_BUILDER_ITERATED_ALGORITHM_H
