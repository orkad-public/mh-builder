/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_AGGREGATION_EVAL_H
#define MH_BUILDER_AGGREGATION_EVAL_H

#include "core/Eval.h"
#include <vector>


namespace opt::eval {
    template<class SOL>
    class AggregationEval : public core::Eval<SOL> {
    public:

        explicit AggregationEval(core::Eval<SOL> &_eval) : eval(_eval), weights({1}) {}

        virtual void init(std::vector<double> _weights) {
            weights = _weights;
        }

        virtual void operator()(SOL &sol) {
            eval(sol);
            auto &fit = sol.fitness();
            auto scal = fit[0] * weights[0];
            for (unsigned long long int i = 1; i < weights.size(); i++)
                scal += fit[i] * weights[i];
            fit.scalar(scal);
        };

    protected:
        core::Eval<SOL> &eval;
        std::vector<double> weights;
    };
}

#endif //MH_BUILDER_AGGREGATION_EVAL_H
