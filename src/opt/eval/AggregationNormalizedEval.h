/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_AGGREGATION_NORMALIZED_EVAL_H
#define MH_BUILDER_AGGREGATION_NORMALIZED_EVAL_H

#include "core/Eval.h"
#include "opt/eval/AggregationEval.h"
#include <vector>
#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"
#include <utility>

namespace opt {
    namespace eval {
        template<class SOL>
        class AggregationNormalizedEval : public AggregationEval<SOL> {
        public:

            AggregationNormalizedEval(core::Eval <SOL> &_eval, std::vector<double> _bounds)
                    :
                    AggregationEval<SOL>(_eval),
                    bounds(std::move(_bounds)) {}

            AggregationNormalizedEval(core::Eval <SOL> &_eval, std::vector<std::pair<double, double>> _bounds)
                    :
                    AggregationEval<SOL>(_eval),
                    bounds() {
                for (auto p: _bounds) {
                    bounds.push_back(p.first);
                    bounds.push_back(p.second);
                }
            }

            void operator()(SOL &sol) {
                this->eval(sol);
                scalarize(sol);
            };

            void operator()(SOL &sol, opt::singleSolution::neighborhood::neighbor::Neighbor <SOL> &neighbor) {
                this->eval(sol, neighbor);
                scalarize(sol);
            }

            void scalarize(SOL &sol) {
                auto &fit = sol.fitness();
                auto scal = normalize(fit[0], 0) * this->weights[0];
                for (unsigned long long int i = 1; i < this->weights.size(); i++)
                    scal += normalize(fit[i], i) * this->weights[i];
                fit.scalar(scal);
            }

            template<typename FIT>
            FIT normalize(FIT &fit, unsigned long long int i) {
                return (fit - bounds[2 * i]) / (bounds[2 * i + 1] - bounds[2 * i]);
            }

        protected:
            std::vector<double> bounds;
        };
    }
}
#endif //MH_BUILDER_AGGREGATION_NORMALIZED_EVAL_H
