/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "TimeCheckpoint.h"

/**
  * constructor, set inner values
**/
template<typename SOL>
TimeCheckpoint<SOL>::TimeCheckpoint() : values(0) {
    timerHelper.start();
}

/**
 * initialisation of inner values
 * @param _sol , here unused
 **/
template<typename SOL>
void TimeCheckpoint<SOL>::init([[maybe_unused]] const SOL &_sol) {
    values.clear();
    timerHelper.start();
}

/**
 * add a checkpoint
 * @param _sol here unused
 **/
template<typename SOL>
void TimeCheckpoint<SOL>::operator()([[maybe_unused]] const SOL &_sol) {
    timerHelper.update();
    values.push_back(timerHelper.elapsedTime());
}

/**
 * provides all stored checkpoint values
 * @return vector of elapsed times
 **/
template<typename SOL>
std::vector<double> &TimeCheckpoint<SOL>::getValues() {
    return values;
}

/**
         * provide the elapsed time since the initialisation
         * @return
         **/
template<typename SOL>
double TimeCheckpoint<SOL>::getElapsedTime() {
    timerHelper.update();
    return timerHelper.elapsedTime();
}