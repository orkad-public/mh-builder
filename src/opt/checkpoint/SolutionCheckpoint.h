/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SOLUTIONCHECKPOINT_H
#define MH_BUILDER_SOLUTIONCHECKPOINT_H

#include "core/Checkpoint.h"
#include <vector>
#include "opt/checkpoint/TimeCheckpoint.h"

using namespace std ;
namespace opt::checkpoint {
    /**
     * Class that allows to store checkpoints.
     * for one checkpoint, the first objective of a fitness of a solution
     * and the elapsed time are stored
     * @tparam SOL : the type of the solution
     */
    template<typename SOL>
    class SolutionCheckpoint : public core::Checkpoint<SOL> {
    public:
        typedef typename SOL::FITNESS FIT;
        typedef typename FIT::VALUE_TYPE VALUE;
    protected:
        std::vector<VALUE> values;
        TimeCheckpoint<SOL> time_checkpoint;
    public:

        /**
         * constructor, set inner values
         */
        SolutionCheckpoint() : values(0) {}

        /**
         * default destructor
         */
        ~SolutionCheckpoint() = default;

        /**
         * initialisation of inner values
         * @param _sol , here unused
         */
        void init(const SOL &_sol);

        /**
         * add a checkpoint
         * @param _sol store the first objective of the fitness
        */
        void operator()(const SOL &_sol);

        /**
         * provide values (first objective of fitness)
         * @tparam SOL
         * @return vector
        */
        virtual std::vector<VALUE> &getValues()  ;

        /**
         * provide elapsed time values
         * @return
        **/
        virtual std::vector<double> &getTimeValues();
    };

#include "SolutionCheckpoint.tpp"

}
#endif //MH_BUILDER_SOLUTIONCHECKPOINT_H
