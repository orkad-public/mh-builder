/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TIMECHECKPOINT_H
#define MH_BUILDER_TIMECHECKPOINT_H

#include "core/Checkpoint.h"
#include <vector>
#include "util/TimerHelper.h"

namespace opt::checkpoint {
    /**
     * class that stores elapsed time during execution each the checkpoint is invoked
     * @tparam SOL
     */
    template<typename SOL>
    class TimeCheckpoint : public core::Checkpoint<SOL> {
    protected:
        std::vector<double> values;   /* stored values for each checkpoint */
        util::TimerHelper timerHelper;
    public:
        /**
         * constructor, set inner values
         */
        explicit TimeCheckpoint() ;

        /**
         * initialisation of inner values
         * @param _sol , here unused
         */
        void init([[maybe_unused]] const SOL &_sol) override ;

        /**
         * add a checkpoint
         * @param _sol here unused
         */
        void operator()([[maybe_unused]] const SOL &_sol) override ;

        /**
         * provides all stored checkpoint values
         * @return vector of elapsed times
         */
        std::vector<double> &getValues() ;

        /**
         * provide the elapsed time since the initialisation
         * @return
         */
        double getElapsedTime() ;
    };
#include "TimeCheckpoint.tpp"
}
#endif //MH_BUILDER_TIMECHECKPOINT_H
