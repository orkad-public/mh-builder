/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ONLYIMPRSOLUTIONCHECKPOINT_H
#define MH_BUILDER_ONLYIMPRSOLUTIONCHECKPOINT_H

#include "opt/checkpoint/SolutionCheckpoint.h"

namespace opt::checkpoint {
    /**
     * this specialization of SolutionCheckpoint only stores
     * checkpoints when the fitness is improved
     * @tparam SOL
     */
    template<typename SOL>
    class OnlyImproveSolutionCheckpoint : public SolutionCheckpoint<SOL> {
    public:
        typedef typename SOL::FITNESS FIT;
    protected:
        FIT last;
    public:
        /**
         * store a checkpoint if the fitness is improved
         * @param _sol the solution
         */
        void operator()(const SOL &_sol) override ;
    };

#include "OnlyImproveSolutionCheckpoint.tpp"
}
#endif //MH_BUILDER_ONLYIMPRSOLUTIONCHECKPOINT_H
