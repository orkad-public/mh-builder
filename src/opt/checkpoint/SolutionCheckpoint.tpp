/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/
#include "opt/checkpoint/SolutionCheckpoint.h"



/**
         * initialisation of inner values
         * @param _sol , here unused
*/
template<typename SOL>
void SolutionCheckpoint<SOL>::init(const SOL &_sol) {
    values.clear();
    time_checkpoint.init(_sol);
}

/**
 * add a checkpoint
 * @param _sol store the first objective of the fitness
*/
template<typename SOL>
void SolutionCheckpoint<SOL>::operator()(const SOL &_sol) {
    values.push_back(_sol.fitness()[0]);
    time_checkpoint(_sol);
}

/**
         * provide values (first objective of fitness)
         * @tparam SOL
         * @return vector
        */
template<typename SOL>
std::vector<typename SolutionCheckpoint<SOL>::VALUE> &SolutionCheckpoint<SOL>::getValues() { return values; }


/**
 * provide elapsed time values
 * @return
 **/
template<typename SOL>
std::vector<double> &SolutionCheckpoint<SOL>::getTimeValues() {
    return time_checkpoint.getValues();
}