/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/
#include "opt/checkpoint/ArchiveCheckpoint.h"



/**
         * initialisation of inner values
         * @param _arch
*/
template<typename ARCHIVE>
void ArchiveCheckpoint<ARCHIVE>::init(const ARCHIVE &_arch) {
    if (_arch.size() > 0) {
        auto oneSol = _arch[0];
        this->numberOfObjectives = oneSol.fitness().objectives().size();
        this->isMaximisationFitness = oneSol.fitness().isMaximisationFitness();
        this->time_checkpoint.init(0.0);
    } else throw std::runtime_error("ArchiveCheckpoint::init(_arch) : Error, the archive is empty ");
}

/**
         * add a checkpoint if the elapsed time between the previous checkpoint >= frequency
         * store objective values of the archive for all objectives
         * @param _sol the given archive
        */
template<typename ARCHIVE>
void ArchiveCheckpoint<ARCHIVE>::operator()(const ARCHIVE &_arch) {
    if ((this->archive_checkpoint.size() == 0 // first checkpoint or delay between last checkpoint >= frequency
         || (this->time_checkpoint.getElapsedTime() - this->time_checkpoint.getValues().back()) >= this->frequency)) {
        this->archive_checkpoint.push_back(_arch);
        this->time_checkpoint(0.0);
    }
}


/**
 * provide best objective values of all checkpoints for a given objective
 * here the best objective value of a checkpoint i can be worst than the objective value of checkpoint i-1
 * @param unsigned long long int the objective number
 * @return vector
*/
template<typename ARCHIVE>
std::vector<typename ArchiveCheckpoint<ARCHIVE>::VALUE>
ArchiveCheckpoint<ARCHIVE>::getValues(unsigned long long int objNum) {
    std::vector<typename ArchiveCheckpoint<ARCHIVE>::VALUE> result;
    for (auto oneArchive: this->archive_checkpoint) {
        typename ArchiveCheckpoint<ARCHIVE>::VALUE best = oneArchive[0].fitness()[objNum];
        for (auto sol : oneArchive) {
            auto objValue = sol.fitness()[objNum] ;
            if (this->isBetter(objValue, best)) best = objValue;
        }
        result.push_back(best);
    }
    return result;
}


/**
         * provide the evolution of best memorised objective values found
         * @param unsigned long long int the objective number
         * @return vector
        */
template<typename ARCHIVE>
std::vector<typename ArchiveCheckpoint<ARCHIVE>::VALUE>
ArchiveCheckpoint<ARCHIVE>::getBestValues(unsigned long long int objNum) {
    std::vector<typename ArchiveCheckpoint<ARCHIVE>::VALUE> result = this->getValues(objNum);
    for (unsigned long long int i = 1; i < result.size(); i++)
        if (this->isBetter(result[i - 1], result[i])) {
            result[i] = result[i - 1];
        }
    return result;
}



/**
 * provide elapsed time values
 * @return
 **/
template<typename ARCHIVE>
std::vector<double> &ArchiveCheckpoint<ARCHIVE>::getTimeValues() {
    return this->time_checkpoint.getValues();
}

/**
         * provide all checkpoints
         * @return
        **/
template<typename ARCHIVE>
std::vector<ARCHIVE> &ArchiveCheckpoint<ARCHIVE>::getCheckpoints() {
    return this->archive_checkpoint;
}
