/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ARCHIVE_CHECKPOINT_H
#define MH_BUILDER_ARCHIVE_CHECKPOINT_H

#include "core/Checkpoint.h"
#include <vector>
#include "opt/checkpoint/TimeCheckpoint.h"

using namespace std ;
namespace opt::checkpoint {
    /**
     * Class that allows to store checkpoints, this class is suitable for archive
     * At each checkpoint, the current archive and time checkpoint are stored
     * At the end of a running process, the archive can provides :
     * (1) for one given objective,  the  best objective values from the archive at instant t,
     * (2) the preservation of the best objective values from t0 to t
     * (3) the elapsed time t
     * @tparam SOL : the type of the solution
     */
    template<typename ARCHIVE>
    class ArchiveCheckpoint : public core::Checkpoint<ARCHIVE> {
    public:
        typedef typename ARCHIVE::SOLUTION_TYPE SOL;
        typedef typename SOL::FITNESS FIT;
        typedef typename FIT::VALUE_TYPE VALUE;
    protected:
        std::vector<ARCHIVE> archive_checkpoint ;
        TimeCheckpoint<double> time_checkpoint; // elapsed time values
        unsigned long long int numberOfObjectives ;
        bool isMaximisationFitness ;
        unsigned long long int frequency ; // the minimum time in milliseconds between stored checkpoints

    public:

        /**
         * constructor, set inner values
         * @param frequency the minimum time in milliseconds between stored checkpoints
         */
        ArchiveCheckpoint(unsigned long long int _frequency = 1000) : archive_checkpoint(0),
            numberOfObjectives(1), isMaximisationFitness(true), frequency(_frequency)  {}

        /**
         * default destructor
         */
        ~ArchiveCheckpoint() = default;

        /**
         * initialisation of inner values
         * @param _sol
         */
        void init(const ARCHIVE &_sol);

        /**
         * add a checkpoint if the elapsed time between the previous checkpoint >= frequency
         * store objective values of the archive for all objectives
         * @param _sol the given archive
        */
        void operator()(const ARCHIVE &_sol);

        /**
         * provide the evolution of best memorised objective values found
         * @param unsigned long long int the objective number
         * @return vector
        */
        virtual std::vector<VALUE> getValues(unsigned long long int objNum)  ;

        /**
         * provide best objective values
         * @param unsigned long long int the objective number
         * @return vector
        */
        virtual std::vector<VALUE> getBestValues(unsigned long long int objNum)  ;
        /**
         * provide elapsed time values shared by all objectives
         * @return
        **/
        virtual std::vector<double> &getTimeValues();
        /**
         * provide all checkpoints
         * @return
        **/
        virtual std::vector<ARCHIVE> &getCheckpoints();

    private:
        /**
         * return true if v1 is better than v2
         * @param v1
         * @param v2
         */
        bool isBetter(VALUE v1, VALUE v2) {
            if (this->isMaximisationFitness) return v1 > v2 ;
            else return v1 < v2 ;
        }

    };

#include "ArchiveCheckpoint.tpp"

}
#endif //MH_BUILDER_ARCHIVE_CHECKPOINT_H
