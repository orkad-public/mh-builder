/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TABULIST_H
#define MH_BUILDER_TABULIST_H

#include "core/Algorithm.h"
#include <vector>

namespace opt {
    namespace memory {
        template<typename IN>
        class TabuList : core::Algorithm<IN> {
        public:

            /**
             * Constructor of tabu list
             * @param _maxSize the maximum size of the tabu list
             * @param _nbIter the number of iteration a input is tabu
             */
            explicit TabuList(unsigned long long int _maxSize, unsigned long long int _nbIter) : maxSize(_maxSize),
                                                                                                 nbIter(_nbIter),
                                                                                                 currentIndex(0) {
                tabuList.reserve(_maxSize);
                tabuList.resize(0);
            }

            /**
             * Init the tabu list
             * @param _input
             */
            virtual void init(IN &_in __attribute__((unused))) {
                tabuList.resize(0);
                currentIndex = 0;
            }

            /**
             * Add a new element in the tabu list
             * @param _in the element to add in tabu list
             */
            virtual void operator()(IN &_in) {
                if (tabuList.size() < maxSize)
                    tabuList.emplace_back(std::pair<IN, unsigned long long int>(_in, 0));
                else
                    tabuList[currentIndex++ % maxSize] = {_in, 0};
            }

            /**
             * Update the tabu list
             */
            virtual void update() {
                for (unsigned long long int i = 0; i < tabuList.size(); i++) {
                    tabuList[i].second++;
                }
            }

            /**
             * Check if input is tabu
             * @param _in
             * @return boolean
             */
            virtual bool isTabu(const IN &_in) {
                for (unsigned long long int i = 0; i < tabuList.size(); i++) {
                    if (tabuList[i].first == _in && tabuList[i].second < nbIter) {
                        return true;
                    }
                }
                return false;
            };

        protected:
            /*! Tabu List */
            std::vector<std::pair<IN, unsigned long long int>> tabuList;
            /*! Maximum size of the tabu list */
            unsigned long long int maxSize;
            /*! Number of iteration a move is tabu */
            unsigned long long int nbIter;
            /*! current index on the tabu list */
            unsigned long long int currentIndex;
        };
    }
}

#endif //MH_BUILDER_TABULIST_H
