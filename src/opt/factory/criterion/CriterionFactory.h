/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CRITERIONFACTORY_H
#define MH_BUILDER_CRITERIONFACTORY_H

#include "core/Criterion.h"
#include "opt/factory/Factory.h"
#include "opt/criterion/EvalCriterion.h"
#include "opt/criterion/IterCriterion.h"
#include "opt/criterion/TimeCriterion.h"

#include <string>

namespace opt {
    namespace factory {
        namespace criterion {

            template<typename SOL, typename EVAL>
            class CriterionFactory
                    : public opt::factory::Factory<core::Criterion<SOL>, std::string, unsigned long long int &, EVAL &> {
            public:

                CriterionFactory() {
                    this->register_key("iter", [](unsigned long long int criterion_length,
                                                  [[maybe_unused]] EVAL &eval ) {
                        return new opt::criterion::IterCriterion<SOL>(criterion_length);
                    });
                    this->register_key("time", [](unsigned long long int criterion_length,
                                                  [[maybe_unused]] EVAL &eval ) {
                        return new opt::criterion::TimeCriterion<SOL>(criterion_length);
                    });
                    this->register_key("eval", [](unsigned long long int criterion_length, EVAL &eval) {
                        return new opt::criterion::EvalCriterion<SOL>(criterion_length, eval);
                    });
                }
            };
        }

    }
}
#endif //MH_BUILDER_CRITERIONFACTORY_H
