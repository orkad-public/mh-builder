/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBORHOODFACTORY_H
#define MH_BUILDER_NEIGHBORHOODFACTORY_H
#include <string>
#include "opt/factory/Factory.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "opt/singleSolution/neighborhood/RandomNeighborhood.h"


namespace opt {
    namespace factory {
        namespace singleSolution {
            template<typename SOL, typename... Args>
            class NeighborhoodFactory
                    : public opt::factory::Factory<opt::singleSolution::neighborhood::Neighborhood<SOL>, std::string, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> &> {
            public:

                NeighborhoodFactory() {
                    this->register_key("rnd", [](opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> &_neighbor) {
                        return new opt::singleSolution::neighborhood::RandomNeighborhood<SOL>(_neighbor);
                    });
                    this->register_key("order", [](opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> &_neighbor) {
                        return new opt::singleSolution::neighborhood::IndexNeighborhood<SOL>(_neighbor);
                    });
                }
            };
        }
    }
}
#endif //MH_BUILDER_NEIGHBORHOODFACTORY_H
