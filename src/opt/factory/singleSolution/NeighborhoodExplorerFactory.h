/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBORHOODEXPLORERFACTORY_H
#define MH_BUILDER_NEIGHBORHOODEXPLORERFACTORY_H

#include <string>
#include "opt/factory/Factory.h"
#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "opt/singleSolution/neighborhood/explorer/BestImprNeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/explorer/FirstImprNeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/explorer/WorstImprNeighborhoodExplorer.h"


namespace opt::factory::singleSolution {

    template<typename SOL, typename EVAL>
    class NeighborhoodExplorerFactory
            : public opt::factory::Factory<opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL>, std::string,
                    opt::singleSolution::neighborhood::Neighborhood<SOL> &, EVAL &> {
    public:

        NeighborhoodExplorerFactory() {
            this->register_key("best", [](opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood,
                                          EVAL &eval) {
                return new opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<SOL>(_neighborhood,
                                                                                                          eval);
            });
            this->register_key("first", [](opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood,
                                           EVAL &eval) {
                return new opt::singleSolution::neighborhood::explorer::FirstImprNeighborhoodExplorer<SOL>(
                        _neighborhood, eval);
            });
            this->register_key("worst-improve", [](opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood,
                                                   EVAL &eval) {
                return new opt::singleSolution::neighborhood::explorer::WorstImprNeighborhoodExplorer<SOL>(
                        _neighborhood, eval);
            });
        }
    };
}

#endif //MH_BUILDER_NEIGHBORHOODEXPLORERFACTORY_H
