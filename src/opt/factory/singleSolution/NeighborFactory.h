/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBOR_FACTORY_H
#define MH_BUILDER_NEIGHBOR_FACTORY_H

#include <string>
#include "opt/factory/Factory.h"
#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"
#include "representation/permutation/neighborhood/neighbor/ShiftNeighbor.h"
#include "representation/permutation/neighborhood/neighbor/AdjSwapNeighbor.h"
#include "representation/permutation/neighborhood/neighbor/TwoOptNeighbor.h"
#include "representation/permutation/neighborhood/neighbor/DoubleBridgeNeighbor.h"
#include "representation/permutation/neighborhood/neighbor/IncrDoubleBridgeNeighbor.h"
#include "representation/permutation/neighborhood/neighbor/ShiftWithoutSwapNeighbor.h"
#include "representation/permutation/neighborhood/neighbor/SwapNeighbor.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"


namespace opt::factory::singleSolution {

    template<typename SOL, typename NEIGHBOR = opt::singleSolution::neighborhood::neighbor::Neighbor<SOL>>
    class NeighborFactory : public opt::factory::Factory<NEIGHBOR, std::string> {
    public:

        NeighborFactory() {
            this->register_key("shift",
                               []() { return new representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL>(); });
            this->register_key("adj",
                               []() { return new representation::permutation::neighborhood::neighbor::AdjSwapNeighbor<SOL>(); });
            this->register_key("shift_without_adj",
                               []() { return new representation::permutation::neighborhood::neighbor::ShiftWithoutSwapNeighbor<SOL>(); });
            this->register_key("swap",
                               []() { return new representation::permutation::neighborhood::neighbor::SwapNeighbor<SOL>(); });
            this->register_key("2opt",
                               []() { return new representation::permutation::neighborhood::neighbor::TwoOptNeighbor<SOL>(); });
            this->register_key("doubleBridge",
                               []() { return new representation::permutation::neighborhood::neighbor::DoubleBridgeNeighbor<SOL>(); });
            this->register_key("Incr2opt",
                               []() { return new representation::permutation::neighborhood::neighbor::IncrDoubleBridgeNeighbor<SOL>(); });
        }
    };
}


#endif //MH_BUILDER_NEIGHBOR_FACTORY_H
