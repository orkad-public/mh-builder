/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PERTURBATIONFACTORY_H
#define MH_BUILDER_PERTURBATIONFACTORY_H
#include <string>
#include "opt/factory/Factory.h"
#include "core/Perturbation.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "opt/singleSolution/perturbation/NeighborhoodPerturbation.h"
#include "opt/perturbation/DummyPerturbation.h"

namespace opt {
    namespace factory {
        namespace singleSolution {
            template<typename SOL, typename EVAL>
            class PerturbationFactory
                    : public opt::factory::Factory<core::Perturbation<SOL>, std::string, opt::singleSolution::neighborhood::Neighborhood<SOL> &, EVAL *, unsigned long long int &> {
            public:

                PerturbationFactory() {
                    this->register_key("neighborhood", [](opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood, EVAL *eval,
                                                          unsigned long long int perturbation_strength) {
                        return new opt::singleSolution::perturbation::NeighborhoodPerturbation<SOL>(_neighborhood, eval, perturbation_strength);
                    });
                    this->register_key("no", []([[maybe_unused]] opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood, [[maybe_unused]] EVAL *eval,
                                                [[maybe_unused]] unsigned long long int perturbation_strength) {
                        return new opt::perturbation::DummyPerturbation<SOL>();
                    });
                }
            };
        }
    }
}
#endif //MH_BUILDER_PERTURBATIONFACTORY_H
