/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FACTORY_H
#define MH_BUILDER_FACTORY_H
#include <string>
#include <unordered_map>
#include <map>
#include <functional>
#include <stdexcept>
#include <cassert>

namespace opt {
    namespace factory {
        template<typename Object, typename Key = std::string, typename... Args>
        class Factory {
        public:
            using Creator = std::function<Object *(Args...)>;

            Factory() {
                map = std::unordered_map<Key, Creator>();
            }

            void register_key(Key key, const Creator &creator) {
                if (map.count(key) == 0) {
                    assert(creator);
                    map.emplace(key, creator);
                } else {
                    throw std::runtime_error("Factory.h : key <" + key + "> already exist in map");
                }

            }


            Object *create(const Key &key, Args &&... args) {
                if (map.count(key) == 1) {
                    assert(map[key]);
                    return map[key](std::forward<Args>(args)...);
                }
                throw std::runtime_error("Factory.h : key <" + key + "> doesn't exist in map");
            }

        protected:
            std::unordered_map <Key, Creator> map;
        };
    }
}
#endif //MH_BUILDER_FACTORY_H
