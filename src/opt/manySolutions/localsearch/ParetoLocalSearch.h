/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PARETOLOCALSEARCH_H
#define MH_BUILDER_PARETOLOCALSEARCH_H
#include "opt/IteratedAlgorithm.h"
#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "opt/manySolutions/explorer/Explorer.h"
#include "opt/manySolutions/selection/Select.h"

namespace opt {
    namespace manySolutions {
        namespace localsearch {

            /**
             * Class representing a simple PLS
             * @tparam ARCHIVE
             */
            template<typename ARCHIVE, typename ARCHIVE_TMP = ARCHIVE>
            class ParetoLocalSearch : public core::Algorithm<ARCHIVE> {
            public:

                typedef typename ARCHIVE::SOLUTION_TYPE SOLUTION;
                using core::Algorithm<ARCHIVE>::operator();

                /**
                 * Constructor of a Pareto Local Search
                 * @param _select
                 * @param _explorer
                 */
                ParetoLocalSearch(opt::manySolutions::selection::Select <ARCHIVE> &_select, opt::manySolutions::explorer::Explorer <ARCHIVE> &_explorer,
                                  bool _arch_as_ref = false) : select(&_select), explorer(&_explorer),
                                                               arch_as_ref(_arch_as_ref) {
                }

                /**
                 * Explore an archive
                 * @param _in
                 */
                virtual void operator()(ARCHIVE &_in, core::Criterion <ARCHIVE> &_criterion) {
                    do {
                        (*select)(_in);
                        ARCHIVE_TMP solution_accepted;
                        for (unsigned long long int i = 0; i < select->size() && _criterion(); i++) {
                            ARCHIVE_TMP explor;
                            if (!arch_as_ref)
                                (*explorer)(_in[(*select)[i]], explor, _criterion);
                            else
                                (*explorer)(_in[(*select)[i]], _in, explor, _criterion);
                            solution_accepted(explor);
                        }
                        _in(solution_accepted);
                    } while (!select->empty() && _criterion());
                }

                void setSelect(opt::manySolutions::selection::Select <ARCHIVE> &_select) {
                    select = &_select;
                }

                void setExplorer(opt::manySolutions::explorer::Explorer <ARCHIVE> &_explorer) {
                    explorer = &_explorer;
                }

            protected:
                opt::manySolutions::selection::Select <ARCHIVE> *select;
                opt::manySolutions::explorer::Explorer <ARCHIVE> *explorer;
                bool arch_as_ref;
            };
        }
    }
}

#endif //MH_BUILDER_PARETOLOCALSEARCH_H
