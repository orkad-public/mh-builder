/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERATEDPARETOLOCALSEARCH_H
#define MH_BUILDER_ITERATEDPARETOLOCALSEARCH_H

#include "opt/IteratedAlgorithm.h"
#include "core/Algorithm.h"
#include "core/Criterion.h"

namespace opt {
    namespace manySolutions {
        namespace localsearch {

            /**
             * Class representing an iterated PLS
             * @tparam ARCHIVE
             */
            template<typename ARCHIVE>
            class IteratedParetoLocalSearch : public opt::IteratedAlgorithm<ARCHIVE> {
            public:
                /**
                 * Constructor of a Iterated PLS
                 * @param _perturbation perturbation criterion
                 * @param _algorithm algorithm wich need to be iterate
                 * @param _criterion stop criterion
                 * @param _update_func update function of output
                 */
                IteratedParetoLocalSearch(core::Algorithm <ARCHIVE> &_perturbation,
                                          core::Algorithm <ARCHIVE> &_localSearch,
                                          core::Criterion <ARCHIVE> &_criterion) :
                        opt::IteratedAlgorithm<ARCHIVE>(
                                _perturbation,
                                _localSearch,
                                _criterion,
                                [](ARCHIVE &_in, ARCHIVE &_current, ARCHIVE &_tmp __attribute__((unused))) {
                                    _in(_current);
                                }, // update func
                                [](ARCHIVE &_in, ARCHIVE &_current) { _current(_in); } // copy func
                        ) {}

            };
        }
    }
}

#endif //MH_BUILDER_ITERATEDPARETOLOCALSEARCH_H
