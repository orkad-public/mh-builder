/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SELECT_H
#define MH_BUILDER_SELECT_H
#include <vector>
#include "core/Algorithm.h"

namespace opt {
    namespace manySolutions {
        namespace selection {
            /**
             * Class representing a select operator
             * @tparam ARCHIVE
             */
            template<typename ARCHIVE>
            class Select : core::Algorithm<ARCHIVE> {
            public:
                typedef typename std::vector<unsigned long long int>::iterator iterator;
                typedef typename std::vector<unsigned long long int>::const_iterator const_iterator;

                /**
                 * Constructor of a select operator
                 * @param _selectOnlyUnvisited strategy selection (true if we want only unvisited solution)
                 */
                explicit Select(bool _selectOnlyUnvisited = true) : selectOnlyUnvisited(_selectOnlyUnvisited),
                                                                    vector(0) {}

                /**
                 * Return the indices of selected solutions
                 * @param _archive
                 * @return the vector of indices of selected solutions
                 */
                virtual void operator()(const ARCHIVE &_archive) = 0;

                /**
               * Get k^th indices
               * @param k
               * @return the k^th indices
               */
                unsigned long long int operator[](unsigned int k) {
                    return vector[k];
                }

                /**
                 * Get k^th indices
                 * @param k
                 * @return the k^th indices
                 */
                unsigned long long int operator[](unsigned int k) const {
                    return vector[k];
                }

                /**
                 * Get size of the vector
                 * @return the vector size
                 */
                [[nodiscard]] unsigned long long int size() const {
                    return vector.size();
                }

                /**
                * Begin iterator of the vector
                */
                [[nodiscard]] const_iterator begin() const {
                    return vector.begin();
                }

                /**
                 * End iterator of the vector
                 */
                [[nodiscard]] const_iterator end() const {
                    return vector.end();
                }

                /**
                 * Begin iterator of the vector
                 */
                iterator begin() {
                    return vector.begin();
                }

                /**
                 * End iterator of the vector
                 */
                iterator end() {
                    return vector.end();
                }

                /**
                 * Test if the vector is empty
                 * @return true if the vector is empty
                 */
                [[nodiscard]] bool empty() const {
                    return vector.empty();
                }

                /**
                 * Set select only unvisited solution
                 * @param b bool
                 */
                void setSelectOnlyUnvisited(bool b) {
                    selectOnlyUnvisited = b;
                }

            protected:
                bool selectOnlyUnvisited;
                std::vector<unsigned long long int> vector;
            };
        }
    }
}

#endif //MH_BUILDER_SELECT_H
