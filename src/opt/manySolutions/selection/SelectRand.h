/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SELECTRAND_H
#define MH_BUILDER_SELECTRAND_H

#include "opt/manySolutions/selection/SelectAll.h"
#include "util/RNGHelper.h"

namespace opt {
    namespace manySolutions {
        namespace selection {
            /**
             * Class representing a select operator which return a random chosen indices vector of selected solution
             * @tparam ARCHIVE
             */
            template<typename ARCHIVE>
            class SelectRand : public SelectAll<ARCHIVE> {
            public:
                /**
                 * Constructor of a select operator
                 * @param _size the number of solution to choose
                 * @param _selectOnlyUnvisited strategy selection (true if we want only unvisited solution)
                 */
                explicit SelectRand(unsigned long long int _size, bool _selectOnlyUnvisited = true)
                        : SelectAll<ARCHIVE>(_selectOnlyUnvisited), size(_size) {}

                virtual ~SelectRand() = default;

                /**
                 * compute the indices vector
                 * @param _archive
                 */
                virtual void operator()(const ARCHIVE &_archive) {
                    SelectAll<ARCHIVE>::operator()(_archive);
                    for (unsigned long long int i = 0; i < size && i < this->vector.size(); i++) {
                        auto j = util::RNGHelper::get()->random(i, this->vector.size());
                        std::swap(this->vector[i], this->vector[j]);
                    }
                    if (this->vector.size() > size)
                        this->vector.resize(size);
                };

            protected:
                unsigned long long int size;
            };
        }
    }
}
#endif //MH_BUILDER_SELECTRAND_H
