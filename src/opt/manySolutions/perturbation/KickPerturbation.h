/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KICKPERTURBATION_H
#define MH_BUILDER_KICKPERTURBATION_H

#include "core/Perturbation.h"
#include "core/Criterion.h"
#include "util/RNGHelper.h"

namespace opt {
    namespace manySolutions {
        namespace perturbation {

            /**
             * Class representing a Kick perturbation
             * @tparam IN input type
             */
            template<typename ARCHIVE>
            class KickPerturbation : public core::Perturbation<ARCHIVE> {
            public:

                typedef typename ARCHIVE::SOLUTION_TYPE SOLUTION;


                using core::Perturbation<ARCHIVE>::operator();

                /**
                 * Constructor of the perturbation
                 * @param _perturbation the solution perturbation operator
                 * @param _strength number of perturbation
                 */
                explicit KickPerturbation(core::Perturbation<SOLUTION> &_perturbation,
                                          unsigned long long int _strength = 0) :
                        perturbation(_perturbation),
                        strength(_strength) {}

                void operator()(ARCHIVE &_in, core::Criterion<ARCHIVE> &_criterion __attribute__((unused))) override {
                    unsigned long long int size = (strength == 0 || _in.size() < strength) ? _in.size() : strength;
                    for (unsigned long long int i = 0; i < size; i++) {
                        int index = util::RNGHelper::get()->random(_in.size());
                        SOLUTION sol = _in[index];
                        perturbation(sol);
                        sol.flag(false);
                        _in[index] = sol;
                    }
                }

            protected:
                core::Perturbation<SOLUTION> &perturbation;
                unsigned long long int strength;
            };

        }
    }
}
#endif //MH_BUILDER_KICKPERTURBATION_H
