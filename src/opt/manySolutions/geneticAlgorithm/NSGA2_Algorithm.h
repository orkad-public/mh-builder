/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NSGA2_ALGORITHM_H
#define MH_BUILDER_NSGA2_ALGORITHM_H

#include "opt/manySolutions/geneticAlgorithm/GeneticAlgorithm.h"

#include "core/Criterion.h"
#include "opt/manySolutions/geneticAlgorithm/selection/Selection.h"
#include "opt/manySolutions/geneticAlgorithm/crossover/Crossover.h"
#include "opt/manySolutions/geneticAlgorithm/mutation/Mutation.h"
#include "opt/manySolutions/geneticAlgorithm/replacement/NSGA2_Replacement.h"

#include "opt/manySolutions/geneticAlgorithm/NSGA2_Solution.h"
#include "opt/manySolutions/geneticAlgorithm/selection/TournamentSelection.h"
#include <vector>

#include <cassert>

namespace opt::manySolutions::geneticAlgorithm {
/**
     * Class representing an NS_GA2 Genetic Algorithm
     * see https://ieeexplore.ieee.org/document/996017
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class NSGA2_Algorithm : public GeneticAlgorithm<POPULATION> {
        typedef POPULATION::SOLUTION_TYPE NSGA2SOL;
        typedef NSGA2SOL::SOLUTION SOL;

    public:

        /**
         * Constructor of a Genetic Algorithm
         * @param _crossover the crossover operator
         * @param _mutation the mutation operator
         * @param _eval the eval operator of an individual
         * @param _criterion stop criterion
         */
        explicit NSGA2_Algorithm(
                crossover::Crossover<POPULATION> *_crossover,
                mutation::Mutation<POPULATION> *_mutation,
                core::Eval<NSGA2SOL> &_eval,
                core::Criterion<POPULATION> *_criterion)
                : GeneticAlgorithm<POPULATION>(nullptr, _crossover, _mutation, nullptr, _criterion), eval(_eval) {
            replacement::NSGA2_Replacement<POPULATION> *replacement = new replacement::NSGA2_Replacement<POPULATION>(
                    _eval);
            this->setReplacement(*replacement);
        }

        /**
         * Default destructor
         */
        virtual ~NSGA2_Algorithm() {
            delete (this->replacement);
            delete (this->selection);
        }

        /**
         * Initialisation of the algorithm according to an initial population
         * @param POPULATION the population
         */
        void init(const POPULATION &pop) override {
            this->GeneticAlgorithm<POPULATION>::init(pop);
            selection::TournamentSelection<POPULATION> *select = new selection::TournamentSelection<POPULATION>(2,
                                                                                                                pop.size());
            this->setSelection(*select);
        }

        // tag::algoNSGA2[]
        /**
         * The operator that runs the nsga-ii genetic algorithm
         * @param POPULATION population
         */
        void operator()(POPULATION &sol) override {
            this->init(sol);
            for (auto &i: sol) {
                eval(i);
            }
            auto fronts = NSGA2_Solution<SOL>::fastNonDominatedSort(sol);  // <1>
            for (auto &f: fronts) NSGA2_Solution<SOL>::crowdingDistanceAssignment(sol, f); // <2>

            while (this->criterion->operator()()) {
                this->numberOfGenerations++;
                this->selection->operator()(sol); // <3>
                this->crossover->operator()(this->selection->getSelectedPopulation());
                this->mutation->operator()(this->crossover->getOffspring());
                this->replacement->operator()(sol, this->mutation->getOffspring()); // <4>
                this->criterion->update();
                this->checkpoint->operator()(sol);
            }
        }
        // end::algoNSGA2[]
    protected:
        core::Eval<NSGA2SOL> &eval;

    };

}
#endif //MH_BUILDER_NSGA2_ALGORITHM_H

