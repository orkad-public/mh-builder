/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_FITNESS_PROPORTIONATE_SELECTION_H
#define MH_BUILDER_GA_FITNESS_PROPORTIONATE_SELECTION_H

#include "Selection.h"
#include "util/RNGHelper.h"
#include <vector>


namespace opt::manySolutions::geneticAlgorithm::selection {
/**
     * Class representing an Fitness Proportionate Selection operator
     * also known as Roulette Wheel Selection
     * this version only works for mono objective and in case of maximization fitness
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class FitnessProportionateSelection : public Selection<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;
        typedef typename POPULATION::SOLUTION_TYPE::FITNESS FITNESS;
        typedef typename FITNESS::VALUE_TYPE VALUE_TYPE;

        /**
         * Constructor of a fitness proportionate selection
         * @param _selectSize number of individuals to select
         */
        explicit FitnessProportionateSelection(unsigned long long int _selectSize)
            : Selection<POPULATION>(_selectSize){}
        /**
         * default destructor
         */
        virtual ~FitnessProportionateSelection() = default ;
        /**
         * The operator that selects (selectSize) individuals
         * @param _pop the population
         */
        void operator()(const POPULATION &_pop) override {
            this->selected.clear();
            VALUE_TYPE totalFitness = _pop[0].fitness()[0] ;  // only works with mono objective
            for (unsigned long long int i = 1; i < _pop.size();i++) {
                totalFitness += _pop[i].fitness()[0];
            }
            util::RNGHelper *rnd = util::RNGHelper::get() ;


            std::vector<double> randomNums;
            for (unsigned long long int i = 0; i < _pop.size(); ++i) {
                randomNums.push_back(rnd->uniform(totalFitness));
            }
            // Sort the random numbers
            sort(randomNums.begin(), randomNums.end());

            VALUE_TYPE cumulativeFitness  ;
            bool deb= true ;
            unsigned long long int currentIndex = 0;

            // Perform Roulette Wheel Selection
            for (const INDIVIDUAL& individual : _pop) {
                if (deb)
                    cumulativeFitness = individual.fitness()[0];
                else {
                    deb = false ;
                    cumulativeFitness += individual.fitness()[0];
                }

                while (currentIndex < _pop.size() && cumulativeFitness >= randomNums[currentIndex]) {
                    this->selected.push_back(individual);
                    currentIndex++;
                }
            }
        }


    };
}
#endif //MH_BUILDER_GA_FITNESS_PROPORTIONATE_SELECTION_H
