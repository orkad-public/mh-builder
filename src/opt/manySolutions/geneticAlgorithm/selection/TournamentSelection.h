/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TOURNAMENT_SELECTION_H
#define MH_BUILDER_TOURNAMENT_SELECTION_H

#include "Selection.h"
#include "util/RNGHelper.h"
#include <vector>


namespace opt::manySolutions::geneticAlgorithm::selection {
/**
     * Class representing a Tournament Selection operator
     * this version only works for mono objective
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class TournamentSelection : public Selection<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;
        typedef typename POPULATION::SOLUTION_TYPE::FITNESS FITNESS;
        typedef typename FITNESS::VALUE_TYPE VALUE;

        /**
         * Constructor of a tournament selection
         * @param _k the size of the tournament
         * @param _selectSize number of individuals to select
         */
        explicit TournamentSelection(unsigned long long int _k,unsigned long long int _selectSize)
            : Selection<POPULATION>(_selectSize), k(_k) {}
        /**
         * default destructor
         */
        virtual ~TournamentSelection() = default ;
        /**
         * The operator that selects individuals
         * @param _pop the initial population
         */
        void operator()(const POPULATION &_pop) override {
            this->selected.clear();
            INDIVIDUAL randIndividual;
            INDIVIDUAL winner;
            for(unsigned int i = 0; i< this->selectSize; i++){
                for(unsigned int j = 0; j < k; j++){
                    randIndividual = _pop[util::RNGHelper::get()->random(_pop.size())];
                    if(j==0){
                        winner = randIndividual;
                    }else{
                        if(randIndividual > winner)
                            winner = randIndividual;
                    }
                }
                this->selected.push_back(winner);
            }
        }
    protected:
        unsigned long long int k ; // the size of the tournament

    };
}

#endif //MH_BUILDER_TOURNAMENT_SELECTION_H
