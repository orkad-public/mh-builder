/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_SELECTION_H
#define MH_BUILDER_GA_SELECTION_H

#include "core/Algorithm.h"


namespace opt::manySolutions::geneticAlgorithm::selection {
/**
     * Class representing an abstract Genetic Selection operator
     * @tparam POPULATION
     */
    template<typename POPULATION>

    /**
         * Constructor of an abstract selection operator
         * concrete subclasses have to implement the operator()(const POPULATION &_pop) which
         * generates a  population with _selectSize selected individuals selected from _pop.
         * @param _selectSize number of individuals to select
         */
    class Selection : public core::Algorithm<POPULATION> {
    public:
        explicit Selection(unsigned long long int _selectSize) : selectSize(_selectSize) {}

        /**
         * default destructor
         */
        virtual ~Selection() = default ;

        /**
         * @return the number of individuals that need to be selected
         */
        unsigned long long int getSelectSize() { return this->selectSize ;}
        /**
         * update the number of individuals that need to be selected
         * @param _selectSize
         */
        void setSelectSize(unsigned long long int _selectSize) {
            this->selectSize = _selectSize ;
            if (this->selected.size()>this->selectSize) this->selected.resize(this->selectSize);
        }

        /** the operator that compute the selection of _selectSize individuals from _pop
         * stores result in the selected variable
         * @param _pop the initial population (unchanged)
         */
        virtual void  operator()([[maybe_unused]] const POPULATION &_pop) = 0 ;


        /**
         * provides the current selected
         * @return
         */
        POPULATION &getSelectedPopulation() { return this->selected ;}
    protected:
        unsigned long long int selectSize ;  // the number of individuals to select
        POPULATION selected ;
    };
}
#endif //MH_BUILDER_GA_SELECTION_H
