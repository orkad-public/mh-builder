/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_RANDOM_SELECTION_H
#define MH_BUILDER_GA_RANDOM_SELECTION_H

#include "Selection.h"
#include "util/RNGHelper.h"
#include <vector>


namespace opt::manySolutions::geneticAlgorithm::selection {
/**
     * Class representing a random Selection operator
     * this version only works for mono objective
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class RandomSelection : public Selection<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;
        typedef POPULATION::SOLUTION_TYPE::FITNESS FITNESS;
        typedef FITNESS::VALUE_TYPE VALUE;

        /**
         * Constructor of a random selection
         * @param _selectSize number of individuals to select
         */
        explicit RandomSelection(unsigned long long int _selectSize)
            : Selection<POPULATION>(_selectSize)  {}
        /**
         * default destructor
         */
        virtual ~RandomSelection() = default ;
        /**
         * The operator that selects individuals randomly
         * @param _pop the population
         */
        void operator()(const POPULATION &_pop) override {
            this->selected.clear();
            INDIVIDUAL randIndividual;
            for(unsigned int i = 0; i< this->selectSize; i++){
                randIndividual = _pop[util::RNGHelper::get()->uniform(_pop.size())] ;
                this->selected.push_back(randIndividual);
            }
        }
    };
}

#endif //MH_BUILDER_GA_RANDOM_SELECTION_H
