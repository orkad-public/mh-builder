/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_MUTATION_H
#define MH_BUILDER_GA_MUTATION_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "util/RNGHelper.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "opt/manySolutions/geneticAlgorithm/Reproduction.h"

using opt::singleSolution::neighborhood::neighbor::IndexNeighbor ;
using namespace opt::manySolutions::geneticAlgorithm ;

namespace opt::manySolutions::geneticAlgorithm::mutation {
    /**
     * Class representing a Mutation operator
     * the strategy depends of the chosen neighbor
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class Mutation : public Reproduction<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;

        /**
         * constructor
         * @param _probability  the probability to make a crossover or not
         * @param _eval the evaluation of an individual
         */
        explicit Mutation(double _probability,
                           core::Eval<INDIVIDUAL> &_eval, IndexNeighbor<INDIVIDUAL> &_indexNeighbor)
                : Reproduction<POPULATION>(_probability, _eval), indexNeighbor(&_indexNeighbor) {
        }


    /**
     * The running process that realises a mutation for the given population
     * the new population is stored in the offspring variable
     * @param _population
     */
        void operator()(const POPULATION &_population) override {
            this->init(_population);
            for (INDIVIDUAL ind : _population) {
                if (util::RNGHelper::get()->normal(1) < this->probability) {
                    INDIVIDUAL mutated = this->operator()(ind);
                    this->eval(mutated);
                    this->offspring.push_back(mutated) ;
                } else
                    this->offspring.push_back(ind) ;
            }
        }

        /**
         * returns the used neighbor operator
         */
        IndexNeighbor<INDIVIDUAL> &getIndexNeighbor() { return *this->indexNeighbor ;}

        /**
         * Set a new neighbor
         * @param neighbor
         */
        void setIndexNeighbor(IndexNeighbor<INDIVIDUAL> & neighbor) { this->indexNeighbor = &neighbor ;}
    private:
        /**
         * run the mutation
         * @param _ind1
         * @return the mutated individual
         */
        INDIVIDUAL operator()(const INDIVIDUAL &_ind) {
            INDIVIDUAL mutated = _ind ;
            this->indexNeighbor->init(mutated);
            this->indexNeighbor->setKey(util::RNGHelper::get()->uniform(indexNeighbor->getMaxKey()));
            indexNeighbor->operator()(mutated);
            return mutated;
        }
    protected:
        opt::singleSolution::neighborhood::neighbor::IndexNeighbor<INDIVIDUAL> *indexNeighbor;
    };
}
#endif //MH_BUILDER_GA_MUTATION_H
