/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NSGA2_SOLUTION_H
#define MH_BUILDER_NSGA2_SOLUTION_H

#include <vector>
#include <memory>

#include "core/Archive.h"

namespace opt::manySolutions::geneticAlgorithm {
/**
 * this class is a decorator of the VectorSolution class and provides the management of rank and
 * crowding distance for NS_GA2 algorithm
 * @tparam SOL the type of the original solution
 */
    template<class SOL>
    class NSGA2_Solution : public SOL {
    public:
        typedef SOL SOLUTION;
        typedef SOL::TYPE_ELEMENT TYPE;
        typedef SOL::FITNESS FIT;
        typedef typename std::vector<TYPE>::iterator iterator;
        typedef typename std::vector<TYPE>::const_iterator const_iterator;

        /**
         * simple constructor from a given initial solution
         **/
        explicit NSGA2_Solution(const SOL &_ind) : ind(_ind), rank(0), crowdingDistance(0.0), n(0) {
        }

        /**
         * default constructor
         */
        NSGA2_Solution() = default;

        /**
         * default destructor
         */
        virtual ~NSGA2_Solution() = default;

        /***************************************************************
         * subsection for the specific management of NSGA2_Solution   *
         * (crowding distance, rank, dominated sets, number of dominate solutions)
         ***************************************************************/

        /**
             * update the rank for an individual
             * @param value the new rank value
             */
        void setRank(const unsigned long long int value) {
            this->rank = value;
        }

        /**
         * provides the rank of an individual
         * @return the rank value
         */
        unsigned long long int getRank() const {
            return this->rank;
        }

        /**
         * set the crowding distance
         * @param value the new value of the crowding distance
         */
        void setCrowdingDistance(const double value) {
            this->crowdingDistance = value;
        }

        /**
         * provides the crowding distance
         * @return the distance
         */
        double getCrowdingDistance() const {
            return this->crowdingDistance;
        }

        /**
         * provides the inner individual
         * @return the individual
         */
        SOL &getIndividual() { return this->ind; }

        /**
         * provides the inner individual
         * @return the individual
         */
        const SOL &getIndividual() const { return this->ind; }

        /**
         * provides the domination counter
         * @return
         */
        unsigned long long int getN() const { return this->n; }

        /**
         * provides the set of individuals that the current individual dominates
         **/
        std::vector<unsigned long long int> &getS() {
            return this->S;
        }


        /**
         * updates the domination counter
         * @param _n
         */
        void setN(unsigned long long int _n) { this->n = _n; }

        /**
                 * According to the NSGA2 algorithm, this static method sets the rank,n,S for all solutions
                 * of the given population, then returns the different fronts.
                 * @param P
                 * @return the set of fronts, each front contains the index of the solution of P
                 */
        static std::vector<std::vector<unsigned long long int>> &fastNonDominatedSort(
                core::Archive<NSGA2_Solution<SOL>> &P) {
            std::vector<std::vector<unsigned long long int>> *F;
            F = new std::vector<std::vector<unsigned long long int>>();
            F->clear();

            for (unsigned long long int ind_p = 0; ind_p < P.size(); ind_p++) {
                P[ind_p].getS().clear();  // S_p = {}
                P[ind_p].setN(0);          // n_p = 0
                for (unsigned long long int ind_q = 0; ind_q < P.size(); ind_q++) {
                    if (P[ind_p].fitness() > P[ind_q].fitness()) { // if (p dominates q)
                        P[ind_p].getS().push_back(ind_q); //   add q to the set of individuals dominated by p
                    } else if (P[ind_p].fitness() < P[ind_q].fitness()) {
                        P[ind_p].setN(P[ind_p].getN() + 1); //  increment the domination counter of p
                    }
                }
                if (P[ind_p].getN() == 0) {      // p belongs to the first front
                    P[ind_p].setRank(0);
                    if (F->size() == 0) F->push_back({ind_p}); else (*F)[0].push_back(ind_p);
                }
            }
            if (F->size() == 0) return *F;
            unsigned long long int i = 0;
            std::vector<unsigned long long int> Q;
            while ((*F)[i].size() != 0) {
                Q.clear();
                for (auto &ind_p: (*F)[i]) {
                    for (unsigned long long int &ind_q: P[ind_p].getS()) {
                        P[ind_q].setN(P[ind_q].getN() - 1);
                        if (P[ind_q].getN() == 0) {
                            P[ind_q].setRank(i + 1);
                            Q.push_back(ind_q);
                        }
                    }
                }
                i = i + 1;
                F->push_back(Q);
            }
            F->pop_back();
            return *F;
        }

        /**
         * set the crowding distance for all individuals stored in Front
         * @param Front
         */
        static void
        crowdingDistanceAssignment(core::Archive<NSGA2_Solution<SOL>> &P, std::vector<unsigned long long int>
        &Front) {
            auto F_size = Front.size();  // number of individuals index in front
            if (F_size == 0) return;
            for (auto &f: Front) P[f].setCrowdingDistance(0.0); // initialize distance
            auto numberOfObjectives = P[Front[0]].fitness().objectives().size();
            for (unsigned long long int ind_obj = 0; ind_obj < numberOfObjectives; ind_obj++) {
                // for each objective value m
                // sort using each objective value
                std::sort(Front.begin(), Front.end(),
                          [&ind_obj, &P](unsigned long long int &ind1, unsigned long long int &ind2) {
                              return P[ind1].fitness().objectives()[ind_obj] < P[ind2].fitness().objectives()[ind_obj];
                          });
                P[Front.front()].setCrowdingDistance(std::numeric_limits<double>::infinity());
                P[Front.back()].setCrowdingDistance(std::numeric_limits<double>::infinity());
                // so that boundary points are always selected
                double minObjectiveValue = P[Front.front()].fitness().objectives()[ind_obj];
                double maxObjectiveValue = P[Front.back()].fitness().objectives()[ind_obj];
                if (maxObjectiveValue - minObjectiveValue == 0) {
                    continue; // skip to the next objective
                }
                for (unsigned long long int i = 1; i < (F_size - 1); i++) // for all other points
                    P[Front[i]].setCrowdingDistance(P[Front[i]].getCrowdingDistance() +
                                                    (P[Front[i + 1]].fitness().objectives()[ind_obj] -
                                                     P[Front[i - 1]].fitness().objectives()[ind_obj]) /
                                                    (maxObjectiveValue - minObjectiveValue)
                    );
            }
        }
        /***************************************************************
         * subsection for the decorator class of VectorSolution
         ***************************************************************/
        /**
             * Get the Fitness
             * @return fitness
             */
        FIT &fitness() override {
            return this->ind.fitness();
        }

        /**
             * Get the Fitness
             * @return fitness
             */
        const FIT &fitness() const override {
            return this->ind.fitness();
        }

        /**
             * Set the Fitness
             * @param f the fitness
             */
        void fitness(FIT f) override {
            this->ind.fitness(f);
        }

        /**
             * If the fitness is valid
             * @return true if fitness is valid
             */
        bool valid() override {
            return this->ind.valid();
        }

        /**
             * If the scalarized fitness is valid
             * @return true if the scalarised fitness is valid
             */
        bool validScalar() override {
            return this->ind.validScalar();
        }

        /**
             * Get the flag
             * @return the flag
             */
        [[nodiscard]] int flag() const override {
            return this->ind.flag();
        }


        /**
             * Equals crowded-comparison comparator
             * @param sol solution
             * @return true solution have the same
             */
        bool operator==(const NSGA2_Solution<SOL> &sol) const {
            return (this->getRank() == sol.getRank()) and (this->getCrowdingDistance() == sol.getCrowdingDistance());
        }

        /**
             * Different comparator
             * @param sol solution
             * @return true solution have note the same crowded comparator
             */
        bool operator!=(const NSGA2_Solution<SOL> &sol) const {
            return !this->operator==(sol);
        }

        /**
             * Worse or equal crowded comparator
             * @param sol solution
             * @return true solution is worse or equal than another solution
             */
        bool operator<=(const NSGA2_Solution<SOL> &sol) const {
            return (this->getRank() > sol.getRank()) or
                   ((this->getRank() == sol.getRank()) and (this->getCrowdingDistance() <= sol.getCrowdingDistance()));
        }

        /**
         * Worse crowded comparator
         * @param sol solution
         * @return true solution is worse than another solution
         */
        bool operator<(const NSGA2_Solution<SOL> &sol) const {
            return (this->getRank() > sol.getRank()) or
                   ((this->getRank() == sol.getRank()) and (this->getCrowdingDistance() < sol.getCrowdingDistance()));
        }

        /**
             * Better or equal crowded comparator
             * @param sol solution
             * @return true solution is better or equal than another solution
             */
        bool operator>=(const NSGA2_Solution<SOL> &sol) const {
            return (this->getRank() < sol.getRank()) or
                   ((this->getRank() == sol.getRank()) and (this->getCrowdingDistance() >= sol.getCrowdingDistance()));
        }

        /**
         * Better crowded comparator
         * @param sol solution
         * @return true solution is better than another solution
         */
        bool operator>(const NSGA2_Solution<SOL> &sol) const {
            return (this->getRank() < sol.getRank()) or
                   ((this->getRank() == sol.getRank()) and (this->getCrowdingDistance() > sol.getCrowdingDistance()));
        }

        /**
             * Set the flag
             * @param v the flag
             */
        void flag(int v) override {
            this->ind.flag(v);
        }

        /**
             * reset the solution
             */
        void reset() override { this->ind.reset(); }


        /**
             * Get k^th variable value
             * @param k k^th variable value
             * @return the k^th variable value
             */
        TYPE operator[](unsigned int k) override {
            return this->ind.operator[](k);
        }

        /**
         * Get k^th variable value
         * @param k k^th variable value
         * @return the k^th variable value
         */
        TYPE operator[](unsigned int k) const override {
            return this->ind.operator[](k);
        }

        /**
             * update one element of the vector
             * @param index  the index of the element to changed
             * @param value the new value
             */
        void updateElement(int index, TYPE value) override {
            this->ind.updateElement(index, value);
        }

        /**
             * Get size of the vector
             * @return the vector size
             */
        [[nodiscard]] unsigned long long int size() const override {
            return this->ind.size();
        }

        /**
         * Add an element i in the vector and invalidate the fitness
         * @param i the element to add
         */
        virtual void push_back(const TYPE i) override {
            this->ind.push_back(i);
        }

        /**
         * Add an element in the vector at position and invalidate the fitness
         * @param position position to add
         * @param element the element to add
         */
        virtual void insert(iterator position, const TYPE element) override {
            this->ind.insert(position, element);
        }

        /**
         * Remove the last element in the vector and invalidate the fitness
         */
        virtual void pop_back() override {
            this->ind.pop_back();
        }

        /**
         * Clear the vector and invalidate the fitness
         */
        virtual void clear() override {
            this->ind.clear();
        }

        /**
         * Replace the vector by another
         * @param v the new vector
         */
        virtual void replace(std::vector<TYPE> &v) override {
            this->ind.replace(v);
        }

        /**
             * Print the vector in an output stream
             * @param os output stream
             */
        void print(std::ostream &os) const override {
            this->ind.print(os);
            os << " rank: " << this->getRank() << " crowding distance: " << this->getCrowdingDistance();
        }

        /**
         * Begin iterator of the vector
         */
        const_iterator begin() const override {
            return this->ind.begin();
        }

        /**
         * End iterator of the vector
         */
        const_iterator end() const override {
            return this->ind.end();
        }

        /**
         * Begin iterator of the vector
         */
        iterator begin() override {
            return this->ind.begin();
        }

        /**
         * End iterator of the vector
         */
        iterator end() override {
            return this->ind.end();
        }

        /**
         * Erase the element at pos
         * @param pos
         * @return iterator
         */
        iterator erase(iterator pos) override {
            return this->ind.erase(pos);
        }

        /**
             * return reference at i
             * @param i
             * @return reference of TYPE
             */
        virtual TYPE &at(unsigned long long int i) override {
            return this->ind.at(i);
        }

/**
         * invert the section between i and j in the permutation
         * @param i start of inversion [include]
         * @param j end of inversion [include]
         * @param invalidate invalidate the fitness
         */
        void invert(const int i, const int j, const bool invalidate = true) override {
            this->ind.invert(i, j, invalidate);
        }

        /**
         * swap the i^th element with j^th element in the permutation
         * @param i the i^th to swap with the j^th
         * @param j the j^th to swap with the i^th
         * @param invalidate invalidate the fitness
         */
        void swap(const int i, const int j, const bool invalidate = true) override {
            this->ind.swap(i, j, invalidate);
        }

        /**
         * move the element at i^th position to j^th position in the final permutation
         * (ex: move (3,5) [0,1,2,3,4,5,6,7,8,9] -> [0,1,2,4,5,3,6,7,8,9])
         * @param i the i^th element to move
         * @param j the position to move the i^th element
         * @param invalidate invalidate the fitness
         */
        void move(const int i, const int j, const bool invalidate = true) override {
            this->ind.swap(i, j, invalidate);
        }


        /**
         * Shuffle the permutation
         */
        virtual void shuffle() override {
            this->ind.shuffle();
        }


    protected:
        SOL ind;
        unsigned long long int rank = 0; // the calculated rank
        double crowdingDistance = 0;
        // the following properties are set during the fast-non-dominated-sort procedure
        std::vector<unsigned long long int> S; // the set of individual index (from a population) that the current individual dominates
        unsigned long long int n = 0; // the domination counter of the current individual
    };
}
#endif //MH_BUILDER_NSGA2_SOLUTION_H
