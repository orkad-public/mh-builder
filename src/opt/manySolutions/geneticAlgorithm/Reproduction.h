/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_REPRODUCTION_H
#define MH_BUILDER_GA_REPRODUCTION_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "util/RNGHelper.h"
#include <utility>

namespace opt::manySolutions::geneticAlgorithm {
    /**
     * Class representing an abstract Crossover operator
     * each concrete subclass have to implement the operator()(const INDIVIDUAL &_parent1, const INDIVIDUAL &_parent2)
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class Reproduction : public core::Algorithm<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;

        /**
         * constructor
         * @param _probability  the probability to make a crossover or not
         * @param _eval the evaluation of an individual
         */
        explicit Reproduction(double _probability,
                           core::Eval<INDIVIDUAL> &_eval)
                : probability(_probability), eval(_eval) {}

        /**
         * default virtual destructor
         */
        virtual ~Reproduction() = default ;

        /**
         * method that initialize the offspring variable
         * @param _population
         */
        void init([[maybe_unused]] const POPULATION &_population) override {
            this->offspring.clear();
        }

        /**
         * abstract method that runs a genetic operator over a given population.
         * resulting population is stored in the offspring variable
         * @param _population
         */
        void operator()(const POPULATION &_population) override = 0 ;

        /**
         * provides the generated offspring
         * @return the offspring
         */
        POPULATION &getOffspring() {
            return this->offspring;
        }

    protected:
        POPULATION offspring;
        double probability;
        core::Eval<INDIVIDUAL> &eval;
    };
}
#endif //MH_BUILDER_GA_REPRODUCTION_H
