/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NSGA2_EVAL_H
#define MH_BUILDER_NSGA2_EVAL_H

#include "core/Eval.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Solution.h"

namespace opt::manySolutions::geneticAlgorithm {
    /**
    * this class is a decorator of the Eval operator
    * @tparam EVAL the type of the original eval class
    */
    template<typename SOL>
    class NSGA2_Eval : public core::Eval<NSGA2_Solution<SOL>> {
    public:

        /**
         * default destructor
         * @param _eval
         */
        explicit NSGA2_Eval(core::Eval<SOL> &_eval) : eval(_eval) {}

        /**
         * default virtual destructor
         */
        virtual ~NSGA2_Eval() = default ;

        /**
         * the eval operator
         * @param sol a solution
         */
        void operator()(NSGA2_Solution<SOL> &sol) override {
            this->eval.operator()(sol);
        }

    protected:
        core::Eval<SOL> &eval; // the original eval
    };
}
#endif //MH_BUILDER_NSGA2_EVAL_H
