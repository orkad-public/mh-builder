/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_ALL_REPLACEMENT_H
#define MH_BUILDER_GA_ALL_REPLACEMENT_H


#include "opt/manySolutions/geneticAlgorithm/replacement/Replacement.h"

namespace opt::manySolutions::geneticAlgorithm::replacement {
/**
     * Class representing the All Replacement operator
     * all individuals of offspring are inserted in the resulting population
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class AllReplacement  : public Replacement<POPULATION>{
    public:
        /**
         * default constructor
         */
        explicit AllReplacement() = default ;

        /**
         * default virtual destructor
         */
        virtual ~AllReplacement() = default ;

        /**
         * the operator that updates the initial population with the offspring
         * the all replacement strategy : all individuals from offspring
         * are included in the resulting population
         */

        void  operator()(POPULATION &_parent_pop, const POPULATION &offspring) override {
            _parent_pop.sort() ; // sorting
            _parent_pop.resize(_parent_pop.size()-offspring.size()) ; // resize: worst individuals are removed
            _parent_pop.insertAll(offspring);; // insert offspring individuals
        }

    };
}
#endif //MH_BUILDER_GA_ALL_REPLACEMENT_H
