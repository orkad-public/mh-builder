/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_NS_GA2_REPLACEMENT_H
#define MH_BUILDER_GA_NS_GA2_REPLACEMENT_H


#include "opt/manySolutions/geneticAlgorithm/replacement/Replacement.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Solution.h"
#include "core/Archive.h"


#include<vector>
#include <limits>
#include <iostream>
#include <algorithm>

using namespace opt::manySolutions::geneticAlgorithm ;

namespace opt::manySolutions::geneticAlgorithm::replacement {
/**
     * Class representing the NS_GA2 Replacement operator
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class NSGA2_Replacement : public Replacement<POPULATION> {
    public:
        typedef  typename POPULATION::SOLUTION_TYPE NSGA2SOL;
        typedef NSGA2SOL::SOLUTION SOL ;
        /**
         * default constructor
         */
        explicit NSGA2_Replacement(core::Eval<NSGA2SOL> &_eval) : eval(_eval) {}

        /**
         * default virtual destructor
         */
        virtual ~NSGA2_Replacement() = default ;

        /**
         * the operator that updates the initial population with the offspring
         * the replacement follows instructions of the NS_GA2 Algorithm
         */

        void  operator()(POPULATION &_parent_pop, const POPULATION &offspring) override {
            // NSGA2 algorithm : _parent_pop : Pt, offspring : Qt
            unsigned long long int N = _parent_pop.size();
            POPULATION R ;
            R(_parent_pop) ; R(offspring) ; // R = _parent_pop union offspring ;
            std::vector<std::vector<unsigned long long int>> F =
                    NSGA2_Solution<SOL>::fastNonDominatedSort(R);
            _parent_pop.resize(0);// Pt = {}
            unsigned long long int i = 0;
            while ((_parent_pop.size() + F[i].size()) <= N)  {  // until the parent population is filled
                NSGA2_Solution<SOL>::crowdingDistanceAssignment(R,F[i]);        // calculate the crowding-distance in F_i
                for (auto &f : F[i]) {
                    eval(R[f]);
                    _parent_pop(R[f]);                   // include the ith nondominated front in the parent pop
                }
                i++;                                           // check the next front for inclusion
            }
            NSGA2_Solution<SOL>::crowdingDistanceAssignment(R,F[i]);
            std::sort(F[i].begin(), F[i].end(), // sort in descending order using crowded comparator <_n
                      [&R](auto const &ind1, auto const &ind2) {
                          return ( (R[ind1].getRank() < R[ind1].getRank()) or
                                   ( (R[ind1].getRank()==R[ind1].getRank()) and (R[ind1].getCrowdingDistance() > R[ind2].getCrowdingDistance())));
                      });
            auto reste = N - _parent_pop.size() ;
            for (unsigned long long int ind=1; ind <= reste; ind++) {
                eval(R[F[i][ind]]);
                _parent_pop.push_back(R[F[i][ind]]);
            }
        }
    protected:
        core::Eval<NSGA2SOL> &eval;
    };
}
#endif //MH_BUILDER_GA_NS_GA2_REPLACEMENT_H
