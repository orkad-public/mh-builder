/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_REPLACEMENT_H
#define MH_BUILDER_GA_REPLACEMENT_H




namespace opt::manySolutions::geneticAlgorithm::replacement {
/**
     * Class representing an abstract Genetic Replacement operator
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class Replacement {
    public:
        /**
         * Constructor of an abstract replacement operator
         * concrete subclasses have their own strategy to produce a new population
         */
        explicit Replacement() = default ;
        /**
         * default virtual destructor
         */
        virtual ~Replacement() = default ;
        /**
         * the operator that updates the initial population with the offspring
         * @param _parent_pop the initial population to be changed
         * @param offspring the offspring population
         */

        virtual void  operator()(POPULATION &_parent_pop, const POPULATION &offspring)  = 0 ;

    };
}
#endif //MH_BUILDER_GA_REPLACEMENT_H
