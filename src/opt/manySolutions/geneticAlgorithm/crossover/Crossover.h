/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_CROSSOVER_H
#define MH_BUILDER_GA_CROSSOVER_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "util/RNGHelper.h"
#include "opt/manySolutions/geneticAlgorithm/Reproduction.h"
#include <utility>

using namespace opt::manySolutions::geneticAlgorithm ;

namespace opt::manySolutions::geneticAlgorithm::crossover {
    /**
     * Class representing an abstract Crossover operator
     * each concrete subclass have to implement the operator()(const INDIVIDUAL &_parent1, const INDIVIDUAL &_parent2)
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class Crossover : public Reproduction<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;

        /**
         * constructor
         * @param _probability  the probability to make a crossover or not
         * @param _eval the evaluation of an individual
         */
        explicit Crossover(double _probability,
                           core::Eval<INDIVIDUAL> &_eval)
                : Reproduction<POPULATION>(_probability, _eval) {}

        /**
         * abstract method that runs a crossover with 2 individuals
         * @param _ind1
         * @param _ind2
         * @return the resulting children
         */
        virtual std::pair<INDIVIDUAL,INDIVIDUAL> operator()(const INDIVIDUAL &_ind1, const INDIVIDUAL &_ind2) = 0;

        /**
         * the process that realizes crossovers over the given population
         * the result is stored in the offspring variable
         * @param _population
         */
        void operator()(const POPULATION &_population) override {
            this->init(_population);
            unsigned long long int pair_size = _population.size()%2 ==0 ? _population.size() : _population.size()-1 ;
            unsigned long long int counter=0 ;
            while (counter < pair_size) {
                INDIVIDUAL ind1 = _population[counter] ; INDIVIDUAL ind2 = _population[counter+1] ;
                if (util::RNGHelper::get()->normal(1) < this->probability) {
                    std::pair<INDIVIDUAL,INDIVIDUAL> pair_result= this->operator()(ind1, ind2);
                    this->eval(pair_result.first) ;  this->eval(pair_result.second) ;
                    this->offspring.push_back(pair_result.first);
                    this->offspring.push_back(pair_result.second);
                } else {
                    this->offspring.push_back(ind1);
                    this->offspring.push_back(ind2) ;
                }
                counter+=2 ;
            }
            if (_population.size()%2 ==1) { // in case of pop size is not pair
                this->offspring.push_back(_population[_population.size()-1]); // complete with the last individual
            }
        }
    };
}
#endif //MH_BUILDER_GA_CROSSOVER_H
