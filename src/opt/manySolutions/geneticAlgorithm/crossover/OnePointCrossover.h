/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_ONE_POINT_CROSSOVER_H
#define MH_BUILDER_GA_ONE_POINT_CROSSOVER_H

#include "opt/manySolutions/geneticAlgorithm/crossover/Crossover.h"
#include "core/Eval.h"
#include "util/RNGHelper.h"


namespace opt::manySolutions::geneticAlgorithm::crossover {
/**
     * Class representing the One Point Crossover operator
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class OnePointCrossover : public Crossover<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;

        /**
         * constructor
         * @param _probability  the probability to make a crossover or not
         * @param _eval the evaluation of an individual
         */
        explicit OnePointCrossover(double _probability,
                                   core::Eval<INDIVIDUAL> &_eval)
                : Crossover<POPULATION>(_probability,_eval) {}

        /**
         * run the  one point crossover ,
         * @param _ind1
         * @param _ind2
         */
        std::pair<INDIVIDUAL, INDIVIDUAL> operator()(const INDIVIDUAL &_ind1, const INDIVIDUAL &_ind2) override{
                unsigned long long int size = _ind1.size();
                unsigned long long int point = util::RNGHelper::get()->random(size-1);
                INDIVIDUAL child1; child1.clear() ;
                INDIVIDUAL child2; child2.clear() ;
                for(unsigned long long int i=0; i<size; i++){
                    if(i<point){
                        child1.push_back(_ind1[i]);
                        child2.push_back(_ind2[i]);
                    }else{
                        child1.push_back(_ind2[i]);
                        child2.push_back(_ind1[i]);
                    }

                }
                std::pair<INDIVIDUAL, INDIVIDUAL> result ;
                result.first=child1 ; result.second=child2 ;
                return result ;
        }


    };
}
#endif //MH_BUILDER_GA_ONE_POINT_CROSSOVER_H