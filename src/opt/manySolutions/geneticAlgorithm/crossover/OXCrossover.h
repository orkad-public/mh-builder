/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_OX_CROSSOVER_H
#define MH_BUILDER_GA_OX_CROSSOVER_H

#include "opt/manySolutions/geneticAlgorithm/crossover/Crossover.h"
#include "core/Eval.h"
#include "util/RNGHelper.h"


namespace opt::manySolutions::geneticAlgorithm::crossover {
/**
     * Class representing the Order Crossover (OX) operator
     * see ref below for explanations:https://www.researchgate.net/figure/Illustration-of-the-OX-crossover_fig3_331580514
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class OXCrossover : public Crossover<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;

        /**
         * constructor
         * @param _probability  the probability to make a crossover or not
         * @param _eval the evaluation of an individual
         */
        explicit OXCrossover(double _probability,
                             core::Eval<INDIVIDUAL> &_eval)
                : Crossover<POPULATION>(_probability, _eval) {}

        /**
         * run the  OX crossover
         * @param _ind1
         * @param _ind2
         * @return the children
         */
        std::pair<INDIVIDUAL, INDIVIDUAL> operator()(const INDIVIDUAL &_ind1, const INDIVIDUAL &_ind2) override {
            unsigned long long int size = _ind1.size();
            unsigned int len = util::RNGHelper::get()->random(size);
            unsigned int position = util::RNGHelper::get()->random(size - len);
            INDIVIDUAL child1;
            INDIVIDUAL child2;
            for (unsigned long long int i = 0, i1 = 0, i2 = 0; i < size; i++) {
                if (i > position && i <= position + len) {
                    child1.push_back(_ind1[i]);
                    child2.push_back(_ind2[i]);
                } else {
                    while (std::find(_ind1.begin() + position+1, (_ind1.begin() + position + len+1), _ind2[i2])
                           != (_ind1.begin() + position + len+1)) i2++;
                    child1.push_back(_ind2[i2]);
                    i2++;
                    while (std::find(_ind2.begin() + position+1, (_ind2.begin() + position + len+1), _ind1[i1])
                           != (_ind2.begin() + position + len+1)) i1++;
                    child2.push_back(_ind1[i1]);
                    i1++;
                }
            }
            std::pair<INDIVIDUAL, INDIVIDUAL> result;
            result.first = child1;
            result.second = child2;
            return result ;
        }


    };
}
#endif //MH_BUILDER_GA_OX_CROSSOVER_H