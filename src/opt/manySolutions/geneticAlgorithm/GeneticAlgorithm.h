/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GENETIC_ALGORITHM_H
#define MH_BUILDER_GENETIC_ALGORITHM_H

#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "opt/manySolutions/geneticAlgorithm/selection/Selection.h"
#include "opt/manySolutions/geneticAlgorithm/crossover/Crossover.h"
#include "opt/manySolutions/geneticAlgorithm/mutation/Mutation.h"
#include "opt/manySolutions/geneticAlgorithm/replacement/Replacement.h"

#include <iostream>

namespace opt::manySolutions::geneticAlgorithm {
/**
     * Class representing an abstract Genetic Algorithm
     * @tparam POPULATION
     */
    template<typename POPULATION>
    class GeneticAlgorithm : public core::Algorithm<POPULATION> {
    public:
        typedef typename POPULATION::SOLUTION_TYPE INDIVIDUAL;

        /**
         * Constructor of a Genetic Algorithm
         * @param _selection the selection operator
         * @param _crossover the crossover operator
         * @param _mutation the mutation operator
         * @param _replacement the replacement population operator
         * @param _criterion stop criterion
         */
        explicit GeneticAlgorithm(selection::Selection<POPULATION>* _selection,
                crossover::Crossover<POPULATION>* _crossover, mutation::Mutation<POPULATION>* _mutation ,
                replacement::Replacement<POPULATION> *_replacement,
                                  core::Criterion<POPULATION> *_criterion)
                : selection(_selection)
        ,crossover(_crossover), mutation(_mutation)
        ,replacement(_replacement)
        { this->criterion = _criterion ;
        }



        /**
         * Default destructor
         */
        virtual ~GeneticAlgorithm()  = default ;

        unsigned long long int getNumberOfGenerations() { return this->numberOfGenerations; }

        /**
         * Initialisation of the algorithm according to an initial population
         * @param POPULATION the population
         */
        void init([[maybe_unused]] const POPULATION &pop) override {
            this->numberOfGenerations = 0;
            this->criterion->init();
            this->checkpoint->init(pop) ;
        }


        // tag::algoGA[]
        /**
         * The operator that runs the genetic algorithm
         * @param POPULATION population
         */
        void operator()(POPULATION &sol) override {
            this->init(sol);
            while (this->criterion->operator()()) {  // <1>
                this->numberOfGenerations++;
                this->selection->operator()(sol);  // <2>
                this->crossover->operator()(this->selection->getSelectedPopulation()); // <3>
                this->mutation->operator()(this->crossover->getOffspring()); // <4>
                this->replacement->operator()(sol,this->mutation->getOffspring()) ; // <5>
                this->criterion->update(); // <6>
                this->checkpoint->operator()(sol); // <7>
            }
        }
        // end::algoGA[]
        /**
         * update the selection  operator
         * @param _selection the new selection operator
         */
        void setSelection(selection::Selection<POPULATION> &_selection) { this->selection = &_selection; }

        /**
         * provides the selection  operator
         * @return the selection  operator
         */
        selection::Selection<POPULATION> &getSelection() { return *this->selection; }



        /**
         * getter method for crossover
         * @return the crossover operator
         */
         crossover::Crossover<POPULATION> &getCrossover() { return *this->crossover ; }

        /**
        * setter method for crossover
        * @param _crossover the new crossover operator
        */
        void setCrossover(crossover::Crossover<POPULATION> &_crossover) { this->crossover = &_crossover; }

        /**
         * getter method for mutation
         * @return the mutation operator
         */
        mutation::Mutation<POPULATION> &getMutation() { return *this->mutation ; }

        /**
        * setter method for mutation
        * @param _mutation the new mutation operator
        */
        void setMutation(mutation::Mutation<POPULATION> &_mutation) { this->mutation = &_mutation; }

        /**
         * getter method for the replacement  operator
         * @return the replacement  operator
         */
        replacement::Replacement<POPULATION> &getReplacement() { return *this->replacement; }

        /**
         * setter method for  replacement  operator
         * @param _replacement the new replacement operator
         */
        void setReplacement(replacement::Replacement<POPULATION> &_replacement) { replacement = &_replacement; }



    protected:
        unsigned long long int numberOfGenerations = 0;
        selection::Selection<POPULATION> *selection;
        crossover::Crossover<POPULATION> *crossover;
        mutation::Mutation<POPULATION> *mutation;
        replacement::Replacement<POPULATION> *replacement;
    };

}
#endif //MH_BUILDER_GENETIC_ALGORITHM_H
