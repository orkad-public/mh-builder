/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_EXPLORER_H
#define MH_BUILDER_EXPLORER_H

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include "core/criterion/DummyCriterion.h"
#include "core/Archive.h"
#include "core/archive/AllAcceptedArchive.h"


namespace opt::manySolutions::explorer {

    /**
     * Class representing an explorer operator
     * @tparam ARCHIVE archive
     */
    template<typename ARCHIVE>
    class Explorer {
    public:
        typedef typename ARCHIVE::SOLUTION_TYPE SOLUTION;

        /**
         * Constructor of an explorer
         * @param _neighborhood the neighborhood of a solution
         * @param _eval the eval function
         */
        explicit Explorer(opt::singleSolution::neighborhood::Neighborhood<SOLUTION> &_neighborhood,
                          core::Eval<SOLUTION> &_eval)
                : neighborhood(_neighborhood), eval(_eval) {}

        virtual ~Explorer() = default;

        /**
         * Initialise the operator
         * @param _solution the solution
         */
        virtual void init(const SOLUTION &_solution) {
            // Initialize the neighborhood
            this->neighborhood.init(_solution);
            archive_change = false;
        }

        /**
        * Operator which explore the solution _in and put all accepted solution in the archive _out
        * @param _in solution to explore
        * @param _out archive
        */
        virtual bool operator()(SOLUTION &_solution, core::Archive<SOLUTION> &_archive) {
            core::archive::AllAcceptedArchive<SOLUTION> _ref;
            _ref(_solution);
            return operator()(_solution, _ref, _archive, dummyCriterion);
        }

        /**
        * Operator which explore the solution _in and put all accepted solution in the archive _out
        * @param _in solution to explore
        * @param _out archive
        */
        virtual bool operator()(SOLUTION &_solution, core::Archive<SOLUTION> &_archive,
                                core::Criterion<ARCHIVE> &_criterion) {
            core::archive::AllAcceptedArchive<SOLUTION> _ref;
            _ref(_solution);
            return operator()(_solution, _ref, _archive, _criterion);
        }

        /**
        * Operator which explore the solution _in and put all accepted solution in the archive _out
        * @param _in solution to explore
        * @param _out archive
        */
        virtual bool
        operator()(SOLUTION &_solution, core::Archive<SOLUTION> &_ref, core::Archive<SOLUTION> &_archive) {
            return operator()(_solution, _ref, _archive, dummyCriterion);
        }

        /**
         * Operator wich explore the solution _in and put all accepted solution in the archive _out
         * @param _in solution to explore
         * @param _out archive
         */
        virtual bool
        operator()(SOLUTION &_solution, core::Archive<SOLUTION> &_ref, core::Archive<SOLUTION> &_archive,
                   core::Criterion<ARCHIVE> &_criterion) {
            init(_solution);

            // Save the main solution
            SOLUTION current = _solution;
            if (!current.valid())
                this->eval(current);

            // The first iteration
            iteration(current, _ref, _archive);

            // Exploration
            while (searchCriterion(_solution) && _criterion()) {
                // Go to the next neighbor
                this->neighborhood.next();
                // Make the next iteration
                iteration(current, _ref, _archive);
            }

            if (!this->neighborhood.hasNextNeighbor())
                _solution.flag(1); //Solution explored
            return archive_change;
        };

        /**
         * Make an iteration (move, eval, replacement if needed, moveback) for the current neighbor
         * @param sol solution
         * @param _eval the evaluation function
         */
        virtual void iteration(SOLUTION &_current, const core::Archive<SOLUTION> &_reference,
                               core::Archive<SOLUTION> &_archive) {
            // Apply move
            this->neighborhood(_current, this->eval);
            // Chose current for out according to the selection strategy
            if (archiveUpdateCriterion(_current, _reference))
                archive_change = _archive(_current) || archive_change;
            // Apply move back
            this->neighborhood.move_back(_current);
        }

        /**
         * Set the neighborhood of the explorer
         * @param _neighborhood
         */
        void setNeighborhood(opt::singleSolution::neighborhood::Neighborhood<SOLUTION> &_neighborhood) {
            neighborhood = _neighborhood;
        }

        /**
         * Test if the search continue according to the strategy, by default, while a solution have a neighbor
         * @param sol the solution
         * @return true if sol have a neighbor
         */
        virtual bool searchCriterion(const SOLUTION &_solution __attribute__((unused))) {
            return this->neighborhood.hasNextNeighbor();
        };

        /**
         * Test if the current solution need to be inserted in the archive, according to the reference solution
         * @param _current current solution
         * @param _reference reference solution
         * @return true if the archive need to be updated with current solution
         */
        virtual bool
        archiveUpdateCriterion(const SOLUTION &_current, const core::Archive<SOLUTION> &_reference) = 0;

    protected:
        opt::singleSolution::neighborhood::Neighborhood<SOLUTION> &neighborhood;
        /*! The Eval function */
        core::Eval<SOLUTION> &eval;
        bool archive_change{};
        core::criterion::DummyCriterion<ARCHIVE> dummyCriterion = *core::criterion::DummyCriterion<ARCHIVE>::getDummyCriterion();
    };
}


#endif //MH_BUILDER_EXPLORER_H
