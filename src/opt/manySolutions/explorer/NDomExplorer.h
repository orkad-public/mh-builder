/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NDOMEXPLORER_H
#define MH_BUILDER_NDOMEXPLORER_H

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/Eval.h"
#include "core/Archive.h"
#include "opt/manySolutions/explorer/Explorer.h"


namespace opt {
    namespace manySolutions {
        namespace explorer {

            /**
             * Class representing an explorer operator where k non-dominated solutions are accepted
             * @tparam ARCHIVE solution
             */
            template<typename ARCHIVE>
            class NDomExplorer : public Explorer<ARCHIVE> {
            public:
                typedef typename ARCHIVE::SOLUTION_TYPE SOLUTION;

                /**
                 * Constructor of an explorer
                 * @param _neighborhood the neighborhood of a solution
                 * @param _eval the eval function
                 */
                explicit NDomExplorer(opt::singleSolution::neighborhood::Neighborhood <SOLUTION> &_neighborhood, core::Eval <SOLUTION> &_eval,
                                      int _size = -1) : Explorer<ARCHIVE>(_neighborhood, _eval), count(0),
                                                        size(_size) {}

                virtual ~NDomExplorer() = default;

                /**
                 * Test if the search continue according to the strategy, by default, while a solution have a neighbor
                 * @param sol the solution
                 * @return true if sol have a neighbor
                 */
                bool searchCriterion(const SOLUTION &_solution) override {
                    return Explorer<ARCHIVE>::searchCriterion(_solution) && (size == -1 || count < size);
                }

                /**
                 * Test if the current solution need to be inserted in the archive, according to the reference solution
                 * @param _current current solution
                 * @param _reference reference solution
                 * @return true if the archive need to be updated with current solution
                 */
                virtual bool
                archiveUpdateCriterion(const SOLUTION &_current, const core::Archive <SOLUTION> &_reference) {
                    for (unsigned long long int i = 0; i < _reference.size(); i++) {
                        if (_reference[i] > _current)
                            return false;
                    }
                    count++;
                    return true;
                };

            protected:
                int count;
                int size;
            };
        }
    }
}
#endif //MH_BUILDER_NDOMEXPLORER_H
