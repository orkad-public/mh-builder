/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FIRSTIMPNDOMEXPLORER_H
#define MH_BUILDER_FIRSTIMPNDOMEXPLORER_H

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/Eval.h"
#include "opt/manySolutions/explorer/ImpNDomExplorer.h"


namespace opt {
    namespace manySolutions {
        namespace explorer {

            /**
             * Class representing an explorer operator where the first solution which dominate the reference solution is accepted
             * @tparam ARCHIVE solution
             */
            template<typename ARCHIVE>
            class FirstImpNDomExplorer : public ImpNDomExplorer<ARCHIVE> {
            public:
                typedef typename ARCHIVE::SOLUTION_TYPE SOLUTION;

                /**
                 * Constructor of an explorer
                 * @param _neighborhood the neighborhood of a solution
                 * @param _eval the eval function
                 */
                explicit FirstImpNDomExplorer(opt::singleSolution::neighborhood::Neighborhood<SOLUTION> &_neighborhood, core::Eval<SOLUTION> &_eval)
                        : ImpNDomExplorer<ARCHIVE>(_neighborhood, _eval, 1) {}

                virtual ~FirstImpNDomExplorer() = default;

            };
        }
    }
}
#endif //MH_BUILDER_FIRSTIMPNDOMEXPLORER_H
