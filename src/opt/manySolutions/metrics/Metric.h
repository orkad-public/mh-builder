/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_METRICS_H
#define MH_BUILDER_METRICS_H

namespace opt::manySolutions::metrics {
    /**
     * abstract class for all metric concrete class. useful for evaluate the quality of an archive
     * @tparam ARCHIVE
     */
    template<typename ARCHIVE>
    class Metric {
    public:
        /**
         * abstract method
         * methods that override this method have to perform an evaluation of the quality of a given archive
         * @param _archive
         * @return a calculated metric value
         */
        virtual double operator()(const ARCHIVE &_archive) = 0;
    };
}

#endif //MH_BUILDER_METRICS_H
