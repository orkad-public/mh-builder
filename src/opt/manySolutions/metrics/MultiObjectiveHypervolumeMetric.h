/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MULTI_OBJECTIVE_HYPERVOLUME_METRIC_H
#define MH_BUILDER_MULTI_OBJECTIVE_HYPERVOLUME_METRIC_H

#include <vector>
#include "util/ArchiveMetricHelper.h"
#include <stdexcept>
#include "opt/manySolutions/metrics/Metric.h"

#include "../../../../external/hypervolume/hv.h"


namespace opt::manySolutions::metrics {
    /**
     * Class representing the Normalized Hypervolume metric
     * @tparam ARCHIVE
     */
    template<typename ARCHIVE>
class MultiObjectiveHypervolumeMetric : public Metric<ARCHIVE> {
    public:

        /**
         * Compute the normalized hypervolume metric according to an archive
         * @param _archive
         * @return the normalized hypervolume
         */
        double operator()(const ARCHIVE &_archive) override {
            if (_archive.size() < 1)
                throw std::runtime_error(
                        "MultiObjectiveHypervolumeMetric : Archive size need to be greater than 0");

            archiveMetricHelper.computeNormalizedFront(_archive);
            return this->computeHypervolume(_archive[0].fitness().isMaximisationFitness());
        }

        /**
         * Compute the normalized hypervolume metric (with fixed bounds) according to an archive
         * @param _archive
         * @param bounds used for the archive normalization
         * @return
         */
        double operator()(const ARCHIVE &_archive, const std::vector<std::pair<double, double>> &bounds) {
            if (_archive.size() < 1)
                throw std::runtime_error(
                        "MultiObjectiveHypervolumeMetric : Archive size need to be greater than 0");

            archiveMetricHelper.computeNormalizedFront(_archive, bounds);
            return this->computeHypervolume(_archive[0].fitness().isMaximisationFitness());
        }

    protected:
        util::ArchiveMetricHelper<ARCHIVE> archiveMetricHelper;

        /**
         *
         * @param isMaximisationFitness
         * @return
         */
        double computeHypervolume(bool isMaximisationFitness) {
            std::vector<std::vector<double>> points = archiveMetricHelper.getNormalizedFront();
            std::vector<double> referencePoint ;
            auto nbObjectives = points[0].size();
            referencePoint.resize(nbObjectives);
            auto nadir = isMaximisationFitness? 0.0 : 1.0 ;
            for (unsigned long long int i=0 ; i< nbObjectives;i++)
                referencePoint[i]=nadir ;

            std::vector<double> data; // prepare data for fpli_hv external function
            auto multiplyByMinusOne = isMaximisationFitness? -1 : 1 ;
            for (unsigned int p_idx = 0 ; p_idx < points.size() ; ++p_idx) {
                for (unsigned int d_idx = 0; d_idx < nbObjectives; ++d_idx) {
                    data.push_back(multiplyByMinusOne * points[p_idx][d_idx]);
                    // multiply by -1 to simulate a minimization problem, that is required for fpli_hv function
                }
            }
            double hypervolume = fpli_hv(&data[0], nbObjectives, points.size(), &referencePoint[0]);
            return hypervolume;
        }
    };
}


#endif //MH_BUILDER_MULTI_OBJECTIVE_HYPERVOLUME_METRIC_H
