/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BIOBJECTIVEHYPERVOLUMEMETRIC_H
#define MH_BUILDER_BIOBJECTIVEHYPERVOLUMEMETRIC_H

#include <vector>
#include "util/ArchiveMetricHelper.h"
#include <stdexcept>
#include "opt/manySolutions/metrics/Metric.h"


namespace opt::manySolutions::metrics {
    /**
     * Class representing the Normalized Hypervolume metric
     * @tparam ARCHIVE
     */
    template<typename ARCHIVE>
class BiObjectiveHypervolumeMetric : public Metric<ARCHIVE> {
    public:

        /**
         * Compute the normalized hypervolume metric according to an archive
         * @param _archive
         * @return the normalized hypervolume
         */
        double operator()(const ARCHIVE &_archive) override {
            if (_archive.size() < 1)
                throw std::runtime_error(
                        "BiObjectiveHypervolumeMetric : Archive size need to be greater than 0");

            if (_archive[0].fitness().objectives().size() != 2)
                throw std::runtime_error("BiObjectiveHypervolumeMetric : the number of objectives must be 2");

            archiveMetricHelper.computeNormalizedFront(_archive);
            return computeHypervolume(_archive[0].fitness().isMaximisationFitness());
        }

        /**
         * Compute the normalized hypervolume metric according to an archive
         * @param _archive
         * @param bounds
         * @return
         */
        double operator()(const ARCHIVE &_archive, const std::vector<std::pair<double, double>> bounds) {
            if (_archive.size() < 1)
                throw std::runtime_error(
                        "BiObjectiveHypervolumeMetric : Archive size need to be greater than 0");

            if (_archive[0].fitness().objectives().size() != 2)
                throw std::runtime_error("BiObjectiveHypervolumeMetric : the number of objectives must be 2");

            archiveMetricHelper.computeNormalizedFront(_archive, bounds);
            return computeHypervolume(_archive[0].fitness().isMaximisationFitness());
        }

    protected:
        util::ArchiveMetricHelper<ARCHIVE> archiveMetricHelper;

        /**
         *
         * @param isMaximisationFitness
         * @return
         */
        double computeHypervolume(bool isMaximisationFitness) {

            archiveMetricHelper.sortNormalizedFrontAccordingToAnObjective(isMaximisationFitness);
            double nadir = (isMaximisationFitness) ? 0.0 : 1.0;

            double hypervolume = 0.0;
            double last_y = nadir;

            for (const auto &point: archiveMetricHelper.getNormalizedFront()) {
                hypervolume += (point[1] - last_y) * (point[0] - nadir);
                last_y = point[1];
            }

            return hypervolume;
        }
    };
}


#endif //MH_BUILDER_BIOBJECTIVEHYPERVOLUMEMETRIC_H
