/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "opt/criterion/EvalCriterion.h"


/**
 * Constructor of a stop criterion based on the number of evaluations
 * @param _max_eval the number of maximum evaluations
 * @param _eval the used eval function
 */
template<typename IN>
EvalCriterion<IN>::EvalCriterion(unsigned long long int _max_eval, core::Eval<IN> &_eval) : max_eval(_max_eval),
                                                                                            eval(_eval) {
    init_eval = this->eval.getEvalCounter();
}

/**
 * initialisation, set the init_eval instance variable
 */
template<typename IN>
void EvalCriterion<IN>::init() {
    this->init_eval = this->eval.getEvalCounter();
}

/**
 * update, set the last_check instance variable
 */
template<typename IN>
void EvalCriterion<IN>::update() {
    this->last_check = (this->eval.getEvalCounter() - this->init_eval);
}

/**
 * tests the stop criterion,
 * returns false if the number max of evaluation is reached
 */
template<typename IN>
bool EvalCriterion<IN>::operator()() {
    this->update();
    return this->last_check < this->max_eval;
}

/**
 * Set the maximum of evaluation
 * @param _max the maximum of evaluation
 */
template<typename IN>
void EvalCriterion<IN>::setMaxEval(unsigned long long int _max) {
    this->max_eval = _max;
}

/**
 * Get the maximum of evaluation
 * @return the maximum of evaluation
 */
template<typename IN>
unsigned long long int EvalCriterion<IN>::get_max_eval() {
    return this->max_eval;
}
