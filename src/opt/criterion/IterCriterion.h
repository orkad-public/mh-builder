/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERCRITERION_H
#define MH_BUILDER_ITERCRITERION_H

#include "core/Criterion.h"


namespace opt::criterion {
    /**
     * Class representing a criterion based on the number of iteration
     * @tparam IN
     */
    template<typename IN>
    class IterCriterion : public core::Criterion<IN> {
    protected:
        unsigned long long int current;         /*! The current number of iteration */
        unsigned long long int max_iter;        /*! The maximum number of iteration */
    public:
        /**
         * Constructor of a criterion based on the number of iteration
         * @param _max_iter the number of maximum iteration
         */
        explicit IterCriterion(unsigned long long int _max_iter) ;

        ~IterCriterion() = default ;

        /**
         * initialisation
         */
        void init() override ;

        /**
         * returns false when the the max number of iterations is reached
         * @return boolean
         */
        bool operator()() override ;

        /**
         * Set the maximum of iteration
         * @param _max the maximum of iteration
         */
        void setMaxIter(unsigned long long int _max) ;

        /**
         * Get the maximum of iteration
         * @return the maximum of iteration
         */
        unsigned long long int get_max() ;

        /**
         * Get the current number of iteration
         * @return current number of iteration
         */
        unsigned long long int getCurrentIter() ;

        /**
         * update the number of iterations
         */
        void update() override ;
    };

#include "IterCriterion.tpp"
}
#endif //MH_BUILDER_ITERCRITERION_H
