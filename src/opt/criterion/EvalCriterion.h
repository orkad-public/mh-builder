/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_EVAL_CRITERION_H
#define MH_BUILDER_EVAL_CRITERION_H

#include "core/Criterion.h"
#include "core/Eval.h"

namespace opt::criterion {
    /**
     * Class representing a criterion based on the number of evaluation
     * @tparam IN here, used for Eval definition
     */
    template<typename IN>
    class EvalCriterion : public core::Criterion<IN> {
    protected:
        unsigned long long int max_eval;        /* The maximum number of evaluations */
        unsigned long long int init_eval;       /* the number of eval after initialisation */
        unsigned long long int last_check;      /* The last check of number evaluations */
        core::Eval<IN> &eval;                   /* The eval function */
    public:
        /**
         * Constructor of a stop criterion based on the number of evaluations
         * @param _max_eval the number of maximum evaluations
         * @param _eval the used eval function
         */
        explicit EvalCriterion(unsigned long long int _max_eval, core::Eval<IN> &_eval) ;

        ~EvalCriterion() = default ;

        /**
         * initialisation, set the init_eval instance variable
         */
        void init() override ;

        /**
         * update, set the last_check instance variable
         */
        void update() override ;

        /**
         * tests the stop criterion,
         * returns false if the number max of evaluation is reached
         */
        bool operator()() override ;

        /**
         * Set the maximum of evaluation
         * @param _max the maximum of evaluation
         */
        void setMaxEval(unsigned long long int _max) ;

        /**
         * Get the maximum of evaluation
         * @return the maximum of evaluation
         */
        unsigned long long int get_max_eval() ;
    };

#include "EvalCriterion.tpp"
}
#endif //MH_BUILDER_EVAL_CRITERION_H
