/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SLOW_PROGRESS_CRITERION_H
#define MH_BUILDER_SLOW_PROGRESS_CRITERION_H

#include "core/Criterion.h"
#include "util/TimerHelper.h"


namespace opt::criterion {
    /**
     * Class representing a stop Criterion
     * Principle : the algorithm stops if there is not a better solution
     * after a given delay
     * @tparam IN
     */
    template<typename IN>
    class SlowProgressCriterion : public core::Criterion<IN> {
    protected:
        long long int delay; /*! maximum time delay in milliseconds */
        util::TimerHelper timerHelper;
    public:

        /**
         * Constructor of a slow progress criterion
         * @param _max_delay_milliseconds the maximum time delay in milliseconds
         */
        explicit SlowProgressCriterion(long long int _max_delay_milliseconds) ;

        virtual ~SlowProgressCriterion() = default ;
        /**
         * Constructor of a default time criterion
         */
        explicit SlowProgressCriterion() ;

        /**
         * initialisation of the timer
         */
        void init() override ;

        /**
         * return false if the max time is reached
         * @return
         */
        bool operator()() override ;

        /**
         * update the timer
         */
        void update() override ;

        /**
         * Get the current getElapsedTime in milliseconds
         * @return the getElapsedTime in milliseconds
         */
        long long int getElapsedTime() ;

        /**
         * Set the maximum time in milliseconds
         * @param _max_milliseconds maximum time in milliseconds
         */
        void setMaxMilliseconds(long long int _max_milliseconds) ;

        /**
         * Get the maximum time in miliseconds
         * @return the maximum time in miliseconds
         */
        long long int getMaxMilliseconds() ;
    };

#include "SlowProgressCriterion.tpp"

}
#endif //MH_BUILDER_SLOW_PROGRESS_CRITERION_H
