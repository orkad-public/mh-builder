/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KEYBOARD_HIT_CRITERION_H
#define MH_BUILDER_KEYBOARD_HIT_CRITERION_H

#include "core/Criterion.h"

#include <iostream>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>


namespace opt::criterion {
    /**
     * Class representing a stop criterion based on a key pressed
     * @tparam IN here, not used
     */
    template<typename IN>
    class KeyboardHitCriterion : public core::Criterion<IN> {
    private:
        bool kbhit()
        {
            termios term;
            tcgetattr(0, &term);

            termios term2 = term;
            term2.c_lflag &= ~ICANON;
            tcsetattr(0, TCSANOW, &term2);

            int bytes_waiting;
            ioctl(0, FIONREAD, &bytes_waiting);

            tcsetattr(0, TCSANOW, &term);

            return bytes_waiting > 0;
        }
    public:
        /**
         * Constructor of a stop criterion when a key is pressed
         */
        explicit KeyboardHitCriterion() {}

        ~KeyboardHitCriterion() = default ;

        /**
         * initialisation, nothing to do
         */
        void init() override {}

        /**
         * update, nothing to do
         */
        void update() override {}

        /**
         * tests the stop criterion,
         * returns false if a key is pressed
         */
        bool operator()() {
            if (this->kbhit()) {
                std::cout << "Interrupt by key pressed" << std::endl;
                return false;
            } else return true ;
        }


    };

}

#endif //MH_BUILDER_KEYBOARD_HIT_CRITERION_H
