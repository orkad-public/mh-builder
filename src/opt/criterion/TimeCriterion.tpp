/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "opt/criterion/TimeCriterion.h"


/**
 * Constructor of a time criterion
 * @param _max_milliseconds the maxmium time in milliseconds
 */
template<typename IN>
TimeCriterion<IN>::TimeCriterion(long long int _max_milliseconds) : max_milliseconds(_max_milliseconds) {}


/**
 * Constructor of a default time criterion
 */
template<typename IN>
TimeCriterion<IN>::TimeCriterion() : max_milliseconds(0) {}

/**
 * initialisation of the timer
 */
template<typename IN>
void TimeCriterion<IN>::init() {
    timerHelper.start();
}

/**
 * return false if the max time is reached
 * @return
 */
template<typename IN>
bool TimeCriterion<IN>::operator()() {
    this->update();
    return timerHelper.elapsedTime() < max_milliseconds;
}

/**
 * update the timer
 */
template<typename IN>
void TimeCriterion<IN>::update() { timerHelper.update(); }

/**
 * Get the current elapsedTime in milliseconds
 * @return the elapsedTime in milliseconds
 */
template<typename IN>
long long int TimeCriterion<IN>::getElapsedTime() {
    return timerHelper.elapsedTime();
}

/**
 * Set the maximum time in milliseconds
 * @param _max_milliseconds maximum time in milliseconds
 */
template<typename IN>
void TimeCriterion<IN>::setMaxMilliseconds(long long int _max_milliseconds) {
    if (_max_milliseconds >= 0)
        max_milliseconds = _max_milliseconds;
    else
        max_milliseconds = 0;
}

/**
 * Get the maximum time in milliseconds
 * @return the maximum time in milliseconds
 */
template<typename IN>
long long int TimeCriterion<IN>::getMaxMilliseconds() {
    return max_milliseconds;
}

