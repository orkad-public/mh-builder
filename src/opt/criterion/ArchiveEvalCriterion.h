/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ARCHIVE_EVAL_CRITERION_H
#define MH_BUILDER_ARCHIVE_EVAL_CRITERION_H

#include "core/Criterion.h"
#include "core/Eval.h"
#include "opt/criterion/EvalCriterion.h"


namespace opt::criterion {
    /**
     * Class representing a stop criterion for archive based on the number of evaluation
     * @tparam IN the archive type
     * @tparam SOL the type of an element of the archive
     *
     */
    template<typename IN, typename SOL>
    class ArchiveEvalCriterion : public core::Criterion<IN> {
    protected:
        EvalCriterion<SOL> evalStop ;
    public:
        /**
         * Constructor of a stop criterion
         */
        ArchiveEvalCriterion(unsigned long long int _max_eval, core::Eval<SOL> &_eval) : evalStop(_max_eval,_eval){}

        /**
         * default destructor
         */
        virtual ~ArchiveEvalCriterion() = default ;

        /**
         * initialisation
         */
        void init() override {
            this->evalStop.init() ;
        }

        /**
         * update
         */
        void update() override {
            this->evalStop.update();
        }

        /**
         * tests the stop criterion,
         * returns false if number of evaluation is achieved
         */
        bool operator()() override {
            return this->evalStop.operator()();
        }

    };

}

#endif //MH_BUILDER_ARCHIVE_EVAL_CRITERION_H
