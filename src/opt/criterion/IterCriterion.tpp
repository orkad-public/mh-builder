/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "opt/criterion/IterCriterion.h"

/**
 * Constructor of a criterion based on the number of iteration
 * @param _max_iter the number of maximum iteration
 */
template<typename IN>
IterCriterion<IN>::IterCriterion(unsigned long long int _max_iter) : current(0), max_iter(_max_iter) {}


/**
 * initialisation
 */
template<typename IN>
void IterCriterion<IN>::init() {
    this->current = 0;
}

/**
 * returns false when the the max number of iterations is reached
 * @return boolean
 */
template<typename IN>
bool IterCriterion<IN>::operator()() {
    return this->current < this->max_iter; // Current is update only in method update
}

/**
 * Set the maximum of iteration
 * @param _max the maximum of iteration
 */
template<typename IN>
void IterCriterion<IN>::setMaxIter(unsigned long long int _max) {
    this->max_iter = _max;
}

/**
 * Get the maximum of iteration
 * @return the maximum of iteration
 */
template<typename IN>
unsigned long long int IterCriterion<IN>::get_max() {
    return this->max_iter;
}

/**
 * Get the current number of iteration
 * @return current number of iteration
 */
template<typename IN>
unsigned long long int IterCriterion<IN>::getCurrentIter() {
    return this->current;
}

/**
 * update the number of iterations
 */
template<typename IN>
void IterCriterion<IN>::update() {
    this->current++;
}

