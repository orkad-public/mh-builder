/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SIMULATEDANNEALING_H
#define MH_BUILDER_SIMULATEDANNEALING_H


#include "core/Eval.h"
#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "core/criterion/CriterionAnd.h"
#include "core/Perturbation.h"
#include "opt/perturbation/DummyPerturbation.h"
#include "opt/singleSolution/localsearch/IteratedLocalSearch.h"
#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"
#include "opt/singleSolution/localsearch/coolingSchedule/CoolingSchedule.h"

namespace opt {
    namespace singleSolution {
        namespace localsearch {
            template<typename SOL>
            class SimulatedAnnealing : public IteratedLocalSearch<SOL> {
            public:

                using IteratedLocalSearch<SOL>::operator();

                /**
                 * constructor for Simulated Algorithm
                 * @param _neighborhoodExplorer
                 * @param _coolingSchedule
                 * @param _criterion
                 * @param _perturbation
                 */
                SimulatedAnnealing(
                        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> &_neighborhoodExplorer,
                        coolingSchedule::CoolingSchedule<SOL> &_coolingSchedule,
                        core::Criterion<SOL> &_criterion,
                        core::Perturbation<SOL> &_perturbation) :
                        opt::singleSolution::localsearch::IteratedLocalSearch<SOL>(
                                _perturbation,
                                _neighborhoodExplorer,
                                * (new core::criterion::CriterionAnd(_criterion, _coolingSchedule)),
                                [&](SOL &_in, SOL &_current, SOL &_tmp) {
                                    if (_current > _tmp || accept(_current, _tmp)) {
                                        _tmp = _current;
                                        if (_current > _in) {
                                            _in = _current;
                                        }
                                    }
                                    coolingSchedule.update();
                                },
                                [](SOL &_in, SOL &_current) { _current = _in; },
                                [](SOL &_current, SOL &_tmp) { _tmp = _current; }),
                        coolingSchedule(_coolingSchedule)
                        {
                            // this->criterion = new core::criterion::CriterionAnd(_criterion, _coolingSchedule) ;
                             }

                /**
                 * constructor for Simulated Algorithm with no perturbation
                 * @param _neighborhoodExplorer
                 * @param _coolingSchedule
                 * @param _criterion
                 */
                SimulatedAnnealing(opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> &_neighborhoodExplorer,
                                   coolingSchedule::CoolingSchedule<SOL> &_coolingSchedule,
                                   core::Criterion<SOL> &_criterion)
                        : SimulatedAnnealing(_neighborhoodExplorer,
                                             _coolingSchedule,
                                             _criterion,
                                             *(new opt::perturbation::DummyPerturbation<SOL>())
                                             ) {}

                void init(const SOL &_sol) override {
                    coolingSchedule.init();
                    opt::singleSolution::localsearch::IteratedLocalSearch<SOL>::init(_sol);
                }

                virtual bool accept(SOL &_sol, SOL &_current) {
                    double proba = exp(
                            (_current.fitness().scalar() - _sol.fitness().scalar()) / coolingSchedule.getTemperature());
                    return util::RNGHelper::get()->uniform() < proba;
                }

            protected:
                coolingSchedule::CoolingSchedule<SOL> &coolingSchedule;

            };
        }
    }
}
#endif //MH_BUILDER_SIMULATEDANNEALING_H
