/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERATEDLOCALSEARCH_H
#define MH_BUILDER_ITERATEDLOCALSEARCH_H

#include "core/Eval.h"
#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "opt/IteratedAlgorithm.h"


namespace opt {
    namespace singleSolution {
        namespace localsearch {
            /**
             * Class representing an ILS
             * @tparam SOL
             */
            template<typename SOL>
            class IteratedLocalSearch : public opt::IteratedAlgorithm<SOL> {
            public:

                //using opt::IteratedAlgorithm<SOL>::operator();

                /**
                 * Constructor of a Iterated LS
                 * @param _perturbation perturbation criterion
                 * @param _algorithm algorithm which need to be iterated
                 * @param _criterion stop criterion
                 */
                IteratedLocalSearch(
                        core::Algorithm<SOL> &_perturbation,
                        core::Algorithm<SOL> &_localSearch,
                        core::Criterion<SOL> &_criterion) :
                        opt::IteratedAlgorithm<SOL>(_perturbation, _localSearch, _criterion) {}

                /**
                 * Constructor of a Iterated LS
                 * @param _perturbation perturbation criterion
                 * @param _localSearch algorithm which need to be iterated
                 * @param _criterion stop criterion
                 * @param _update_func
                 * @param _copy_func
                 * @param _tmp_func
                 */
                IteratedLocalSearch(
                        core::Algorithm<SOL> &_perturbation,
                        core::Algorithm<SOL> &_localSearch,
                        core::Criterion<SOL> &_criterion,
                        std::function<void(SOL &_in, SOL &_current, SOL &_tmp)> _update_func,
                        std::function<void(SOL &_current, SOL &_tmp)> _copy_func,
                        std::function<void(SOL &_current, SOL &_tmp)> _tmp_func) :
                        opt::IteratedAlgorithm<SOL>(_perturbation, _localSearch, _criterion, _update_func, _copy_func,
                                                    _tmp_func) {}
            };
        }
    }
}



#endif //MH_BUILDER_ITERATEDLOCALSEARCH_H
