/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_HILLCLIMBING_H
#define MH_BUILDER_HILLCLIMBING_H


#include "core/Eval.h"
#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"

namespace opt::singleSolution::localsearch {

    /**
     * Class representing a class descent local search (HillClimbing)
     * @tparam SOL
     */
    template<typename SOL>
    class HillClimbing : public LocalSearch<SOL> {
    public:

        /**
         * Constructor of a Hillclimbing algorithm
         * @param _neighborhoodExplorer the neighborhood explorer
         * @param _eval the evaluation function
         */
        explicit HillClimbing(
                opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> &_neighborhoodExplorer,
                core::Eval<SOL> &_eval) :
                LocalSearch<SOL>(_neighborhoodExplorer, _eval) {}

        /**
        * The continuator of a hillClimbing
        * @param sol
        * @return true if the current sol is better than bestSol
        */
        bool continuator([[maybe_unused]] SOL &_sol ) override {
            return this->currentSol > this->bestSol;
        }

    };
}
#endif //MH_BUILDER_HILLCLIMBING_H
