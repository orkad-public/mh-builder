/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "SimpleCoolingSchedule.h"

/**
         * constructor
         * @param _initialTemperature
         * @param _finalTemperature
         * @param _alpha decrease coefficient
         * @param _kmax max iterations at the same temperature
         */
template<class SOL>
SimpleCoolingSchedule<SOL>::SimpleCoolingSchedule(double _initialTemperature, double _finalTemperature,
                                                  double _alpha,
                                                  unsigned long long int _kmax) :
        initialTemperature(_initialTemperature),
        finalTemperature(_finalTemperature),
        alpha(_alpha),
        kmax(_kmax) {
    this->temperature = _initialTemperature;
}

/**
 * Initialisation
 *
 */
template<class SOL>
void SimpleCoolingSchedule<SOL>::init() {
    k = 0;
};

/**
 * update the temperature
 *
 */
template<class SOL>
void SimpleCoolingSchedule<SOL>::update() {
    if (k >= kmax) {
        this->temperature = this->temperature * alpha;
        k = 0;
    } else {
        k++;
    }
};

/**
 * Test if the algorithm continues (result true in that case)
 */
template<class SOL>
bool SimpleCoolingSchedule<SOL>::operator()() {
    return this->temperature > finalTemperature;
}