/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SIMPLECOOLINGSCHEDULE_H
#define MH_BUILDER_SIMPLECOOLINGSCHEDULE_H

#include "opt/singleSolution/localsearch/coolingSchedule/CoolingSchedule.h"

namespace opt::singleSolution::localsearch::coolingSchedule {
    /**
    * Simple Cooling Schedule of the temperature in the simulated algorithm
    */
    template<class SOL>
    class SimpleCoolingSchedule : public CoolingSchedule<SOL> {
    protected:
        double initialTemperature;  /*! Initial temperature !*/
        double finalTemperature;    /*! threshold temperature !*/
        double alpha;               /*! Decrease Coefficient !*/
        unsigned long long int kmax;/*! Max iterations at the same temperature !*/
        unsigned long long int k;   /*! NUmber of iterations !*/
    public:
        /**
         * constructor
         * @param _initialTemperature
         * @param _finalTemperature
         * @param _alpha decrease coefficient
         * @param _kmax max iterations at the same temperature
         */
        explicit SimpleCoolingSchedule(double _initialTemperature, double _finalTemperature, double _alpha,
                                       unsigned long long int _kmax) ;

        /**
         * Initialisation
         *
         */
        void init() override ;

        /**
         * update the temperature
         *
         */
        void update() override ;

        /**
         * Test if the algorithm continues (result true in that case)
         */
        bool operator()() override ;
    };

#include "SimpleCoolingSchedule.tpp"

}

#endif //MH_BUILDER_SIMPLECOOLINGSCHEDULE_H
