/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_LOCALSEARCH_H
#define MH_BUILDER_LOCALSEARCH_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include "core/criterion/DummyCriterion.h"
#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"


namespace opt::singleSolution::localsearch {

    /**
     * Class representing an algorithm related to localsearch
     * @tparam SOL
     */
    template<typename SOL>
    class LocalSearch : public core::Algorithm<SOL> {
    public:

        /**
        * Constructor of a simple Local Search
        * no neighborhoodExplorer is set, must be done later via the setter method
        * @param _eval the evaluation
        */
        explicit LocalSearch(core::Eval<SOL> &_eval) : neighborhoodExplorer(nullptr), eval(_eval) {
        }

        /**
         * Constructor of a simple Local Search
         * @param _neighborhoodExplorer the neighborhood explorer
         * @param _eval the evaluation
         */
        explicit LocalSearch(
                opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> &_neighborhoodExplorer,
                core::Eval<SOL> &_eval) : neighborhoodExplorer(&_neighborhoodExplorer), eval(_eval) {
        }

        /**
         * Constructor of a simple Local Search
         * @param _neighborhoodExplorer the neighborhood explorer
         * @param _eval the evaluation
         * @param _criterion the stop criterion
         */
        explicit LocalSearch(
                opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> &_neighborhoodExplorer,
                core::Eval<SOL> &_eval, core::Criterion<SOL> &_criterion) :
                neighborhoodExplorer(&_neighborhoodExplorer), eval(_eval) {
            this->criterion = &_criterion;
        }

        /**
         * Initialisation of the algorithm according to an initial solution
         * @param sol solution
         */
        virtual void init(const SOL &sol) override {
            bestSol = sol;
            if (!bestSol.valid()) {
                eval(bestSol);
            }
            currentSol = bestSol;
            (*this->criterion).init();
        }

        /**
         * The operator that runs the algorithm
         * @param sol solution
         */
        void operator()(SOL &sol) override {
            operator()(sol, *this->criterion);
        }

        /**
         * The operator that runs the algorithm with a specific criterion
         * @param sol solution
         * @param criterion criterion
         */
        virtual void operator()(SOL &_sol, core::Criterion<SOL> &_criterion) override {
            this->init(_sol);
            (_criterion).init();
            if (this->neighborhoodExplorer == nullptr)
                throw std::runtime_error("LocalSearch.h : the neighborhood explorer must be set");

            this->neighborhoodExplorer->operator()(this->currentSol, this->eval, _criterion);
            this->checkpoint->operator()(this->currentSol);

            while (this->continuator(_sol) && _criterion()) {
                this->updateBestSolution();
                this->neighborhoodExplorer->operator()(this->currentSol, this->eval, _criterion);
                this->checkpoint->operator()(this->currentSol);

            }
            this->updateBestSolution();
            _sol = this->bestSol;

        };

        /**
         * The continuator of a local search
         * @param sol
         * @return true if the localSearch continue
         */
        virtual bool continuator([[maybe_unused]] SOL &_sol) {
            return true;
        }

        /**
         * Get the best Solution
         * @return the best solution
         */
        SOL &best() {
            return bestSol;
        }

        /**
         * Get the current solution
         * @return the current solution
         */
        SOL &current() {
            return currentSol;
        }

        /**
         * Set the neighborhood explorer
         * @param _neighborhoodExplorer neighborhood explorer
         */
        virtual void setNeighborhoodExplorer(
                opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> &_neighborhoodExplorer) {
            neighborhoodExplorer = &_neighborhoodExplorer;
        }

        /**
        * Get the neighborhood explorer
        * @param _neighborhoodExplorer neighborhood explorer
        */
        virtual opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *
        getNeighborhoodExplorer() {
            return neighborhoodExplorer;
        }
    private:
        /**
         * update bestSol if the currentSol is better
         */
        void updateBestSolution() {
            if (this->currentSol > this->bestSol) {
                this->bestSol = this->currentSol;
            }
        }

    protected:
        /*! the neighborhood explorer */
        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *neighborhoodExplorer;
        /*! the evaluation function */
        core::Eval<SOL> &eval;
        /*! The best solution found */
        SOL bestSol;
        /*! The current solution */
        SOL currentSol;
    };
}

#endif //MH_BUILDER_LOCALSEARCH_H
