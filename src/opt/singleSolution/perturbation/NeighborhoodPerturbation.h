/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBORHOODPERTURBATION_H
#define MH_BUILDER_NEIGHBORHOODPERTURBATION_H

#include "core/Perturbation.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"

namespace opt {
    namespace singleSolution {
        namespace perturbation {

            /**
             * Class representing a neighborhood perturbation
             * @tparam SOL solution type
             */
            template<typename SOL>
            class NeighborhoodPerturbation : public core::Perturbation<SOL> {
            public:

                using core::Perturbation<SOL>::operator();

                /**
                 * Constructor of the perturbation
                 * @param _neighborhood the neighborhood operator
                 * @param _eval the eval function
                 * @param _strength number of neighborhood perturbation
                 */
                explicit NeighborhoodPerturbation(opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood,
                                                  core::Eval<SOL> *_eval = nullptr,
                                                  unsigned long long int _strength = 1,
                                                  bool _with_eval = true) :
                        neighborhood(&_neighborhood), strength(_strength), eval(_eval), with_eval(_with_eval) {}

                void operator()(SOL &_in, core::Criterion<SOL> &_criterion) override {
                    for (unsigned long long int i = 0; i < strength && _criterion(); ++i) {
                        neighborhood->init(_in);
                        neighborhood->operator()(_in);
                        neighborhood->apply(_in);
                    }
                    if (with_eval && eval != nullptr)
                        (*eval)(_in);
                }

                /**
                 * Set neighborhood operator
                 * @param _neighborhood neighborhood
                 */
                void setNeighborhood(opt::singleSolution::neighborhood::Neighborhood<SOL> &_neighborhood) {
                    neighborhood = &_neighborhood;
                }

                /**
                 * Set perturbation strength
                 * @param _strength perturbation strenght
                 */
                void setStrength(unsigned long long int _strength) {
                    strength = _strength;
                }

            protected:
                /*! The neighborhood operator */
                opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood;
                /*! The strength of the perturbation */
                unsigned long long int strength;
                core::Eval<SOL> *eval;
                bool with_eval;
            };
        }
    }
}


#endif //MH_BUILDER_NEIGHBORHOODPERTURBATION_H
