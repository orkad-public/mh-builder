/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RANDOMNEIGHBORHOOD_H
#define MH_BUILDER_RANDOMNEIGHBORHOOD_H

#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "util/RNGHelper.h"

namespace opt {
    namespace singleSolution {
        namespace neighborhood {

            /**
             * Class representing a randomized neighborhood
             * @tparam SOL
             */
            template<typename SOL>
            class RandomNeighborhood : public IndexNeighborhood<SOL> {
            public:

                /**
                 * Constructor of a randomized Neighborhood
                 * @param _neighbor
                 */
                explicit RandomNeighborhood(neighbor::IndexNeighbor<SOL> &_neighbor) : IndexNeighborhood<SOL>(
                        _neighbor) {}

                /**
                 * Initialise the vector of randomized neighborhood
                 * @param sol
                 */
                void init(const SOL &sol) override {
                    IndexNeighborhood<SOL>::init(sol);
                    random_key = util::RNGHelper::get()->random(this->keys.size());
                    this->getIndexNeighbor()->setKey(this->keys[random_key]);
                }

                /**
                 * Compute next neighbor according to the key
                 */
                void next() override {
                    if (this->hasNextNeighbor()) {
                        std::swap(this->keys[random_key], this->keys[this->k++]);
                        random_key = util::RNGHelper::get()->random(this->k, this->keys.size());
                        this->getIndexNeighbor()->setKey(this->keys[random_key]);
                    } else throw std::runtime_error("Error: No next neighbor. Do you initialize the neighborhood ?");
                }

            protected:

                unsigned int random_key;

            };
        }
    }
}

#endif //MH_BUILDER_RANDOMNEIGHBORHOOD_H
