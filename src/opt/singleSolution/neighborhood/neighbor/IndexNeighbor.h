/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INDEXNEIGHBOR_H
#define MH_BUILDER_INDEXNEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"

namespace opt::singleSolution::neighborhood::neighbor {
    /**
     * Abstract Class for indexed neighbor
     * principle: the neighbor operator manages two variables key and maxKey
     * maxKey corresponds to the number max of available movements according to  the concrete neighbor
     * key (a value in [0, maxKey[ corresponds to one of these maxKey movements,
     * key is used by the move method
     * @tparam SOL the solution type
     */
    template<typename SOL>
    class IndexNeighbor : public Neighbor<SOL> {
    public:

        /**
         * Set key to a Neighbor
         * @param _key the key
         */
        virtual void setKey(unsigned long long int _key) {
            if (_key <= maxKey) {
                this->key = _key;
                this->flag = false;
            } else throw std::runtime_error("Error: _key > maxKey. Do you initialize maxKey ?");
        }

        /**
         * Get key of the Neighbor
         * @return the key
         */
        virtual unsigned long long int getKey() {
            return this->key;
        }

        /**
         * Set maximum key
         * @param _maxKey
         */
        virtual void setMaxKey(unsigned long long int _maxKey) {
            this->maxKey = _maxKey;
        }

        /**
         * Get maximum key
         */
        virtual unsigned long long int getMaxKey() {
            return this->maxKey;
        }

    protected:
        /*! the key */
        unsigned long long int key = 0;
        unsigned long long int maxKey = 0;
    };
}
#endif //MH_BUILDER_INDEXNEIGHBOR_H
