/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBOR_H
#define MH_BUILDER_NEIGHBOR_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include <ostream>
#include <string>
#include <vector>

namespace opt::singleSolution::neighborhood::neighbor {

    /**
     * abstract class representing a Neighbor (or mutation for GA)
     * @tparam SOL
     */
    template<typename SOL>
    class Neighbor : public core::Algorithm<SOL> {
    public:

        typedef typename SOL::FITNESS FIT;

        /**
         * constructor with no parameter
         * the name of the neighbor operator is not defined
         */
        Neighbor() : name("NO-NAME") {}


        /*
         * default destructor
         */
        virtual ~Neighbor() = default ;

        /**
         * Make the modification of the solution
         * @param sol the solution to modify
         */
        virtual void operator()(SOL &sol) {
            this->do_move(sol);
        };

        /**
         * Flag the neighbor and apply move
         * @param sol the solution
         */
        virtual void do_move(SOL &sol) {
            this->flag = true;
            this->previousFitness = sol.fitness();
            this->previousSolution = sol;
            this->move(sol);
        }

        /**
         * Flag the neighbor, apply move and eval solution
         * @param sol the solution
         */
        virtual void do_move(SOL &sol, core::Eval<SOL> &eval) {
            this->do_move(sol);
            eval(sol);
        }

        /**
         * Test if the neighbor is flagged, if yes, apply move back, else throw a runtime_error
         * @param sol the solution
         */
        virtual void do_move_back(SOL &sol) {
            if (flag) {
                this->move_back(sol);
                flag = false;
                sol.fitness(previousFitness);
            } else throw std::runtime_error("Error: you cannot apply a move back before apply a move");
        }

        /**
         * Print the neighbor to an output stream
         * @param os
         */
        virtual void printOn(std::ostream &os) const {
            os << "NO PRINT IMPLEMENTED FOR THIS NEIGHBOR";
        }

        /**
         * really apply move to a solution
         * @param _sol
         */
        virtual void apply([[maybe_unused]] SOL &_sol) {};

        /**
         * provides the name of the neighbor
         * @return
         */
        virtual std::string getName() const {
            return name;
        }



    protected:
        /**
        * Save the solution for a move back and apply
        * @param sol the solution
        */
        virtual void move(SOL &sol) = 0;

        /**
         * Apply Move back
         * @param sol the solution
         */
        virtual void move_back(SOL &sol) {
            sol = this->previousSolution;
        };

        /*! flag for a move apply */
        bool flag = false;
        /*! previous fitness before move */
        FIT previousFitness;
        /*! previous solution before move */
        SOL previousSolution;

        /*! Name of neighbor */
        std::string name;


    };

    /**
     * An alias of Neighbor for GA
     */
    template<typename SOL> using Mutation = Neighbor<SOL>;

    /**
     * To print a ruleset with output stream
     * @tparam SOL rule type
     * @param os output stream
     * @param Neighbor neighbor
     * @return output stream
     */
    template<typename SOL>
    std::ostream &operator<<(std::ostream &os, const Neighbor<SOL> &neighbor) {
        neighbor.printOn(os);
        return os;
    }
}

#endif //MH_BUILDER_NEIGHBOR_H
