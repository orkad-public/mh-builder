/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INDEX_NEIGHBORHOOD_H
#define MH_BUILDER_INDEX_NEIGHBORHOOD_H

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "util/DebugHelper.h"

namespace opt::singleSolution::neighborhood {

    /**
     * Class representing an indexed neighborhood
     * @tparam SOL
     */
    template<typename SOL>
    class IndexNeighborhood : public Neighborhood<SOL> {
    public:

        /**
         * Constructor of an Indexed Neighborhood
         * @param _neighbor
         */
        explicit IndexNeighborhood(neighbor::IndexNeighbor<SOL> &_neighbor) : Neighborhood<SOL>(_neighbor), k(0) {

        }

        /**
         * Initialise the vector of indexed neighborhood
         * @param sol
         */
        void init(const SOL &sol) override {
            if (this->neighbor == nullptr)
                throw std::runtime_error("Error: No neighbor set.");
            this->neighbor->init(sol);

            if (this->getIndexNeighbor()->getMaxKey() != this->keys.size()) {
                this->keys.resize(this->getIndexNeighbor()->getMaxKey());
                for (unsigned long long int i = 0; i < this->getIndexNeighbor()->getMaxKey(); ++i) {
                    this->keys[i] = i;
                }
            }

            this->k = 0;
            this->getIndexNeighbor()->setKey(this->keys[this->k]);
        }

        /**
         * Compute next neighbor according to the key
         */
        void next() override {
            if (this->hasNextNeighbor()) {
                this->getIndexNeighbor()->setKey(this->keys[++this->k]);
            } else throw std::runtime_error("Error: No next neighbor. Do you initialize the neighborhood ?");
        }

        /**
         * Test if the neighborhood have a new neighbor
         * @return
         */
        bool hasNextNeighbor() override {
            return this->k < this->getIndexNeighbor()->getMaxKey() - 1;
        }

        /**
         * provides the index neighbor
         * @return
         */
        neighbor::IndexNeighbor<SOL> *getIndexNeighbor() {
            if (this->neighbor == nullptr)
                throw std::runtime_error("Error: No neighbor set.");
            return ((neighbor::IndexNeighbor<SOL> *) this->neighbor);
        }



        /**
         * Set vector of keys
         * @param _keys
         */
        void setKeys(std::vector<unsigned long long int> &_keys) {
            keys = _keys;
        }

        /**
         * Get vector of keys
         * @return keys
         */
        std::vector<unsigned long long int> getKeys() {
            return keys;
        }

    protected:
        /*! vector of neighbor keys*/
        std::vector<unsigned long long int> keys;
        /*! current key */
        unsigned long long int k;

    };
}
#endif //MH_BUILDER_INDEX_NEIGHBORHOOD_H
