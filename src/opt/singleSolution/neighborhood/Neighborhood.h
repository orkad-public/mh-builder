/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBORHOOD_H
#define MH_BUILDER_NEIGHBORHOOD_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"

namespace opt::singleSolution::neighborhood {

    /**
     * Class representing a neighborhood
     * which is associated with a neighbor operator (class Neighbor)
     * @tparam SOL
     */
    template<typename SOL>
    class Neighborhood : public core::Algorithm<SOL> {
    public:
        /**
         * Constructor of a Neighborhood
         * @param _neighbor the neighbor operator
         */
        explicit Neighborhood(neighbor::Neighbor<SOL> &_neighbor) : neighbor(&_neighbor) {};

        /**
         * Constructor of a Neighborhood
         * fix the neighbor operator to null
         * (must be done later via the setter method)
         */
        explicit Neighborhood() : neighbor(nullptr) {};

//        /**
//         * Initialize the neighborhood
//         * @param _sol the solution
//         */
//        void init([[maybe_unused]] const SOL &_sol) override {
//
//        }

        /**
         * Apply the move for the current neighbor
         * @param _sol the solution to move
         */
        void operator()(SOL &_sol) override {
            if (this->neighbor == nullptr)
                throw std::runtime_error("Error: No neighbor set.");
            this->neighbor->do_move(_sol);
        }

        /**
         * Apply the move for the current neighbor and eval it
         * @param _sol the solution
         * @param _eval the evaluation function
         */
        virtual void operator()(SOL &_sol, core::Eval<SOL> &_eval) {
            if (this->neighbor == nullptr)
                throw std::runtime_error("Error: No neighbor set.");
            this->neighbor->do_move(_sol, _eval);
        }

        /**
         * Apply the move back for the current neighbor
         * @param _sol the solution
         */
        virtual void move_back(SOL &_sol) {
            if (this->neighbor == nullptr)
                throw std::runtime_error("Error: No neighbor set.");
            this->neighbor->do_move_back(_sol);
        }

        /**
         * Compute the next neighbor
         */
        virtual void next() = 0;

        /**
         * Set type of Neighbor operator
         * @param _neighbor the neighbor
         */
        virtual void setNeighbor(neighbor::Neighbor<SOL> &_neighbor) {
            this->neighbor = &_neighbor;
        }

        /**
         * Get Neighbor
         * @return the current neighbor
         */
        neighbor::Neighbor<SOL> *getNeighbor() {
            return this->neighbor;
        }

        /**
         * Test if the neighborhood have a new neighbor, by default always true
         * @return true
         */
        virtual bool hasNextNeighbor() {
            return true;
        }

        /**
         *
         * @param current
         */
        virtual void apply(SOL &current) {
            neighbor->apply(current);
        }

    protected:
        /*! The neighbor operator of the neighborhood */
        neighbor::Neighbor<SOL> *neighbor;
    };
}
#endif //MH_BUILDER_NEIGHBORHOOD_H
