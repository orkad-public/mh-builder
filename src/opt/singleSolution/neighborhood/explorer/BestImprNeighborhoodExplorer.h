/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BESTIMPRNEIGHBORHOODEXPLORER_H
#define MH_BUILDER_BESTIMPRNEIGHBORHOODEXPLORER_H

#include "core/Eval.h"
#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"

namespace opt::singleSolution::neighborhood::explorer {
    /**
     * Class representing a best improvement exploration strategy for Neighborhood
     * @tparam SOL
     */
    template<typename SOL>
    class BestImprNeighborhoodExplorer : public NeighborhoodExplorer<SOL> {
    public:

        /**
         * Constructor for a best improvement neighborhood explorer
         * @param _neighborhood the neighborhood operator
         * @param _eval the evaluation function
         */
        explicit BestImprNeighborhoodExplorer(Neighborhood<SOL> &_neighborhood, core::Eval<SOL> &_eval)
                : NeighborhoodExplorer<SOL>(_neighborhood, _eval) {}

        /**
         * Test if current neighbor solution is better than the selected one
         * @param sol the solution
         * @param _current the current solution
         * @param _selected the selected solution
         * @return true if the current neighbor is better than out solution
         */
        bool replacementCriterion([[maybe_unused]] const SOL &sol , const SOL &_current, const SOL &_selected) override {
            return _current > _selected;
        };

    };
}

#endif //MH_BUILDER_BESTIMPRNEIGHBORHOODEXPLORER_H
