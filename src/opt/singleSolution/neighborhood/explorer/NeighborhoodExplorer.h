/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBORHOODEXPLORER_H
#define MH_BUILDER_NEIGHBORHOODEXPLORER_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include "core/criterion/DummyCriterion.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"


namespace opt::singleSolution::neighborhood::explorer {

    /**
     * Class representing an exploration strategy for Neighborhood
     * @tparam SOL
     */
    template<typename SOL>
    class NeighborhoodExplorer : public core::Algorithm<SOL> {
    public:

        /**
         * Constructor for a neighborhood explorer
         * @param _neighborhood the neighborhood operator
         * @param _eval the evaluation function
         */
        explicit NeighborhoodExplorer(Neighborhood<SOL> &_neighborhood, core::Eval<SOL> &_eval)
                : neighborhood(&_neighborhood), eval(_eval) {}

        /**
         * Get neighborhood
         * @return neighborhood
        */
        Neighborhood<SOL> *getNeighborhood() {
            return neighborhood;
        }

        /**
         * Set neighborhood
         * @param _neighborhood
         */
        void setNeighborhood(Neighborhood<SOL> &_neighborhood) {
            this->neighborhood = _neighborhood;
        }

        /**
         * Operator of a neighborhood explorer
         * @param sol
         */
        virtual void operator()(SOL &sol) {
            operator()(sol, eval, *this->criterion);
        }

        /**
         * Operator of a neighborhood explorer
         * @param sol
         * @param criterion temporary criterion
         */
        virtual void operator()(SOL &sol, core::Criterion<SOL> &criterion) {
            operator()(sol, eval, criterion);
        }

        /**
         * Operator of a neighborhood explorer with an evaluation function and a criterion
         * @param sol solution
         * @param _eval the evaluation function
         * @param criterion criterion
         */
        virtual void operator()(SOL &sol, core::Eval<SOL> &_eval, core::Criterion<SOL> &criterion) {

            // Initialize the neighborhood
            neighborhood->init(sol);

            // Save the main solution
            if (!sol.valid()) _eval(sol);
            SOL current = sol;
            SOL selected = sol;

            // The first iteration
            if (this->searchCriterion(sol, current, selected) && criterion()) {
                this->iteration(sol, current, selected);
            }
            // Exploration
            while (this->searchCriterion(sol, current, selected) && criterion()) {
                this->neighborhood->next(); // Go to the next neighbor
                this->iteration(sol,  current, selected); // Make the next iteration
            }

            sol = selected;
        }



        /**
         * Test if the selected solution need to be replaced according to the main solution
         * @param sol the main solution
         * @param _current the current solution
         * @param _selected the selected solution
         * @return true if selected need to be replaced
         */
        virtual bool replacementCriterion(const SOL &sol, const SOL &_current, const SOL &_selected) = 0;

        /**
         * Test if the search continues according to the strategy, by default, while a solution have a neighbor
         * @param sol the solution
         * @param _current the current solution
         * @param _selected the selected solution
         * @return true if the exploration can continue
         */
        virtual bool
        searchCriterion([[maybe_unused]] const SOL &sol, [[maybe_unused]] const SOL &_current,
                        [[maybe_unused]] const SOL &_selected) {
            return this->neighborhood->hasNextNeighbor();
        }

    protected:
        /*! The Neighborhood operator */
        Neighborhood<SOL> *neighborhood;
        /*! The Eval function */
        core::Eval<SOL> &eval;

        /**
         * Make an iteration (move, eval, replacement if needed, move_back) for the current neighbor
         * @param sol solution
         * @param _eval the evaluation function
         */
        virtual void iteration(SOL &sol, SOL &current, SOL &selected) {
            this->neighborhood->operator()(current, this->eval); // do a move on the current solution
            if (this->replacementCriterion(sol, current, selected)) { // change if improvement
                selected = current;
            }
            this->neighborhood->move_back(current); // move_back
        }
    };
}

#endif //MH_BUILDER_NEIGHBORHOODEXPLORER_H
