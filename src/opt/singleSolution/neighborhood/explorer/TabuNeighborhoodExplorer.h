/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TABUNEIGHBORHOODEXPLORER_H
#define MH_BUILDER_TABUNEIGHBORHOODEXPLORER_H

#include "core/Eval.h"
#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/Criterion.h"
#include "opt/memory/TabuList.h"

namespace opt::singleSolution::neighborhood::explorer {
    /**
     * Class representing a tabu exploration strategy for Neighborhood
     * @tparam SOL
     */
    template<typename SOL>
    class TabuNeighborhoodExplorer : public NeighborhoodExplorer<SOL> {
    public:

        /**
         * Constructor for a tabu neighborhood explorer
         * @param _neighborhood the neighborhood operator
         * @param _eval the evaluation function
         */
        explicit TabuNeighborhoodExplorer(NeighborhoodExplorer<SOL> &_neighborhoodExplorer,
                                          core::Eval<SOL> &_eval, unsigned long long int _maxSize,
                                          unsigned long long int _nbIter) :
                NeighborhoodExplorer<SOL>(*_neighborhoodExplorer.getNeighborhood(), _eval),
                neighborhoodExplorer(&_neighborhoodExplorer),
                tabuList(_maxSize, _nbIter),
                firstNeighbor(true),
                firstLO(false) {}

        /**
       * Operator of a neighborhood explorer
       * @param sol
       */
        void operator()(SOL &sol) override {
            firstNeighbor = true;
            NeighborhoodExplorer<SOL>::operator()(sol);
            this->tabuList(sol);
            this->tabuList.update();
        }

        /**
       * Operator of a neighborhood explorer with an evaluation function and a criterion
       * @param sol solution
       * @param _eval the evaluation function
       * @param criterion criterion
       */
        void operator()(SOL &sol, core::Eval<SOL> &_eval, core::Criterion<SOL> &criterion) override {
            firstNeighbor = true;
            NeighborhoodExplorer<SOL>::operator()(sol, _eval, criterion);
            this->tabuList(sol);
            this->tabuList.update();
        }

        /**
         * Test if current neighbor is better than selected one
         * @param sol the solution
         * @param _current the current solution
         * @param _selected the selected solution
         * @return true if the current neighbor is better than selected solution
         */
        bool replacementCriterion(const SOL &sol, const SOL &_current, const SOL &_selected) override {
            if (!this->tabuList.isTabu(_current) && firstNeighbor && firstLO) {
                firstNeighbor = false;
                return true;
            }
            return !this->tabuList.isTabu(_current) &&
                   (*neighborhoodExplorer).replacementCriterion(sol, _current, _selected);
        };

        /**
        * Test if the search continue according to the strategy, by default, while a solution have a neighbor
        * @param sol the solution
        * @param _current the current solution
        * @param _selected the selected solution
        * @return true if sol have a neighbor
        */
        bool searchCriterion(const SOL &sol, const SOL &_current, const SOL &_selected) override {
            firstLO = firstLO ||
                      (!(*neighborhoodExplorer).searchCriterion(sol, _current, _selected) && sol == _selected);
            return (*neighborhoodExplorer).searchCriterion(sol, _current, _selected);
        }

    protected:
        NeighborhoodExplorer<SOL> *neighborhoodExplorer;
        opt::memory::TabuList<SOL> tabuList;
        bool firstNeighbor;
        bool firstLO;
    };
}
#endif //MH_BUILDER_TABUNEIGHBORHOODEXPLORER_H
