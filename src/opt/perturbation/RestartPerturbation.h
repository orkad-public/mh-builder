/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RESTARTPERTURBATION_H
#define MH_BUILDER_RESTARTPERTURBATION_H
#include "core/Perturbation.h"
#include "core/Criterion.h"
#include "core/Algorithm.h"

namespace opt {
    namespace perturbation {

        /**
         * Class representing a restart perturbation
         * @tparam IN input type
         */
        template<typename IN>
        class RestartPerturbation : public core::Perturbation<IN> {
        public:

            using core::Perturbation<IN>::operator();

            /**
             * Constructor of the perturbation
             * @param _neighborhood the neighborhood operator
             * @param _strength number of neighborhood perturbation
             */
            explicit RestartPerturbation(core::Algorithm<IN> &_init) : init(_init) {}

            void operator()(IN &_in, core::Criterion<IN> &_criterion __attribute__((unused))) override {
                init(_in);
            }

        protected:
            core::Algorithm<IN> &init;
        };
    }
}

#endif //MH_BUILDER_RESTARTPERTURBATION_H
