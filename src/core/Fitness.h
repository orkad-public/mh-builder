/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FITNESS_H
#define MH_BUILDER_FITNESS_H

#include <vector>
#include <ostream>
#include <iostream>
#include <iterator>
#include <iomanip>
#include <sstream>


namespace core {
    /**
     * Class representing a Fitness
     * @tparam VALUE Type of Fitness (float, int, etc.)
     * @tparam SCAL_VALUE Type of Scalarised Fitness (float, int, etc.)
     */
    template<class VALUE=double, class SCAL_VALUE=VALUE>
    class Fitness {
    public:

        typedef VALUE VALUE_TYPE;

        /**
         * Create a new Fitness
         */
        Fitness() {
          valid = false;
          valid_scalar = false;
          objectives_.resize(1);
        }

        /**
         * If the fitness is valid (solution doesn't change since the last evaluation)
         * @return bool
         */
        [[nodiscard]] bool isValid() const {
          return valid;
        }

        /**
        * If the scalarised fitness is valid (solution doesn't change since the last evaluation)
        * @return bool
        */
        bool isValidScalar() const {
          return valid_scalar;
        }

        /**
        * Make fitness valid (call this method after evaluation)
        */
        void validate() {
          valid = true;
        }

        /**
        * Make fitness invalid (call this method after any solution modification)
        */
        void invalidate() {
          valid = false;
          valid_scalar = false;
        }

        /**
        * Make scalarised fitness invalid (call this method after any solution modification)
        */
        void invalidate_scalar() {
          valid_scalar = false;
        }

        /**
         * Get the objectives vector
         * @return objective vector
         */
        std::vector<VALUE> objectives() const {
          return objectives_;
        }

        /**
         * Get the objectives vector
         * @return objective vector
         */
        std::vector<VALUE>& objectives() {
          return objectives_;
        }

        

        /**
         * Set the objectives vector and make fitness valid
         * @param objv objectives vector
         */
        void objectives(const std::vector<VALUE> objv) {
          objectives_ = objv;
          valid = true;
        }

        /**
         * Get the constraint vector
         * @return constraint vector
         */
        std::vector<VALUE> constraints() {
          return constraints_;
        }

        /**
         * Set the constraint vector
         * @param cons constraint vector
         */
        void constraints(std::vector<VALUE> cons) {
          constraints_ = cons;
          valid = true;
        }

        /**
         * Set the scalarised fitness value and make scalar fitness valid
         * @param v
         */
        void scalar(SCAL_VALUE v) {
          scalar_ = v;
          valid_scalar = true;
        }

        /**
         * Get the scalarised fitness value
         * @return the scalarised fitness value
         */
        virtual SCAL_VALUE scalar() {
          if ( scalar_ != 0)
            return scalar_;
          else {
            double rate = 1.0 / objectives_.size();
            SCAL_VALUE r = 0.0;
            for (auto &a : objectives_) {
              r += (rate * a);
            }
            return r;
          }
        }

        /**
         * Get the scalarised fitness value
         * @return the scalarised fitness value
         */
        const SCAL_VALUE &scalar() const {
          return scalar_;
        }

        /**
         * Get the k^th value of the objectives vector
         * @param k the k^th value
         * @return the k^th value of the objectives vector
         */
        VALUE &operator[](unsigned int k) {
          return objectives_[k];
        }

        /**
         * Get the k^th value of the objectives vector
         * @param k the k^th value
         * @return the k^th value of the objectives vector
         */
        const VALUE &operator[](unsigned int k) const {
          return objectives_[k];
        }

        /**
         * Get the k^th value of the constraint vector
         * @param k the k^th value
         * @return the k^th value of the constraint vector
         */
        VALUE &getConstraint(unsigned int k) {
          return constraints_[k];
        }

        /**
         * Print the fitness, if it is not valid, print INVALID
         * @param _os the output stream
         */
        virtual void printOn(std::ostream &_os) const {
          if (valid)
            std::copy(objectives_.begin(), objectives_.end(), std::ostream_iterator<VALUE>(_os, " "));
          else
            _os << "INVALID ";
          if (valid_scalar)
            _os << "(" << scalar_ << ") ";
        }

        /**
         * Read the fitness from an input stream
         * @param is input stream
         */
        virtual void readFrom(std::istream &is) {
          char c;
          VALUE tmp;
          std::string s;
          objectives_.resize(0);
          valid_scalar = false;
          while (true) {
            c = is.peek();
            if (c == ' ') {
              is.get(c);
              c = is.peek();
            }
            if (c == 'I') {
              valid = false;
              is >> s;
              break;
            }
            if (c == '(') {
              is >> s;
              s = s.substr(1, s.size() - 2);
              std::istringstream ss(s);
              ss >> scalar_;
              valid_scalar = true;
              break;
            } else if (c == ' ') {
              break;
            }
            is >> tmp;
            objectives_.push_back(tmp);
            valid = true;
          }
        }

        /**
         * Comparator for scalarized fitness value
         * @param s the scalarized value to compare
         * @return 0 if equal, -1 if lower than s, 1 if better than s
         */
        int scalarComp(SCAL_VALUE s) const {
          if (scalar_ == s)
            return 0;
          if (scalar_ < s)
            return -1;
          else
            return 1;
        }

        /**
         * Test if it is a maximisation Fitness
         * @return True if it maximise
         */
        [[nodiscard]] virtual bool isMaximisationFitness() const = 0;

    protected:
        /*! The objectives vector */
        std::vector<VALUE> objectives_;

        /*! The constraints vector */
        std::vector<VALUE> constraints_;

        /*! The scalarized vector */
        SCAL_VALUE scalar_ = 0;

        /*! If fitness is valid */
        bool valid = false;

        /*! If scalarized fitness is valid */
        bool valid_scalar = false;
    };

    /**
     * Operator <<  to print a FitnessValue
     * @tparam VALUE Type of fitness
     * @param _os output stream
     * @param _o fitness
     * @return output stream _os
     */
    template<class VALUE>
    std::ostream &operator<<(std::ostream &_os, const Fitness<VALUE> &_o) {
      _o.printOn(_os);
      return _os;
    }
}

#endif //MH_BUILDER_FITNESS_H
