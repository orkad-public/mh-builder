/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ALGORITHM_H
#define MH_BUILDER_ALGORITHM_H

#include "core/criterion/DummyCriterion.h"
#include "core/Checkpoint.h"
#include "core/checkpoint/DummyCheckpoint.h"
#include <stdexcept>

namespace core {

    /**
       * Abstract class representing a controlled algorithm
       * Elements of control are the following:
       * 1) a stop criterion : an operator that allow to stop or pursuit the execution of an algorithm
       * 2) a checkpoint : an operator that can store values during the execution algorithm for further analyses
       * the developer that implements an algorithm is free to use or not these control element
       * @tparam IN Representation of solution
       */
    template<typename IN, typename OUT= void>
    class Algorithm {
    public:
        /**
         * the initialisation of an algorithm with no parameters
         * should be overloaded
         */
        virtual void init() {
            throw std::runtime_error("Algorithm.h : init() is not implemented !");
        }

        /**
         * the initialisation of an algorithm with a parameter (a solution for instance)
         * should be overloaded
         */
        virtual void init([[maybe_unused]] const IN &in) {
            throw std::runtime_error("Algorithm.h : init(const IN) is not implemented !");
        }

        /**
         * evaluation of the algorithm for the given solution
         * @param _in
         * @return
         */
        virtual OUT operator()(IN &_in) {
            if (!this->criterion->operator()()) return; // stop criterion test
            return operator()(_in, *criterion);
        }

        /**
         * evaluation of the algorithm for the given solution, which will not modified
         * @param _in
         * @return
         */
        virtual OUT operator()(const IN &_in) {
            if (!this->criterion->operator()()) return; // stop criterion test
            return operator()(_in, *criterion);
        }

        /**
         * evaluation of the algorithm for the given solution which is not modified (const)
         * @param _in
         * @param _criterion a specific criterion is given for this evaluation
         * @return
         */
        virtual OUT operator()([[maybe_unused]] const IN &_in, [[maybe_unused]] Criterion<IN> &_criterion) {
            throw std::runtime_error("Algorithm.h : operator(const IN,criterion) is not implemented !");
        }

        /**
         * evaluation of the algorithm for the given solution which is not modified (const)
         * @param _in
         * @param _criterion a specific criterion is given for this evaluation
         * @return
         */
        virtual OUT operator()([[maybe_unused]] IN &_in, [[maybe_unused]] Criterion<IN> &_criterion) {
            throw std::runtime_error("Algorithm.h : operator(IN, criterion) is not implemented !");
        }

        /**
         * update the stop criterion
         * @param _criterion the new stop criterion
         */
        void setCriterion(core::Criterion<IN> &_criterion) { criterion = &_criterion; }

        /**
         * provides the stop criterion
         * @return the stop criterion
         */
        core::Criterion<IN> &getCriterion() { return *criterion; }

        /**
         * update the checkpoint
         * @param _checkpoint the new checkpoint
         */
        void setCheckpoint(core::Checkpoint<IN> &_checkpoint) {
            checkpoint = &_checkpoint;
        }

        /**
         * provides the checkpoint
         * @return the checkpoint
         */
        core::Checkpoint<IN> &getCheckpoint() { return *checkpoint; }

    protected:
        /*! default stop criterion */
        Criterion<IN> *criterion = criterion::DummyCriterion<IN>::getDummyCriterion();
        /*! default checkpoint */
        Checkpoint<IN> *checkpoint = core::checkpoint::DummyCheckpoint<IN>::getDummyCheckpoint();
    };
}

#endif //MH_BUILDER_ALGORITHM_H
