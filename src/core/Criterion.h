/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CRITERION_H
#define MH_BUILDER_CRITERION_H

namespace core {
    /**
     * Abstract Class representing a stop criterion for an algorithm
     * Subclasses that extend this abstract class can use or not the IN parameter
     * @tparam IN Type of the solution for an algorithm which generally
     *            represents a solution type or an archive of solutions.
     *            used sometimes in concrete subclasses
     *  a classical use is :
     *  1. c.init()
     *  2. while (c()) {  // if the operator () returns true, the algorithm continues
     *  3.   do something()
     *  4.   c.update(); // update inner values of the stop criterion, may be unused
     *  5. }
     */
    template <typename IN>
    class Criterion {
    public:
        /**
         * Default destructor
         */
        virtual ~Criterion() = default;

        /**
         * Initialization of the criterion
         * according to the concrete criterion which extends this abstract method
         */
        virtual void init() = 0 ;

        /**
         * The operator which checks the criterion
         * when this operator returns true, the algorithm continues
         * @return the boolean
         */
        virtual bool operator()( ) = 0 ;

        /**
         * update inner values of the stop criterion, may be unused
         */
        virtual void update() = 0;
    };
}
#endif //MH_BUILDER_CRITERION_H
