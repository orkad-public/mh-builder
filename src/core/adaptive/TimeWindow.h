/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TIME_WINDOW_H
#define MH_BUILDER_TIME_WINDOW_H

#include "core/Algorithm.h"
#include <functional>
#include <iostream>
#include "opt/criterion/TimeCriterion.h"
#include "AdaptiveController.h"



namespace core::adaptive {
    /**
     * adaptive strategy which modifies the structure of the algorithm
     * at different times indicated by the ‘durations’ vector
     * @tparam IN the type of the solution
     * @tparam ALGO the type of the algorithm
     */
    template<typename IN, typename ALGO>
    class TimeWindow : public core::adaptive::AdaptiveController<IN,ALGO> {
    public:
        /**
         * Initialisation of the Time Adaptive Strategy
         * @param _algorithm the given algorithm
         * @param _functions the vector of functions which modify the structure of the algorithm
         * @param _durations the vector of elapsed times to adapt the algorithm
         */
        TimeWindow(ALGO &_algorithm, std::vector<std::function<void(ALGO &)>> &_functions,
                   std::vector<long long int> &_durations)
                   : core::adaptive::AdaptiveController<IN,ALGO>(_algorithm, _functions),durations(_durations){
            if (this->durations.size() != _functions.size()) {
                std::cerr << "durations vector and functions vector must be the same size !!";
                exit(0);
            }
        }

        // tag::strat[]
        /**
         * run the algorithm with the adaptive strategy
         * @param _in the initial solution
         */
        void operator()(IN &_in) override {
            for (unsigned long long int i = 0; i < this->durations.size(); i++) {
                opt::criterion::TimeCriterion<IN> timeCriterion(this->durations[i]);
                this->functions[i](this->algorithm);
                timeCriterion.init();
                this->algorithm(_in, timeCriterion);
            }
        }
        // end::strat[]
    protected:
        std::vector<long long int> &durations;
    };
}


#endif //MH_BUILDER_TIME_WINDOW_H
