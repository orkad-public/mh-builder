/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ADAPTIVE_CONTROLLER_H
#define MH_BUILDER_ADAPTIVE_CONTROLLER_H

#include "core/Algorithm.h"
#include <functional>
#include <iostream>
#include "opt/criterion/TimeCriterion.h"


namespace core::adaptive {
    /**
     * abstract class for all adaptive strategies of a given algorithm
     * @tparam IN the type of a solution
     * @tparam ALGO the type of the algorithm
     */
    template<typename IN, typename ALGO>
    class AdaptiveController : public core::Algorithm<IN> {
    public:
        /**
         * Initialisation
         * @param _algorithm the algorithm
         * @param _functions the vector of functions that modify the structure of the algorithm
         */
        AdaptiveController(ALGO &_algorithm, std::vector<std::function<void(ALGO &)>> &_functions) :
                algorithm(_algorithm), functions(_functions) {}
        /**
         * run the algorithm, change dynamically the structure of the algorithm thanks to
         * the vector of functions
         * @param _in the initial solution
         */
        virtual void operator()(IN &_in) override = 0 ;

    protected:
        ALGO &algorithm;
        std::vector<std::function<void(ALGO &)>> &functions;
    };
}

#endif //MH_BUILDER_ADAPTIVE_CONTROLLER_H
