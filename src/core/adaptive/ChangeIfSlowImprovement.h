/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CHANGE_IF_SLOW_IMPROVEMENT_H
#define MH_BUILDER_CHANGE_IF_SLOW_IMPROVEMENT_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include <functional>
#include <iostream>
#include "opt/criterion/TimeCriterion.h"
#include "AdaptiveController.h"



namespace core::adaptive {
    /**
     * adaptive strategy that regularly modifies the structure of the algorithm
     * if the solution does not improve by a given percentage
     * @tparam IN the type of the solution
     * @tparam ALGO the type of the algorithm
     */
    template<typename IN, typename ALGO>
    class ChangeIfSlowImprovement : public core::adaptive::AdaptiveController<IN,ALGO> {
    public:
        /**
         * Initialisation of the Eval Switch Adaptive Strategy
         * @param _algorithm the given algorithm
         * @param _functions the set of alternative functions which modify the structure of the algorithm
         * @param _duration the elapsed time where the switch test is performed in seconds
         * @param _improvement_percentage the percentage that determines if a switch is required
         * @param _max_iterations the maximum number of iterations
         * @param _eval       The eval function
         */
        ChangeIfSlowImprovement(ALGO &_algorithm, std::vector<std::function<void(ALGO &)>> &_functions,
                                      int _duration, double _improvement_percentage, int _max_iterations, core::Eval<IN> &_eval)
                : core::adaptive::AdaptiveController<IN,ALGO>(_algorithm, _functions),
                  duration(_duration),improvement_percentage(_improvement_percentage),
                  max_iterations(_max_iterations), eval(_eval) {}

        // tag::strat[]
        /**
         * run the algorithm with the adaptive strategy
         * @param _in the initial solution
         */
        void operator()(IN &_in) override {
            // init phase
            auto fitness_scalar_value = _in.fitness().scalar() ;
            int counter=1 ; int select_function=0 ;
            opt::criterion::TimeCriterion<IN> timeCriterion(duration*1000);
            timeCriterion.init();
            // perform algorithm with the specific adaptive strategy
            while (counter <= this->max_iterations ) {
                auto current_fitness_value = _in.fitness().scalar() ;
                if ((abs(fitness_scalar_value-current_fitness_value)*100)/fitness_scalar_value <
                    this->improvement_percentage) {
                    this->functions[select_function % this->functions.size()](this->algorithm);
                    select_function++ ;
                }
                fitness_scalar_value = current_fitness_value ;
                timeCriterion.init();
                this->algorithm(_in, timeCriterion);
                counter++ ;
            }
        }
        // end::strat[]
    protected:
        int duration ;
        double improvement_percentage ;
        int max_iterations ;
        core::Eval<IN> &eval ;
    };
}

#endif //MH_BUILDER_CHANGE_IF_SLOW_IMPROVEMENT_H
