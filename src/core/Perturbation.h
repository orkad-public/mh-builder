/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PERTURBATION_H
#define MH_BUILDER_PERTURBATION_H
#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "core/criterion/DummyCriterion.h"

namespace core {

    /**
     * Class representing a solution perturbation
     * @tparam IN solution
     */
    template <typename IN>
    class Perturbation: public core::Algorithm<IN> {
    public:

        /**
         * The operator of perturbation
         * @param in Solution
         */
        virtual void operator()(IN &_in) {
          operator()(_in, *this->criterion);
        }

        /**
         * The operator of perturbation with a criterion
         * @param sol solution
         * @param criterion temporary criterion
         */
        virtual void operator()(IN &_in, Criterion<IN> &criterion) = 0;

    };
}

#endif //MH_BUILDER_PERTURBATION_H
