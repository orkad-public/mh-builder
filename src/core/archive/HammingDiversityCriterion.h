/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/
#ifndef MH_BUILDER_HAMMINGDIVERSITYCRITERION_H
#define MH_BUILDER_HAMMINGDIVERSITYCRITERION_H

#include "core/archive/SolutionSpaceDiversityCriterion.h"
#include <vector>

namespace core::archive {
    /**
     * Class representing Hamming diversity criterion used in decision space.
     * @tparam VECTOR_SOLUTION : the type of the solution.
     * */
    template<typename VECTOR_SOLUTION>
    class HammingDiversityCriterion : public core::archive::SolutionSpaceDiversityCriterion<VECTOR_SOLUTION> {
    public:

        /**
         * Compute all the distances between the solutions of an archive.
         * Reset the global diversity and clear already stored distances
         * in 'distances' and 'max_distances'.**/
        virtual void compute_all_distances(const std::vector<VECTOR_SOLUTION> &archive);


    } ;

#include "HammingDiversityCriterion.tpp"
}

#endif // MH_BUILDER_HAMMINGDIVERSITYCRITERION_H