/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Amadeu Almeida Coco and additional contributors (see Authors)
****************************************************************************************/

#include "util/RNGHelper.h"

#include "core/archive/ObjectiveSpaceDiversityCriterion.h"
#include "core/archive/ObjectiveSpaceRegion.h"

#include <float.h>
#include <list>
#include <stdexcept>
#include <vector>

/**
  * This class implements the Adaptive Grid Archiving (AGA) algorithm.
  * Knowles and Corne introduced this method as a criterion for 
  * solutions diversity in objective space.
  * For further information, please read the paper "Properties of an 
  * adaptive archiving algorithm for storing nondominated vectors"
  * (https://ieeexplore.ieee.org/abstract/document/1197686). 
  * **/

template <typename VECTOR_SOLUTION>
/***
  * Constructor of the class AGADiversityCriterion.
  * @param __div number of dimensions of the objective space.
  * **/
AGADiversityCriterion<VECTOR_SOLUTION>::AGADiversityCriterion(uint16_t _div) {
    if(_div > 0 && (_div & (_div-1)) == 0){
        this->div = _div;
    } else{
        throw std::runtime_error("[Error] AGA: Division parameter must be a power of 2 greater than 0.");
        exit(0);
    }
}

/**
  *  Check if the variable _archive is set and if the convergance to CPORs is guaranteed.
*/
template <typename VECTOR_SOLUTION>
void AGADiversityCriterion<VECTOR_SOLUTION>::checkExceptions() {
    if(!this->_archive){
        throw std::runtime_error("Can't compute least contributive solution for AGA on nullptr archive.");
    }
    if(!this->_archive->size()){
        throw std::runtime_error("Can't compute least contributive solution for AGA on empty archive.");
    }
    if(!convergence_to_CPORs_guaranteed()){
        std::cerr << "[Warning] AGA: CPORs convergence not guaranteed, use different archive size or div parameter." << std::endl;
    }
}

/**
  * Check whether the archive size is greater than or equal to the 
  * minimum value required to ensure the convergence to a local 
  * optima which covers the largest objective range in objective space,
  * in each objective.
  * @return true if it is possible to guarantee the convergence of the archive.
*/
template <typename VECTOR_SOLUTION>
bool AGADiversityCriterion<VECTOR_SOLUTION>::convergence_to_CPORs_guaranteed(){
    num_objectives = this->_archive->operator[](0).fitness().objectives().size();
    return this->_archive->size() > (std::pow(this->div,num_objectives) - std::pow(div-1,num_objectives) + 2*num_objectives);
}

/**
  * Compute the extreme points of a Pareto front. 
  * @return the extreme points of the pareto front.
*/
template <typename VECTOR_SOLUTION>
std::vector<std::pair<double, double>> AGADiversityCriterion<VECTOR_SOLUTION>::computeExtremuns() {
    extremums.reserve(num_objectives);
    for(unsigned i = 0; i < num_objectives; ++i){
        auto min_score = DBL_MAX;
        auto max_score = DBL_MIN;
        for(unsigned j = 0; j < this->_archive->size(); ++j){
            auto obj_values = this->_archive->operator[](j).fitness().objectives()  ;
            double current_score = obj_values[i];
            if(current_score < min_score){
                min_score = current_score;
            }
            if(current_score > max_score){
                max_score = current_score;
            }
        }
        extremums.push_back(std::make_pair(min_score, max_score));
    }
    return extremums;
}

/**
  * Check if a Pareto point is an extremum. 
  * @param obj_values vector containing the value of each objective in a solution.
  * @return true if a Pareto point is an extremum.
*/
template <typename VECTOR_SOLUTION>
bool AGADiversityCriterion<VECTOR_SOLUTION>::checkExtremums(std::vector<double>& obj_value) {
    for(const std::pair<double,double>& extreme_point : extremums) {
        for (unsigned i = 0; i < num_objectives; i++) {
            if(extreme_point.first == obj_value[i] || 
                extreme_point.second == obj_value[i]) {
                return true;
            }
        }
    }
    return false;
}
        
/**
  * Compute the region where extreme points of a Pareto front. 
  * @param obj_values vector containing the value of each objective in a solution.
  * @return region of the objective search space in which a point of the Pareto-Front is located.
*/
template <typename VECTOR_SOLUTION>
std::vector<unsigned> AGADiversityCriterion<VECTOR_SOLUTION>::computeRegion(vector<double> obj_values) {
    std::vector<unsigned> point_region;
    for (unsigned j = 0; j < num_objectives; j++) {
        double current_objective = obj_values[j];
        double lower_bound = extremums[j].first;
        double upper_bound = extremums[j].second;
        double region_size = (upper_bound - lower_bound)/this->div;
        double obj_difference = (current_objective - lower_bound);
        auto objective_region = static_cast<unsigned>(floor(obj_difference/region_size));
        if(current_objective == upper_bound) {
            objective_region--;
        }
        point_region.push_back(objective_region);
    }
    return point_region;
}

/**
  * Checks if a region already exists in region_list. 
  * @param sol_index solution index
  * @param point_region region of the objective search space in which a point of the Pareto-Front is located.
  * @param region_list list of regions in the objective space.
  * @return true if a region already exists in region_list. 
*/
template <typename VECTOR_SOLUTION>
bool AGADiversityCriterion<VECTOR_SOLUTION>::checkRegionExistence(unsigned sol_index, vector<unsigned> point_region, 
                                                                  std::list<OSR>& region_list) {
    auto it = region_list.begin();
    bool region_exists = false;
    while (it != region_list.end()) {
        vector<unsigned> iterator_region = it->region();
        for (unsigned index = 0; index < num_objectives; index++) {
            if (iterator_region[index] != point_region[index]) {
                break;
            }
            if (index + 1 == num_objectives) {
                region_exists = true;
                it->add(sol_index);
            }
        }
        if (region_exists) {
            it = region_list.end();
        } else {
            ++it;
        }
    }
    return region_exists;
}

/**
  * Checks if the new solution is on the most populated region of the archive. 
  * @param max_region_id most populated region id.
  * @param sol non-dominated solution that is a candidate to enter the archive.    
  * @return true if the new solution is in the most populated region.  
*/
template <typename VECTOR_SOLUTION>
bool AGADiversityCriterion<VECTOR_SOLUTION>::checkMostPopulatedRegion(const vector<unsigned>& max_region_id, const VECTOR_SOLUTION& sol) {
    bool same_region = true;
    auto curr_obj_values = sol.fitness().objectives();
    std::vector<unsigned> curr_point_region = computeRegion(curr_obj_values);            
    for(size_t i = 0; i < num_objectives; i++) {
        if(max_region_id[i] != curr_point_region[i]) {   
            same_region = false;
            break;
        }
    }
    return same_region;
}      

/**
 * Randomly remove a solution from the archive belonging to the most 
 * populated region of the objective space.
 * @param archive archive containing a set of non dominated solutions.
 * @param sol non-dominated solution that is a candidate to enter the archive.   
 * @return the solution that will be removed from the archive.
 */
template <typename VECTOR_SOLUTION>
long long AGADiversityCriterion<VECTOR_SOLUTION>::least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol) {
    checkExceptions();
    num_objectives = this->_archive->operator[](0).fitness().objectives().size();
    unsigned int archive_size = archive.size();
    computeExtremuns();
    /**
      * Use a list of ObjectiveSpaceRegion to divide objective space 
      * and to sum up solution population in a specific area.
      * Each position of the list has a different ID and holds a vector 
      * of indexes for all the solutions in the said area.
    * **/
    std::list<OSR> region_list;
    for (unsigned i = 0; i < archive_size; i++) {
        std::vector<double> obj_values = this->_archive->operator[](i).fitness().objectives();
        if(!checkExtremums(obj_values)) {
            std::vector<unsigned> point_region = computeRegion(obj_values);
            bool region_exists = checkRegionExistence(i, point_region,region_list);
            if(!region_exists) {
                OSR pareto_point({i},point_region);
                region_list.push_back(pareto_point);
            }
        }  
    }
    auto it = region_list.begin();
    unsigned max_region_size = 0;
    std::vector<unsigned> max_region_indexes;
    std::vector<unsigned> max_region_id;
    while( it != region_list.end()) {
        unsigned region_size = it->size();
        if (region_size > max_region_size) {
            max_region_size = region_size;
            max_region_indexes = it->value();
            max_region_id = it->region();
        }
        ++it;
    }
    bool same_region = checkMostPopulatedRegion(max_region_id, sol);   
    if (!same_region) {
        int random_key = util::RNGHelper::get()->random(max_region_size);
        return max_region_indexes[random_key];
    }
    return -1;
}