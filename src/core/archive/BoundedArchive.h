/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BOUNDEDARCHIVE_H
#define MH_BUILDER_BOUNDEDARCHIVE_H

#include "core/Archive.h"
#include "util/RNGHelper.h"
#include <functional>

using namespace std;


namespace core::archive {
    /**
     * Class represented a decorator for a bounded archive
     * @tparam SOLUTION
     */
    template<typename SOLUTION, typename ARCHIVE>
    class BoundedArchive : public core::Archive<SOLUTION> {
    public:
        typedef typename ARCHIVE::iterator iterator;
        typedef typename ARCHIVE::const_iterator const_iterator;

        BoundedArchive() : inner_archive(new ARCHIVE()) {
            this->maxSize = 0;
            this->choose_strategy = [](const core::Archive<SOLUTION> &x) {
                return util::RNGHelper::get()->uniform(x.size());
            };
        }

        /**
         * Constructor of a bounded archive
         * @param _archive the archive
         * @param _maxSize the maximum number of solution in the archive
         * @param _choose_strategy the method for choose solution (by default random solution)
         */
        BoundedArchive(ARCHIVE *_archive,
                       unsigned long long int _maxSize,
                       std::function<unsigned long long int(
                               const core::Archive<SOLUTION> &_archive)> _choose_strategy = [](
                               const core::Archive<SOLUTION> &x) {
                           return util::RNGHelper::get()->uniform(x.size());
                       }) :
                inner_archive(_archive) {
            this->maxSize = _maxSize;
            this->choose_strategy = _choose_strategy;
        }
        /**
         * copy-constructor of a bounded archive
         */
        BoundedArchive(const BoundedArchive &other)  {
            this->maxSize = other.maxSize;
            this->choose_strategy=other.choose_strategy;
            this->inner_archive=new ARCHIVE() ;
            for (auto &sol : other.inner_archive->get_solutions())
                this->inner_archive->push_back(sol);
        }
        /**
         * Updates archive with a solution _solution
         * @param _solution the solution to add
         * @return true if the _solution is added to the archive
         */
        bool operator()(const SOLUTION &_solution) override {
            bool hasChange = this->inner_archive->operator()(_solution);
            if (hasChange && this->inner_archive->size() > this->maxSize)
                resize();
            return hasChange;
        };

        /**
         * Updates archive with a archive _archive
         * @param _archive the archive to add
         * @return true if at least one solution of archive is added to the current archive
         */
        bool operator()(const core::Archive<SOLUTION> &_archive) override {
            if (this->maxSize == 0) {
                this->maxSize = _archive.getMaxSize();
                this->choose_strategy = _archive.getChooseStrategy();
            }

            bool hasChange = this->inner_archive->operator()(_archive);
            if (hasChange && this->inner_archive->size() > this->maxSize) {
                resize();
            }
            return hasChange;
        };

        /**
         * Update archive itself (in case of a solution changes)
         * @return true is the archive changes
         */
        bool update() override {
            return this->inner_archive->update();
        }

        /**
         * Resize archive if his size is superior of maxSize
         */
        virtual void resize() {
            if (this->inner_archive->size() > this->maxSize) {
                while (this->inner_archive->size() > this->maxSize) {
                    this->inner_archive->erase(this->inner_archive->begin() + this->choose_strategy(*this->inner_archive));
                }
            }
        }

        /**
         * Erase the element at pos
         * @param pos
         * @return iterator
         */
        iterator erase(iterator pos) override {
            return this->inner_archive->erase(pos);
        }

        /**
        * Get k^th solution
        * @param k
        * @return the k^th solution
        */
        SOLUTION &operator[](unsigned int k) override {
            return this->inner_archive->operator[](k);
        }

        /**
         * Get k^th solution
         * @param k
         * @return the k^th solution
         */
        SOLUTION operator[](unsigned int k) const override {
            return this->inner_archive->operator[](k);
        }

        /**
         * Get size of the archive
         * @return the vector size
         */
        [[nodiscard]] unsigned long long int size() const override {
            return this->inner_archive->size();
        }

        /**
         * Add a solution in the archive without update
         * @param i the element to add
         */
        void push_back(const SOLUTION &i) override {
            this->inner_archive->push_back(i);
        }

        /**
         * Remove the last element in the archive
         */
        void pop_back() override {
            this->inner_archive->pop_back();
        }

        /**
         * Clear the archive
         */
        void clear() override {
            this->inner_archive->clear();
        }

        /**
         * Begin iterator of the archive
         */
        const_iterator begin() const override {
            return this->inner_archive->begin();
        }

        /**
         * End iterator of the archive
         */
        const_iterator end() const override {
            return this->inner_archive->end();
        }

        /**
         * Begin iterator of the archive
         */
        iterator begin() override {
            return this->inner_archive->begin();
        }

        /**
         * End iterator of the archive
         */
        iterator end() override {
            return this->inner_archive->end();
        }

        /**
         * Sort the archive
         */
        void sort() override {
            this->inner_archive->sort();
        }
        /**
          * get the archive content
          * @return
          */
        std::vector<SOLUTION>& get_solutions() override {
            return this->inner_archive->get_solutions();
        }


    protected:
        ARCHIVE *inner_archive;
    };
}


#endif //MH_BUILDER_BOUNDEDARCHIVE_H
