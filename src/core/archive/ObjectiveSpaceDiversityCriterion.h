/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Amadeu Almeida Coco and additional contributors (see Authors)
****************************************************************************************/
#ifndef __OBJECTIVE_SPACE_DIVERSITY_CRITERION_H__
#define __OBJECTIVE_SPACE_DIVERSITY_CRITERION_H__

#include <vector>
#include "core/archive/ArchiveDiversityCriterion.h"

namespace core::archive {
    template <typename VECTOR_SOLUTION>
class ObjectiveSpaceDiversityCriterion : public core::archive::ArchiveDiversityCriterion<VECTOR_SOLUTION> {
        
        /**
         * Return the index of a least contributive solution to the global diversity.
         * If provided solution can not improve global diversity, -1 is returned.
         * Otherwise the least contributive solution index is returned and 
         * 'distances' and 'max_distances' are updated. **/
        virtual long long least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol) = 0;
    };
} // namespace core

#endif