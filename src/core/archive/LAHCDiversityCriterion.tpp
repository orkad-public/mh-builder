/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Amadeu Almeida Coco and additional contributors (see Authors)
****************************************************************************************/

#include "core/archive/ObjectiveSpaceDiversityCriterion.h"

#include <float.h>
#include <list>
#include <stdexcept>
#include <vector>

/**
  *  Check if the variable _archive is set and if the convergance to CPORs is guaranteed.
  */
template <typename VECTOR_SOLUTION>
void LAHCDiversityCriterion<VECTOR_SOLUTION>::checkExceptions() {
    if(!this->_archive){
        throw std::runtime_error("Can't compute least contributive solution for LAHC on nullptr archive.");
    }
    if(!this->_archive->size()){
        throw std::runtime_error("Can't compute least contributive solution for LAHC on empty archive.");
    }
}

/**
  * Build the list containing the current set of points in the pareto front. 
  * @param candidate_solution non-dominated solution that is a candidate to enter the file.
  */
template <typename VECTOR_SOLUTION>
std::list <std::vector<double> > LAHCDiversityCriterion<VECTOR_SOLUTION>::buildList(std::vector<double>& candidate_solution) {
    std::list <std::vector<double> > pareto_front_points;
    for (unsigned i = 0; i < this->_archive->size(); i++) {
        auto obj_values = this->_archive->operator[](i).fitness().objectives();
        pareto_front_points.push_back(obj_values);
    }
    pareto_front_points.push_back(candidate_solution);
    return pareto_front_points;
}


/**
  * Compute the individual contribution of an archive solution to the hypervolume.
  * @param archive_size size of the archive.
  * @param points set of Pareto front solutions. It contains all the solutions 
  * from the archive plus the one found by the heuristic.
  * @return the individual contribution of a solution to the hypervolume.
*/
template <typename VECTOR_SOLUTION>
double LAHCDiversityCriterion<VECTOR_SOLUTION>::computeContribution(unsigned archive_size, 
                                    std::list <std::vector<double> >& points) {
    double point_contribution = 0.0;
    vector<double> max_values = computeMaxValues(points);
    while (points.size() > archive_size) {
        vector<double> solution = points.back();
        points.pop_back();  
        vector< vector<double> > spawns;
        vector<double> opposite_corner = computeOppositeCorner(solution, points);
        double hypervolume = 1.0;
        for(unsigned j = 0; j < num_objectives; j++) {
            hypervolume *= std::fabs(opposite_corner[j] - solution[j]);
            vector<double> spawn = solution;
            spawn[j] = opposite_corner[j]; 
            spawns.push_back(spawn);
        }
        point_contribution += hypervolume;
        for(unsigned j = 0; j < spawns.size(); j++) {
            bool dominated = checkDominance(spawns[j], points, max_values);
            if(!dominated) {
                points.push_back(spawns[j]);
            }
        }
    }
    return point_contribution;
}

/**
  * Compute the maximum value for each objective in the current Pareto front. 
  * @param points list containing all points on the current Pareto
  * front (except for the popped one).
  * @return a vector containing the maximum value of each objective.
  */
template <typename VECTOR_SOLUTION>
std::vector<double> LAHCDiversityCriterion<VECTOR_SOLUTION>::computeMaxValues(std::list<std::vector<double> >& points) {
    vector<double> max_values;
    for(unsigned i = 0; i < num_objectives; i++) {
        auto max_value = DBL_MIN;
        for(const auto & score : points) {
            if(score[i] > max_value){
                max_value = score[i];
            }
        }  
        max_values.push_back(max_value);
    }
    return max_values;
}

/**
  * Compute the opposite corner of a hypercuboid corresponding to the 
  * hypervolume exclusively dominated by a solution.
  * @param solution Solution whose individual contribution to the 
  * hypervolume is currently calculated.
  * @param points list containing all points on the current Pareto
  * front (except for the popped one).
  * @return a vector with the points of the opposite corner.
  */
template <typename VECTOR_SOLUTION>
std::vector<double> LAHCDiversityCriterion<VECTOR_SOLUTION>::computeOppositeCorner(std::vector<double>& solution,
                                        std::list<std::vector<double> >& points) {  
    std::vector<double> opposite_corner;
    for(unsigned i = 0; i < num_objectives; i++) {
        auto min_max_value = DBL_MAX;
        for(const auto & score : points) {
            if(score[i] < min_max_value && score[i] > solution[i]){
                min_max_value = score[i];
            }
        }  
        opposite_corner.push_back(min_max_value);
    }
    return opposite_corner;
    }

/**
  * Checks whether a point spawned by the one popped from the list 
  * is not dominated by any other point in the current Pareto front.
  * @param spawn new point in the objective space, spawned by the last 
  * point popped from the list.
  * @param points list containing all points on the current Pareto
  * front (except for the popped one).
  * @param max_values vector containing the maximum value of each objective.
  * @return True if the spawned point is not dominated by any other. 
  */
template <typename VECTOR_SOLUTION>
bool LAHCDiversityCriterion<VECTOR_SOLUTION>::checkDominance(std::vector<double>& spawn, std::list<std::vector<double> >& points,
                        std::vector<double>& max_values) {
    for(unsigned i = 0; i < num_objectives; i++) {
        if(spawn[i] >= max_values[i]) {
            return true;
        }
    }
    for(const auto & value : points) {
        bool dominated_point = true;
        for(unsigned i = 0; i < num_objectives; i++) {
            if(spawn[i] < value[i]){
                dominated_point = false;
                break;
            }
        }
        if (dominated_point == true) {
            return true;
        }
    }
    return false;
}
        
template <typename VECTOR_SOLUTION>
long long LAHCDiversityCriterion<VECTOR_SOLUTION>::least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol) {
    checkExceptions();
    num_objectives = this->_archive->operator[](0).fitness().objectives().size();
    std::vector<double> candidate_solution = sol.fitness().objectives();
    auto archive_size = static_cast<unsigned>(archive.size());
    std::list <std::vector<double> > pareto_front_points = buildList(candidate_solution);
    vector<double> sol_hypervolume_contributions;
    for (unsigned i = 0; i < archive_size + 1 ; i++) {
        auto points = pareto_front_points;
        vector<double> evaluated_solution = *(std::next(points.begin(), i));
        points.erase(std::next(points.begin(), i));
        points.push_back(evaluated_solution);
        double hypervolume_contribution = computeContribution(archive_size, points);
        sol_hypervolume_contributions.push_back(hypervolume_contribution);
    }
    auto least_contribution = DBL_MAX;
    unsigned least_contribution_index = archive_size + 1;
    for (unsigned i = 0; i < sol_hypervolume_contributions.size(); i++) {
        if(least_contribution > sol_hypervolume_contributions[i]) {
           least_contribution = sol_hypervolume_contributions[i];
            least_contribution_index = i; 
        }
    }
    if(least_contribution_index != archive_size + 1) {
        return least_contribution_index;
    } else {
        return -1;
    }
}