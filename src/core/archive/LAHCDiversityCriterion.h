/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Amadeu Almeida Coco and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_LAHC_DIVERSITY_CRITERION_H
#define MH_BUILDER_LAHC_DIVERSITY_CRITERION_H


#include "core/archive/ObjectiveSpaceDiversityCriterion.h"

#include <float.h>
#include <list>
#include <stdexcept>
#include <vector>

namespace core::archive {
     /**
     * This class implements the Lebesgue Archiving Hillclimber (LAHC) algorithm. 
     * Knowles, Corne and Fleischer proposed this approach as a criterion for 
     * solutions diversity in the objective space. For further information, please 
     * refer to the paper "Bounded Archiving using the Lebesgue 
     * Measure" (https://ieeexplore.ieee.org/document/1299401).
     * */
    template <typename VECTOR_SOLUTION>
    class LAHCDiversityCriterion : public core::archive::ObjectiveSpaceDiversityCriterion<VECTOR_SOLUTION>{
        private:
        /**
         * The number of objectives in a MO problem.
        */
        unsigned num_objectives;  

        /**
         *  Check if the variable _archive is set and if the convergance to CPORs is guaranteed.
        */
        void checkExceptions();
        /**
         * Build the list containing the current set of points in the pareto front. 
         * @param candidate_solution non-dominated solution that is a candidate to enter the file.
        */
        std::list <std::vector<double> > buildList(std::vector<double>& candidate_solution);


        /**
         * Compute the individual contribution of an archive solution to the hypervolume.
         * @param archive_size size of the archive.
         * @param points set of Pareto front solutions. It contains all the solutions 
         * from the archive plus the one found by the heuristic.
         * @return the individual contribution of a solution to the hypervolume.
        */
        double computeContribution(unsigned archive_size, 
                                     std::list <std::vector<double> >& points);

        /**
         * Compute the maximum value for each objective in the current Pareto front. 
         * @param points list containing all points on the current Pareto
         * front (except for the popped one).
         * @return a vector containing the maximum value of each objective.
        */
        std::vector<double> computeMaxValues(std::list<std::vector<double> >& points);

        /**
         * Compute the opposite corner of a hypercuboid corresponding to the 
         * hypervolume exclusively dominated by a solution.
         * @param solution Solution whose individual contribution to the 
         * hypervolume is currently calculated.
         * @param points list containing all points on the current Pareto
         * front (except for the popped one).
         * @return a vector with the points of the opposite corner.
        */
        std::vector<double> computeOppositeCorner(std::vector<double>& solution,
                                                  std::list<std::vector<double> >& points);
        /**
         * Checks whether a point spawned by the one popped from the list 
         * is not dominated by any other point in the current Pareto front.
         * @param spawn new point in the objective space, spawned by the last 
         * point popped from the list.
         * @param points list containing all points on the current Pareto
         * front (except for the popped one).
         * @param max_values vector containing the maximum value of each objective.
         * @return True if the spawned point is not dominated by any other. 
        */
        bool checkDominance(std::vector<double>& spawn, std::list<std::vector<double> >& points,
                            std::vector<double>& max_values);

        public:

        explicit LAHCDiversityCriterion() = default;
        
        virtual long long least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol);
       
    };
#include "LAHCDiversityCriterion.tpp"   
}
#endif // MH_BUILDER_LAHC_DIVERSITY_CRITERION_H
