/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Amadeu Almeida Coco and additional contributors (see Authors)
****************************************************************************************/
#ifndef MH_BUILDER_AGA_DIVERSITY_CRITERION_H
#define MH_BUILDER_AGA_DIVERSITY_CRITERION_H
#include <list>
#include <vector>

#include "core/archive/ObjectiveSpaceDiversityCriterion.h"
#include "core/archive/ObjectiveSpaceRegion.h"

namespace core::archive {
    /**
     * This class implements the Adaptive Grid Archiving (AGA) algorithm.
     * Knowles and Corne introduced this method as a criterion for 
     * solutions diversity in objective space.
     * For further information, please read the paper "Properties of an 
     * adaptive archiving algorithm for storing nondominated vectors"
     * (https://ieeexplore.ieee.org/abstract/document/1197686). 
     * */
    using OSR = typename core::archive::ObjectiveSpaceRegion<unsigned int, unsigned int>;
    template <typename VECTOR_SOLUTION>
    class AGADiversityCriterion : public core::archive::ObjectiveSpaceDiversityCriterion<VECTOR_SOLUTION>{
        private:
         /**
         * Division parameter for objective space.
        * */
        uint64_t div;
        /**
         * The number of objectives in a MO problem.
        */
        unsigned num_objectives;
        /**
         * The extreme points of the pareto front.
        */
        std::vector<std::pair<double, double>> extremums;

        /**
         *  Check whether the archive size is greater than or equal to the 
         *  minimum value required to ensure the convergence to a local 
         * optima which covers the largest objective range in objective space,
         * in each objective.
        */
        bool convergence_to_CPORs_guaranteed();

        /**
         *  Check if the variable _archive is set and if the convergance to CPORs is guaranteed.
        */
        void checkExceptions();

        /**
         * Compute the extreme points of a Pareto front. 
         * @return the extreme points of the pareto front.
         */
        std::vector<std::pair<double, double>> computeExtremuns();

        /**
         * Check if a Pareto point is an extremum. 
         * @param obj_values vector containing the value of each objective in a solution.
         * @return true if a Pareto point is an extremum.
         */
        bool checkExtremums(std::vector<double>& obj_value);
        
        /**
         * Compute the region where extreme points of a Pareto front. 
         * @param obj_values vector containing the value of each objective in a solution.
         * @return region of the objective search space in which a point of the Pareto-Front is located.
         */
        std::vector<unsigned> computeRegion(vector<double> obj_values);

        /**
         * Checks if a region already exists in region_list. 
         * @param sol_index solution index
         * @param point_region region of the objective search space in which a point of the Pareto-Front is located.
         * @param region_list list of regions in the objective space.
         * @return true if a region already exists in region_list. 
         */
        bool checkRegionExistence(unsigned sol_index, vector<unsigned> point_region, 
                                  std::list<OSR>& region_list);

        /**
         * Checks if the new solution is on the most populated region of the archive. 
         * @param max_region_id most populated region id.
         * @param sol non-dominated solution that is a candidate to enter the file.   
         * @return true if the new solution is in the most populated region.  
         */
        bool checkMostPopulatedRegion(const vector<unsigned>& max_region_id, const VECTOR_SOLUTION& sol);

        public:
        /**
          * Constructor of the class AGADiversityCriterion.
          * @param __div number of dimensions of the objective space.
          * **/
        explicit AGADiversityCriterion(uint16_t _div);
        /**
          * Randomly remove a solution from the archive belonging to the most 
          * populated region of the objective space.
          * @param archive archive containing a set of non dominated solutions.
          * @param sol non-dominated solution that is a candidate to enter the archive.   
        */
        virtual long long least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol);
    };
#include "AGADiversityCriterion.tpp"    
}
#endif // MH_BUILDER_AGA_DIVERSITY_CRITERION_H
