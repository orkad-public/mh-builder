/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/


#include "core/archive/HammingDiversityCriterion.h"
    
/**
  * Compute all the distances between the solutions of an archive.
  * Reset the global diversity and clear already stored distances
  * in 'distances' and 'max_distances'.**/

template<typename VECTOR_SOLUTION>
void HammingDiversityCriterion<VECTOR_SOLUTION>::compute_all_distances(const std::vector<VECTOR_SOLUTION>& archive){               
    for(unsigned i = 0; i < archive.size(); ++i){
        std::vector<double> v;
        v.reserve(archive.size());
        double max = 0.0;
        for(unsigned j = 0; j < archive.size(); ++j){
            double d = 0.0;
            if(i != j) //it aint much, but prevent N comparisons.
                d = this->hamming_distance(archive[i], archive[j]) ;
            this->global_diversity += d;
            v.push_back(d);
            if(max < d)
                max = d;
        }
        this->distances.push_back(v);
        this->max_distances.push_back(max);
    }
}


