/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/

#include "core/archive/SolutionSpaceDiversityCriterion.h"
        
template <typename VECTOR_SOLUTION>
long long SolutionSpaceDiversityCriterion<VECTOR_SOLUTION>::least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol) {
    this->global_diversity = 0;
    this->distances.clear();
    this->max_distances.clear();
    this->compute_all_distances(archive);
    long long least = -1;
    std::vector<double> sol_distances; // distances between sol and archive solutions.
    sol_distances.reserve(this->max_distances.size());
    double max_sol = 0.0; // max distance for sol.

    // Compute distances between archive and sol.
    for(unsigned i = 0; i < archive.size(); ++i) {
        double d = this->hamming_distance(archive[i],sol);
        max_sol += d;
        sol_distances.push_back(d);
    }

    // Check if sol can contribute better than a solution in archive.
    double greatest_difference = 0.0;
    for(unsigned i = 0; i < this->max_distances.size(); ++i) {
        // If sol can replace a solution
        if (this->max_distances[i] < max_sol - sol_distances[i]) {
        // Compute the contribution difference
        double difference = max_sol - sol_distances[i] - this->max_distances[i];
            if (difference > greatest_difference) {
            greatest_difference = difference;
                least = i;
            }
        }
    }
    if (least >= 0) {
        //store removable solution distances
        std::vector<double> removed_sol = this->distances[least];
        double removed_max = this->max_distances[least];
        this->global_diversity += max_sol - removed_max;

        // Replace least solution by sol in 'distances' and 'max_distances'.
        // Update max_distances before, to use distances between the removed solution and sol.
        for (unsigned i = 0; i < archive.size(); ++i) {
            //replace last contributive by the solution.
            if (i == least) {
                this->max_distances[i] = max_sol - sol_distances[least];
                this->distances[i][i] = 0;
            } else {
                this->max_distances[i] += sol_distances[i] - removed_sol[i];
                this->distances[i][least] = sol_distances[i];
                this->distances[least][i] = sol_distances[i];
            }
        }
    }
    return least;
}

template <typename VECTOR_SOLUTION>
double SolutionSpaceDiversityCriterion<VECTOR_SOLUTION>::get_global_diversity() const{
    return global_diversity;
}

/**
        * Return the hamming distance between two vector solutions.
        * The hamming distance is the number of positions at which
        * the corresponding symbols are different.
        * The complexity time is linear O(n).
        **/
template<typename VECTOR_SOLUTION>
double SolutionSpaceDiversityCriterion<VECTOR_SOLUTION>::hamming_distance(const VECTOR_SOLUTION s1, const VECTOR_SOLUTION s2) const {
    double distance = 0;
    for(unsigned i = 0; i < s2.size(); ++i){
        if(!(s1[i] == s2[i])) // Use !(a == b) instead of (a != b) to prevent heavy refractoring.
            distance++;
    }
    return distance;
}

/**
 * Return the jaccard distance between two vector solutions.
 **/
template<typename VECTOR_SOLUTION>
double SolutionSpaceDiversityCriterion<VECTOR_SOLUTION>::jaccard_distance(const VECTOR_SOLUTION sol1,
                                                                    const VECTOR_SOLUTION sol2) const {
    double similarity = 0;

    for (double i = 0; i < sol2.size(); ++i) {
        if (sol2[i] == sol1[i])
            similarity++;
    }
    return 1 - (similarity / sol2.size() - similarity);
}