/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/

#ifndef __ARCHIVE_DIVERSITY_CRITERION__
#define __ARCHIVE_DIVERSITY_CRITERION__

#include "core/Archive.h"

#include <vector>

namespace core::archive{
    /**
     * Base class for diversity criteria used for (mainly) bounded archives.
     * All diversity criterion must inherit this class.
     * */
    template <typename VECTOR_SOLUTION>
    class ArchiveDiversityCriterion{
        public:
        
        /**
         * Return the index of a least contributive solution to the global diversity.
         * If provided solution can not improve global diversity, -1 is returned.
         * Otherwise the least contributive solution index is returned and 
         * 'distances' and 'max_distances' are updated. **/
        virtual long long least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol) = 0;
        
        void init(core::Archive<VECTOR_SOLUTION>* a){
            _archive = a;
        }

        protected:

        core::Archive<VECTOR_SOLUTION>* _archive;
    };
}
#endif