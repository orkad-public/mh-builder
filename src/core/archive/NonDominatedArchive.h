/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NONDOMINATEDARCHIVE_H
#define MH_BUILDER_NONDOMINATEDARCHIVE_H

#include "core/Archive.h"


namespace core::archive {
    /**
     * Class represented an archive of non-dominated archive
     * @tparam SOLUTION
     */
    template<typename SOLUTION>
    class NonDominatedArchive : public Archive<SOLUTION> {
    public:
        /**
         * Updates archive with a solution _solution
         * @param _solution the solution to add
         * @return true if the _solution is added to the archive
         */
        virtual bool operator()(const SOLUTION &_solution) override {
            return update(_solution);
        };

        /**
         * Updates archive with a archive _archive
         * @param _archive the archive to add
         * @return true if at least one solution of archive is added to the current archive
         */
        virtual bool operator()(const Archive<SOLUTION> &_archive) override {
            bool oneSolutionInserted = false;
            for (unsigned long long int i = 0; i < _archive.size(); ++i)
                oneSolutionInserted = update(_archive[i]) || oneSolutionInserted;
            return oneSolutionInserted;
        };

        /**
         * Update archive itself (in case a solution change)
         * @return true is the archive changes
         */
        bool update() override {
            Archive<SOLUTION>::update();
            bool haveChange = false;
            for (unsigned long long int i = 0; i < this->archive.size(); ++i) {
                bool dominated = false;
                for (unsigned long long int j = i + 1; j < this->archive.size() && !dominated; ++j) {
                    // Remove dominated solution
                    if (this->archive[j] < this->archive[i]) {
                        this->archive.erase(this->archive.begin() + j);
                        j--; // Update iterator
                        haveChange = true;
                    } else if (this->archive[j] >= this->archive[i]) { // if sol is dominated break loop
                        dominated = true;
                    }
                }
                if (dominated) {
                    this->archive.erase(this->archive.begin() + i);
                    i--; // Update iterator
                    haveChange = true;
                }
            }
            return haveChange;
        }

    protected:
        /**
         * Updates the archive with a solution
         * @param _solution
         * @return true if the solution is added to archive
         */
        bool update(const SOLUTION &_solution) {
            bool dominated = false;
            //bool neutral = false;
            for (unsigned long long int i = 0; i < this->archive.size(); ++i) {
                // Remove dominated solution
                if (this->archive[i] < _solution) {
                    this->archive.erase(this->archive.begin() + i);
                    i--; // Update iterator
                } else if (this->archive[i] >= _solution) { // if sol is dominated break loop
                    dominated = true;
                    break;
                }
            }

            if (!dominated) {
                this->archive.push_back(_solution);
            }
            return !dominated;
        }
    };
}

#endif //MH_BUILDER_NONDOMINATEDARCHIVE_H
