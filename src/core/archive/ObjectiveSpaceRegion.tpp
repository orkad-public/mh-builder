/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/
#include "core/archive/ObjectiveSpaceRegion.h"

/***
 * Constructor of a ObjectiveSpaceRegion. 
 * @param ppi points of a pareto front that belong to a given region.
 * @param id identifier of the region where a pareto point is located.
 * **/
template<typename PPI, typename REGION>
ObjectiveSpaceRegion<PPI,REGION>::ObjectiveSpaceRegion(const std::vector<PPI>& ppi,  const std::vector<REGION>& id) : 
pareto_points_indexes(ppi), region_id(id)
{
}

/**
  * Add a solution from the archive to its region in the objective space.
  * @param index solution index in the archive.
  */
template<typename PPI, typename REGION>
void ObjectiveSpaceRegion<PPI,REGION>::add(unsigned index) {
    pareto_points_indexes.push_back(index);
}

/**
  * Get the identifier of a region in the objective space.
  */
template<typename PPI, typename REGION>
std::vector<REGION>& ObjectiveSpaceRegion<PPI,REGION>::region() {
    return region_id;
}

/**
  * Get the identifier of a region in the objective space.
  */
template<typename PPI, typename REGION>
const std::vector<REGION>& ObjectiveSpaceRegion<PPI,REGION>::region() const {
    return region_id;
}

/**
  * Get a vector containing the indexes of solutions located in a specific region.
  */
template<typename PPI, typename REGION>
std::vector<PPI>& ObjectiveSpaceRegion<PPI,REGION>::value() {
    return pareto_points_indexes;
}
/**
  * Get a vector containing the indexes of solutions located in a specific region.
  */
template<typename PPI, typename REGION>
const std::vector<PPI>& ObjectiveSpaceRegion<PPI,REGION>::value() const {
    return pareto_points_indexes;
}

template<typename PPI, typename REGION>
unsigned ObjectiveSpaceRegion<PPI,REGION>::size() const {
    return pareto_points_indexes.size();
}
