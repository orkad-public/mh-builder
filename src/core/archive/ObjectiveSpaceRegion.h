/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_OBJECTIVE_SPACE_REGION_H
#define MH_BUILDER_OBJECTIVE_SPACE_REGION_H
#include <vector>
#include <cmath>
#include <optional>
#include <stdexcept>
#include <string>


namespace core::archive {
    /**
     * ObjectiveSpaceRegion is a data structure that stores the indices of 
     * Pareto front solutions located within the same region of the objective 
     * space. Each region has an identifier (ID) for each objective, varying from 0 to 
     * 2^dimension-1, and a list of solutions that share the same ID.
     * **/
    /* Legende: PPI - Pareto Point Index.
    */
    template<typename PPI, typename REGION>
    class ObjectiveSpaceRegion{
        private:
            /**
             * Pareto points that belong to a region of the objective space.
             * **/
            std::vector<PPI> pareto_points_indexes;
            /**
             * A vector with the ID of a solution subset in the objective 
             * space region. Its size is equal to the number of objectives, 
             * and each position has a value between 0 and 2^dimension-1, 
             * representing the region where a set of solutions is located 
             * on each objective space.
             * **/
            std::vector<REGION> region_id;

        public:
            /***
             * Constructor of a ObjectiveSpaceRegion. 
             * @param ppi points of a pareto front that belong to a given region.
             * @param id identifier of the region where a pareto point is located.
             * **/
            ObjectiveSpaceRegion(const std::vector<PPI>& ppi,  const std::vector<REGION>& id);

            /**
             * Add a solution from the archive to its region in the objective space.
             * @param index solution index in the archive.
            */
            void add(unsigned index);

            /**
             * Get the identifier of a region in the objective space.
            */
            std::vector<REGION>& region();

            /**
             * Get the identifier of a region in the objective space.
            */
            const std::vector<REGION>& region() const;

            /**
             * Get a vector containing the indexes of solutions located in a specific region.
            */
            std::vector<PPI>& value();

            /**
             * Get a vector containing the indexes of solutions located in a specific region.
            */
            const std::vector<PPI>& value() const;

            /**
             * Get the number of solutions in a region of the objective space.
            */
            unsigned size() const;
    };
#include "ObjectiveSpaceRegion.tpp"    
}
#endif // MH_BUILDER_OBJECTIVE_SPACE_REGION_H