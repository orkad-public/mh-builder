/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/
#ifndef __BOUNDED_DIVERSITY_ARCHIVE__
#define __BOUNDED_DIVERSITY_ARCHIVE__
#include "core/archive/BoundedArchive.h"
#include "core/archive/ArchiveDiversityCriterion.h"

namespace core::archive {

    /**
     * Inherits BoundedArchive class to extends its functionalities, especially by using a diversity
     * criterion to accept solutions when archive is full.
     * @tparam SOLUTION
     * @tparam ARCHIVE
     * */
    template <typename SOLUTION, typename ARCHIVE>
class BoundedDiversityArchive : public core::archive::BoundedArchive<SOLUTION, ARCHIVE> {
      public:
        typedef typename ARCHIVE::iterator iterator;
        typedef typename ARCHIVE::const_iterator const_iterator;

          BoundedDiversityArchive(ARCHIVE *_archive, unsigned long long int _maxSize, core::archive::ArchiveDiversityCriterion<SOLUTION>* adc){
            this->inner_archive = _archive;
            this->maxSize = _maxSize;
            this->_adc = adc;
        }

        /**
         * Updates archive with a solution _solution
         * @param _solution the solution to add
         * @return true if the _solution is added to the archive
         */
        bool operator()(const SOLUTION &_solution) override {
          bool added = false;
          // Check if archive is full.
          if(this->size() == this->maxSize){
            unsigned long long init_size = this->maxSize;
            // If solution could be added but can not because of the size limit.
            this->maxSize++;
            if(BoundedArchive<SOLUTION, ARCHIVE>::operator()(_solution)){
              this->inner_archive->pop_back();
              this->maxSize = init_size;
              if(_adc){
                _adc->init(this->inner_archive);
                long long r = _adc->least_contributive_solution(this->inner_archive->get_solutions(), _solution);
                if(r > -1){
                  this->erase(this->begin() + r);
                  added = BoundedArchive<SOLUTION, ARCHIVE>::operator()(_solution);
                }
              }
            }
            this->maxSize = init_size;
          }else{
            added = BoundedArchive<SOLUTION, ARCHIVE>::operator()(_solution);
          }
          return added;
        };

        /**
         * Updates archive with a archive _archive
         * @param _archive the archive to add
         * @return true if at least one solution of archive is added to the current archive
         */
        bool operator()(const core::Archive<SOLUTION> &_archive) override {
          bool sol_inserted = false;
          for(unsigned i = 0; i < _archive.size();++i){
            sol_inserted = this->operator()(_archive[i]) || sol_inserted;
          }
          return sol_inserted;
        };

        /**
         * Update archive hiself (in case a solution change)
         * @return true is the archive change
         */
        bool update() override {
          return BoundedArchive<SOLUTION, ARCHIVE>::update();
        }

        /**
         * Resize archive if his size is superior of maxSize
         */
        virtual void resize() {
          BoundedArchive<SOLUTION, ARCHIVE>::resize();
        }

        /**
         * Erase the element at pos
         * @param pos
         * @return iterator
         */
        iterator erase(iterator pos) override {
          return BoundedArchive<SOLUTION, ARCHIVE>::erase(pos);
        }

        /**
        * Get k^th solution
        * @param k
        * @return the k^th solution
        */
        SOLUTION& operator[](unsigned int k) override {
          return BoundedArchive<SOLUTION, ARCHIVE>::operator[](k);
        }

        /**
         * Get k^th solution
         * @param k
         * @return the k^th solution
         */
        SOLUTION operator[](unsigned int k) const override {
          return BoundedArchive<SOLUTION, ARCHIVE>::operator[](k);
        }

        /**
         * Get size of the archive
         * @return the vector size
         */
        [[nodiscard]] unsigned long long int size() const override {
          return BoundedArchive<SOLUTION, ARCHIVE>::size();
        }

        /**
         * Add a solution in the archive without update
         * @param i the element to add
         */
        void push_back(const SOLUTION &i) override {
          BoundedArchive<SOLUTION, ARCHIVE>::push_back(i);
        }

        /**
         * Remove the last element in the archive
         */
        void pop_back() override {
          BoundedArchive<SOLUTION, ARCHIVE>::pop_back();
        }

        /**
         * Clear the archive
         */
        void clear() override{
          BoundedArchive<SOLUTION, ARCHIVE>::clear();
        }

        /**
         * Begin iterator of the archive
         */
        const_iterator begin() const override {
          return BoundedArchive<SOLUTION, ARCHIVE>::begin();
        }

        /**
         * End iterator of the archive
         */
        const_iterator end() const override {
          return BoundedArchive<SOLUTION, ARCHIVE>::end();
        }

        /**
         * Begin iterator of the archive
         */
        iterator begin() override {
          return BoundedArchive<SOLUTION, ARCHIVE>::begin();
        }

        /**
         * End iterator of the archive
         */
        iterator end() override {
          return BoundedArchive<SOLUTION, ARCHIVE>::end();
        }

        /**
         * Sort the archive
         */
        void sort() override {
          BoundedArchive<SOLUTION, ARCHIVE>::sort();
        }

        /**
         * set the diversity Criterion
         * @param adc
         */
        void setDiversityCriterion(core::archive::ArchiveDiversityCriterion<SOLUTION>* adc){
          _adc = adc;
        }

    protected:
      /**
      * Criterion used to determine which solution to remove when archive is full.
      * */
      core::archive::ArchiveDiversityCriterion<SOLUTION>* _adc;
    };
}

#endif // __BOUNDED_DIVERSITY_ARCHIVE__
