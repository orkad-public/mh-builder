/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/
#ifndef __SOLUTION_SPACE_DIVERSITY_CRITERION__
#define __SOLUTION_SPACE_DIVERSITY_CRITERION__

#include "core/archive/ArchiveDiversityCriterion.h"
#include <vector>

namespace core::archive {
template <typename VECTOR_SOLUTION>
class SolutionSpaceDiversityCriterion : public core::archive::ArchiveDiversityCriterion<VECTOR_SOLUTION> {
    protected:
    /**
      * Distances between every solution.
      * **/
    std::vector<std::vector<double>> distances;
    /**
      * The highest distances for solutions.
      * */
    std::vector<double> max_distances;
    /**
      * Diversity of an archive.
      * */
    double global_diversity;

    public:
        
    virtual void compute_all_distances(const std::vector<VECTOR_SOLUTION>& solutions) = 0;
    virtual long long least_contributive_solution(const std::vector<VECTOR_SOLUTION>& archive, const VECTOR_SOLUTION& sol);
    double get_global_diversity() const;

    /**
         * Return the jaccard distance between two vector solutions.
         **/
    double jaccard_distance(const VECTOR_SOLUTION sol1, const VECTOR_SOLUTION sol2) const ;
    /**
            * Return the hamming distance between two vector solutions.
            * The hamming distance is the number of positions at which
            * the corresponding symbols are different.
            * The complexity time is linear O(n).
            **/
    double hamming_distance(const VECTOR_SOLUTION s1, const VECTOR_SOLUTION s2) const;
    };
#include "SolutionSpaceDiversityCriterion.tpp"
}
#endif // SOLUTION_SPACE_DIVERSITY_CRITERION_H