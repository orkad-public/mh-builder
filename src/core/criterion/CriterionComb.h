/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CRITERIONCOMB_H
#define MH_BUILDER_CRITERIONCOMB_H

#include "core/Criterion.h"

using namespace core;

namespace core::criterion {

    /**
     * abstract class representing a combination of two criteria (left and right)
     * @tparam IN the type of a solution
     */
    template<typename IN>
    class CriterionComb : public Criterion<IN> {
    protected:
        Criterion<IN> *left;    /* the first criterion */
        Criterion<IN> *right;   /* the second criterion */
    public:
        /**
         * Constructor of a combination of two criteria
         * @param _left the first criterion
         * @param _right the second criterion
         */
        explicit CriterionComb(Criterion<IN> &_left, Criterion<IN> &_right) : left(&_left), right(&_right) {}

        ~CriterionComb() = default; // default destructor

        /**
         * Initialization of the criterion
         * calls init methods of inner criteria
         */
        void init() override;

        /**
         * update of the criterion
         * calls update methods of inner criteria
         */
        void update() override;

        /**
         * Set the first criterion
         * @param _left the first criterion
         */
        void setLeft(Criterion<IN> &_left);

        /**
         * Set the second criterion
         * @param _right the second criterion
         */
        void setRight(Criterion<IN> &_right);
    };

#include "CriterionComb.tpp"

}
#endif //MH_BUILDER_CRITERIONCOMB_H
