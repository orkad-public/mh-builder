/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CRITERIONOR_H
#define MH_BUILDER_CRITERIONOR_H

#include "core/criterion/CriterionComb.h"
#include "core/Criterion.h"

using namespace core;

namespace core::criterion {

    /**
     * Class representing an OR combination of two criteria
     * if one of the two criteria answers true, the algorithm continues
     * @tparam IN
     */
    template<typename IN>
    class CriterionOr : public CriterionComb<IN> {
    public:
        /**
         * Constructor of an OR combination of two criteria
         * @param _left the first criterion
         * @param _right the second criterion
         */
        explicit CriterionOr(Criterion<IN> &_left, Criterion<IN> &_right) : CriterionComb<IN>(_left, _right) {}

        ~CriterionOr() {} // default destructor

        /** test stop criterion
         * return true if at least one of the inner criteria returns true
         * @return boolean
         */
        bool operator()() override ;
    };

#include "CriterionOr.tpp"
}
#endif //MH_BUILDER_CRITERIONOR_H
