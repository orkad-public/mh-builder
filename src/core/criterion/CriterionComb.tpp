/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "core/criterion/CriterionComb.h"

/**
         * Initialization of the criterion
         * calls init methods of inner criteria
         */
template<typename IN>
void CriterionComb<IN>::init() {
    left->init();
    right->init();
}

/**
 * update of the criterion
 * calls update methods of inner criteria
 */
template<typename IN>
void CriterionComb<IN>::update() {
    left->update();
    right->update();
};

/**
 * Set the first criterion
 * @param _left the first criterion
 */
template<typename IN>
void CriterionComb<IN>::setLeft(Criterion<IN> &_left) {
    left = &_left;
}

/**
 * Set the second criterion
 * @param _right the second criterion
 */
template<typename IN>
void CriterionComb<IN>::setRight(Criterion<IN> &_right) {
    right = &_right;
}