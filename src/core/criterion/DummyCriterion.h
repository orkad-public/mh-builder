/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DUMMYCRITERION_H
#define MH_BUILDER_DUMMYCRITERION_H

#include "core/Criterion.h"

namespace core::criterion {

    /**
     * Class representing a dummy criterion :
     * the test if an algorithm can continue is always true (operator() )
     * @tparam IN the type of the solution (not used for this specific criterion)
     * implemented as a singleton
     */
    template<typename IN>
    class DummyCriterion : public Criterion<IN> {
    public:
        /**
         * get DummyCriterion singleton
         * @return dummyCriterion
         */
        static DummyCriterion *getDummyCriterion();

        /**
         * Initialization of the criterion
         * empty for this class
         */
        void init() override ;

        /**
         * this operator returns true so the algorithm continues
         * @return true
         */
        bool operator()( ) override ;

        /**
         * update : empty for this class
         */
        void update() override ;
    private:
        /* private constructor */
        DummyCriterion() {}
        /* static variable used for the singleton */
        static DummyCriterion<IN> *singleton ;
    };

    #include "DummyCriterion.tpp"

}
#endif //MH_BUILDER_DUMMYCRITERION_H
