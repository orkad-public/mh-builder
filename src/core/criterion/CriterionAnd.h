/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CRITERIONAND_H
#define MH_BUILDER_CRITERIONAND_H

#include "core/criterion/CriterionComb.h"
#include "core/Criterion.h"

using namespace core;

namespace core::criterion {

    /**
     * Class representing an AND combination of two criteria,
     * both criteria  must be true to continue the algorithm
     * @tparam IN the type of the solution (or archive)
     */
    template<typename IN>
    class CriterionAnd : public CriterionComb<IN> {
    public:
        /**
         * Constructor of an AND combination of two criteria
         * @param _left the first criterion
         * @param _right the second criterion
         */
        explicit CriterionAnd(Criterion<IN> &_left, Criterion<IN> &_right) : CriterionComb<IN>(_left, _right) {}
        ~CriterionAnd()=default ;

        /** test stop criterion
         * return true if both inner criteria return true
         * @return boolean
         */
        bool operator()() override ;
    };

#include "CriterionAnd.tpp"
}
#endif //MH_BUILDER_CRITERIONAND_H
