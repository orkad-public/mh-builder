/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "core/criterion/DummyCriterion.h"


/**
     * Initialization of the criterion
     * empty for this class
*/
    template<typename IN>
    void DummyCriterion<IN>::init() {}

/**
     * this operator returns true so the algorithm continues
     * @return true
**/
    template<typename IN>
    bool DummyCriterion<IN>::operator()() { return true; }

/**
 * update : empty for this class
 */
    template<typename IN>
    void DummyCriterion<IN>::update() {}

/** static method to obtain the singleton **/
    template<typename IN>
    DummyCriterion<IN> *DummyCriterion<IN>::getDummyCriterion() {
        if (singleton == nullptr) singleton = new DummyCriterion<IN>();
        return singleton;
    }

    template<typename IN>
    DummyCriterion<IN> *DummyCriterion<IN>::singleton = nullptr;

