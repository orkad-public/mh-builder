/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SOLUTION_H
#define MH_BUILDER_SOLUTION_H

#include "core/fitness/FitnessMax.h"

namespace core {
    /**
     * Class representing a Solution
     * @tparam FIT Type (float, int, etc.)
     */
    template<typename FIT = core::fitness::FitnessMax<>>
    class Solution {
    public:
        typedef FIT FITNESS;
        /**
         * default constructor
         */
        Solution() = default;
        /**
         * default destructor
         */
        virtual ~Solution() = default;
        /**
         * Get the Fitness
         * @return fitness
         */
        virtual FIT &fitness() {
          return fitness_;
        }

        /**
         * Get the Fitness (const version)
         * @return fitness
         */
        virtual const FIT &fitness() const {
          return fitness_;
        }

        /**
         * Set the Fitness
         * @param f the fitness
         */
        virtual void fitness(FIT f) {
          fitness_ = f;
        }

        /**
         * If the fitness is valid
         * @return true if fitness is valid
         */
        virtual bool valid() {
          return fitness_.isValid();
        }

        /**
         * If the scalarized fitness is valid
         * @return true if the scalarised fitness is valid
         */
        virtual bool validScalar() {
          return fitness_.isValidScalar();
        }

        /**
         * Get the flag
         * @return the flag
         */
        [[nodiscard]] virtual int flag() const {
          return inner_flag;
        }

        /**
         * Set the flag
         * @param v the flag
         */
        virtual void flag(int v) {
          inner_flag = v;
        }

        /**
         * Equals comparator
         * @param sol solution
         * @return true solution have the same fitness
         */
        virtual bool operator==(const Solution<FIT> &sol) const {
          return this->fitness_ == sol.fitness();
        }

        /**
         * Different comparator
         * @param sol solution
         * @return true solution have note the same fitness
         */
        virtual bool operator!=(const Solution<FIT> &sol) const {
          return this->fitness_ != sol.fitness();
        }

        /**
         * Worse or equal comparator
         * @param sol solution
         * @return true solution is worse or equal than another solution
         */
        virtual bool operator<=(const Solution<FIT> &sol) const {
          return this->fitness_ <= sol.fitness();
        }

        /**
         * Worse comparator
         * @param sol solution
         * @return true solution is worse than another solution
         */
        virtual bool operator<(const Solution<FIT> &sol) const {
          return this->fitness_ < sol.fitness();
        }

        /**
         * Better or equal comparator
         * @param sol solution
         * @return true solution is better or equal than another solution
         */
        virtual bool operator>=(const Solution<FIT> &sol) const {
          return this->fitness_ >= sol.fitness();
        }

        /**
         * Better comparator
         * @param sol solution
         * @return true solution is better than another solution
         */
        virtual bool operator>(const Solution<FIT> &sol) const {
          return this->fitness_ > sol.fitness();
        }
        /**
         * Print the solution in an output stream
         * @param os output stream
         */
        virtual void print(std::ostream &os) const {
            this->fitness().printOn(os);
            os  << "NO REPRESENTATION" ;
            if (this->flag()) os << " (VISITED)";
        }
    protected:
        /*! the fitness value */
        FIT fitness_;
        /*! the flag */
        int inner_flag = 0;
    };

    /**
     * To print a solution with output stream
     * @tparam FIT Fitness type
     * @param os output stream
     * @param solution solution
     * @return output stream
     */
    template<typename FIT>
    std::ostream &operator<<(std::ostream &os, const Solution<FIT> &solution) {
      solution.print(os);
      return os;
    }
}

#endif //MH_BUILDER_SOLUTION_H
