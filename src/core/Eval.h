/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_EVAL_H
#define MH_BUILDER_EVAL_H

#include "core/Algorithm.h"


namespace core {

    template<typename SOL>
    class Eval : public core::Algorithm<SOL> {
    public:
        typedef SOL SOLUTION;
        /**
         * default constructor, initialize the counter val to zero
         */
        explicit Eval() : eval_counter(0) {}

        /**
         * updating the eval
         * @param sol
         */
        virtual void operator()([[maybe_unused]] SOL &sol ) {};

        /**
         * reset the eval counter
         */
        void resetCounter() {
          this->eval_counter = 0;
        }

        unsigned long long int getEvalCounter() {
          return this->eval_counter;
        }

    protected:
        unsigned long long int eval_counter = 0;
    };
}

#endif //MH_BUILDER_EVAL_H
