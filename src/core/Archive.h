/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ARCHIVE_H
#define MH_BUILDER_ARCHIVE_H
#include <functional>
#include "util/RNGHelper.h"
#include <iostream>


namespace core {
    /**
     * Class representing an archive
     * @tparam SOLUTION
     */
     template < typename SOLUTION >
    class Archive {
    public:
        typedef typename std::vector<SOLUTION>::iterator iterator;
        typedef typename std::vector<SOLUTION>::const_iterator const_iterator;
        typedef SOLUTION SOLUTION_TYPE;

        Archive() : updated(true), maxSize(-1) {}

        explicit Archive(SOLUTION &_solution) : updated(true), maxSize(-1),
            choose_strategy([](const Archive<SOLUTION> &x){ return util::RNGHelper::get()->uniform(x.size());}) {
          this->operator()(_solution);
        }

        /**
         * Updates archive with a solution _solution
         * @param _solution the solution to add
         * @return true if the _solution is added to the archive
         */
        virtual bool operator()(const SOLUTION &_solution) = 0;

        /**
         * Updates archive with a archive _archive
         * @param _archive the archive to add
         * @return true if at least one solution of archive is added to the current archive
         */
        virtual bool operator()(const Archive<SOLUTION> &_archive) = 0;

        /**
        * Get k^th solution
        * @param k
        * @return the k^th solution
        */
        virtual SOLUTION& operator[](unsigned int k) {
          return this->archive[k];
        }

        /**
         * Get k^th solution
         * @param k
         * @return the k^th solution
         */
        virtual SOLUTION operator[](unsigned int k) const {
          return this->archive[k];
        }

        /**
         * Get size of the archive
         * @return the vector size
         */
        [[nodiscard]] virtual unsigned long long int size() const {
          return this->archive.size();
        }

        /**
         * Add a solution in the archive without update
         * @param i the element to add
         */
        virtual void push_back(const SOLUTION &i) {
          this->archive.push_back(i);
          updated = false;
        }

        /**
         * insert at the end all solutions from archive _arch
         * @param _arch
         */
        virtual void insertAll(const Archive &_arch) {
            this->archive.insert(this->archive.end(),_arch.begin(),_arch.end());
            this->updated = false;
        }
        /**
         * Test if archive is updated or not
         * @return
         */
        virtual bool isUpdated() {
          return this->updated;
        }

        /**
         * Update archive itself (in case of a solution changes)
         * @return true is the archive changes
         */
        virtual bool update() {
          this->updated = true;
          return false;
        }

        /**
         * Remove the last element in the archive
         */
        virtual void pop_back() {
          this->archive.pop_back();
        }

        /**
         * Clear the archive
         */
        virtual void clear() {
          this->archive.clear();
        }

        /**
         * Begin iterator of the archive
         */
        virtual const_iterator begin() const {
          return this->archive.begin();
        }

        /**
         * End iterator of the archive
         */
        virtual const_iterator end() const {
          return this->archive.end();
        }

        /**
         * Begin iterator of the archive
         */
        virtual iterator begin() {
          return this->archive.begin();
        }

        /**
         * End iterator of the archive
         */
        virtual iterator end() {
          return this->archive.end();
        }

        /**
         * Erase the element at pos
         * @param pos
         * @return iterator
         */
        virtual iterator erase(iterator pos) {
          return this->archive.erase(pos);
        }

        virtual void resize(unsigned int size){
          this->archive.resize(size);
        }

        /**
         * Sort the archive according to the fitness
         */
        virtual void sort() {
          std::sort(this->archive.begin(), this->archive.end(), [&](SOLUTION i, SOLUTION j) {
              return i.fitness().lexicoComp(j.fitness());
          });
        }


        /**
         * Get the maxSize
         */
         unsigned long long int getMaxSize()  const {
          return this->maxSize;
         }

         /**
          * get the archive content
          * @return the vector of solutions stored in the archive
          */
         virtual std::vector<SOLUTION>& get_solutions() {
           return this->archive;
         }
         /**
          * provide the best solution for each objective of the solutions
          * @return the vector v of solutions such as v[objective_number] = best solution
          */
          virtual const std::vector<SOLUTION>  getBestSolutionForEachObjective() const {
              auto numberOfObjectives= this->archive[0].fitness().objectives().size() ;
              std::vector<SOLUTION> results ;
              for (unsigned long long  obj =0 ; obj< numberOfObjectives;obj++) {  //for each objective
                  unsigned long long int indBest = 0;
                  for (unsigned long long int ind = 1; ind < this->archive.size(); ind++)
                      if (this->archive[indBest].fitness().objectives()[obj] <
                          this->archive[ind].fitness().objectives()[obj]) indBest = ind;
                  results.push_back(this->archive[indBest]);
              }
              return results ;
          }
         /**
          * Get the selection function that returns the index of a solution
          */
         std::function<unsigned long long int (const Archive<SOLUTION> &_archive)> getChooseStrategy() const {
           return this->choose_strategy;
         };


    protected:
        std::vector<SOLUTION> archive; // stores the solutions of the archive
        bool updated;  // set to true if the archive changes
        unsigned long long int maxSize;  // limits the archive with a max size, equals to -1 if unbounded
        std::function<unsigned long long int (const Archive<SOLUTION> &_archive)> choose_strategy;
    };

    /**
    * To print an archive with output stream
    * @tparam SOLUTION solution type
    * @param os output stream
    * @param _archive the archive()
    * @return output stream
    */
    template<typename SOLUTION>
    std::ostream &operator<<(std::ostream &os, const Archive<SOLUTION> &_archive) {
      for(SOLUTION sol: _archive)
        os << sol << std::endl;
      return os;
    }
}

#endif //MH_BUILDER_ARCHIVE_H
