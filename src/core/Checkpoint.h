/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CHECKPOINT_H
#define MH_BUILDER_CHECKPOINT_H

#include "core/Criterion.h"

namespace core {

    /**
     * Abstract class representing a checkpoint for an algorithm
     * any algorithm can insert different checkpoints to obtain specific logs after an execution
     * Subclasses that extend this abstract class can use or not the IN parameter
     * @tparam IN Type of the solution for an algorithm which generally
     *            represents a solution type or an archive of solutions.
     */
    template <typename IN>
    class Checkpoint {
    public:

        /**
         * Default destructor
         */
        virtual ~Checkpoint() = default;

        /**
         * Initialization of the checkpoint
         * according to the concrete criterion which extends this abstract method,
         * the in parameter may be unused.
         * @param in input
         */
        virtual void init([[maybe_unused]] const IN &_in ) = 0 ;

        /**
         * The operator which executes a checkpoint
         * @param _in
         */
        virtual void operator()([[maybe_unused]] const IN &_in ) = 0 ;
    };
}

#endif //MH_BUILDER_CHECKPOINT_H
