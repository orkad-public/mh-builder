/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DUMMYCHECKPOINT_H
#define MH_BUILDER_DUMMYCHECKPOINT_H

#include "core/Checkpoint.h"

namespace core::checkpoint {
    /**
     * Concrete class representing a dummy checkpoint (do nothing !)
     * implemented as a singleton
     * @tparam IN
     */
    template<typename IN>
    class DummyCheckpoint : public Checkpoint<IN> {
    public:
        /**
         * provide the singleton instance of the DummyCheckpoint
         * @return
         */
        static DummyCheckpoint *getDummyCheckpoint();

        /**
         * initialisation of the checkpoint
         * @param _in the solution or archive, maybe unused
         */
        void init([[maybe_unused]] const IN &_in) override ;

        /**
         * set a checkpoint
         * @param _in the solution or archive, maybe unused
         */
        void operator()([[maybe_unused]] const IN &_in ) override ;
        
    private:
        DummyCheckpoint() {} /* private constructor */
        static DummyCheckpoint<IN> *singleton;
    };
#include "DummyCheckpoint.tpp"

}

#endif //MH_BUILDER_DUMMYCHECKPOINT_H
