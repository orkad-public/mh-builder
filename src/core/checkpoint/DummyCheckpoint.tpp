/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#include "core/checkpoint/DummyCheckpoint.h"

template<typename IN>
void DummyCheckpoint<IN>::init([[maybe_unused]]const IN &_in) {}

template<typename IN>
void DummyCheckpoint<IN>::operator()([[maybe_unused]] const IN &_in) { }

template<typename IN>
DummyCheckpoint<IN> *DummyCheckpoint<IN>::singleton = nullptr;

template<typename IN>
DummyCheckpoint<IN> *DummyCheckpoint<IN>::getDummyCheckpoint() {
    if (singleton == nullptr) singleton = new DummyCheckpoint<IN>();
    return singleton;
}