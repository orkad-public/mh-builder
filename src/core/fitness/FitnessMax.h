/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FITNESS_MAX_H
#define MH_BUILDER_FITNESS_MAX_H

#include "core/Fitness.h"

using namespace core;


namespace core::fitness {
    /**
     * Class representing a Maximization Fitness
     * @tparam VALUE Type of Fitness (float, int, etc.)
     * @tparam SCAL_VALUE Type of Scalarised Fitness (float, int, etc.)
     */
    template<class VALUE=double, class SCAL_VALUE=double>
    class FitnessMax : public Fitness<VALUE, SCAL_VALUE> {
    public:
        /**
         * default constructor
         */
        FitnessMax() : Fitness<VALUE, SCAL_VALUE>() {}

        /**
         * Equals comparator
         * @param _f fitness
         * @return true if fitness is equals
         */
        bool operator==(const FitnessMax &_f) const {
            if (this->valid_scalar && _f.isValidScalar())
                return this->scalarComp(_f.scalar()) == 0;
            unsigned long long int s = this->objectives_.size();
            for (unsigned long long int i = 0; i < s; i++)
                if (this->objectives_[i] != _f[i])
                    return false;
            return true;
        }

        /**
         * Not Equals comparator
         * @param _f fitness
         * @return true if fitness is not equals
         */
        bool operator!=(const FitnessMax &_f) const {
            return !operator==(_f);
        }

        /**
         * worse or equal than comparator
         * @param _f fitness
         * @return true if fitness is worse or equal than _f
         */
        bool operator<=(const FitnessMax &_f) const {
            if (!this->valid || !_f.isValid())
                return (!this->valid && _f.isValid());

            if (this->valid_scalar && _f.isValidScalar())
                return this->scalarComp(_f.scalar()) < 1;
            unsigned long long int s = this->objectives_.size();
            for (unsigned long long int i = 0; i < s; i++)
                if (this->objectives_[i] > _f[i])
                    return false;
            return true;
        }

        /**
         * worse than comparator
         * @param _f fitness
         * @return true if fitness is worse than _f
         */
        bool operator<(const FitnessMax &_f) const {
            return operator<=(_f) && !operator==(_f);
        }

        /**
         * better or equal than comparator
         * @param _f fitness
         * @return true if fitness is better or equal than _f
         */
        bool operator>=(const FitnessMax &_f) const {
            if (!this->valid || !_f.isValid())
                return (this->valid && !_f.isValid());

            if (this->valid_scalar && _f.isValidScalar())
                return this->scalarComp(_f.scalar()) > -1;
            unsigned long long int s = this->objectives_.size();
            for (unsigned long long int i = 0; i < s; i++)
                if (this->objectives_[i] < _f[i])
                    return false;
            return true;
        }

        /**
         * better than comparator
         * @param _f fitness
         * @return true if fitness is better than _f
         */
        bool operator>(const FitnessMax &_f) const {
            return operator>=(_f) && !operator==(_f);
        }

        /**
         * lexicographic comparator
         * @param _f fitness
         * @return true if fitness is better than _f with lexicographic comparison
         */
        bool lexicoComp(const FitnessMax &_f) const {
            unsigned long long int s = this->objectives_.size();
            for (unsigned long long int k = 0; k < s; k++)
                if (this->objectives_[k] > _f[k])
                    return true;
                else if (this->objectives_[k] < _f[k])
                    return false;
            return false;
        }

        double normalizedWeight(unsigned int nbTermsMax) {
            double normalizedValue = 0;
            unsigned long long int s = this->objectives_.size();
            for (unsigned long long int i = 0; i < s - 1; i++) normalizedValue += this->objectives_[i];
            return normalizedValue + (this->objectives_[s - 1] / nbTermsMax);
        }

        /**
         * Test if it is a maximisation Fitness
         * @return True if it maximise
         */
        [[nodiscard]] bool isMaximisationFitness() const override {
            return true;
        };

    };
}

#endif //MH_BUILDER_FITNESS_MAX_H
