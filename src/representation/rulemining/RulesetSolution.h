/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULESETSOLUTION_H
#define MH_BUILDER_RULESETSOLUTION_H

#include "representation/VectorSolution.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/data/Individual.h"


namespace representation {
    namespace rulemining {
        /**
         * Class representing a ruleset solutions
         * @tparam FIT Fitness Type
         * @tparam TYPE Type of the vector (default Rule)
         */
        template<typename FIT, typename TYPE = data::Rule<FIT>>
        class RulesetSolution : public representation::VectorSolution<FIT, TYPE> {
        public:

            typedef TYPE RULE;

            /**
               * Construct a ruleset solution
               */
            explicit RulesetSolution() : representation::VectorSolution<FIT, TYPE>(0) {
            }

            /**
            * reset the solution
            */
            void reset() override {
                this->clear();
            }
            /**
              * Constructor with a list of rules
              * @param _rules the list of rules
              */
            explicit RulesetSolution(const std::vector<RULE> &_rules) {
                for (auto &r: _rules) {
                    this->push_back(r);
                }
            }

            /**
              * Constructor with o rule
              * @param _rule the rule
              */
            explicit RulesetSolution(const RULE &_rule) {
                this->push_back(_rule);
            }

            /**
            * Operator ==, test if two ruleset are equals
            * @param ruleset to compare
            * @return true if the two ruleset are equals
            */
            bool operator==(const RulesetSolution<FIT> ruleset) const {
                if (this->size() != ruleset.size()) return false;
                if (this->size() == 0) return true;
                for (unsigned long long int i = 0; i < ruleset.size(); i++) {
                    bool found = false;
                    for (unsigned long long int j = 0; j < ruleset.size(); j++) {
                        if (this->vec_[i] == ruleset[j])
                            found = true;
                    }
                    if (!found)
                        return false;
                }
                return true;
            }

            /**
             * Evaluate an individual according to the ruleset
             * @param individual
             * @return true if individual match ruleset, else False
             */
            [[nodiscard]] bool eval(const data::Individual &individual) const {
                for (const auto &rule: this->vec_) {
                    if (rule.eval(individual))
                        return true;
                }
                return false;
            }
        };

        /**
         * To print a ruleset with output stream
         * @tparam FIT Fitness type
         * @tparam TYPE Vector type
         * @param os output stream
         * @param rule solution
         * @return output stream
         */
        template<typename FIT, typename TYPE = data::Rule<FIT>>
        std::ostream &operator<<(std::ostream &os, const RulesetSolution<FIT, TYPE> &rulesetSolution) {
            os << rulesetSolution.fitness() << std::endl << "RULESET (" << rulesetSolution.size() << ") {";
            for (auto e: rulesetSolution)
                os << e << " ";
            os << " }";
            if (rulesetSolution.flag())
                os << " (VISITED) ";
            return os;
        }
    }
}
#endif //MH_BUILDER_RULESETSOLUTION_H
