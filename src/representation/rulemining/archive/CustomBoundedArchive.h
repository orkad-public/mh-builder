/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CUSTOMBOUNDEDARCHIVE_H
#define MH_BUILDER_CUSTOMBOUNDEDARCHIVE_H

#include "core/archive/BoundedArchive.h"
#include "util/RNGHelper.h"




        namespace representation::rulemining::archive {
            /**
             * Class represented a bounded archive for rulemining
             * @tparam SOLUTION
             */
            template<typename SOLUTION, typename ARCHIVE>
            class CustomBoundedArchive : public core::archive::BoundedArchive<SOLUTION, ARCHIVE> {
            public:

                /**
                * Constructor of a custom bounded archive
                * @param _archive the archive
                * @param _maxSize the maximum number of solution in the archive
                * @param _choose_strategy the method for choose solution (by default random solution)
                */
                CustomBoundedArchive(ARCHIVE *_archive,
                                     unsigned long long int _maxSize,
                                     std::function<unsigned long long int(
                                             const core::Archive<SOLUTION> &_archive)> _choose_strategy = [](
                                             const core::Archive<SOLUTION> &x) {
                                         return util::RNGHelper::get()->uniform(x.size());
                                     }) :
                        core::archive::BoundedArchive<SOLUTION, ARCHIVE>(_archive, _maxSize, _choose_strategy) {
                }

                CustomBoundedArchive() : core::archive::BoundedArchive<SOLUTION, ARCHIVE>() {}

                /**
                * Resize archive if his size is superior of maxSize
                */
                void resize() {
                    if (this->inner_archive->size() > this->maxSize) {
                        int maxTerms = 0;
                        for (auto sol: *(this->inner_archive)) { //!\ this number of terms is already negatif
                            if (sol.fitness().objectives()[2] < maxTerms) maxTerms = sol.fitness().objectives()[2];
                        }
                        maxTerms = -maxTerms; // Put positif this value
                        std::sort(this->inner_archive->begin(), this->inner_archive->end(), [&](SOLUTION i, SOLUTION j) {
                            return i.fitness().normalizedWeight(maxTerms) > j.fitness().normalizedWeight(maxTerms);
                        });
                        this->inner_archive->resize(this->maxSize);
                    }
                }
            };
        }


#endif //MH_BUILDER_CUSTOMBOUNDEDARCHIVE_H
