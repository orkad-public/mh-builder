/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULEMINING_H
#define MH_BUILDER_RULEMINING_H

#include <string>
#include <fstream>
#include <vector>
#include <typeinfo>


#include "util/StringHelper.h"
#include "representation/rulemining/data/CategoricalAttribute.h"
#include "representation/rulemining/data/Individual.h"
#include "representation/rulemining/data/Operator.h"
#include "representation/rulemining/data/Term.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/RulesetSolution.h"



namespace representation {
    namespace rulemining {
        /**
         * Class representing a Rulemining Problem
         */
        class Rulemining {
        public:
            static const unsigned long long int MAX_TERMS = 9;

            /**
             * Create a new Rulemining instance
             * @param instance_desc a description file
             * @param instance_ind the individuals file
             */
            explicit Rulemining(const std::string &instance_desc, const std::string &instance_ind) {
                readDescription(instance_desc);
                filterNoIgnoredAttributes();
                readIndividuals(instance_ind);
            }

            /**
            * Read a description file
            * @param instance_desc a description file
            */
            void readDescription(const std::string &instance_desc) {
                std::ifstream input(instance_desc);

                if (!input)
                    throw std::runtime_error("Error: unable to open description benchmark file");

                std::string line;
                bool parsingPredictionStarted = false;

                data::CategoricalAttribute::counter = 0;
                attributes.clear();

                while (std::getline(input, line)) {
                    if (util::StringHelper::contains(line, "@attribute") && !parsingPredictionStarted) {
                        std::vector<std::string> data = util::StringHelper::split(line, " ");
                        if (data.size() != 3)
                            throw std::runtime_error("parse error (attribute line)");
                        addAttribute(data[1], data[2]);
                    } else if (util::StringHelper::contains(line, "@prediction") && !parsingPredictionStarted) {
                        parsingPredictionStarted = true;
                        std::vector<std::string> data = util::StringHelper::split(line, " ");
                        if (data.size() == 4) // no option @ignore
                            setPrediction(data[1], data[2], data[3], "");
                        else if (data.size() == 5 && util::StringHelper::contains(line, "@ignore"))
                            setPrediction(data[2], data[3], data[4], data[1]);
                        else
                            throw std::runtime_error("parse error (prediction line)");

                    }
                }

                input.close();
            }

            /**
            * Read an individuals file
            * @param instance_ind an individuals file
            */
            void readIndividuals(const std::string &instance_ind) {
                bool mode_direct=true ;
                std::ifstream input(instance_ind);

                if (!input)
                    throw std::runtime_error("Error: unable to open individuals benchmark file");

                std::string line;
                /* read first line : Nb Individuals: xxx mode: indexed_value
                   available mode values are direct_value or indexed_value
                   mode option is optional, by default : direct_value */
                std::getline(input, line);
                std::vector<std::string> data = util::StringHelper::split(line, " ");
                std::string error_message_first_line;
                error_message_first_line = "Error, first line individuals, available syntax is :\n \
                                          Nb Individuals: XX  [ mode: (direct_value | indexed_value) ]\n";
                if (data.size() !=3 && data.size()!=5)
                 throw std::runtime_error(error_message_first_line);
                if (data.size() == 5) {
                    if (data[3]!="mode:" || (data[4]!="indexed_value" && data[4]!="direct_value"))
                        throw std::runtime_error(error_message_first_line);
                    mode_direct=(data[4]=="direct_value");
                }

                individuals.clear();

                while (std::getline(input, line)) {
                    std::vector<std::string> data = util::StringHelper::split(line.substr(1, line.length() - 2), ",");
                    data::Individual individual;
                    for (const auto &a: data) {
                        std::vector<std::string> token = util::StringHelper::split(a, " ");
                        unsigned long long int id_attribute = stoi(token[0]);
                        if (!attributes[id_attribute].isIgnored()) {
                            if (mode_direct) {
                                try {
                                    individual.addAttribute(id_attribute,
                                                            attributes[id_attribute].getValueId(token[1]));
                                } catch (const std::runtime_error &error) {
                                    std::cerr << "Error individuals file: line : " << line << std::endl;
                                    std::cerr << "id_attribute:" << std::to_string(id_attribute) << ":" << token[1]
                                              << std::endl;
                                    throw std::runtime_error("getValueId not found");
                                }
                            } else {
                                if (std::stoull(token[1]) >= attributes[id_attribute].size()) {
                                    std::cerr << "Error individuals file: line : " << line << std::endl;
                                    std::cerr << "id_attribute:" << std::to_string(id_attribute) << ":" << token[1]
                                              << std::endl;
                                    throw std::runtime_error("bad indexed value");
                                }
                                individual.addAttribute(id_attribute, stoi(token[1]));
                            }
                        }
                    }
                    individuals.push_back(individual);
                    if (prediction.eval(individual))
                        positive_individuals.push_back(individual);
                }
                input.close();
            }

            /**
             * build the noIgnoredAttributes vector
             */
            void filterNoIgnoredAttributes() {
                noIgnoredAttributes.clear();
                for (auto &att: attributes) if (!att.isIgnored()) noIgnoredAttributes.push_back(att);
            }

            /**
             * Parse an operator according to a string value
             * @param operatorToParse
             * @return the operator
             */
            static data::Operator parseOperator(const std::string &operatorToParse) {
                if (operatorToParse == "=")
                    return data::OP_EQUALS;
                if (operatorToParse == "<")
                    return data::OP_LT;
                if (operatorToParse == ">")
                    return data::OP_GT;
                return data::OP_MISSING;
            }

            /**
             * Parse a list of terms
             * @param terms string to parse
             * @return vector of terms
             */
            std::vector<data::Term> parseTerms(const std::string &terms) {
                std::vector<data::Term> v;
                std::vector<std::string> data = util::StringHelper::split(terms, ",");
                for (const auto &d: data) {
                    std::vector<std::string> token = util::StringHelper::split(d, " ");
                    data::CategoricalAttribute a = getCategoricalAttribute(token[0]);
                    data::Operator o = parseOperator(token[1]);
                    unsigned long long int value_id = a.getValueId(token[2]);
                    v.emplace_back(a.getId(), o, value_id);
                }
                return v;
            }

            /**
             * Parse a list of rules
             * @param terms string to parse
             * @return vector of rules
             */
            template<typename FIT>
            std::vector<data::Rule<FIT>> parseRules(const std::string &terms) {
                std::vector<data::Rule<FIT>> v;
                std::vector<std::string> data = util::StringHelper::split(terms, ";");
                for (const auto &d: data) {
                    v.emplace_back(parseTerms(d));
                }
                return v;
            }

            /**
             * To print a comprehensive solution
             * @tparam FIT fitness vector
             * @param os
             * @param rulesetSolution
             * @param printFitness
             * @param compact
             * @return
             */
            template<typename FIT>
            std::ostream &
            printSolution(std::ostream &os, const representation::rulemining::RulesetSolution<FIT> rulesetSolution,
                          bool printFitness = true, bool compact = false) {
                if (printFitness)
                    os << rulesetSolution.fitness() << " ";

                if (!compact) {
                    os << "RULESET (" << rulesetSolution.size() << " rules) { \n";
                    for (auto rule: rulesetSolution) {
                        os << "\t RULE (" << rule.size() << " terms) [ \n ";
                        for (auto term: rule) {
                            os << "\t\t" << attributes[term.getAttributeId()].getName() << " " << term.getOperator()
                               << " " << attributes[term.getAttributeId()].getValue(term.getValueId()) << " \n";
                        }
                        os << "\t]\n";
                    }
                    os << "}";
                } else {
                    os << "{";
                    unsigned long long int r = 0;
                    for (auto rule: rulesetSolution) {
                        os << "[";
                        unsigned long long int t = 0;
                        for (auto term: rule) {

                            os << attributes[term.getAttributeId()].getName() << term.getOperator()
                               << attributes[term.getAttributeId()].getValue(term.getValueId());
                            t++;
                            if (t < rule.size())
                                os << ",";
                        }
                        os << "]";
                        r++;
                        if (r < rulesetSolution.size())
                            os << ",";
                    }
                    os << "}";
                }

                return os;
            }

            /**
             * To print a comprehensive solution
             * @tparam FIT fitness vector
             * @param os
             * @param rulesetSolution
             * @return
             */
            template<typename FIT>
            std::ostream &printRule(std::ostream &os, const data::Rule<FIT> rule) {
                os << "RULE (" << rule.size() << ") [ ";
                for (auto term: rule) {
                    os << "(" << attributes[term.getAttributeId()].getName() << " " << term.getOperator() << " "
                       << attributes[term.getAttributeId()].getValue(term.getValueId()) << ") ";
                }
                os << "] ";
                return os;
            }

            /**
             * Get the categorical attribute by his name
             * @param attribute_name
             * @return the categorical attribute
             */
            data::CategoricalAttribute getCategoricalAttribute(const std::string &attribute_name) {
                for (auto a: attributes) {
                    if (a.getName() == attribute_name)
                        return a;
                }
                throw std::runtime_error("Error: unable to found attribute");
            }

            /**
             * Get the individuals vector
             * @return the individuals vector
             */
            [[nodiscard]] std::vector<data::Individual> getIndividuals() const {
                return individuals;
            }

            /**
             * Get the individuals vector
             * @return the individuals vector
             */
            std::vector<data::Individual> &getIndividuals() {
                return individuals;
            }

            /**
             * Get the positive individuals vector
             * @return the positive individuals vector
             */
            [[nodiscard]] std::vector<data::Individual> getPositiveIndividuals() const {
                return positive_individuals;
            }

            /**
             * Get the positive individuals vector
             * @return the positive individuals vector
             */
            std::vector<data::Individual> &getPositiveIndividuals() {
                return positive_individuals;
            }

            /**
             * Get one individual
             * @return an Individual
             */
            [[nodiscard]] data::Individual getIndividual(unsigned long long int i) const {
                return individuals[i];
            }

            /**
             * Get one individual
             * @return an Individual
             */
            data::Individual &getIndividual(unsigned long long int i) {
                return individuals[i];
            }

            /**
             * Get the number of individual
             * @return the number of individual
             */
            [[nodiscard]] unsigned long long int getNbIndividual() const {
                return individuals.size();
            }

            /**
             * Get the prediction
             * @return the prediction
             */
            [[nodiscard]] data::Term getPrediction() const {
                return prediction;
            }

            /**
             * Get the attributes that are not ignored
             * @return the attributes
             */
            [[nodiscard]] std::vector<data::CategoricalAttribute> getAttributes() const {
                return noIgnoredAttributes;
            }

            /**
            * Get the attributes that are not ignored
            * @return the attributes
            */
            std::vector<data::CategoricalAttribute> &getAttributes() {
                return noIgnoredAttributes;
            }

            /**
             * Get one attribute
             * @return an attribute
             */
            data::CategoricalAttribute &getAttribute(unsigned long long int i) {
                return noIgnoredAttributes[i];
            }

            /**
             * Get the number of no ignored attributes
             * @return the number of no ignored attributes
             */
            [[nodiscard]] unsigned long long int getNbAttributes() const {
                return noIgnoredAttributes.size();
            }

        private:

            /**
             * Set prediction for an instance
             * @param attribute_name the name of the prediction attribute
             * @param operator_value the operator of comparison
             * @param value the value to valid prediction
             * @param ignoreOption the list of attributes to be ignored (string under the form '@ignore{att1,att2,...,attn}')
             */
            void setPrediction(const std::string &attribute_name, const std::string &operator_value,
                               const std::string &value, const std::string &ignoreOption) {
                std::vector<std::string> attrsIgnored;
                attrsIgnored.clear();
                if (ignoreOption.length()) {
                    std::string list = ignoreOption.substr(8, (ignoreOption.length() - 9));
                    attrsIgnored = util::StringHelper::split(list, ",");
                }
                for (auto &attribute: attributes) {
                    if (std::find(attrsIgnored.begin(), attrsIgnored.end(), attribute.getName()) != attrsIgnored.end())
                        attribute.setIgnored();
                    if (attribute.getName() == attribute_name) {
                        prediction.setAttribute(attribute.getId());
                        prediction.setOperator(parseOperator(operator_value));
                        prediction.setValue(attribute.getValueId(value));
                    }
                }
            }

            /**
             * Add an attribute to the lists of attributes
             * @param name name of the attribute
             * @param data the string corresponding to the categorical value
             */
            void addAttribute(const std::string &name, const std::string &data) {
                bool orderable = (data[0] == '<');
                data::CategoricalAttribute attribute(name, orderable);
                std::vector<std::string> elt = util::StringHelper::split(
                        data.substr((!orderable) ? 1 : 2, data.length() - 2), ",");
                for (const auto &e: elt) {
                    std::string v = (e[e.length() - 1] == '}') ? e.substr(0, e.length() - 1) : e;
                    attribute.addValue(v);
                }
                attributes.push_back(attribute);
            }

            std::vector<data::CategoricalAttribute> noIgnoredAttributes;
            std::vector<data::CategoricalAttribute> attributes;
            std::vector<data::Individual> individuals;
            std::vector<data::Individual> positive_individuals;
            data::Term prediction;
        };
    }
}
#endif //MH_BUILDER_RULEMINING_H
