/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULESETNEIGHBOR_H
#define MH_BUILDER_RULESETNEIGHBOR_H

namespace representation::rulemining::neighborhood::neighbor {

    /**
     * Class representing a neighbor of a Ruleset
     * @tparam RULE
     */
    template<typename RULESET>
    class RulesetNeighbor : public opt::singleSolution::neighborhood::neighbor::Neighbor<RULESET> {
    public:

        typedef typename RULESET::RULE RULE;

        /**
         * Default constructor
         */
        RulesetNeighbor() = default;

        /**
         * Default destructor
         */
        virtual ~RulesetNeighbor() = default;

        /**
         * Constructor of a ruleset neighbor according to a rule
         * @param _term
         */
        explicit RulesetNeighbor(RULE _rule) : rule(_rule) {}

        /**
         * Set rule of the neighbor
         * @param _rule
         */
        void setRule(RULE &_rule) {
            this->rule = _rule;
        }

        /**
         * Get rule of the neighbor
         */
        [[nodiscard]] RULE getRule() const {
            return this->rule;
        }

    protected:
        RULE rule;
    };
}


#endif //MH_BUILDER_RULESETNEIGHBOR_H
