/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MODIFRULENEIGHBOR_H
#define MH_BUILDER_MODIFRULENEIGHBOR_H

#include "representation/rulemining/neighborhood/neighbor/RulesetNeighbor.h"
#include "representation/rulemining/neighborhood/neighbor/RuleNeighbor.h"

namespace representation {
    namespace rulemining {
        namespace neighborhood {
            namespace neighbor {

                /**
                  * Class representing a modification rule neighbor of a Ruleset
                  * @tparam RULE
                  */
                template<typename RULESET>
                class ModifRuleNeighbor : public RulesetNeighbor<RULESET> {
                public:

                    typedef typename RULESET::RULE RULE;

                    /**
                     * Constructor of a modif rule neighbor according to a rule neighbor and a position
                     * @param _term
                     */
                    explicit ModifRuleNeighbor(RuleNeighbor<RULE> *_ruleNeighbor, unsigned long long int _index_rule) :
                            ruleNeighbor(_ruleNeighbor), index_rule(_index_rule) {}


                    /**
                     * Default destructor
                     */
                    ~ModifRuleNeighbor() {
                    }

                    /**
                     * Print the neighbor to an output stream
                     * @param os
                     */
                    virtual void printOn(std::ostream &os) const {
                        os << "MODIFRULE (" << *ruleNeighbor << " at " << index_rule << ")";
                    }

                protected:
                    RuleNeighbor<RULE> *ruleNeighbor;
                    unsigned long long int index_rule;

                    /**
                    * Apply move to the solution
                    * @param sol the solution
                    */
                    void move(RULESET &_ruleset) override {
                        (*ruleNeighbor)(_ruleset.at(index_rule));
                    }

                    /**
                     * Apply move back
                     * @param sol the solution
                     */
                    void move_back(RULESET &_ruleset) {
                        ruleNeighbor->do_move_back(_ruleset.at(index_rule));
                    }
                };
            }
        }
    }
}
#endif //MH_BUILDER_MODIFRULENEIGHBOR_H
