/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ADDRULENEIGHBOR_H
#define MH_BUILDER_ADDRULENEIGHBOR_H

#include "representation/rulemining/neighborhood/neighbor/RulesetNeighbor.h"

namespace representation {
    namespace rulemining {
        namespace neighborhood {
            namespace neighbor {

                /**
                 * Class representing an add rule neighbor of a Ruleset
                 * @tparam RULE
                 */
                template<typename RULESET>
                class AddRuleNeighbor : public RulesetNeighbor<RULESET> {
                public:

                    typedef typename RULESET::RULE RULE;

                    /**
                     * Constructor of an add rule neighbor according to a rule
                     * @param _rule
                     */
                    explicit AddRuleNeighbor(RULE _rule) : RulesetNeighbor<RULESET>(_rule) {}

                    /**
                     * Default destructor
                     */
                    ~AddRuleNeighbor() override = default;

                    /**
                     * Print the neighbor to an output stream
                     * @param os
                     */
                    virtual void printOn(std::ostream &os) const {
                        os << "ADDRULE (" << this->rule << ")";
                    }

                protected:
                    /**
                    * Apply move to the solution
                    * @param sol the solution
                    */
                    void move(RULESET &_ruleset) override {
                        _ruleset.push_back(this->rule);
                    }

                    /**
                     * Apply move back
                     * @param sol the solution
                     */
                    void move_back(RULESET &_ruleset) {
                        _ruleset.pop_back();
                    }
                };
            }
        }
    }
}

#endif //MH_BUILDER_ADDRULENEIGHBOR_H
