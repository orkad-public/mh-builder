/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DROPTERMNEIGHBOR_H
#define MH_BUILDER_DROPTERMNEIGHBOR_H

#include "representation/rulemining/neighborhood/neighbor/RuleNeighbor.h"
#include "representation/rulemining/data/Term.h"

namespace representation {
    namespace rulemining {
        namespace neighborhood {
            namespace neighbor {

                /**
                  * Class representing a drop term neighbor of a Rule
                  * @tparam RULE
                  */
                template<typename RULE>
                class DropTermNeighbor : public RuleNeighbor<RULE> {
                public:
                    /**
                     * Constructor of a drop term neighbor according to a term
                     * @param _term
                     */
                    explicit DropTermNeighbor(representation::rulemining::data::Term _term) : RuleNeighbor<RULE>(
                            _term) {}

                    /**
                     * Default destructor
                     */
                    ~DropTermNeighbor() override = default;

                    /**
                     * Print the neighbor to an output stream
                     * @param os
                     */
                    virtual void printOn(std::ostream &os) const {
                        os << "DROPTERM (" << this->term << ")";
                    }

                protected:
                    /**
                    * Apply move to the solution
                    * @param sol the solution
                    */
                    void move(RULE &_rule) override {
                        _rule.pop_back(this->term);
                    }

                    /**
                     * Apply move back
                     * @param sol the solution
                     */
                    void move_back(RULE &_rule) {
                        _rule.push_back(this->term);
                    }
                };
            }
        }
    }
}
#endif //MH_BUILDER_DROPTERMNEIGHBOR_H
