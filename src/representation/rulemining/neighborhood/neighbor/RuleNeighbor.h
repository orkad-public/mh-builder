/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULENEIGHBOR_H
#define MH_BUILDER_RULENEIGHBOR_H


#include "representation/rulemining/data/Term.h"

namespace representation::rulemining::neighborhood::neighbor {
    /**
     * Class representing a neighbor of a Rule
     * @tparam RULE
     */
    template<typename RULE>
    class RuleNeighbor : public opt::singleSolution::neighborhood::neighbor::Neighbor<RULE> {
    public:
        /**
         * Default constructor
         */
        RuleNeighbor() = default;

        /**
         * Default destructor
         */
        virtual ~RuleNeighbor() = default;

        /**
         * Constructor of a rule neighbor according to a term
         * @param _term
         */
        explicit RuleNeighbor(representation::rulemining::data::Term _term) : term(_term) {}

        /**
         * Set term of the neighbor
         * @param _term
         */
        void setTerm(representation::rulemining::data::Term &_term) {
            this->term = _term;
        }

        /**
         * Get term of the neighbor
         */
        [[nodiscard]] representation::rulemining::data::Term getTerm() const {
            return this->term;
        }

    protected:
        representation::rulemining::data::Term term;
    };
}

#endif //MH_BUILDER_RULENEIGHBOR_H
