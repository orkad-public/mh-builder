/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULESETNEIGHBORHOOD_H
#define MH_BUILDER_RULESETNEIGHBORHOOD_H

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/data/Operator.h"
#include "representation/rulemining/RulesetEval.h"

#include "representation/rulemining/neighborhood/RuleNeighborhood.h"
#include "representation/rulemining/neighborhood/neighbor/RuleNeighbor.h"
#include "representation/rulemining/neighborhood/neighbor/RulesetNeighbor.h"

#include "neighbor/AddRuleNeighbor.h"
#include "neighbor/DropRuleNeighbor.h"
#include "neighbor/ModifRuleNeighbor.h"
#include "util/RNGHelper.h"

using namespace representation::rulemining::data;

namespace representation::rulemining::neighborhood {
    /**
     * Class representing a ruleset neighborhood
     * @tparam RULESET
     */
    template<typename RULESET>
    class RulesetNeighborhood : public opt::singleSolution::neighborhood::Neighborhood<RULESET> {
    public:
        typedef typename RULESET::RULE RULE;

        /**
         * Constructor of a ruleset Neighborhood
         * @param _neighbor
         * @param _instance
         */
        explicit RulesetNeighborhood(representation::rulemining::Rulemining &_instance,
                                     unsigned long long int _nbMaxRules = -1) : instance(_instance),
                                                                                ruleNeighborhood(instance,
                                                                                                 false),
                                                                                nbMaxRules(_nbMaxRules) {}

        ~RulesetNeighborhood() {
            for (auto &neighbor: neighbors)
                if (neighbor != NULL) {
                    delete neighbor;
                    neighbor = NULL;
                }
            neighbors.clear();
            for (auto &neighbor: generate_neighbors)
                if (neighbor != NULL) {
                    delete neighbor;
                    neighbor = NULL;
                }
            generate_neighbors.clear();
        }

        /**
         * Initialise the vector of randomized neighborhood
         * @param sol
         */
        void init(const RULESET &ruleset) override {
            for (auto &neighbor: neighbors) {
                delete neighbor;
                neighbor = NULL;
            }
            neighbors.clear();
            for (auto &neighbor: generate_neighbors) {
                delete neighbor;
                neighbor = NULL;
            }
            generate_neighbors.clear();
            generateAllNeighbors(ruleset);
            util::RNGHelper::get()->shuffle(neighbors.begin(), neighbors.end());
            this->neighbor = neighbors[0];
            key = 0;
        }

        /**
         * Compute next neighbor according to the key
         */
        void next() override {
            if (hasNextNeighbor()) {
                this->neighbor = neighbors[++key];
            } else throw std::runtime_error("Error: No next neighbor. Do you initialize the neighborhood ?");
        }

        /**
         * Test if the neighborhood have a new neighbor
         * @return
         */
        bool hasNextNeighbor() override {
            return key < neighbors.size() - 1;
        }

        /**
         * Get size of the neighborhood
         * @return the size of the neighborhood
         */
        [[nodiscard]] unsigned long long int size() const {
            return neighbors.size();
        }

    private:

        /**
        * Generate all neighbor according to a ruleset
        * @param ruleset
        */
        void generateAllNeighbors(const RULESET &ruleset) {
            // POUR CHAQUE ATTRIBUTS AJOUTER UNE REGLE AVEC L'ATTRIBUT
            if (ruleset.size() < nbMaxRules) {
                for (auto &a: instance.getAttributes()) {
                    generateAddRuleNeighbor(a);
                }
            }
            // POUR CHAQUE REGLE
            for (unsigned long long int i = 0; i < ruleset.size(); i++) {
                neighbors.push_back(new neighbor::DropRuleNeighbor<RULESET>(i)); // DROP RULE
                //RuleNeighborhood<RULE> ruleNeighborhood(instance);
                ruleNeighborhood.init(ruleset[i],
                                      ruleset.fitness()[rulemining::RulesetEval<RULESET>::INDEX_CONFIDENCE]);
                for (auto ruleNeighbor: ruleNeighborhood.getNeighbors()) {
                    neighbors.push_back(new neighbor::ModifRuleNeighbor<RULESET>(ruleNeighbor, i));
                    generate_neighbors.push_back(ruleNeighbor);
                }
            }
        }

        /**
        * Generate all add neighbors according to an attribute
        * @param categoricalAttribute
        */
        void generateAddRuleNeighbor(const CategoricalAttribute &categoricalAttribute) {
            if (categoricalAttribute.getId() != instance.getPrediction().getAttributeId()) {
                for (unsigned long long int i = 0; i < categoricalAttribute.size(); i++) {
                    neighbors.push_back(new neighbor::AddRuleNeighbor<RULESET>(
                            RULE(Term(categoricalAttribute.getId(), OP_EQUALS, i))));
                    if (categoricalAttribute.isOrderable()) {
                        neighbors.push_back(new neighbor::AddRuleNeighbor<RULESET>(
                                RULE(Term(categoricalAttribute.getId(), OP_LT, i))));
                        neighbors.push_back(new neighbor::AddRuleNeighbor<RULESET>(
                                RULE(Term(categoricalAttribute.getId(), OP_GT, i))));
                    }
                }
            }
        }

    protected:
        std::vector<neighbor::RulesetNeighbor < RULESET> *>
        neighbors;
        std::vector<neighbor::RuleNeighbor < RULE> *>
        generate_neighbors;
        rulemining::Rulemining &instance;
        RuleNeighborhood <RULE> ruleNeighborhood;
        /*! current key */
        unsigned long long int key{};
        unsigned long long int nbMaxRules;
    };
}

#endif //MH_BUILDER_RULESETNEIGHBORHOOD_H
