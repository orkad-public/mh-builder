/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULENEIGHBORHOOD_H
#define MH_BUILDER_RULENEIGHBORHOOD_H

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/data/CategoricalAttribute.h"
#include "representation/rulemining/neighborhood/neighbor/RuleNeighbor.h"
#include "neighbor/AddTermNeighbor.h"
#include "neighbor/DropTermNeighbor.h"
#include "neighbor/ModifTermNeighbor.h"

#include "representation/rulemining/data/Term.h"

namespace representation {
    namespace rulemining {
        namespace neighborhood {
            /**
             * Class representing a rule neighborhood
             * @tparam SOL
             */
            template<typename RULE>
            class RuleNeighborhood : public opt::singleSolution::neighborhood::Neighborhood<RULE> {
            public:
                /**
                 * Constructor of a rule Neighborhood
                 * @param _neighbor
                 * @param _instance
                 */
                explicit RuleNeighborhood(representation::rulemining::Rulemining &_instance, bool _free = true) :
                        instance(_instance), maxTerms(9), free(_free) {}

                ~RuleNeighborhood() {
                    if (free) {
                        for (auto &neighbor: neighbors)
                            if (neighbor != NULL) {
                                delete neighbor;
                                neighbor = NULL;
                            }
                    }
                    neighbors.clear();
                }

                /**
              * Initialise the vector of randomized neighborhood
              * @param rule
              */
                void init(const RULE &rule) override {
                    this->init(rule, 0.0);
                }

                /**
                 * Initialise the vector of randomized neighborhood
                 * @param rule
                 * @param confidence
                 */
                void init(const RULE &rule, double confidence) {
                    //opt::singleSolution::neighborhood::Neighborhood<RULE>::init(rule);
                    if (free) for (auto neighbor: neighbors) delete neighbor;
                    neighbors.clear();
                    generateAllNeighbors(rule, confidence);
                    util::RNGHelper::get()->shuffle(neighbors.begin(), neighbors.end());
                    this->neighbor = neighbors[0];
                    key = 0;
                }

                /**
                 * Compute next neighbor according to the key
                 */
                void next() override {
                    if (hasNextNeighbor()) {
                        this->neighbor = neighbors[++key];
                    } else throw std::runtime_error("Error: No next neighbor. Do you initialize the neighborhood ?");
                }

                /**
                 * Test if the neighborhood have a new neighbor
                 * @return
                 */
                bool hasNextNeighbor() override {
                    return key < neighbors.size() - 1;
                }

                /**
                 * Get size of the neighborhood
                 * @return the size of the neighborhood
                 */
                [[nodiscard]] unsigned long long int size() const {
                    return neighbors.size();
                }

                /**
                 * Get vector of neighbors
                 * @return the vector of neighbors
                 */
                std::vector<neighbor::RuleNeighbor<RULE> *> getNeighbors() const {
                    return neighbors;
                }

            private:

                /**
                * Generate all neighbor according to a rule
                * @param rule
                */
                void generateAllNeighbors(const RULE &rule, double confidence = 0.0) {
                    // POUR CHAQUE ATTRIBUTS
                    for (const auto &a: instance.getAttributes()) {
                        bool found = false;
                        unsigned long long int index = rule.search_term(a.getId(), &found);
                        if (found) { // Si l'attribut est dans la règle alors : DROP/MODIF
                            generateDropNeighbor(rule, index);
                            generateModifNeighbor(rule, a, index);
                        } else if (confidence != 1 &&
                                   rule.size() < maxTerms) { // Si l'attribut n'est pas dans la règle alors : ADD
                            generateAddNeighbor(rule, a);
                        }
                    }
                }

                /**
                 * Generate all add neighbors according to an attribute
                 * @param rule
                 * @param categoricalAttribute
                 */
                void generateAddNeighbor(const RULE &rule,
                                         const representation::rulemining::data::CategoricalAttribute &categoricalAttribute) {
                    if (categoricalAttribute.getId() != instance.getPrediction().getAttributeId()) {
                        if (rule.size() < rulemining::Rulemining::MAX_TERMS) {
                            for (unsigned long long int i = 0; i < categoricalAttribute.size(); i++) {
                                neighbors.push_back(
                                        new neighbor::AddTermNeighbor<RULE>(representation::rulemining::data::Term(categoricalAttribute.getId(), representation::rulemining::data::OP_EQUALS, i)));
                                if (categoricalAttribute.isOrderable()) {
                                    neighbors.push_back(
                                            new neighbor::AddTermNeighbor<RULE>(representation::rulemining::data::Term(categoricalAttribute.getId(), representation::rulemining::data::OP_LT, i)));
                                    neighbors.push_back(
                                            new neighbor::AddTermNeighbor<RULE>(representation::rulemining::data::Term(categoricalAttribute.getId(), representation::rulemining::data::OP_GT, i)));
                                }
                            }
                        }
                    }
                }

                /**
                 * Generate all drop neighbors according to a position
                 * @param rule
                 * @param index
                 */
                void generateDropNeighbor(const RULE &rule, unsigned long long int index) {
                    if (rule.size() > 1) { // DROP TERM
                        neighbors.push_back(new neighbor::DropTermNeighbor<RULE>(rule[index]));
                    }
                }

                /**
                 * Generate all modif neighbor according to an attribute
                 * @param rule
                 * @param categoricalAttribute
                 * @param index
                 */
                void generateModifNeighbor(const RULE &rule,
                                           const representation::rulemining::data::CategoricalAttribute &categoricalAttribute,
                                           unsigned long long int index) {
                    if (!categoricalAttribute.isOrderable()) { // SANS RELATION D'ORDRE
                        generateModifNeighborsForUnorderableValues(rule, categoricalAttribute, index);
                    } else { // AVEC RELATION D'ORDRE
                        generateModifNeighborsForOrderableValues(rule, categoricalAttribute, index);
                    }
                }

                /**
                 * Generate all modif neighbor according to an unorderable attribute
                 * @param rule
                 * @param categoricalAttribute
                 * @param index
                 */
                void generateModifNeighborsForUnorderableValues(const RULE &rule,
                                                                const representation::rulemining::data::CategoricalAttribute &categoricalAttribute,
                                                                unsigned long long int index) {
                    for (unsigned long long int i = 0; i < categoricalAttribute.size(); i++) {
                        if (i != rule[index].getValueId()) { // Pour chaque changement de valeur possible
                            representation::rulemining::data::Term t = rule[index];
                            t.setValue(i);
                            neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(t));
                        }
                    }
                }

                /**
                 * Generate all modif neighbor according to an orderable attribute
                 * @param rule
                 * @param categoricalAttribute
                 * @param index
                 */
                void generateModifNeighborsForOrderableValues(const RULE &rule,
                                                              const representation::rulemining::data::CategoricalAttribute &categoricalAttribute,
                                                              unsigned long long int index) {
                    switch (rule[index].getOperator()) {
                        case representation::rulemining::data::OP_EQUALS:
                            if (rule[index].getValueId() > 0) {
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_EQUALS, rule[index].getValueId() - 1)));
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_GT, rule[index].getValueId() - 1)));
                            }
                            if (rule[index].getValueId() < categoricalAttribute.size() - 1) {
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_EQUALS, rule[index].getValueId() + 1)));
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_LT, rule[index].getValueId() + 1)));
                            }
                            break;
                        case representation::rulemining::data::OP_LT:
                            if (rule[index].getValueId() > 0) {
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_EQUALS, rule[index].getValueId() - 1)));
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_LT, rule[index].getValueId() - 1)));
                                if (rule[index].getValueId() > 1) {
                                    neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                            representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_GT, rule[index].getValueId() - 2)));
                                }
                            }
                            if (rule[index].getValueId() < categoricalAttribute.size() - 1) {
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_LT, rule[index].getValueId() + 1)));
                            }
                            break;
                        case representation::rulemining::data::OP_GT:
                            if (rule[index].getValueId() > 0) {
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_GT, rule[index].getValueId() - 1)));
                            }
                            if (rule[index].getValueId() < categoricalAttribute.size() - 1) {
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_EQUALS, rule[index].getValueId() + 1)));
                                neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                        representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_GT, rule[index].getValueId() + 1)));
                                if (rule[index].getValueId() < categoricalAttribute.size() - 2) {
                                    neighbors.push_back(new neighbor::ModifTermNeighbor<RULE>(
                                            representation::rulemining::data::Term(rule[index].getAttributeId(), representation::rulemining::data::OP_LT, rule[index].getValueId() + 2)));
                                }
                            }
                            break;
                        case representation::rulemining::data::OP_BETWEEN:
                            throw std::runtime_error("Error: not implemented yet");
                        case representation::rulemining::data::OP_MISSING:
                            throw std::runtime_error("Error: not a valid operator");
                    }
                }

            protected:
                std::vector<representation::rulemining::neighborhood::neighbor::RuleNeighbor<RULE> *> neighbors;
                rulemining::Rulemining &instance;
                unsigned long long int maxTerms;
                /*! current key */
                unsigned long long int key{};
                bool free;
            };
        }
    }
}
#endif //MH_BUILDER_RULENEIGHBORHOOD_H
