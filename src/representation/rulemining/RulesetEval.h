/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULESETEVAL_H
#define MH_BUILDER_RULESETEVAL_H

#include <cmath>
#include "core/Eval.h"
#include "representation/rulemining/Rulemining.h"

namespace representation {
    namespace rulemining {

        /**
         * EvalType To Define which objectives use
         */
        typedef enum rmEval {
            FMEASURE_ONLY,
            GMEAN_ONLY,
            SENSITIVITY_AND_CONFIDENCE,
            SENSITIVITY_CONFIDENCE_NBTERMS,
            SENSITIVITY_CONFIDENCE_NBTERMS_SPECIFICITY,
            ALL_METRICS
        } RuleminingEvalType;

        /**
         * Class for evaluate a Ruleset
         * @tparam SOL Solution type
         */
        template<class SOL>
        class RulesetEval : public core::Eval<SOL> {
        public:

            static const unsigned long long int INDEX_SENSITIVITY = 0;
            static const unsigned long long int INDEX_CONFIDENCE = 1;
            static const unsigned long long int INDEX_NBTERMS = 2;
            static const unsigned long long int INDEX_SPECIFICITY=3 ;

            /**
             * Constructor for the eval function
             * @param _instanceRulemining  Instance of a rulemining problem
             * @param _ruleminingEvalType  Define the objectives
             */
            explicit RulesetEval(Rulemining &_instanceRulemining,
                                 RuleminingEvalType _ruleminingEvalType = SENSITIVITY_CONFIDENCE_NBTERMS) :
                    instanceRulemining(_instanceRulemining), ruleminingEvalType(_ruleminingEvalType),
                    numberOfEvaluation(0) {
            }

            /**
             * Evaluate a rulemining solution
             * @param _sol solution
             */
            void operator()(SOL &_sol) {
                auto &fit = _sol.fitness();
                auto &objv = fit.objectives();

                computeScore(_sol);

                switch (ruleminingEvalType) {
                    case FMEASURE_ONLY:
                        objv.resize(1);
                        objv[0] = fmeasure();
                        fit.objectives(objv);
                        break;
                    case GMEAN_ONLY:
                        objv.resize(1);
                        objv[0] = gmean();
                        fit.objectives(objv);
                        break;
                    case SENSITIVITY_AND_CONFIDENCE:
                        objv.resize(2);
                        objv[INDEX_SENSITIVITY] = sensitivity();
                        objv[INDEX_CONFIDENCE] = confidence();
                        fit.objectives(objv);
                        if (objv[INDEX_SENSITIVITY] == -1 or objv[INDEX_CONFIDENCE] == -1)
                            fit.invalidate();
                        break;
                    case SENSITIVITY_CONFIDENCE_NBTERMS:
                        objv.resize(3);
                        objv[INDEX_SENSITIVITY] = sensitivity();
                        objv[INDEX_CONFIDENCE] = confidence();
                        objv[INDEX_NBTERMS] = -nbTerm(_sol);
                        fit.objectives(objv);
                        if (objv[INDEX_SENSITIVITY] == -1 or objv[INDEX_CONFIDENCE] == -1)
                            fit.invalidate();
                        break;
                    case SENSITIVITY_CONFIDENCE_NBTERMS_SPECIFICITY:
                        objv.resize(4);
                        objv[INDEX_SENSITIVITY] = sensitivity();
                        objv[INDEX_CONFIDENCE] = confidence();
                        objv[INDEX_NBTERMS] = -nbTerm(_sol);
                        objv[INDEX_SPECIFICITY] = specificity();
                        fit.objectives(objv);
                        if (objv[INDEX_SENSITIVITY] == -1 or objv[INDEX_CONFIDENCE] == -1 or objv[INDEX_SPECIFICITY]==-1)
                            fit.invalidate();
                        break;
                    case ALL_METRICS:
                        objv.resize(10);
                        objv[0] = fmeasure();
                        objv[1] = gmean();
                        objv[2] = sensitivity();
                        objv[3] = confidence();
                        objv[4] = specificity();
                        objv[5] = mcc();
                        objv[6] = nbVP;
                        objv[7] = nbFN;
                        objv[8] = nbFP;
                        objv[9] = nbVN;
                        fit.objectives(objv);
                        if (objv[1] == -1 or objv[2] == -1)
                            fit.invalidate();
                }

                numberOfEvaluation++;
            }

            /**
             * Set a new rulemining eval type
             * @param _ruleminingEvalType
             */
            void setRuleminingEvalType(RuleminingEvalType _ruleminingEvalType) {
                ruleminingEvalType = _ruleminingEvalType;
            }

            /**
             * Get the number of evaluation
             * @return the numbe of evaluation
             */
            unsigned long long int getNumberOfEvaluation() {
                return numberOfEvaluation;
            }

            /**
             * Compute score of a solution (True Positive, False Positive, etc.)
             * @param _sol solution
             */
            void computeScore(SOL &_sol) {
                nbP = nbN = nbVP = nbVN = nbFP = nbFN = 0;
                for (unsigned long long int i = 0; i < instanceRulemining.getNbIndividual(); i++) {
                    bool isCheck = _sol.eval(instanceRulemining.getIndividual(i));
                    bool samePrediction = instanceRulemining.getPrediction().eval(instanceRulemining.getIndividual(i));
                    if (isCheck) {
                        nbP++;
                        (samePrediction) ? nbVP++ : nbFP++;
                    } else {
                        nbN++;
                        (samePrediction) ? nbFN++ : nbVN++;
                    }
                }
            }

            /**
             * Compute sensitivity
             * @return the sensitivity
             */
            double sensitivity() {
                if (nbVP + nbVN != 0)
                    return (double) (nbVP) / (nbVP + nbFN);
                else
                    return -1;
            }

            /**
             * Compute confidence
             * @return the confidence
             */
            double confidence() {
                if (nbP > 0) {
                    return (double) (nbVP) / nbP;
                }
                return -1;
            }

            /**
             * Compute the number of terms in a solution
             * @param _sol solution
             * @return the number of terms
             */
            double nbTerm(SOL &_sol) {
                double nbTerm = 0;
                for (auto &rule: _sol)
                    nbTerm += rule.size();
                return nbTerm;
            }

            /**
             * Compute the f-measure
             * @param beta (default = 1)
             * @return the f-measure
             */
            double fmeasure(double beta = 1.0) {
                double c = confidence();
                double s = sensitivity();
                double fm = ((1.0 + beta * beta) * c * s) / (c + beta * beta * s);
                return fm;
            }

            /**
             * Compute the specificity
             * @return the specificity
             */
            double specificity() {
                if (nbVN + nbFP > 0) {
                    return (double) (nbVN) / (nbVN + nbFP);
                }
                return -1;
            }

            /**
             * Compute the geometric mean
             * @return gmean
             */
            double gmean() {
                double spec = specificity();
                double sens = sensitivity();
                if (spec < 0 or sens < 0)
                    return -1;
                return sqrt(spec * sens);
            }

            /**
             * Compute the Matthews correlation coefficient
             * @return MCC
             */
            double mcc() {
                if (nbVP + nbFP > 0 && nbVP + nbFN > 0 && nbVN + nbFP > 0 && nbVN + nbFN > 0) {
                    return ((double) (nbVP * nbVN) - (double) (nbFP * nbFN)) /
                           sqrt((nbVP + nbFP) * (nbVP + nbFN) * (nbVN + nbFP) * (nbVN + nbFN));
                }
                return -1;
            }

        protected:
            rulemining::Rulemining instanceRulemining;
            RuleminingEvalType ruleminingEvalType;
            unsigned long long int nbP{}, nbN{};
            unsigned long long int nbVP{}, nbVN{}, nbFP{}, nbFN{};
            unsigned long long int numberOfEvaluation;
        };
    }
}


#endif //MH_BUILDER_RULESETEVAL_H
