/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INITRULESET_H
#define MH_BUILDER_INITRULESET_H
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetEval.h"
#include "InitRule.h"
namespace representation {
    namespace rulemining {
        namespace algorithm {
            template<typename RULESET>
            class InitRuleset {
            public:

                typedef typename RULESET::RULE RULE;

                InitRuleset(Rulemining &_instance, rulemining::RulesetEval<RULESET> &_eval) : instance(_instance),
                                                                                              eval(_eval) {}

                void operator()(RULESET &_sol) {
                    InitRule <RULE> initRule(instance);
                    RULE rule1;
                    RULE rule2;
                    initRule(rule1);
                    initRule(rule2);
                    RULESET new_sol;
                    new_sol.push_back(rule1);
                    new_sol.push_back(rule2);
                    _sol = new_sol;
                }

            protected:
                Rulemining &instance;
                rulemining::RulesetEval<RULESET> &eval;
            };

        }
    }
}
#endif //MH_BUILDER_INITRULESET_H
