/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INITRULE_H
#define MH_BUILDER_INITRULE_H

#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/data/Term.h"
#include "representation/rulemining/data/Operator.h"
#include "representation/rulemining/data/CategoricalAttribute.h"
#include "representation/rulemining/data/Individual.h"

using namespace representation::rulemining::data ;


namespace representation {
    namespace rulemining {
        namespace algorithm {
            template<typename RULE>
            class InitRule {
            public:
                explicit InitRule(Rulemining &_instance) : instance(_instance) {}

                void operator()(RULE &rule) {
                    // On récupère 1 Individu positif avec la prédiction
                    Individual individual = instance.getPositiveIndividuals()[util::RNGHelper::get()->random(
                            instance.getPositiveIndividuals().size())];

                    //Créer une règle vide
                    RULE r;

                    // Pour chaque attribut de l'individu, on ajoute l'attribut dans la solution
                    for (auto id: individual.getAttributeIds()) {
                        //Ne pas ajouter l'attribut prédiction !
                        if (instance.getPrediction().getAttributeId() != id &&
                            individual.getAttributes()[id] < RAND_MAX - 1) {
                            Term term(id, OP_EQUALS, individual.getIdValue(id));
                            r.push_back(term);
                        }
                    }

                    // On réduit la taille de la règle; min 2 termes et max 9
                    auto taille = static_cast<unsigned long long int>(util::RNGHelper::get()->random(2, 9));

                    while (r.size() > taille) {
                        r.erase(r.begin() + util::RNGHelper::get()->random(static_cast<int>(r.size())));
                    }

                    // On ajoute du bruit (plus de bruit sur les règles plus longues)

                    //Find all attribute not in rule
                    std::vector<CategoricalAttribute> attributes_free;
                    for (auto &a: instance.getAttributes()) {
                        if (a.getId() != instance.getPrediction().getAttributeId()) {
                            bool found = false;
                            r.search_term(a.getId(), &found);
                            if (!found)
                                attributes_free.push_back(a);
                        }
                    }

                    for (int i = 0; i < std::max(static_cast<int>(r.size()) / 3, 1) && !attributes_free.empty(); ++i) {
                        int index = util::RNGHelper::get()->random(static_cast<int>(attributes_free.size()));
                        Operator op = getRandomOperator(attributes_free[index].isOrderable());
                        Term term(attributes_free[index].getId(), op,
                                  util::RNGHelper::get()->random(static_cast<int >(attributes_free[index].size())));
                        r.push_back(term);
                        attributes_free.erase(attributes_free.begin() + index);
                    }

                    // On supprime des termes
                    for (int i = 0; i < std::max(static_cast<int>(r.size()) / 3, 1); ++i) {
                        r.erase(r.begin() + util::RNGHelper::get()->random(static_cast<int>(r.size())));
                    }

                    rule = r;

                }

            protected:
                Rulemining &instance;
            };
        }
    }
}

#endif //MH_BUILDER_INITRULE_H
