/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INITPOPULATIONRULESET_H
#define MH_BUILDER_INITPOPULATIONRULESET_H

#include "core/Algorithm.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetEval.h"
#include "InitRuleset.h"


namespace representation {
    namespace rulemining {
        namespace algorithm {
            /**
             * Class for initialisation of a population of a ruleset
             * @tparam ARCHIVE
             */
            template<typename ARCHIVE>
            class InitPopulationRuleset : public core::Algorithm<ARCHIVE> {
            public:

                typedef typename ARCHIVE::SOLUTION_TYPE SOLUTION;

                /**
                 * Default constructor
                 * @param _instance
                 * @param _eval
                 * @param _init_size (default = 100)
                 */
                InitPopulationRuleset(representation::rulemining::Rulemining &_instance,
                                      representation::rulemining::RulesetEval<SOLUTION> &_eval,
                                      unsigned long long int _init_size = 100) :
                        instance(_instance),
                        eval(_eval),
                        init_size(_init_size) {

                }

                /**
                 * Add in archive some solutions with the initRuleset algorithm
                 * @param _archive
                 */
                void operator()(ARCHIVE &_archive) {
                    _archive.clear();
                    InitRuleset<SOLUTION> initRuleset(instance, eval);
                    SOLUTION sol;
                    for (unsigned long long int j = 0; j < init_size; j++) {
                        initRuleset(sol);
                        eval(sol);
                        _archive(sol);
                    }
                }

            protected:
                Rulemining &instance;
                rulemining::RulesetEval<SOLUTION> &eval;
                unsigned long long int init_size;
            };
        }
    }
}

#endif //MH_BUILDER_POPULATIONRULESET_H
