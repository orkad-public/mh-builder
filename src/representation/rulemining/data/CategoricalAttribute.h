/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_CATEGORICALATTRIBUTE_H
#define MH_BUILDER_CATEGORICALATTRIBUTE_H
#include <string>
#include <vector>

namespace representation {
    namespace rulemining {
        namespace data {
            /**
             * Class representing a categorical attribute
             */
            class CategoricalAttribute {
            public:
                /**
                 * Constructor of a categorical attribute
                 * @param _name name of the attribute
                 * @param _orderable if it is orderable
                 */
                explicit CategoricalAttribute(std::string _name, bool _orderable = false, bool _ignored = false) :
                        name(std::move(_name)), id(counter), orderable(_orderable), ignored(_ignored) {
                    counter++;
                }

                /**
                 * Get name of the attribute
                 * @return the name of the attribute
                 */
                [[nodiscard]] std::string getName() const {
                    return name;
                }

                /**
                 * Get the id of the attribute
                 * @return the name of the attribute
                 */
                [[nodiscard]] unsigned long long int getId() const {
                    return id;
                }

                /**
                 * Add a value id in the values vector
                 * @param value
                 */
                void addValue(const std::string &value) {
                    values.push_back(value);
                }

                /**
                 * Get the id of a value according to his name
                 * @param value the name of the value
                 * @return the id, throw an error if not found
                 */
                unsigned long long int getValueId(const std::string &value) {
                    for (unsigned long long int i = 0; i < values.size(); ++i) {
                        if (values[i] == value) {
                            return i;
                        }
                    }
                    throw std::runtime_error("getValueId not found");
                    return values.size();
                }

                /**
                 * Get number of values
                 * @return number of values
                 */
                [[nodiscard]] unsigned long long int size() const {
                    return values.size();
                }

                /**
                 * Get if categorical is orderable or not
                 * @return True is the categorical attribute is orderable, False else
                 */
                [[nodiscard]] bool isOrderable() const {
                    return orderable;
                }

                /**
                 * Get if categorical is ignored or not
                 * @return True is the categorical attribute is ignored, False else
                 */
                [[nodiscard]] bool isIgnored() const {
                    return ignored;
                }

                /**
                 * Set the attribute as ignored
                 */
                void setIgnored() { ignored = true; }

                /**
                 * Get value according to id
                 * @param _id values
                 * @return value name
                 */
                [[nodiscard]] std::string getValue(unsigned long long int _id) const {
                    return values[_id];
                }


                static unsigned long long int counter;

            protected:
                std::string name;
                unsigned long long int id;
                std::vector<std::string> values;
                bool orderable;
                bool ignored;
            };

            unsigned long long int CategoricalAttribute::counter = 0;
        }
    }
}
#endif //MH_BUILDER_CATEGORICALATTRIBUTE_H
