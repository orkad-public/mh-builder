/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULE_H
#define MH_BUILDER_RULE_H
#include "representation/VectorSolution.h"
#include "representation/rulemining/data/Term.h"
#include <utility>

namespace representation {
    namespace rulemining {
        namespace data {
            /**
             * Class representing a rule
             * @tparam FIT Fitness Type
             */
            template<typename FIT>
            class Rule : public representation::VectorSolution<FIT, Term> {
            public:
                /**
                 * Default constructor with an empty vector of terms
                 */
                Rule() = default;

                /**
                 * Constructor with a list of terms
                 * @param _terms the list of terms
                 */
                explicit Rule(const std::vector<Term> &_terms) {
                    for (auto &t: _terms) {
                        push_back(t);
                    }
                }

                /**
                 * Constructor with only one term
                 * @param _term
                 */
                explicit Rule(const Term &_terms) {
                    push_back(_terms);
                }
                /**
                * reset the solution
                */
                void reset() override {
                    this->clear();
                }
                /**
                 * Operator ==, test if two rules are equals
                 * @param rule to compare
                 * @return true if the two rules are equals
                 */
                bool operator==(const Rule<FIT> rule) const {
                    if (this->size() != rule.size()) return false;
                    for (unsigned long long int i = 0; i < rule.size(); i++) {
                        if (this->vec_[i].getAttributeId() != rule[i].getAttributeId())
                            return false;
                        else if (this->vec_[i].getOperator() != rule[i].getOperator())
                            return false;
                        else if (this->vec_[i].getValueId() != rule[i].getValueId())
                            return false;
                    }
                    return true;
                }

                /**
                 * Evaluate an individual according to the terms vector
                 * @param individual
                 * @return true if the individual match, else false
                 */
                [[nodiscard]] bool eval(const Individual &individual) const {
                    for (const auto &t: this->vec_) {
                        if (!t.eval(individual))
                            return false;
                    }
                    return true;
                }

                /**
                 * Search index of a specified term with a dichotomy algorithm
                 * @param attributes_id the attributes id to search
                 * @param found
                 * @return index of term or index where the term must be placed if not found
                 */
                int search_term(unsigned long long int attributes_id, bool *found) const {
                    int g = 0;
                    int d = this->size() - 1;
                    // DICHOTOMIE POWAAAAAA !!!
                    *found = false;
                    while (g <= d) {
                        int m = (g + d) / 2;
                        if (this->vec_[m].getAttributeId() == attributes_id) {
                            *found = true;
                            return m;
                        } else if (this->vec_[m].getAttributeId() > attributes_id) {
                            d = m - 1;
                        } else {
                            g = m + 1;
                        }
                    }
                    return g;
                }

                /**
                 * Search index of a specified term with a dichotomy algorithm
                 * @param term the term to search
                 * @param found
                 * @return index of term or index where the term must be placed if not found
                 */
                int search_term(const Term term, bool *found) const {
                    return search_term(term.getAttributeId(), found);
                }

                /**
                 * Add term if attribute_id is not in the vector, else replace it
                 * @param term to insert
                 */
                void push_back(const Term term) {
                    bool found = false;
                    int index = search_term(term, &found);
                    if (!found)
                        this->vec_.insert(this->vec_.begin() + index, term);
                    else
                        throw std::runtime_error(
                                "Error: the attribute id of the term is already in the rule, use modif_term instead");
                    this->fitness_.invalidate();
                }

                /**
                * Add term if attribute_id is not in the vector, else replace it
                * @param term to insert
                */
                void pop_back(const Term term) {
                    bool found = false;
                    int index = search_term(term, &found);
                    if (found) {
                        this->vec_.erase(this->vec_.begin() + index);
                    } else {
                        throw std::runtime_error("Error: the attribute id of the term is not found in the rule");
                    }
                }

                /**
                 * Get term according to an attribute id
                 * @param term
                 * @return the term with the same attribute id as the term in parametter
                 */
                Term getTerm(const Term term) {
                    bool found = false;
                    int index = search_term(term, &found);
                    if (found)
                        return this->vec_[index];
                    else
                        throw std::runtime_error("Error: the attribute id of the term is not found in the rule");
                }

                /**
                 * Modify the term according to the attribute id of the term in parametter
                 * @param term
                 */
                void modif_term(const Term term) {
                    bool found = false;
                    int index = search_term(term, &found);
                    if (found)
                        this->vec_[index] = term;
                    else
                        throw std::runtime_error("Error: the attribute id of the term is not found in the rule");
                }

                /**
                 * Print rule with attributes and values name
                 * @param os
                 * @param attributes list of attributes
                 * @return os
                 */
                std::ostream &printOn(std::ostream &os, const std::vector<CategoricalAttribute> &attributes) {
                    os << "RULE (" << this->size() << ") [ ";
                    for (auto e: this->vec_) {
                        e.printOn(os, attributes);
                        os << " ";
                    }

                    os << "] ";
                    return os;
                }
            };

            /**
             * To print a rule with output stream
             * @tparam FIT Fitness type
             * @param os output stream
             * @param rule solution
             * @return output stream
             */
            template<typename FIT>
            std::ostream &operator<<(std::ostream &os, const Rule<FIT> &rule) {
                os << "RULE (" << rule.size() << ") [ ";
                for (auto e: rule)
                    os << e << " ";
                os << "] ";
                return os;
            }

        }
    }
}
#endif //MH_BUILDER_RULE_H
