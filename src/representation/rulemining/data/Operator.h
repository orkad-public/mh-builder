/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_OPERATOR_H
#define MH_BUILDER_OPERATOR_H

#include <string>
#include <iostream>
#include "util/RNGHelper.h"

namespace representation {
    namespace rulemining {
        namespace data {

            /**
             * Enum representing an operator
             */
            typedef enum Op {
                OP_EQUALS,
                OP_LT,
                OP_GT,
                OP_BETWEEN,
                OP_MISSING
            } Operator;

            /**
             * Generate a random operator
             * @param isOrderable
             * @return
             */
            Operator getRandomOperator(bool isOrderable) {
                if (isOrderable) {
                    int n = util::RNGHelper::get()->random(3);
                    switch (n) {
                        case 0:
                            return OP_EQUALS;
                            break;
                        case 1:
                            return OP_LT;
                            break;
                        default:
                            return OP_GT;
                    }
                } else {
                    return OP_EQUALS;
                }
            }

            /**
             * To print the operator to the output stream
             * @param out the output stream
             * @param op the operator
             * @return the output stream
             */
            std::ostream &operator<<(std::ostream &out, const Operator op) {
                switch (op) {
                    case OP_EQUALS:
                        out << "=";
                        break;
                    case OP_LT:
                        out << "<";
                        break;
                    case OP_GT:
                        out << ">";
                        break;
                    case OP_BETWEEN:
                        out << "<>";
                        break;
                    default:
                        out << "MISSING OPERATOR";
                        break;
                }
                return out;
            }
        }
    }
}
#endif //MH_BUILDER_OPERATOR_H
