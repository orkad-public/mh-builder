/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TERM_H
#define MH_BUILDER_TERM_H

#include <iostream>

#include <vector>
#include "representation/rulemining/data/Individual.h"
#include "representation/rulemining/data/Operator.h"
#include "representation/rulemining/data/CategoricalAttribute.h"
#include <utility>


namespace representation {
    namespace rulemining {
        namespace data {
            /**
             * Class representing a term
             */
            class Term {
            public:

                /**
                 * Default constructor with default parameter (avoid to use it)
                 */
                Term() : attribute_id(0), op(OP_EQUALS), value_id(0) {
                }

                /**
                 * Constructor of a term, with an attribute_id, an operator and a value id
                 * @param _attribute_id
                 * @param _op
                 * @param _value_id
                 */
                Term(unsigned long long int _attribute_id, Operator _op, unsigned long long int _value_id)
                        : attribute_id(_attribute_id), op(_op), value_id(_value_id) {
                }

                /**
                 * Evaluate a term according to an individual
                 * @param individual
                 * @return true if the individual match the term, else false
                 */
                [[nodiscard]] bool eval(const Individual &individual) const {
                    switch (op) {
                        case OP_EQUALS:
                            return value_id == individual.getIdValue(attribute_id);
                        case OP_GT:
                            return value_id < individual.getIdValue(attribute_id);
                        case OP_LT:
                            return value_id > individual.getIdValue(attribute_id);
                        default:
                            return false;
                    }
                }

                /**
                 * Set attribute id of the term
                 * @param _attribute
                 */
                void setAttribute(unsigned long long int _attribute_id) {
                    attribute_id = _attribute_id;
                }

                /**
                 * Set the comparison operator
                 * @param _op
                 */
                void setOperator(Operator _op) {
                    op = _op;
                }

                /**
                 * Set the value id of the term
                 * @param _value
                 */
                void setValue(unsigned long long int _value_id) {
                    value_id = _value_id;
                }

                /**
                 * Get the attribute id of the term
                 * @return the attribute id
                 */
                [[nodiscard]] unsigned long long int getAttributeId() const {
                    return attribute_id;
                }

                /**
                 * Get the comparison operator
                 * @return the operator
                 */
                [[nodiscard]] Operator getOperator() const {
                    return op;
                }

                /**
                 * Get the value id of the term
                 * @return value id
                 */
                [[nodiscard]] unsigned long long int getValueId() const {
                    return value_id;
                }

                /**
                 * Print term with attributes and values name
                 * @param os
                 * @param attributes list of attributes
                 * @return os
                 */
                std::ostream &printOn(std::ostream &os, const std::vector<CategoricalAttribute> &attributes) {
                    os << attributes[attribute_id].getName() << " " << op << " "
                       << attributes[attribute_id].getValue(value_id);
                    return os;
                }

            protected:
                unsigned long long int attribute_id;
                Operator op;
                unsigned long long int value_id;
            };

            /**
             * To print a term with output stream
             * @param os output stream
             * @param term term
             * @return output stream
             */
            std::ostream &operator<<(std::ostream &os, const Term &term) {
                os << "(" << term.getAttributeId() << " " << term.getOperator() << " " << term.getValueId() << ") ";
                return os;
            }
        }
    }
}

#endif //MH_BUILDER_TERM_H
