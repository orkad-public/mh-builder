/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INDIVIDUAL_H
#define MH_BUILDER_INDIVIDUAL_H
#include <string>
#include <vector>
#include <map>
#include <iostream>

namespace representation {
    namespace rulemining {
        namespace data {
            /**
             * Class representing an Individual
             */
            class Individual {
            public:

                /**
                 * Constructor of Individual
                 *
                 */
                explicit Individual() : map_attrs() {
                }

                /**
                 * Get value id of an attribute
                 * @param id_attribute
                 * @return the id of the value
                 */
                [[nodiscard]] unsigned long long int getIdValue(unsigned long long int id_attribute) const {
                    return map_attrs.at(id_attribute);
                }

                /**
                 * Add a new couple (attribute_id, value_id) to the hashmap
                 * @param id the attribute_id
                 * @param value the value_id
                 */
                void addAttribute(unsigned long long int id, unsigned long long int value) {
                    map_attrs[id] = value;
                }

                /**
                 * Get the vector of couple (attribute_id, value_id)
                 * @return the attributes
                 */
                std::map<unsigned long long int, unsigned long long int> getAttributes() const {
                    return map_attrs;
                }

                /**
                 * Get the vector of couple (attribute_id, value_id)
                 * @return the attributes
                 */
                std::map<unsigned long long int, unsigned long long int> &getAttributes() {
                    return map_attrs;
                }

                /**
                 * get all the id attributes
                 * @return the attribute ids
                 */
                std::vector<unsigned long long int> getAttributeIds() const {
                    std::vector<unsigned long long int> ids;
                    for (auto const &pair: map_attrs) ids.push_back(pair.first);
                    return ids;
                }

            protected:
                std::map<unsigned long long int, unsigned long long int> map_attrs;
            };

            /**
             * To print a individual with output stream
             * @param os output stream
             * @param rule solution
             * @return output stream
             */
            std::ostream &operator<<(std::ostream &os, const Individual &individual) {
                /*
              for(unsigned long long int i = 0; i < individual.getAttributes().size(); i++)
                os << i << " -> " << individual.getAttributes()[i] << ",";*/
                for (auto const &pair: individual.getAttributes()) {
                    os << pair.first << " -> " << pair.second << ",";
                }
                return os;
            }
        }
    }
}
#endif //MH_BUILDER_INDIVIDUAL_H
