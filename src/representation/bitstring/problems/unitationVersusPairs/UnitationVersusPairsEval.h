/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_UNITATION_VERSUS_PAIRS_H
#define MH_BUILDER_UNITATION_VERSUS_PAIRS_H

#include "representation/bitstring/BitstringSolution.h"
#include "core/fitness/FitnessMax.h"
#include "core/Eval.h"


namespace representation::bitstring::problems::unitationVersusPairs {

    /**
     * Class representing an evaluator for the "Unitation versus Pairs" Problem
     * this problem is presented in DOI : 10.1109/ICEC.1994.350037
     * @tparam SOL solution type
     */
    template<typename SOL>
    class UnitationVersusPairsEval : public core::Eval<SOL> {
    public:
        /**
         * default constructor
         */
        UnitationVersusPairsEval() = default ;

        /**
         * default destructor
         */
        virtual ~UnitationVersusPairsEval() = default ;
        /**
         * Evaluate a bitstring solution
         * @param sol a solution
         */
        void operator()(SOL &sol) override {
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            objv.resize(0);
            objv.push_back(countOne(sol));
            objv.push_back(countPairs(sol));
            fit.objectives(objv);
            this->eval_counter++;
        }



        /**
         * Count the number of one in the solution
         * @param sol solution
         * @return the number of one in the solution
         */
        static int countOne(SOL &sol) {
            int p = 0;
            for (unsigned long long int j = 0; j < sol.size(); ++j) {
                if (sol[j])
                    p++;
            }
            return p;
        }
        /**
         * Count pairs in the solution, a pair either is a sequence of (0,1) or (1,0)
         * @param sol solution
         * @return the number of pairs in the solution
         */
        static int countPairs(SOL &sol) {
            int p = 0;
            for (unsigned long long int j = 1; j < sol.size(); ++j) {
                if (sol[j-1] != sol[j]) p++;
            }
            return p;
        }
    };
}


#endif //MH_BUILDER_ONE_MAX_EVAL_H
