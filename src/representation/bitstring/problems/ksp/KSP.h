/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KSP_H
#define MH_BUILDER_KSP_H
#include "representation/bitstring/BitstringSolution.h"
#include "core/fitness/FitnessMax.h"
#include <fstream>
namespace representation {
    namespace bitstring {
        namespace problems {
            namespace ksp {
                /*! Define type for a KSP solution */
                class KSPSol : public representation::bitstring::BitstringSolution<core::fitness::FitnessMax<int>> {
                public:
                    /**
                     * Construct a ksp solution of size n (all variables at 0)
                     * @param n size of bitstring
                     */
                    explicit KSPSol(unsigned long long int n = 0)
                            : representation::bitstring::BitstringSolution<core::fitness::FitnessMax<int>>(n),
                              weight(0) {}

                    /**
                     * Set current weight in the knapsack
                     * @param w the weight
                     */
                    void setWeight(int w) {
                        weight = w;
                    }

                    /**
                     * Get the current weight in the knwapsack
                     * @return weight
                     */
                    int getWeight() {
                        return weight;
                    }

                protected:
                    /*! current weight */
                    int weight;
                };

                /**
                 * Class representing a KSP (Knapsack Problem)
                 */
                class KSP {
                public:
                    /**
                     * Create a new KSP instance
                     * @param instanceFile a data files
                     */
                    explicit KSP(const std::string &instanceFile) {
                        readInstance(instanceFile);
                    }

                    /**
                     * Read an instance file
                     * @param instanceFile a data files
                     */
                    void readInstance(const std::string &instanceFile) {
                        std::string line, tmp;

                        std::ifstream input(instanceFile);
                        if (!input)
                            throw std::runtime_error("Error: unable to open benchmark file");

                        input >> n >> wmax;

                        profits.resize(n);
                        weights.resize(n);

                        for (unsigned long long int i = 0; i < n; ++i) {
                            input >> profits[i] >> weights[i];
                        }
                    }

                    /**
                     * Get the number of item
                     * @return the number of iter
                     */
                    unsigned long long int getN() {
                        return n;
                    }

                    /**
                     * Get the maximum knapsack capacity
                     * @return the maximum knapsack capacity
                     */
                    int getWmax() {
                        return wmax;
                    }

                    /**
                     * Get the profit of i^th item
                     * @param i the item i
                     * @return the profit of i^th item
                     */
                    int getProfit(unsigned long long int i) {
                        return profits[i];
                    }

                    /**
                     * Get the Weight of i^th item
                     * @param i the item i
                     * @return the Weight of i^th item
                     */
                    int getWeight(unsigned long long int i) {
                        return weights[i];
                    }

                protected:
                    unsigned long long int n;
                    int wmax;
                    std::vector<int> profits;
                    std::vector<int> weights;
                };
            }
        }
    }
}
#endif //MH_BUILDER_KSP_H
