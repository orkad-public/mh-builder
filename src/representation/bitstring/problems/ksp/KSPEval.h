/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KSPEVAL_H
#define MH_BUILDER_KSPEVAL_H

#include "representation/bitstring/BitstringSolution.h"
#include "representation/bitstring/problems/ksp/KSP.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "core/fitness/FitnessMax.h"
#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"

#include "core/Eval.h"
#include <fstream>


namespace representation::bitstring::problems::ksp {

    /**
     * Class representing an evaluator for Knapsack problem
     */
    class KSPEval : public core::Eval<KSPSol> {
    public:

        /**
         * Constructor of the evaluation
         * @param _ksp data for evaluator
         */
        explicit KSPEval(KSP &_ksp) : ksp(_ksp) {}

        /**
         * Evaluate a KSP solution
         * @param sol a solution
         */
        void operator()(KSPSol &sol) override {
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            auto cons = fit.constraints();
            objv.resize(1);
            cons.resize(1);

            profit(sol, objv, cons);

            fit.objectives(objv);
            fit.constraints(cons);

            if (cons[0] > ksp.getWmax())
                fit.invalidate();
            this->eval_counter++;
        }

        /**
         * Evaluation a KSP solution according to a neighbor
         * @param sol the solution
         * @param neighbor the neighbor
         */
        void operator()(KSPSol &sol,
                        opt::singleSolution::neighborhood::neighbor::Neighbor<KSPSol> &neighbor) {
            if (auto *flipNeighbor = dynamic_cast<representation::bitstring::neighborhood::neighbor::FlipNeighbor<KSPSol> *>(&neighbor)) {
                operator()(sol, *flipNeighbor);
            } else {
                operator()(sol);
            }
            this->eval_counter++;
        }

        /**
         * Evaluation a KSP solution according to a flip neighbor
         * @param sol the solution
         * @param neighbor the neighbor
         */
        void operator()(KSPSol &sol,
                        representation::bitstring::neighborhood::neighbor::FlipNeighbor<KSPSol> &neighbor) {
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            auto cons = fit.constraints();

            flip(sol, objv, cons, neighbor.getKey());

            fit.objectives(objv);
            fit.constraints(cons);

            if (cons[0] > ksp.getWmax()) {
                fit.invalidate();
            }
            this->eval_counter++;
        }

        /**
         * Compute the new fitness for a solution modification at index
         * @param sol solution
         * @param obj objectives
         * @param cons constraints
         * @param index the bit index that changed
         */
        void flip(KSPSol &sol, std::vector<int> &obj, std::vector<int> &cons, int index) {
            if (sol[index]) {
                cons[0] += ksp.getWeight(index);
                obj[0] += ksp.getProfit(index);
            } else {
                cons[0] -= ksp.getWeight(index);
                obj[0] -= ksp.getProfit(index);
            }
        }

        /**
         * Compute the profit and weight for a solution
         * @param sol the solution
         * @param obj objectives
         * @param cons constraints
         */
        void profit(KSPSol &sol, std::vector<int> &obj, std::vector<int> &cons) {
            int weight = 0, profit = 0;
            for (unsigned long long int i = 0; i < sol.size() && weight <= ksp.getWmax(); ++i) {
                if (sol[i]) {
                    profit += ksp.getProfit(i);
                    weight += ksp.getWeight(i);
                }
            }

            obj[0] = profit;
            cons[0] = weight;
        }

    protected:
        /*! KSP data */
        KSP ksp;
    };
}


#endif //MH_BUILDER_KSPEVAL_H
