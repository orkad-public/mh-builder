/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ONE_MAX_EVAL_H
#define MH_BUILDER_ONE_MAX_EVAL_H

#include "representation/bitstring/BitstringSolution.h"
#include "core/fitness/FitnessMax.h"
#include "core/Eval.h"


namespace representation::bitstring::problems::onemax {

    /**
     * Class representing an evaluator for OneMax problem
     * @tparam SOL solution type
     */
    template<typename SOL>
    class OneMaxEval : public core::Eval<SOL> {
    public:

        /**
         * Evaluate a OneMax solution
         * @param sol a solution
         */
        void operator()(SOL &sol) override {
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            objv.resize(0);
            objv.push_back(count(sol));
            fit.objectives(objv);
            this->eval_counter++;
        }

        /**
         * Compute the new fitness for a solution modification at index
         * @param sol solution
         * @param fit fitness before modification of the solution
         * @param index the bit to change
         * @return the new fitness
         */
        static int flip(SOL &sol, unsigned int fitness, int index) {
            if (sol[index])
                return fitness + 1;
            else
                return fitness - 1;
        }

        /**
         * Count the number of one in the solution
         * @param sol solution
         * @return the number of one in the solution
         */
        static int count(SOL &sol) {
            int p = 0;
            for (unsigned long long int j = 0; j < sol.size(); ++j) {
                if (sol[j])
                    p++;
            }
            return p;
        }
    };
}


#endif //MH_BUILDER_ONE_MAX_EVAL_H
