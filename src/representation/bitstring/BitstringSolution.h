/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BITSTRING_SOLUTION_H
#define MH_BUILDER_BITSTRING_SOLUTION_H

#include "representation/VectorSolution.h"


namespace representation::bitstring {
    /**
     * Class representing bitstring based solutions
     * @tparam FIT Fitness Type
     */
    template<typename FIT>
    class BitstringSolution : public representation::VectorSolution<FIT, unsigned int> {
    public:
        typedef unsigned int TYPE ;
        /**
         * Construct a bitstring solution of size n (all variables at 0)
         * @param n size of bitstring
         */
        explicit BitstringSolution(unsigned long long int n = 0)
                : representation::VectorSolution<FIT, unsigned int>(n) {
            for (unsigned long long int i = 0; i < n; i++)
                this->vec_.push_back(util::RNGHelper::get()->random(2)); // either 0 or 1
        }
        /**
         * reset the solution
         * all elements are set to 0
         */
        void reset() override {
            for (unsigned long long int i = 0; i < this->vec_.size(); ++i) {
                this->vec_[i] = 0;
            }
        }

    };

    /**
     * To print a solution with output stream
     * @tparam FIT Fitness type
     * @param os output stream
     * @param sol solution
     * @return output stream
     */
    template<class FIT>
    std::ostream &operator<<(std::ostream &os, const BitstringSolution<FIT> &sol) {
        sol.print(os);
        return os;
    }
}

#endif //MH_BUILDER_BITSTRING_SOLUTION_H
