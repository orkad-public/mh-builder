/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FLIP_NEIGHBOR_H
#define MH_BUILDER_FLIP_NEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

namespace representation::bitstring::neighborhood::neighbor {

    /**
     * Class representing a classic one-flip neighbor
     * principle : the solution s1 is a neighbor of the solution s2
     *             if there is only one bit of s1 that differs from s2
     * @tparam SOL Solution Type, which is a bitstring solution
     */
    template<typename SOL>
    class FlipNeighbor : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:
        /**
         * constructor, set the name of the neighbor
         */
         FlipNeighbor() { this->name="FlipNeighbor" ; }

        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            this->maxKey = sol.size();
        }

    protected:
        /**
         * Apply a flip of a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            sol.updateElement(this->key, ! sol.operator[](this->key));
        }
    };
}

#endif //MH_BUILDER_FLIPNEIGHBOR_H
