/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERATEDGREEDY_H
#define MH_BUILDER_ITERATEDGREEDY_H

#include "core/Algorithm.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include "opt/singleSolution/localsearch/IteratedLocalSearch.h"
#include "util/RNGHelper.h"
#include "representation/permutation/perturbation/DeconstructionReconstructionPerturbation.h"


namespace representation {
    namespace permutation {
        namespace localsearch {

            template<typename SOL>
            class IteratedGreedy : public opt::singleSolution::localsearch::IteratedLocalSearch<SOL> {
            public:

                using opt::singleSolution::localsearch::IteratedLocalSearch<SOL>::operator();

                /**
                 * Constructor of a Iterated LS
                 * @param _perturbation perturbation criterion
                 * @param _algorithm algorithm which need to be iterated
                 * @param _criterion stop criterion
                 */
                IteratedGreedy(core::Algorithm<SOL> &_localSearch,
                               core::Eval<SOL> &_eval,
                               core::Criterion<SOL> &_criterion,
                               unsigned long long int _d,
                               double _constT = 0.4) :

                        opt::singleSolution::localsearch::IteratedLocalSearch<SOL>(
                                deconstructionReconstructionPerturbation,
                                _localSearch,
                                _criterion,
                                [&](SOL &_in, SOL &_current, SOL &_tmp) {
                                    if (_current > _tmp) {
                                        _tmp = _current;
                                        if (_current > _in)
                                            _in = _current;
                                    } else if (accept(_current, _tmp)) {
                                        _current = _tmp;
                                    }
                                },
                                [](SOL &_in, SOL &_current) { _current = _in; },
                                [](SOL &_current, SOL &_tmp) { _tmp = _current; }),
                        deconstructionReconstructionPerturbation(&_eval, _d),
                        constT(_constT) {}

                virtual bool accept(SOL &_sol, SOL &_current) {
                    double proba = exp((_sol.fitness().scalar() - _current.fitness().scalar()) / constT);
                    return util::RNGHelper::get()->uniform() < proba;
                }

            protected:
                representation::permutation::perturbation::DeconstructionReconstructionPerturbation<SOL> deconstructionReconstructionPerturbation;
                double constT;

            };
        }
    }
}

#endif //MH_BUILDER_ITERATEDGREEDY_H
