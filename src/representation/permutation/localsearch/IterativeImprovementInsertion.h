/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERATIVEIMPROVEMENTINSERTION_H
#define MH_BUILDER_ITERATIVEIMPROVEMENTINSERTION_H


#include "core/Eval.h"
#include "core/Criterion.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"
#include "util/RNGHelper.h"
#include "representation/permutation/perturbation/DeconstructionReconstructionPerturbation.h"
#include "representation/permutation/algorithm/BestInsertion.h"

namespace representation {
    namespace permutation {
        namespace localsearch {
            /**
             * Class representing an Iterative Improvement Insertion (
             * Source : A simple and effective iterated greedy algorithm for the permutation flowshop scheduling problem
             * R Ruiz, T Stützle - European journal of operational research, 2007 - Elsevier
             * @tparam SOL
             */
            template<typename SOL>
            class IterativeImprovementInsertion : public opt::singleSolution::localsearch::LocalSearch<SOL> {
            public:

                using opt::singleSolution::localsearch::LocalSearch<SOL>::operator();

                /**
                 * Constructor of an Hillclimbing
                 * @param _neighborhoodExplorer the neighborhood explorer
                 * @param _eval the evaluation function
                 */
                explicit IterativeImprovementInsertion(core::Eval<SOL> &_eval, core::Criterion<SOL> &_criterion)
                        : opt::singleSolution::localsearch::LocalSearch<SOL>(_eval), criterion(_criterion) {}

                void init(const SOL &sol) override {
                    opt::singleSolution::localsearch::LocalSearch<SOL>::init(sol);
                    if (indexOfJobs.size() < sol.size()) {
                        for (unsigned long long int i = indexOfJobs.size(); i < sol.size(); ++i) {
                            indexOfJobs.push_back(i);
                        }
                    }
                }

                void operator()(SOL &_in, core::Criterion<SOL> &_criterion) override {
                    this->init(_in);
                    bool improve = true;
                    while (improve && _criterion() && criterion()) {
                        improve = false;
                        util::RNGHelper::get()->shuffle(indexOfJobs.begin(), indexOfJobs.end());
                        for (unsigned long long int i = 0; i < _in.size(); i++) {
                            // Search job indexOfJobs[i] and erased it
                            unsigned long long int k = 0;
                            while (this->currentSol[k] != indexOfJobs[i]) k++;
                            this->currentSol.erase(this->currentSol.begin() + k);
                            representation::permutation::algorithm::BestInsertion<SOL>::bestInsert(this->currentSol, indexOfJobs[i], this->eval);
                            if (this->currentSol > this->bestSol) {
                                improve = true;
                                this->bestSol = this->currentSol;
                            }
                            this->checkpoint->operator()(this->currentSol);
                        }
                    }

                    _in = this->bestSol;
                }

            protected:
                std::vector<unsigned long long int> indexOfJobs;
                core::Criterion<SOL> &criterion;
            };
        }
    }
}
#endif //MH_BUILDER_ITERATIVEIMPROVEMENTINSERTION_H
