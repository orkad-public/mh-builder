/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ADJSWAPNEIGHBOR_H
#define MH_BUILDER_ADJSWAPNEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

namespace representation::permutation::neighborhood::neighbor {
    /**
     * Class representing a classic Adjacent Swap neighbor
     * @tparam SOL Solution Type
     */
    template<typename SOL>
    class AdjSwapNeighbor : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:
        /**
         * constructor, set the name of the neighbor
         */
        AdjSwapNeighbor() { this->name="AdjSwapNeighbor" ; }

        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            this->maxKey = sol.size();
        }

    protected:
        /**
         * Apply an adjacent swap on a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            if (this->key < sol.size() - 1)
                sol.swap(this->key, this->key + 1);
            else
                sol.swap(0, this->key);
        }

    };
}

#endif //MH_BUILDER_ADJSWAPNEIGHBOR_H
