/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SWAPNEIGHBOR_H
#define MH_BUILDER_SWAPNEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

namespace representation::permutation::neighborhood::neighbor {
    /**
     * Class representing a classic Swap neighbor operator
     * @tparam SOL Solution Type
     */
    template<typename SOL>
    class SwapNeighbor : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:
        /*
         * constructor, set the name of the neighbor
         */
        SwapNeighbor() { this->name = "SwapNeighbor"; }

        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            this->maxKey = sol.size() * (sol.size() - 1) / 2;
        }

    protected:
        /**
         * Apply a swap on a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            auto n = static_cast<unsigned long long int>((1 + sqrt(1 + 8 * this->key)) / 2);
            auto first = this->key - (n - 1) * n / 2;
            auto second = sol.size() - n + first;
            sol.swap(first, second);
        }

        /**
         * Apply a move back on a solution
         * @param sol the solution
         */
        void move_back(SOL &sol) override {
            move(sol); // involution function
        }

    };
}


#endif //MH_BUILDER_SWAPNEIGHBOR_H
