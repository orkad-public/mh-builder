/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TWOOPTNEIGHBOR_H
#define MH_BUILDER_TWOOPTNEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include <cmath>

namespace representation::permutation::neighborhood::neighbor {

    /**
     * Class representing a classic Two opt neighbor
     * principle:
     *  select two non-adjacent edges (x_i, x_i+1) and (x_j,x_j+1)
     *  then, substitute these edges by (x_i,x_j) and (x_i+1, x_j+1)
     * @tparam SOL Solution Type
     */
    template<typename SOL>
    class TwoOptNeighbor : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:
        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            this->maxKey = sol.size() * (sol.size() - 3) / 2;
        }


    protected:
        /**
         * Apply a two opt on a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            unsigned long long int first, second ;
            auto x = static_cast<unsigned long long int>((3 + sqrt(9 + 8 * this->key)) / 2);
            first = this->key - (x * (x - 3) / 2) + 1;
            second = first + sol.size() - x;

            sol.invert(first, second);
        }

    };
}

#endif //MH_BUILDER_TWOOPTNEIGHBOR_H
