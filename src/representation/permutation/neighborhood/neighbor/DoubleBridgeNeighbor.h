/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DOUBLEBRIDGENEIGHBOR_H
#define MH_BUILDER_DOUBLEBRIDGENEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include <vector>
#include <algorithm>

namespace representation::permutation::neighborhood::neighbor {

    /**
     * Class representing a double bridge neighbor
     * Principle: the solution is divided into four parts : { A, B , C, D}
     * next, some parts are moved as following : { A, D, C, B}
     * @tparam SOL Solution Type
     */
    template<typename SOL>
    class DoubleBridgeNeighbor : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:

        DoubleBridgeNeighbor() {
            this->name = "DoubleBridge";
        }

        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            auto n = sol.size();
            this->maxKey = ((n-3)*(n-2)*(n-1)) / 6 ;
        }

    protected:
        /**
         * Apply a double bridge move  on a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            auto n = sol.size();
            auto found=false;
            unsigned long long int deb_B=1, deb_C=2, deb_D=3 ; // start index for segments B,C,D (A always equals to 0)
            unsigned long long int cpt=-1 ;
            // find deb_B,deb_C,deb_D indexes that delimits segments A,B,C,D of sol according to current key
            for (unsigned long long int _deb_B =1 ; _deb_B <=(n-3) &&!found; _deb_B++)
                for (unsigned long long int _deb_C=_deb_B+1; _deb_C<=(n-2)&& !found;_deb_C++)
                    for (unsigned long long int _deb_D=_deb_C+1; _deb_D<=(n-1) &&!found;_deb_D++) {
                        cpt++ ;
                        if (this->key == cpt ) { found = true; deb_B= _deb_B ; deb_C=_deb_C; deb_D=_deb_D ; }
                    }
            // before move { B, C, D} deb_X designates the start index of segment X (deb_A = 0)
            std::rotate(sol.begin()+deb_B,sol.begin()+deb_D,sol.end()); // move segment D after segment A: { A,C,D,B}
            std::rotate(sol.begin()+deb_B+n-deb_D,sol.begin()+deb_C+n-deb_D,sol.end()); // move segment C before B { A, D , C, B}
        }
    };
}

#endif //MH_BUILDER_DOUBLEBRIDGENEIGHBOR_H
