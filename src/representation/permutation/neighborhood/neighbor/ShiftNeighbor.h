/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SHIFT_NEIGHBOR_H
#define MH_BUILDER_SHIFT_NEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

namespace representation::permutation::neighborhood::neighbor {
    /**
     * Class representing a classic shift neighbor:
     * "the shift operator consists in choosing two positions i and j,
     * insert the character located at position j into position i,
     * and then shift all characters formerly located between i (included) and j (excluded)
     * by one step to the right if i < j, or one step to the left otherwise."
     * @tparam SOL Solution Type
     */
    template<typename SOL>
    class ShiftNeighbor : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:
        /**
         * constructor, set the name of the neighbor
         */
        ShiftNeighbor() { this->name="ShiftNeighbor" ; }
        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            this->maxKey = (sol.size() - 1) * (sol.size() - 1);
        }

    protected:
        /**
         * Apply a shift on a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            unsigned long long int first, second ;
            if (this->key <= sol.size() - 2) {
                first = 0;
                second = this->key + 1;
            } else {
                first = (this->key - 1) / (sol.size() - 2);
                second = (this->key - 1) % (sol.size() - 2);
                if (second >= first - 1)
                    second += 2;
            }
            sol.move(first, second);
        }
    };
}
#endif //MH_BUILDER_SHIFT_NEIGHBOR_H
