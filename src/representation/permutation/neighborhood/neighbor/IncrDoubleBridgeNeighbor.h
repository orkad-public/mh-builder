/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_INCRDOUBLEBRIDGENEIGHBOR_H
#define MH_BUILDER_INCRDOUBLEBRIDGENEIGHBOR_H

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

namespace representation::permutation::neighborhood::neighbor {

    /**
     * Class representing a classic Two opt neighbor
     * @tparam SOL Solution Type
     */
    template<typename SOL>
    class IncrDoubleBridgeNeighbor
            : public opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> {
    public:

        IncrDoubleBridgeNeighbor() {
            this->name = "IncrDoubleBridge";
            this->data.resize(2);
        }

        /**
         * Initialize the maximum key according to the size of the solution
         * @param sol the solution
         */
        void init(const SOL &sol) override {
            this->maxKey = sol.size() * (sol.size() - 1);
        }

    protected:
        /**
         * Apply a two opt on a solution
         * @param sol the solution
         */
        void move(SOL &sol) override {
            if (this->key < sol.size() - 1) {
                this->data[0] = 0;
                this->data[1] = this->key + 1;
            } else {
                this->data[0] = this->key / (sol.size() - 1);
                this->data[1] = this->key % (sol.size() - 1);
                if (this->data[1] >= this->data[0])
                    this->data[1] += 1;
            }

            if (this->data[0] > this->data[1])
                std::swap(this->data[0], this->data[1]);

            isApply = false;
        }

        /**
         * Apply a move back on a solution
         * @param sol the solution
         */
        void move_back(SOL &sol) override {
            if (isApply) {
                sol.invert(this->data[0], this->data[1] - 1);
            }
        }

        void apply(SOL &sol) override {
            sol.invert(this->data[0], this->data[1] - 1, false);
            isApply = true;
        }

        bool isApply;
    private:
        /*! Data of neighbor */
        std::vector<unsigned int> data;

    };
}

#endif //MH_BUILDER_INCRDOUBLEBRIDGENEIGHBOR_H
