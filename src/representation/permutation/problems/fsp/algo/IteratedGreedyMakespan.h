/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERATEDGREEDYMAKESPAN_H
#define MH_BUILDER_ITERATEDGREEDYMAKESPAN_H

#include "representation/permutation/localsearch/IteratedGreedy.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"


namespace representation {
    namespace permutation {
        namespace problems {
            namespace fsp {
                namespace algo {
                    template<typename SOL>
                    class IteratedGreedyMakespan
                            : public representation::permutation::localsearch::IteratedGreedy<SOL> {
                    public:

                        IteratedGreedyMakespan(core::Eval<SOL> &_eval,
                                               core::Criterion<SOL> &_criterion,
                                               opt::singleSolution::localsearch::LocalSearch<SOL> &_localSearch) :
                                representation::permutation::localsearch::IteratedGreedy<SOL>(_localSearch, _eval,
                                                                                              _criterion, 4) {
                        }

                    };
                }
            }
        }
    }
}
#endif //MH_BUILDER_ITERATEDGREEDYMAKESPAN_H
