/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEH_H
#define MH_BUILDER_NEH_H

#include "core/Eval.h"
#include "core/Algorithm.h"
#include "representation/permutation/problems/fsp/FSP.h"

#include "representation/permutation/algorithm/BestInsertion.h"

namespace representation::permutation::problems::fsp::algo {
    template<typename SOL>
    class NEH : core::Algorithm<SOL> {
    public:
        typedef typename SOL::FITNESS FIT;

        explicit NEH(FSP &_instance, core::Eval<SOL> &_eval) : instance(_instance), eval(_eval) {}

        void operator()(SOL &sol) override {
            if (!this->criterion->operator()()) return; // stop criterion test
            SOL best, tmp;
            std::vector<unsigned long long int> indexes;
            std::vector<unsigned long long int> fitness;

            // init
            for (unsigned long long int i = 0; i < instance.getN(); i++) {
                fitness.push_back(instance.getTotalProcessingTime(i));
                indexes.push_back(i);
            }

            std::sort(indexes.begin(), indexes.end(), [&](int i, int j) {
                return fitness[i] < fitness[j];
            });

            // first member
            best.push_back(indexes[0]);

            // for each other members
            for (unsigned long long int i = 1; i < instance.getN(); i++)
                // insert at the best position
                representation::permutation::algorithm::BestInsertion<SOL>::bestInsert(best, indexes[i],
                                                                                       eval);

            eval(best);
            sol = best;
        };

        void operator()(SOL &sol, FSP &_instance) {
            instance = _instance;
            operator()(sol);
        }

    protected:
        FSP &instance;
        core::Eval<SOL> &eval;
    };
}
#endif //MH_BUILDER_NEH_H
