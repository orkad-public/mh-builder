/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Asuncion Gomez and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FSPEVAL_H
#define MH_BUILDER_FSPEVAL_H

#include "core/Eval.h"
#include "FSP.h"

namespace representation::permutation::problems::fsp {
    /**
     * Class representing an evaluator for Flowshop problem
     */
    template<typename FSPSol>
    class FSPEval : public core::Eval<FSPSol> {
    public:

        /**
         * Constructor of the evaluation
         * @param _fsp data for evaluator
         */
        explicit FSPEval(FSP &_fsp) : fsp(_fsp) {
            completion_time.resize(fsp.getM());
            for (unsigned int i = 0; i < fsp.getM(); i++)
                completion_time[i].resize(fsp.getN());
        }

        /**
         * Evaluate a FSP solution
         * @param sol a solution
         */
        void operator()(FSPSol &sol) override {
            /*! fitness solution */
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            computeCompletionTime(sol);

            objv.resize(1);

            /*! makespan */
            objv[0] = maxCompTime(sol);

            fit.objectives(objv);

            this->eval_counter++;
        }

        virtual int maxCompTime(const FSPSol &_fs) {
            int k = _fs.size();
            return completion_time[fsp.getM() - 1][_fs[k - 1]];
        }

    protected:
        /*! FSP data */
        FSP fsp;
        std::vector<std::vector<unsigned long long int>> completion_time;
        bool first_eval = true;
        std::vector<unsigned long long int> last_sol_eval;

        virtual void computeFullCompletionTime(const FSPSol &_fs) {


            unsigned long long int k = _fs.size();
            completion_time[0][_fs[0]] = fsp.getProcessingTime(0, _fs[0]);
            for (unsigned long long int i = 1; i < k; i++)
                completion_time[0][_fs[i]] =
                        completion_time[0][_fs[i - 1]] + fsp.getProcessingTime(0, _fs[i]);
            for (unsigned int j = 1; j < fsp.getM(); j++)
                completion_time[j][_fs[0]] =
                        completion_time[j - 1][_fs[0]] + fsp.getProcessingTime(j, _fs[0]);
            for (unsigned int j = 1; j < fsp.getM(); j++)
                for (unsigned int i = 1; i < k; i++)
                    completion_time[j][_fs[i]] =
                            std::max(completion_time[j][_fs[i - 1]], completion_time[j - 1][_fs[i]]) +
                            fsp.getProcessingTime(j, _fs[i]);
        }

        virtual void computeCompletionTime(const FSPSol &_fs) {
            unsigned int k = _fs.size();
            if (first_eval) {
                first_eval = false;
                computeFullCompletionTime(_fs);
                last_sol_eval.resize(k);
                for (unsigned int i = 0; i < k; i++)
                    last_sol_eval[i] = _fs[i];
                return;
            }
            // compare
            unsigned int prefix = 0;
            while (prefix < k && last_sol_eval[prefix] == _fs[prefix])
                prefix++;
            // compute
            if (prefix == 0) {
                computeFullCompletionTime(_fs);
            } else {
                completion_time[0][_fs[0]] = fsp.getProcessingTime(0, _fs[0]);
                for (unsigned int i = prefix; i < k; i++)
                    completion_time[0][_fs[i]] =
                            completion_time[0][_fs[i - 1]] + fsp.getProcessingTime(0, _fs[i]);
                for (unsigned int j = 1; j < fsp.getM(); j++)
                    for (unsigned int i = prefix; i < k; i++)
                        completion_time[j][_fs[i]] =
                                std::max(completion_time[j][_fs[i - 1]], completion_time[j - 1][_fs[i]]) +
                                fsp.getProcessingTime(j, _fs[i]);
            }
            // store
            if (last_sol_eval.size() != k)
                last_sol_eval.resize(k);
            for (unsigned int i = prefix; i < k; i++)
                last_sol_eval[i] = _fs[i];
        }
    };
}

#endif //MH_BUILDER_FSPEVAL_H
