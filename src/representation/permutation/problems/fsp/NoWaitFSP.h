/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NOWAITFSP_H
#define MH_BUILDER_NOWAITFSP_H


namespace representation::permutation::problems::fsp {

#include "representation/permutation/problems/fsp/FSP.h"
#include <algorithm>

    /**
        * Class representing a No-Wait FlowShop Problem) instance
    */
class NoWaitFSP : public representation::permutation::problems::fsp::FSP {
    public:
        explicit NoWaitFSP(const std::string &instanceFile) : FSP(instanceFile) {
            this->initDelay();
        }

        /**
         * initialize the delay matrix d (Bertolissi 2000)
         * d[i][j] corresponds to the delay between the start of job i and the start of following job j
         * on the same first machine
         */
        void initDelay() {
            d.resize(this->N);
            for (unsigned long long int i = 0; i < this->N; i++) {
                d[i].resize(this->N);
                for (unsigned long long int j = 0; j < this->N; j++) {
                    if (i != j) {
                        this->d[i][j] = this->processing_time[0][i];
                        int maxi = 0;
                        for (unsigned long long int r = 0; r < this->M; r++) {
                            int nb = 0;
                            for (unsigned long long int h = 1; h <= r; h++)
                                nb = nb + this->processing_time[h][i];
                            for (unsigned long long int h = 0; h < r; h++)
                                nb = nb - this->processing_time[h][j];
                            maxi = std::max(maxi, nb);
                        }
                        this->d[i][j] += maxi;
                    }
                }
            }
        }

        /**
         *
         * @param i job i
         * @param j job j following i
         * @return the delay between the start of job i and the start of following job j on the first machine
         */
        unsigned long long int
        delay(const unsigned long long int i, const unsigned long long int j) const {
            return this->d[i][j];
        }

    protected:
        /*! delay matrix d (Bertolissi 2000)
         * d[i][j] corresponds to the delay between the start of job i and the start of following job j
         * on the same first machine*/
        std::vector<std::vector<unsigned long long int>> d;
    };
}
#endif //MH_BUILDER_NOWAITFSP_H
