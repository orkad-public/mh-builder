/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NOWAITFSPEVAL_H
#define MH_BUILDER_NOWAITFSPEVAL_H

#include "core/Eval.h"
#include "NoWaitFSP.h"

namespace representation::permutation::problems::fsp {
    /**
     * Class representing an evaluator for Flowshop problem
     */
    template<typename FSPSol>
    class NoWaitFSPEval : public core::Eval<FSPSol> {
    public:

        /**
        ** Constructor of the evaluation
        ** @param _fsp data for evaluator
        */
        explicit NoWaitFSPEval(NoWaitFSP &_fsp) : fsp(_fsp) {
        }

        /**
         * Evaluate a NWFSP solution
         * @param sol a solution
         */
        void operator()(FSPSol &sol) override {
            /*! fitness solution */
            auto &fit = sol.fitness();
            auto objv = fit.objectives();

            /** single objective : makespan **/
            objv.resize(1);
            objv[0] = makespan(sol);
            fit.objectives(objv);
            this->eval_counter++;
        }

        virtual unsigned long long int makespan(const FSPSol &_fs) const {
            return completionTime(_fs.size() - 1, _fs);
        }

    protected:
        /*! FSP data */
        NoWaitFSP fsp;


        virtual unsigned long long int completionTime(const unsigned long long int pos, const FSPSol &_fs) const {
            if (pos == 0) return fsp.getTotalProcessingTime(_fs[0]);
            else {
                int time = 0;
                for (unsigned long long int k = 1; k <= pos; k++) time += fsp.delay(_fs[k - 1], _fs[k]);
                return time + fsp.getTotalProcessingTime(_fs[pos]);
            }
        }

    };
}
#endif //MH_BUILDER_NOWAITFSPEVAL_H
