/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TWOGREEDY_H
#define MH_BUILDER_TWOGREEDY_H


#include <vector>
#include "core/Algorithm.h"
#include "core/Criterion.h"
#include "representation/permutation/problems/tsp/TSP.h"
#include "util/RNGHelper.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"
#include "opt/eval/AggregationEval.h"
#include "SimpleGreedy.h"


namespace representation::permutation::problems::tsp::algo {
    template<typename ARCH, typename SOL>
    class TwoGreedyImpr : public core::Algorithm<ARCH> {
    public:

        explicit TwoGreedyImpr(
                TSP &_instance1,
                TSP &_instance2,
                opt::singleSolution::localsearch::LocalSearch<SOL> &_localSearch,
                opt::eval::AggregationEval<SOL> &_eval,
                core::Criterion<SOL> &_criterion) :
                instance1(_instance1),
                instance2(_instance2),
                localSearch(_localSearch),
                eval(_eval),
                criterion(_criterion) {}

        void operator()(ARCH &arch) {
            SOL sol;

            SimpleGreedy<SOL> simpleGreedy(instance1);
            //FIRST OBJECTIVE
            criterion.init();
            simpleGreedy(sol, instance1);
            eval.init({1, 0});
            eval(sol);
            localSearch(sol, criterion);
            arch.push_back(sol);

            //SECOND OBJECTIVE
            criterion.init();
            simpleGreedy(sol, instance2);
            eval.init({0, 1});
            eval(sol);
            localSearch(sol, criterion);
            arch.push_back(sol);

            //REMOVE SCALAR
            for (SOL &solution: arch)
                solution.fitness().invalidate_scalar();

        };

    protected:
        tsp::TSP &instance1;
        tsp::TSP &instance2;
        opt::singleSolution::localsearch::LocalSearch<SOL> &localSearch;
        opt::eval::AggregationEval<SOL> &eval;
        core::Criterion<SOL> &criterion;
    };
}


#endif //MH_BUILDER_TWOGREEDY_H
