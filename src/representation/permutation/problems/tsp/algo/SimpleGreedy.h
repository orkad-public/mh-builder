/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef TSP_SIMPLEGREEDY_H
#define TSP_SIMPLEGREEDY_H

#include <vector>
#include "core/Algorithm.h"
#include "representation/permutation/problems/tsp/TSP.h"
#include "util/RNGHelper.h"


namespace representation::permutation::problems::tsp::algo {
    template<typename SOL>
    class SimpleGreedy : public core::Algorithm<SOL> {
    public:

        explicit SimpleGreedy(TSP &_instance) : instance(_instance) {}

        void operator()(SOL &sol) {
            sol.clear();

            std::vector<int> v;
            unsigned long long int pos;
            unsigned long long int n = instance.getMatrixSize();
            for (unsigned long long int i = 0; i < n; i++) {
                v.push_back(i);
            }

            pos = static_cast<unsigned long long int>(util::RNGHelper::get()->random(n));

            sol.push_back(v[pos]);
            v.erase(v.begin() + pos);

            while (!v.empty()) {
                pos = 0;
                for (unsigned long long int i = 1; i < v.size(); i++)
                    if (instance.getDistance(sol[sol.size() - 1], v[i]) <
                        instance.getDistance(sol[sol.size() - 1], v[pos]))
                        pos = i;
                sol.push_back(v[pos]);
                v.erase(v.begin() + pos);

            }
            canonize(sol);
        };


        void canonize(SOL &sol) {
            while (sol[0] != 0)
                sol.move(instance.getNumberOfCities() - 1, 0, false);


            unsigned long long pos_1 = 0, pos_2 = 0;
            for (unsigned long long i = 0; i < instance.getNumberOfCities(); i++) {
                if (sol[i] == 1)
                    pos_1 = i;
                if (sol[i] == 2)
                    pos_2 = i;
            }
            if (pos_2 < pos_1) {
                for (unsigned long long i = 1; i <= instance.getNumberOfCities() / 2; i++)
                    sol.swap(i, instance.getNumberOfCities() - i, false);

            }

        }


        void operator()(SOL &sol, TSP &_instance) {
            instance = _instance;
            operator()(sol);
        }

    protected:
        tsp::TSP &instance;
    };
}


#endif //TSP_SIMPLEGREEDY_H
