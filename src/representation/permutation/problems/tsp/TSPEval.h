/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TSPEVAL_H
#define MH_BUILDER_TSPEVAL_H

#include "TSP.h"
#include "core/Eval.h"
#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"


namespace representation::permutation::problems::tsp {
    template<typename TSPSOL>
    class TSPEval : public core::Eval<TSPSOL> {
    public:
        explicit TSPEval(TSP &_tsp) : tsp(_tsp) {}

        /**
         * Evaluate a TSP solution
         * @param sol a solution
         */
        void operator()(TSPSOL &sol) {
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            objv[0] = totalDistance(sol);
            fit.objectives(objv);
        }

        /**
         * Evaluation a TSP solution according to a neighbor
         * @param sol the solution
         * @param neighbor the neighbor
         */
        void
        operator()(TSPSOL &sol, opt::singleSolution::neighborhood::neighbor::Neighbor<TSPSOL> &neighbor) {
            if (neighbor.getName() == "DOUBLEBRIDGE") {
                auto &fit = sol.fitness();
                auto objv = fit.objectives();
                objv[0] = two_opt(sol, objv[0], neighbor.getData()->at(0), neighbor.getData()->at(1));
                fit.objectives(objv);
            } else if (neighbor.getName() == "INCRDOUBLEBRIDGE") {
                auto &fit = sol.fitness();
                auto objv = fit.objectives();
                objv[0] = smart_two_opt(sol, objv[0], neighbor.getData()->at(0), neighbor.getData()->at(1));
                fit.objectives(objv);
            } else {
                operator()(sol);
            }

        }

        double smart_two_opt(TSPSOL &sol, double dist, unsigned long long int i, unsigned long long int j) {
            dist = dist
                   - tsp.getDistance(sol[(i != 0) ? (i - 1) : (tsp.getNumberOfCities() - 1)], sol[i])
                   - tsp.getDistance(sol[j - 1], sol[j])
                   + tsp.getDistance(sol[i], sol[j])
                   + tsp.getDistance(sol[(i != 0) ? (i - 1) : (tsp.getNumberOfCities() - 1)], sol[j - 1]);
            return dist;
        }

        double two_opt(TSPSOL &sol, double dist, unsigned long long int i, unsigned long long int j) {
            dist = dist
                   + tsp.getDistance(sol[(i != 0) ? (i - 1) : (tsp.getNumberOfCities() - 1)], sol[i])
                   + tsp.getDistance(sol[j - 1], sol[j])
                   - tsp.getDistance(sol[i], sol[j])
                   - tsp.getDistance(sol[(i != 0) ? (i - 1) : (tsp.getNumberOfCities() - 1)], sol[j - 1]);
            return dist;
        }


        double totalDistance(const TSPSOL &sol) {
            double dist = 0;
            for (unsigned long long int i = 0; i < (sol.size() - 1); i++) {
                dist += tsp.getDistance(sol[i], sol[i + 1]);
            }
            dist += tsp.getDistance(sol[sol.size() - 1], sol[0]);
            return dist;
        }

        const std::vector<double> getMinMax() {
            std::vector<double> bounds;
            bounds.push_back(lowerDistance());
            bounds.push_back(upperDistance());
            return bounds;
        }

    protected:
        TSP tsp;

        double lowerDistance() {
            double min = tsp.getDistance(0, 1);
            for (unsigned long long int i = 0; i < tsp.getNumberOfCities(); i++)
                for (unsigned long long int j = 0; j < tsp.getNumberOfCities(); j++)
                    if (i != j && tsp.getDistance(i, j) < min)
                        min = tsp.getDistance(i, j);
            return min * tsp.getNumberOfCities();
        }

        double upperDistance() {
            double max = tsp.getDistance(0, 1);
            for (unsigned long long int i = 0; i < tsp.getNumberOfCities(); i++)
                for (unsigned long long int j = 0; j < tsp.getNumberOfCities(); j++)
                    if (i != j && tsp.getDistance(i, j) > max)
                        max = tsp.getDistance(i, j);
            return max * tsp.getNumberOfCities();
        }
    };
}


#endif //TSP_TSPEval_H
