/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef AMH_EXTENDS_TSPEvalBiObjective_H
#define AMH_EXTENDS_TSPEvalBiObjective_H

#include "TSP.h"
#include "core/Eval.h"
#include "TSPEval.h"
#include "opt/singleSolution/neighborhood/neighbor/Neighbor.h"


namespace representation::permutation::problems::tsp {

    template<typename TSPSOL>
    class TSPEvalBiObjective : public core::Eval<TSPSOL> {
    public:
        explicit TSPEvalBiObjective(const TSPEval<TSPSOL> _instance1, const TSPEval<TSPSOL> _instance2)
                :
                eval1(_instance1),
                eval2(_instance2) {}

        void operator()(TSPSOL &sol) {
            auto &fit = sol.fitness();
            auto objv = fit.objectives();
            objv.resize(2);
            objv[0] = eval1.totalDistance(sol);
            objv[1] = eval2.totalDistance(sol);
            fit.objectives(objv);
        }

        /**
         * Evaluation a TSP solution according to a neighbor
         * @param sol the solution
         * @param neighbor the neighbor
         */
        void
        operator()(TSPSOL &sol, opt::singleSolution::neighborhood::neighbor::Neighbor<TSPSOL> &neighbor) {
            if (neighbor.getName() == "DOUBLEBRIDGE") {
                auto &fit = sol.fitness();
                auto objv = fit.objectives();
                objv[0] = eval1.two_opt(sol, objv[0], neighbor.getData()->at(0), neighbor.getData()->at(1));
                objv[1] = eval2.two_opt(sol, objv[0], neighbor.getData()->at(0), neighbor.getData()->at(1));
                fit.objectives(objv);
            } else if (neighbor.getName() == "INCRDOUBLEBRIDGE") {
                auto &fit = sol.fitness();
                auto objv = fit.objectives();
                objv[0] = eval1.smart_two_opt(sol, objv[0], neighbor.getData()->at(0),
                                              neighbor.getData()->at(1));
                objv[0] = eval2.smart_two_opt(sol, objv[0], neighbor.getData()->at(0),
                                              neighbor.getData()->at(1));
                fit.objectives(objv);
            } else {
                operator()(sol);
            }
        }

        const std::vector<std::pair<double, double>> getMinMax() {
            std::vector<std::pair<double, double>> bounds;
            auto tmp = eval1.getMinMax();
            bounds.push_back(std::pair<double, double>(tmp[0], tmp[1]));
            tmp = eval2.getMinMax();
            bounds.push_back(std::pair<double, double>(tmp[0], tmp[1]));
            return bounds;
        }

    protected:
        TSPEval<TSPSOL> eval1;
        TSPEval<TSPSOL> eval2;
    };
}


#endif //AMH_EXTENDS_TSPEvalBiObjective_H
