/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TSP_H
#define MH_BUILDER_TSP_H

#include "representation/permutation/PermutationSolution.h"
#include <fstream>
#include "util/StringHelper.h"


namespace representation::permutation::problems::tsp {


    /**
     * Structure of a City
     */
    typedef struct {
        /*! id of the City */
        int id;
        /*! x-coordinate */
        double x;
        /*! y-coordinate */
        double y;
    } City;

    /*! Define type for a TSP solution */
    template<typename FIT>
    class TSPSol : public representation::permutation::PermutationSolution<FIT> {
    public:
        /**
         * Construct a tsp solution of size n
         * @param n size of permutation (number of jobs)
         */
        explicit TSPSol(unsigned long long int n = 0)
                : representation::permutation::PermutationSolution<FIT>(n) {}
    };

    /**
     * Class representing a TSP (Traveling Salesman Problem)
     */
    class TSP {
    public:

        /**
         * Create a new TSP instance
         * @param instanceFile a data files from TSPLIB
         */
        explicit TSP(const std::string &instanceFile) {
            readInstance(instanceFile);
        }

        /**
         * Create a new TSP instance randomly
         * @param _n  the size of the TSP
         * @param _seed the random seed
         */
        explicit TSP(int _n, int _seed) {
            generate(_n, _seed);
        }

        /**
         * create a new TSP instance from a given distance matrix
         * @param the distance matrix
         * @param nb_cities
         */
        explicit TSP(std::vector<std::vector<double>> distanceMatrix) {
            this->initInstanceFromDistanceMatrix(distanceMatrix);
        }
        void initInstanceFromDistanceMatrix(std::vector<std::vector<double>> v) {
            this->numberOfCities = v.size();
            this->distanceMatrix = v;
        }




        /**
         * Read an instance file from TSPLIB
         * @param instanceFile a data files from TSPLIB
         */
        void readInstance(const std::string &instanceFile) {
            std::string line;
            std::string tmp;

            std::ifstream input(instanceFile);
            if (!input)
                throw std::runtime_error("Error: unable to open benchmark file");

            //Get Dimension
            while (line.rfind("DIMENSION", 0) != 0)
                std::getline(input, line);

            std::vector<std::string> data = util::StringHelper::get()->split(line, " ");
            numberOfCities = static_cast<unsigned long long int>(std::stoi(data[data.size() - 1]));


            //Get Edge_weight_type
            while (line.rfind("EDGE_WEIGHT_TYPE", 0) != 0)
                std::getline(input, line);

            data = util::StringHelper::split(line, " ");
            tmp = data[data.size() - 1];

            //Parse
            if (tmp.rfind("EXPLICIT", 0) == 0) {
                distanceMatrix.resize(numberOfCities);
                // skip file info
                while (line.rfind("START", 0) != 0 && line.rfind("EDGE_WEIGHT_FORMAT", 0) != 0) {
                    std::getline(input, line);
                }
                data = util::StringHelper::split(line, " ");
                tmp = data[data.size() - 1];
                if (tmp.rfind("UPPER_DIAG_ROW", 0) == 0) {
                    std::getline(input, line);
                    for (unsigned long long int i = 0; i < numberOfCities; i++) {
                        distanceMatrix[i].resize(numberOfCities);
                    }
                    for (unsigned long long int i = 0; i < numberOfCities; i++) {
                        for (unsigned long long int j = i + 1; j < numberOfCities; j++) {
                            input >> distanceMatrix[i][j];
                            distanceMatrix[j][i] = distanceMatrix[i][j];
                        }
                    }
                } else {
                    for (unsigned long long int i = 0; i < numberOfCities; i++) {
                        distanceMatrix[i].resize(numberOfCities);
                        for (unsigned long long int j = 0; j < numberOfCities; j++) {
                            input >> distanceMatrix[i][j];
                        }
                    }
                    while (line.rfind("EOF", 0) != 0)
                        std::getline(input, line);
                }
            } else if (tmp.rfind("EUC_2D", 0) == 0) {

                while (line.rfind("NODE_COORD_SECTION", 0) != 0)
                    std::getline(input, line);

                cities.resize(numberOfCities);
                for (unsigned long long int i = 0; i < numberOfCities; i++) {
                    input >> cities[i].id >> cities[i].x >> cities[i].y;
                }
                computeDistances();
            } else {
                std::cerr << "INVALID FORMAT" << std::endl;
            }
        }

        /**
         * Compute the distance matrix
         */
        void computeDistances() {
            distanceMatrix.resize(numberOfCities);
            for (unsigned long long int i = 0; i < numberOfCities; i++) {
                distanceMatrix[i].resize(numberOfCities);
                distanceMatrix[i][i] = 0;
                for (unsigned long long int j = 0; j < i; j++) {
                    if (i != j) {
                        double x = sqrt(std::pow((cities[i].x - cities[j].x), 2) +
                                        std::pow((cities[i].y - cities[j].y), 2)) + 0.5;
                        distanceMatrix[i][j] = (int) x;
                        distanceMatrix[j][i] = (int) x;
                    }
                }
            }
        }

        double getDistance(unsigned long long int i, unsigned long long int j) {
            return this->distanceMatrix[i][j];
        }

        unsigned long long int getMatrixSize() {
            return distanceMatrix.size();
        }

        unsigned long long int getNumberOfCities() {
            return numberOfCities;
        }

        /**
         * generation of a new TSP
         * @param _n the number of cities
         * @param _seed the random seed
         * @param ub the max position
         */
        void generate(int _n, int _seed, int ub = 3163) {
            numberOfCities = _n;
            seed = _seed;

            if (_seed != -1)
                util::RNGHelper::get()->reseed(_seed);

            cities.resize(numberOfCities);

            for (unsigned long long int i = 0; i < numberOfCities; i++) {
                cities[i].id = i;
                cities[i].x = util::RNGHelper::get()->uniform(ub);
                cities[i].y = util::RNGHelper::get()->uniform(ub);
            }

            computeDistances();
        }

    protected:
        /*! the number of cities */
        unsigned long long int numberOfCities = 0;
        /*! the distances matrix */
        std::vector<std::vector<double>> distanceMatrix;
        /*! the list of cities */
        std::vector<City> cities;
        /*! seed */
        unsigned int seed;
    };
}


#endif //MH_BUILDER_TSP_H
