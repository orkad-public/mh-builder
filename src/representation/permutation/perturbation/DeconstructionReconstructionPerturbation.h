/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DECONSTRUCTIONRECONSTRUCTIONPERTURBATION_H
#define MH_BUILDER_DECONSTRUCTIONRECONSTRUCTIONPERTURBATION_H

#include "core/Perturbation.h"
#include "core/Eval.h"
#include "core/Criterion.h"
#include <vector>
#include "util/RNGHelper.h"
#include "representation/permutation/algorithm/BestInsertion.h"

namespace representation {
    namespace permutation {
        namespace perturbation {

            /**
             * Class representing a deconstruction reconstruction perturbation
             * @tparam SOL solution type
             */
            template<typename SOL>
            class DeconstructionReconstructionPerturbation : public core::Perturbation<SOL> {
            public:

                using core::Perturbation<SOL>::operator();

                /**
                 * Constructor of the perturbation
                 * @param _neighborhood the neighborhood operator
                 * @param _eval the eval function
                 * @param _strength number of neighborhood perturbation
                 */
                explicit DeconstructionReconstructionPerturbation(core::Eval<SOL> *_eval = nullptr,
                                                                  unsigned long long int _strength = 1) :
                        strength(_strength), eval(_eval) {}

                void operator()(SOL &_sol, core::Criterion<SOL> &_criterion __attribute__((unused))) override {
                    unsigned long long int index;
                    std::vector<unsigned long long int> indexes;

                    // deconstruction
                    for (unsigned long long int k = 0; k < strength; k++) {
                        index = static_cast<unsigned long long int>(util::RNGHelper::get()->random(_sol.size()));
                        indexes.push_back(_sol[index]);
                        _sol.erase(_sol.begin() + index);
                        _sol.fitness().invalidate();
                    }

                    // reconstruction
                    util::RNGHelper::get()->shuffle(indexes.begin(), indexes.end());
                    for (auto i: indexes)
                        representation::permutation::algorithm::BestInsertion<SOL>::bestInsert(_sol, i, *eval);
                }

                /**
                 * Set perturbation strength
                 * @param _strength perturbation strength
                 */
                void setStrength(unsigned long long int _strength) {
                    strength = _strength;
                }

            protected:
                /*! The strength of the perturbation */
                unsigned long long int strength;
                core::Eval<SOL> *eval;
            };
        }
    }
}


#endif //MH_BUILDER_DECONSTRUCTIONRECONSTRUCTIONPERTURBATION_H
