/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PERMUTATION_SOLUTION_H
#define MH_BUILDER_PERMUTATION_SOLUTION_H

#include "representation/VectorSolution.h"
#include "util/RNGHelper.h"


namespace representation::permutation {
    /**
     * Class representing permutation based solutions
     * @tparam FIT Fitness Type
     * @tparam TYPE Type of the vector (default unsigned long long int)
     */
    template<typename FIT, typename TYPE = unsigned long long int>
    class PermutationSolution : public representation::VectorSolution<FIT, TYPE> {
    public:

        /**
         * Construct a permutation solution of size n
         * @param n size of permutation
         */
        explicit PermutationSolution(unsigned long long int n = 0) : representation::VectorSolution<FIT, TYPE>(n) {
            for (unsigned long long int i = 0; i < n; i++)
                this->vec_.push_back(i);
        }

        /**
         * Reset the permutation to the original order (works only with elt 0 to n)
         */
        void reset() override {
            for (unsigned long long int i = 0; i < this->vec_.size(); ++i) {
                this->vec_[i] = i;
            }
        }
    };

    /**
     * To print a solution with output stream
     * @tparam FIT Fitness type
     * @param os output stream
     * @param sol solution
     * @return output stream
     */
    template<class FIT>
    std::ostream &operator<<(std::ostream &os, const PermutationSolution<FIT> &sol) {
        sol.print(os);
        return os;
    }
}

#endif //MH_BUILDER_PERMUTATION_SOLUTION_H
