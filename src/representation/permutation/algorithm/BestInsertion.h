/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BESTINSERTION_H
#define MH_BUILDER_BESTINSERTION_H
#include "util/RNGHelper.h"
#include "core/Eval.h"

namespace representation {
    namespace permutation {
        namespace algorithm {

            template<typename SOL>
            class BestInsertion {
            public:
                typedef typename SOL::FITNESS FIT;

                static void bestInsert(SOL &_sol, unsigned long long int k, core::Eval <SOL> &_eval) {
                    SOL tmp;
                    SOL tmpv;
                    unsigned long long int best_j = 0;
                    unsigned long long int count = 1;
                    FIT best_f;
                    // find best insertion
                    for (unsigned long long int j = 0; j < _sol.size() + 1; j++) {
                        tmpv = _sol;
                        tmpv.insert(tmpv.begin() + j, k);
                        tmp = tmpv;
                        _eval(tmp);
                        if (j == 0 || tmp.fitness() >= best_f) {
                            if (tmp.fitness() > best_f) {
                                best_j = j;
                                count = 1;
                            } else if (util::RNGHelper::get()->flip(1.0 / (count + 1.0))) {
                                best_j = j;
                            }
                            best_f = tmp.fitness();
                        }
                    }
                    tmpv = _sol;
                    tmpv.insert(tmpv.begin() + best_j, k);
                    _sol = tmpv;
                    _eval(_sol);
                }
            };
        }
    }
}
#endif //MH_BUILDER_BESTINSERTION_H
