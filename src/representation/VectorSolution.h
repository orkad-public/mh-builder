/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_VECTOR_SOLUTION_H
#define MH_BUILDER_VECTOR_SOLUTION_H

#include "core/Solution.h"
#include "util/RNGHelper.h"

namespace representation {
    /**
     * Class representing vector based solutions
     * @tparam FIT Fitness Type
     * @tparam TYPE Type of the vector (default unsigned long long int)
     */
    template<typename FIT, typename TYPE = unsigned long long int>
    class VectorSolution : public core::Solution<FIT> {
    public:
        typedef typename std::vector<TYPE>::iterator iterator;
        typedef typename std::vector<TYPE>::const_iterator const_iterator;

        typedef TYPE TYPE_ELEMENT ;
        /**
         * Construct a vector solution of size n
         * @param n size of permutation
         */
        explicit VectorSolution(unsigned long long int n = 0) {
            vec_.reserve(n);
        }

        /**
        * Construct a vector solution with an existing vector
        * @param _vector
        */
        explicit VectorSolution(std::vector<TYPE> _vector) : vec_(_vector) {
        }
        /**
         * reset the solution
         */
        virtual void reset() = 0 ;

        /**
         * Get k^th variable value
         * @param k k^th variable value
         * @return the k^th variable value
         */
        virtual TYPE operator[](unsigned int k) {
            return this->vec_[k];
        }

        /**
         * Get k^th variable value
         * @param k k^th variable value
         * @return the k^th variable value
         */
        virtual TYPE operator[](unsigned int k) const {
            return this->vec_[k];
        }

        /**
         * update one element of the vector
         * @param index  the index of the element to changed
         * @param value the new value
         */
        virtual void updateElement(int index, TYPE value) {
            this->vec_[index]=value ;
        }

        /**
         * Get size of the vector
         * @return the vector size
         */
        [[nodiscard]] virtual unsigned long long int size() const {
            return this->vec_.size();
        }

        /**
         * Add an element i in the vector and invalidate the fitness
         * @param i the element to add
         */
        virtual void push_back(const TYPE i) {
            vec_.push_back(i);
            this->fitness_.invalidate();
        }

        /**
         * Add an element in the vector at position and invalidate the fitness
         * @param position position to add
         * @param element the element to add
         */
        virtual void insert(iterator position, const TYPE element) {
            vec_.insert(position, element);
            this->fitness_.invalidate();
        }

        /**
         * Remove the last element in the vector and invalidate the fitness
         */
        virtual void pop_back() {
            vec_.pop_back();
            this->fitness_.invalidate();
        }

        /**
         * Clear the vector and invalidate the fitness
         */
        virtual void clear() {
            vec_.clear();
            this->fitness_.invalidate();
        }

        /**
         * Replace the vector by another
         * @param v the new vector
         */
        virtual void replace(std::vector<TYPE> &v) {
            vec_ = v;
            this->fitness_.invalidate();
        }

        /**
         * Print the vector in an output stream
         * @param os output stream
         */
        virtual void print(std::ostream &os) const {
            this->fitness_.printOn(os);
            os << ' ' << this->size() << ' ';
            for (auto e = this->begin();e != this->end(); e++) {
                os << *e << " ";
            }
        }

        /**
         * Begin iterator of the vector
         */
        virtual const_iterator begin() const {
            return this->vec_.begin();
        }

        /**
         * End iterator of the vector
         */
        virtual const_iterator end() const {
            return this->vec_.end();
        }

        /**
         * Begin iterator of the vector
         */
        virtual iterator begin() {
            return this->vec_.begin();
        }

        /**
         * End iterator of the vector
         */
        virtual iterator end() {
            return this->vec_.end();
        }

        /**
         * Erase the element at pos
         * @param pos
         * @return iterator
         */
        virtual iterator erase(iterator pos) {
            return vec_.erase(pos);
        }

        /**
         * return reference at i
         * @param i
         * @return reference of TYPE
         */
        virtual TYPE &at(unsigned long long int i) {
            return vec_.at(i);
        }
/**
         * invert the section between i and j in the permutation
         * @param i start of inversion [include]
         * @param j end of inversion [include]
         * @param invalidate invalidate the fitness
         */
        virtual void invert(const int i, const int j, const bool invalidate = true) {
            std::reverse(this->vec_.begin() + i, this->vec_.begin() + j + 1);
            if (invalidate)
                this->fitness_.invalidate();
        }

        /**
         * swap the i^th element with j^th element in the permutation
         * @param i the i^th to swap with the j^th
         * @param j the j^th to swap with the i^th
         * @param invalidate invalidate the fitness
         */
        virtual void swap(const int i, const int j, const bool invalidate = true) {
            std::swap(this->vec_[i], this->vec_[j]);
            if (invalidate)
                this->fitness_.invalidate();
        }

        /**
         * move the element at i^th position to j^th position in the final permutation
         * (ex: move (3,5) [0,1,2,3,4,5,6,7,8,9] -> [0,1,2,4,5,3,6,7,8,9])
         * @param i the i^th element to move
         * @param j the position to move the i^th element
         * @param invalidate invalidate the fitness
         */
        virtual void move(const int i, const int j, const bool invalidate = true) {
            if (i < j) {
                auto tmp = this->vec_[i];
                std::move(this->vec_.begin() + i + 1, this->vec_.begin() + j + 1, this->vec_.begin() + i);
                this->vec_[j] = tmp;
            } else {
                auto tmp = this->vec_[i];
                std::move_backward(this->vec_.begin() + j, this->vec_.begin() + i, this->vec_.begin() + i + 1);
                this->vec_[j] = tmp;
            }
            if (invalidate)
                this->fitness_.invalidate();
        }



        /**
         * Shuffle the permutation
         */
        virtual void shuffle() {
            util::RNGHelper::get()->shuffle(this->vec_.begin(), this->vec_.end());
            this->fitness_.invalidate();
        }

    protected:
        /*! Vector of the Permutation */
        std::vector<TYPE> vec_;
    };
}

#endif //MH_BUILDER_VECTOR_SOLUTION_H
