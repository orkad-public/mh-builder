/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ARCHIVEMETRICHELPER_H
#define MH_BUILDER_ARCHIVEMETRICHELPER_H

#include <vector>
#include <stdexcept>

namespace util {
    /**
     * helper class for archive of solutions
     * allows to compute a normalized archive (where each value are in [0,1])
     * @tparam ARCHIVE
     **/
    template <typename ARCHIVE>
    class ArchiveMetricHelper {
    public:
        /**
         * provide for all objectives of an archive the min and max bounds
         * the computeBounds method must be invoked before this method
         * @return
         **/
        std::vector<std::pair<double, double>> &getBounds() {
            return this->bounds;
        }
        /**
         * provide the normalized Front, this vector is the result  of the execution of the
         * "computeNormalizedFront" method
         **/

        std::vector<std::vector<double>> &getNormalizedFront() {
            return this->normalizedFront;
        }

        /**
         * computes the min and max bounds for all objectives of the given archive
         * stores the result in the "bounds" instance variable  such as :
         * bounds[i].first : the min value for the objective no i,
         * bounds[i].second : the max value for the objective no i,
         * @param _archive
         **/
        void computeBounds(const ARCHIVE &_archive) {
          if ( _archive.size() < 1)
            throw std::runtime_error("Archive size must be greater than 1");
          else {
            unsigned long long int nbObj = _archive[0].fitness().objectives().size();
            this->bounds.resize(nbObj);
            for(unsigned long long int i = 0; i < nbObj; i++) {
              this->bounds[i] = {_archive[0].fitness().objectives()[i], _archive[0].fitness().objectives()[i]};
              for (unsigned long long int j = 1; j < _archive.size(); j++) {
                this->bounds[i].first = std::min(this->bounds[i].first, _archive[j].fitness().objectives()[i]);
                this->bounds[i].second = std::max(this->bounds[i].second, _archive[j].fitness().objectives()[i]);
              }
            }
          }
        }

        /**
         * computes the normalized front for the given archive
         * the result is stored in the "normalizedFront" instance variable
         *
         **/
        void computeNormalizedFront(const ARCHIVE &_archive) {
          this->computeBounds(_archive);
          this->computeNormalizedFront(_archive, bounds);
        }

        /**
         * computes the normalized front for the given archive
         * min and max bounds of each objective required for the normalisation are given
         * the result is stored in the "normalizedFront" instance variable
         * @param _archive
         * @param _bounds
         **/
        void computeNormalizedFront(const ARCHIVE &_archive, const std::vector<std::pair<double, double>> &_bounds) {
            if ( _archive.size() < 1)
                throw std::runtime_error("Archive size must be greater than 1");
          normalizedFront.resize(_archive.size());
          unsigned long long int numberOfObjectives=_archive[0].fitness().objectives().size();
          for (unsigned long long int i = 0; i < _archive.size(); i++) {
            normalizedFront[i].resize(numberOfObjectives);
            for (unsigned long long int j = 0; j < normalizedFront[i].size() ; j++) {
              normalizedFront[i][j] = (_archive[i].fitness().objectives()[j] - _bounds[j].first) / (_bounds[j].second - _bounds[j].first);
            }
          }
        }

        /**
         * update the normalized front by a sort according to an objective
         * @param k the objective number
         **/
        void sortNormalizedFrontAccordingToAnObjective(unsigned long long int k) {
          std::sort(normalizedFront.begin(), normalizedFront.end(), [&](std::vector<double> i, std::vector<double> j) {
              return i[k] < j[k];
          });
        }

        protected:
          std::vector<std::pair<double, double>> bounds;
          std::vector<std::vector<double>> normalizedFront;
    };
}

#endif //MH_BUILDER_ARCHIVEMETRICHELPER_H
