/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_STRINGHELPER_H
#define MH_BUILDER_STRINGHELPER_H

namespace util {
    /**
     * Singleton providing useful functions on character strings
     */
    class StringHelper {
        static StringHelper *singleton;

        StringHelper() = default;

    public:

        StringHelper(const StringHelper &) = delete;

        StringHelper &operator=(const StringHelper &) = delete;

        /**
         * Get the singleton item
         * @return singleton
         */
        static StringHelper *get() {
          if (!singleton)
            singleton = new StringHelper;
          return singleton;
        }

        /**
         * Identify if a string is a number (all charaters are digits) or not
         * @param str string to inspect
         * @return true if s is a number else false
         */
        static bool isNumber(const std::string& s)
        {
          return !s.empty() && std::find_if(s.begin(), s.end(), [](char c) {return !std::isdigit(c);}) == s.end();
        }

        /**
         * Split a string according to a delimiter
         * @param str string to split
         * @param delimiter the delimiter
         * @return a vector a string
         */
        static std::vector<std::string> split(const std::string &str, std::string delimiter) {
          std::vector<std::string> tokens;
          if (!str.empty()) {
            // Skip delimiters at beginning.
            std::string::size_type lastPos = str.find_first_not_of(delimiter[0], 0);
            // Find first "non-delimiter".
            std::string::size_type pos = str.find(delimiter, lastPos);
            bool stop = false;
            while (std::string::npos != pos || !stop) {
              // Found a token, add it to the vector.
              tokens.push_back(str.substr(lastPos, pos - lastPos));
              // Skip delimiters.  Note the "not_of"
              if (std::string::npos != pos) lastPos = pos + delimiter.size();
              else stop = true;

              // Find next "non-delimiter"
              pos = str.find(delimiter, lastPos);
            }
          }
          return tokens;
        }

        /**
         * Test if a string str contains the string a
         * @param str string to test
         * @param a string must be in str
         * @return true if a is in str
         */
        static bool contains(const std::string &str, const std::string &a) {
          return str.find(a) != std::string::npos;
        }
    };

    StringHelper *StringHelper::singleton = nullptr;
}

#endif //MH_BUILDER_STRINGHELPER_H
