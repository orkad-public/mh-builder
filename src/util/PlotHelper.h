/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PLOTHELPER_H
#define MH_BUILDER_PLOTHELPER_H

#include <cassert>
#include "core/Archive.h"

#include "../external/mathplotlib/mathplotlib.h"

namespace plt {
    bool named_scatter_arch_biobj(const std::string &name, const std::vector<double> &x, const std::vector<double> &y,
                                  const double s = 1.0) {
      matplotlibcpp::detail::_interpreter::get();

      assert(x.size() == y.size());

      PyObject *xarray = matplotlibcpp::detail::get_array(x);
      PyObject *yarray = matplotlibcpp::detail::get_array(y);

      PyObject *kwargs = PyDict_New();
      PyDict_SetItemString(kwargs, "label", PyString_FromString(name.c_str()));
      PyDict_SetItemString(kwargs, "s", PyLong_FromLong(s));

      PyObject *plot_args = PyTuple_New(2);
      PyTuple_SetItem(plot_args, 0, xarray);
      PyTuple_SetItem(plot_args, 1, yarray);

      PyObject *res = PyObject_Call(matplotlibcpp::detail::_interpreter::get().s_python_function_scatter, plot_args,
                                    kwargs);

      Py_DECREF(plot_args);
      Py_DECREF(kwargs);
      if (res) Py_DECREF(res);

      return res;
    }


    template<typename SOL>
    bool scatter_arch_biobj(const core::Archive<SOL> &arch, const double s = 10, std::string named = "") {
      std::vector<double> x(arch.size());
      std::vector<double> y(arch.size());
      for (unsigned long long int i = 0; i < arch.size(); i++) {
        auto &fit = arch[i].fitness();
        auto objv = fit.objectives();
        x[i] = objv[0];
        y[i] = objv[1];
      }
      if ( named == "")
        return matplotlibcpp::scatter<double, double>(x, y, s);
      else
        return named_scatter_arch_biobj(named, x, y, s);
    }

}

#endif //MH_BUILDER_PLOTHELPER_H
