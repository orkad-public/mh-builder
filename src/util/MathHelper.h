/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MATHHELPER_H
#define MH_BUILDER_MATHHELPER_H


namespace util {
    /**
     * Singleton providing useful mathematical functions
     */
    class MathHelper {
        static MathHelper *singleton;

        MathHelper() = default;

    public:

        MathHelper(const MathHelper &) = delete;

        MathHelper &operator=(const MathHelper &) = delete;

        /**
         * Get the singleton item
         * @return singleton
         */
        static MathHelper *get() {
          if (!singleton)
            singleton = new MathHelper;
          return singleton;
        }


        /**
         * Compute the euclidian distance between two vectors
         * @param a
         * @param b
         * @tparam VECTYPE type of the vectors
         * @return euclidian distance between a and b
         */
        template <typename VECTYPE = double>
        static double euclidanDistance(const std::vector<VECTYPE> &a, const std::vector<VECTYPE> &b) {
          if ( a.size() != b.size() )
            throw std::runtime_error("euclidianDistance : Error a and b must have the same size");

          double sum = 0.0;
          for (unsigned long long int i = 0; i < a.size(); ++i)
            sum += std::pow(a[i] - b[i], 2.0);
          return std::sqrt(sum);
        }

        /**
         * Compute the mean value of a vector
         * @tparam VECTYPE type of vector
         * @param vec
         * @return mean value of the vector
         */
        template <typename VECTYPE = double>
        static double meanVector(const std::vector<VECTYPE> &vec) {
          double sum = 0.0;
          for (const auto &elt: vec)
            sum += elt;
          return sum / vec.size();
        }


    };

    MathHelper *MathHelper::singleton = nullptr;
}

#endif //MH_BUILDER_MATHHELPER_H
