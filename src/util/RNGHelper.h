/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RNGHELPER_H
#define MH_BUILDER_RNGHELPER_H

#include <random>
#include <algorithm>

namespace util {
    /**
     * Singleton providing useful functions for random
     */
    class RNGHelper {
        static RNGHelper *singleton;

        RNGHelper() {
          std::random_device rd;
          reseed(rd());
        }

    public:
        RNGHelper(const RNGHelper &) = delete;
        
        RNGHelper &operator=(const RNGHelper &) = delete;

        /**
         * Get the singleton item
         * @return singleton
         */
        static RNGHelper *get() {
          if (!singleton){
            singleton = new RNGHelper;
            std::atexit(RNGHelper::free);
          }
          return singleton;
        }

        inline static void free() {delete singleton;}

        /**
         * Give a new seed with default mt19937 (mersenne twister)
         */
        void reseed() {
          mt = std::mt19937();
        }

        /**
         * Give a new seed
         * @param s the seed
         */
        void reseed(int s) {
          mt = std::mt19937(s);
        }

        /**
         * Get the seed
         * @return the seed
         */
        unsigned int raw() {
          return mt();
        }

        /**
         * Give a random integer between [0, max[
         * @param max
         * @return int
         */
        int random(int max) {
          std::uniform_int_distribution<> dis(0, max - 1);
          return dis(mt);
        }

        /**
         * Give a random integer between [min, max[
         * @param min
         * @param max
         * @return int
         */
        int random(int min, int max) {
          std::uniform_int_distribution<> dis(min, max - 1);
          return dis(mt);
        }

        /**
         * Give a random double between [0, max[ with a normal distribution
         * @param min
         * @param max
         * @return double
         */
        double normal(double max) {
          std::normal_distribution<> dis(0, max - 1);
          return dis(mt);
        }

        /**
         * Give a random double between [0, max[  with a uniform distribution
         * @param min
         * @param max
         * @return double
         */
        double uniform(double max = 1.0) {
          return uniform(0.0, max);
        }

        /**
        * Give a random double between [min, max[  with a uniform distribution
        * @param min
        * @param max
        * @return double
        */
        double uniform(double min, double max) {
          std::uniform_real_distribution<double> dis(min, max);
          return dis(mt);
        }

        /**
        * Give true according to the rate
        * @param rate (default 0.5)
        * @return bool
        */
        bool flip(double rate = 0.5) {
          return uniform() < rate;
        }

        /**
         * Shuffle value between begin and end
         * @tparam T the type (an iterator)
         * @param begin
         * @param end
         */
        template<class T>
        void shuffle(T begin, T end) {
          std::shuffle(begin, end, mt);
        }

    protected:
        /*! mersenne twister */
        std::mt19937 mt{};
    };

    RNGHelper *RNGHelper::singleton = nullptr;
}

#endif //MH_BUILDER_RNGHELPER_H
