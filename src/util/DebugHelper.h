/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DEBUGHELPER_H
#define MH_BUILDER_DEBUGHELPER_H

#include <iostream>
#include <vector>
#include <fstream>
#include "util/RNGHelper.h"

namespace util {

// debug helper
    template<class TH, class...TT>
    void dbgaux(TH head) {
        std::cerr << head << " ";
    }

    template<class TE, class...TT>
    void dbgaux(std::vector <TE> head) {
        std::cerr << "[ ";
        for (TE &e: head)
            dbgaux(e);
        std::cerr << "]";
    }

    template<class TE, class TA, class...TT>
    void dbgaux(std::pair <TE, TA> head) {
        std::cerr << "( ";
        dbgaux(head.first);
        std::cerr << ";";
        dbgaux(head.second);
        std::cerr << ")";
    }

    template<class TH, class...TT>
    void dbgaux(TH head, TT...tail) {
        dbgaux(head);
        dbgaux(tail...);
    }

    template<class ...Args>
    void dbg(Args ...args) {
        std::cerr << "[debug: ";
        dbgaux(args...);
        std::cerr << "]" << std::endl;
    }

    void dbg() {
        std::cerr << "[debug]" << std::endl;
    }

    void dbgPause() {
        std::cin.get();
    }

    int dbgRand() {
        int i = util::RNGHelper::get()->random(1000);
        dbg("RAND ", i);
        return i;
    }

    template<typename T>
    void save_data(std::string filename, T value, bool clear = false) {
        std::ofstream output(filename, std::ofstream::out | std::ofstream::app);
        output << value << std::endl;
        output.close();
    }
}
#endif //MH_BUILDER_DEBUGHELPER_H
