/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PARSERPARAM_H
#define MH_BUILDER_PARSERPARAM_H

#include <utility>
#include <string>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <algorithm>

namespace util {
    /**
     * Simple interface in order to create vector of ParserParam
     */
    class ParserParamInterface {
    public:
        virtual ~ParserParamInterface() = default;
        
        virtual bool isNameOrAliases(const std::string &_name) = 0;
        virtual void readValue(std::string _value) = 0;

        void setUserDefined(bool b) {
          user_defined = b;
        }

        static bool readBool(std::string bool_value) {
          std::transform(bool_value.begin(), bool_value.end(), bool_value.begin(), ::tolower);
          if (bool_value == "1" || bool_value == "true")
            return true;
          if (bool_value == "0" || bool_value == "false")
            return false;
          throw std::runtime_error("MH-builder Parsing : illegal boolean value");
        }

    protected:
        bool user_defined{};
    };

    /**
     * Class representing a parameter
     * @tparam PARAM param type
     */
    template <typename PARAM>
    class ParserParam : public ParserParamInterface {
    public:

        /**
         * Constructor of a parameter value
         * @param _name of the parameter
         * @param _value value
         */
        ParserParam(std::string _name, PARAM _value) : name(std::move(_name)), value(_value)  {
        }

        virtual ~ParserParam() = default;
        /**
         * Test if name in parameter is the same of this parameter or an alis
         * @param _name
         * @return true if it is the same name
         */
        bool isNameOrAliases(const std::string &_name) override {
          if ( name == _name)
            return true;
          for (const auto &alias: aliases) {
            if (alias == _name)
              return true;
          }
          return false;
        }

        /**
         * Add alias to parameter
         */
        template <typename ...TT>
        ParserParam* addAlias(const std::string& alias, TT..._aliases) {
          aliases.push_back(alias);
          return addAlias(_aliases...);
        }

        /**
         * Add alias to parameter (to make end of previous recursive function)
         */
        ParserParam* addAlias() {
          return this;
        }

        /**
         * Set a description of the parameter
         * @param _description
         * @return this
         */
        ParserParam* set_description(const std::string& _description) {
          description = _description;
          return this;
        }

        /**
         * Default value when the parameter is call without value
         * @param _implicit_value
         * @return this
         */
        ParserParam* setImplicit(PARAM _implicit_value) {
          implicit_value = _implicit_value;
          has_implicit_value = true;
          return this;
        }

        /**
         * Read value of attribute and convert it;
         * @param _value
         */
        void readValue(std::string _value) override {
          std::istringstream is(_value);
          if (std::is_same<PARAM,bool>::value)
            value = this->readBool(_value);
          else
            is >> value;
          has_read_value = true;
        }

        /**
         * Get value of the parameter
         * @return value
         */
        PARAM getValue() {
          if ( this->user_defined && !has_read_value && this->has_implicit_value )
            return implicit_value;
          return value;
        }

    protected:
        std::string name;
        PARAM value;
        PARAM default_value;
        PARAM implicit_value;
        bool has_implicit_value = false;
        bool has_read_value = false;
        std::vector<std::string> aliases;
        std::string description;
    };
}

#endif //MH_BUILDER_PARSERPARAM_H
