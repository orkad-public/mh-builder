/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PARSERHELPER_H
#define MH_BUILDER_PARSERHELPER_H
#include "ParserParam.h"

namespace util {
  class ParserHelper {
  public:
      /**
       * Constructor of param helper
       * @param _lower_params true if param is not sensible to casse
       */
      explicit ParserHelper(bool _lower_params = false) : lower_params(_lower_params) {};

      template <typename PARAM_TYPE>
      ParserParam<PARAM_TYPE>* setAs(const std::string& name, PARAM_TYPE value) {
        auto parserParam = new ParserParam<PARAM_TYPE>(name, value);
        parameters.push_back(parserParam);
        return parserParam;
      }

      ~ParserHelper(){
        for(auto &parameter: parameters)delete parameter;
      }

      template <typename PARAM_TYPE>
      PARAM_TYPE getAs(const std::string& name) {
        auto param = getParam(name);
        return dynamic_cast<ParserParam<PARAM_TYPE>*>(param)->getValue();
      }

      bool hasParam(const std::string &name) {
        for(auto &p : parameters)
          if ( p->isNameOrAliases(name))
            return true;
        return false;
      }

      void parse(int argc, char *argv[]) {
        for (int i = 1; i < argc; ++i) {
          std::string current_arg = argv[i];
          if ( current_arg.length() <= 1 )
            throw std::runtime_error("MH-Builder Parsing: parameter length 1 char or less not accepted");
          if ( current_arg[0] == '-') {
            if ( current_arg[1] == '-' ) {
              // --param
              size_t k = current_arg.find('=');
              if ( k != std::string::npos ) {
                // --param=
                std::string tmp = current_arg.substr(0, k);
                if ( lower_params )
                  std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);

                auto param = getParam(tmp);
                param->setUserDefined(true);
                param->readValue(current_arg.substr(k+1, std::string::npos));
              } else {
                if ( lower_params )
                  std::transform(current_arg.begin(), current_arg.end(), current_arg.begin(), ::tolower);
                auto param = getParam(current_arg);
                param->setUserDefined(true);
              }
            }
          }
        }
      }

      ParserParamInterface* getParam(const std::string &name) {
        for(auto &p : parameters) {
          if ( p->isNameOrAliases(name))
            return p;
        }
        std::string msg_error = "MH-Builder Parser: parameter `" + name + "` doesn't exist";
        throw std::runtime_error(msg_error);
      }
  protected:
      bool lower_params;
      std::vector<util::ParserParamInterface*> parameters;
  };
}

#endif //MH_BUILDER_PARSERHELPER_H
