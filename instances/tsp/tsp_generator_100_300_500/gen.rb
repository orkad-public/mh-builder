#!/bin/ruby

[100, 300, 500].each do |n|
  [?A, ?B].each.with_index do |c, i|
    30.times do |k|
      cmd = './portgen %d %d > g%s%d_%02d'%[n, k+1+1000*(i+1), c, n, k+1]
      puts cmd
      system cmd
    end
  end
end
