/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_PERMUTATIONSOLUTIONTEST_H
#define MH_BUILDER_PERMUTATIONSOLUTIONTEST_H

#include "representation/permutation/PermutationSolution.h"
#include "representation/permutation/neighborhood/neighbor/SwapNeighbor.h"
#include "gtest/gtest.h"

namespace test {

    class PermutationSolutionTest : public ::testing::Test {
    protected:
        PermutationSolutionTest() : permutationSolution(10) {};

        ~PermutationSolutionTest() override = default;

        void SetUp() override {
          permutationSolution.reset();
        }

        void TearDown() override {}

        representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> permutationSolution;

    };

    TEST_F(PermutationSolutionTest, TestConstructorOfPermutationOfSize10) {
      for (int i = 0; i < 10; ++i) {
        ASSERT_EQ((unsigned int)(i), permutationSolution[i]);
      }
    }

    TEST_F(PermutationSolutionTest, TestInvertInPermutation) {
      permutationSolution.invert(3, 5);
      unsigned int expected[] = {0, 1, 2, 5, 4, 3, 6, 7, 8, 9};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], permutationSolution[i]);
    }

    TEST_F(PermutationSolutionTest, TestSwapInPermutation) {
      permutationSolution.swap(3, 5);
      unsigned int expected[] = {0, 1, 2, 5, 4, 3, 6, 7, 8, 9};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], permutationSolution[i]);
      permutationSolution.swap(5, 3);
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ((unsigned int)(i), permutationSolution[i]);
    }

    TEST_F(PermutationSolutionTest, TestMoveInPermutation) {
      representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> sol(4);
      representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> tmp = sol;
      tmp.move(0, 1); //Insert Forward
      unsigned int expected[] = {1, 0, 2, 3};
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
      tmp = sol;
      tmp.move(0,2);
      // 1 2 0 3
      expected[0] = 1; expected[1] = 2; expected[2] = 0;
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
      tmp = sol;
      tmp.move(0,3);
      // 1 2 3 0
      expected[0] = 1; expected[1] = 2; expected[2] = 3; expected[3] = 0;
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
      tmp = sol;
      tmp.move(1,0);
      // 1 0 2 3
      expected[0] = 1; expected[1] = 0; expected[2] = 2; expected[3] = 3;
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
      tmp = sol;
      tmp.move(1,2);
      // 0 2 1 3
      expected[0] = 0; expected[1] = 2; expected[2] = 1; expected[3] = 3;
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
      tmp = sol;
      tmp.move(1,3);
      // 0 2 3 1
      expected[0] = 0; expected[1] = 2; expected[2] = 3; expected[3] = 1;
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
      tmp = sol;
      tmp.move(2,0);
      // 2 0 1 3
      expected[0] = 2; expected[1] = 0; expected[2] = 1; expected[3] = 3;
      for (int i = 0; i < 4; ++i)
        ASSERT_EQ(expected[i], tmp[i]);
    }
}

#endif //MH_BUILDER_PERMUTATIONSOLUTIONTEST_H
