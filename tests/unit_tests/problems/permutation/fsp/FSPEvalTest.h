/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FSPEVALTEST_H
#define MH_BUILDER_FSPEVALTEST_H

#include "representation/permutation/problems/fsp/FSPEval.h"
#include "representation/permutation/problems/fsp/FSP.h"

using namespace representation::permutation::problems ;
namespace test {

    class FSPEvalTest : public ::testing::Test {
    protected:
        typedef core::fitness::FitnessMin<> FIT;
        typedef fsp::FSPSol<FIT> SOL;

        FSPEvalTest() : fsp("../../instances/fsp/020_05_01.txt"), fspSol(fsp.getN()), fspEval(fsp) {}

        ~FSPEvalTest() override = default;

        void SetUp() override {}

        void TearDown() override {}

        fsp::FSP fsp;
        SOL fspSol;
        fsp::FSPEval<SOL> fspEval;
    };

    TEST_F(FSPEvalTest, TestDefaultOperator) {
      fspEval(fspSol);
      ASSERT_EQ(1448, fspSol.fitness().operator[](0));
    }

}

#endif //MH_BUILDER_FSPEVALTEST_H
