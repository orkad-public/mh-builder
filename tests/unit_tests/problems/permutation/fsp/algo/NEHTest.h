/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEHTEST_H
#define MH_BUILDER_NEHTEST_H
#include "representation/permutation/problems/fsp/algo/NEH.h"

namespace test {

    class NEHTest : public ::testing::Test {
    protected:
        typedef core::fitness::FitnessMin<> FIT;
        typedef fsp::FSPSol<FIT> SOL;

        NEHTest() : fsp("../../instances/fsp/005_05_01.txt"), fspSol(fsp.getN()), fspEval(fsp), neh(fsp, fspEval) {}

        ~NEHTest() override = default;

        void SetUp() override {}

        void TearDown() override {}

        fsp::FSP fsp;
        SOL fspSol;
        fsp::FSPEval<SOL> fspEval;
        fsp::algo::NEH<SOL> neh;//(FSP &_instance, FSPEval<SOL> &_eval) {}
    };

    TEST_F(NEHTest, TestDefaultOperator) {
      unsigned long long int expected[] = {2,0,3,1,4};
      neh(fspSol);
      ASSERT_EQ(static_cast<unsigned long long int>(5), fspSol.size());
      for (unsigned long long int i = 0; i < fspSol.size(); i++) {
        ASSERT_EQ(expected[i], fspSol[i]);
      }
    }

}

#endif //MH_BUILDER_NEHTEST_H
