/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SHIFTINDEXNEIGHBORHOODTEST_H
#define MH_BUILDER_SHIFTINDEXNEIGHBORHOODTEST_H

#include "representation/permutation/PermutationSolution.h"
#include "representation/permutation/neighborhood/neighbor/ShiftNeighbor.h"
#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"

namespace test {
    class ShiftIndexNeighborhoodTest : public ::testing::Test {
    protected:

        typedef representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> SOL;

        ShiftIndexNeighborhoodTest() : permutationSolution(10), indexNeighborhood(shiftNeighbor) {};

        virtual ~ShiftIndexNeighborhoodTest() override = default;

        void SetUp() override {
          indexNeighborhood.init(permutationSolution);
        }

        void TearDown() override {}

        SOL permutationSolution;
        representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL> shiftNeighbor;
        opt::singleSolution::neighborhood::IndexNeighborhood<SOL> indexNeighborhood;
    };

    TEST_F(ShiftIndexNeighborhoodTest, RightBehavior) {
        auto k = shiftNeighbor.getMaxKey() ;
        auto count=0 ;
      while (indexNeighborhood.hasNextNeighbor()) {
          ASSERT_NO_THROW(indexNeighborhood(permutationSolution));
          ASSERT_NO_THROW(indexNeighborhood.next());
          count++ ;
      }
      ASSERT_EQ(k-1,count);
      ASSERT_ANY_THROW(indexNeighborhood.next());
    }


    TEST_F(ShiftIndexNeighborhoodTest, TestMoveAndMoveBack) {
        SOL before = permutationSolution ;
        indexNeighborhood(permutationSolution) ;
        indexNeighborhood.move_back(permutationSolution) ;
        for (unsigned long long int i = 0; i < permutationSolution.size(); ++i) {
            ASSERT_EQ(before[i], permutationSolution[i]);
        }
    }
}

#endif //MH_BUILDER_SHIFTINDEXNEIGHBORHOODTEST_H
