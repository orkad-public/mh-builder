/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ADJSWAPNEIGHBORTEST_H
#define MH_BUILDER_ADJSWAPNEIGHBORTEST_H

#include "representation/permutation/PermutationSolution.h"
#include "representation/permutation/neighborhood/neighbor/AdjSwapNeighbor.h"


#include "gtest/gtest.h"

namespace test {

    class AdjSwapNeighborTest : public ::testing::Test {
    protected:

        typedef representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> SOL;

        AdjSwapNeighborTest() : permutationSolution(10) {};

        ~AdjSwapNeighborTest() override = default;

        void SetUp() override {
          adjSwapNeighbor.init(permutationSolution);
        }

        void TearDown() override {}

        SOL permutationSolution;
        representation::permutation::neighborhood::neighbor::AdjSwapNeighbor<SOL> adjSwapNeighbor;
    };

    TEST_F(AdjSwapNeighborTest, TestExhaustiveSolOperatorTwo) {
        SOL smallSol(4);
        unsigned int expected[][4] = {
                {1, 0, 2, 3},
                {0, 2, 1, 3},
                {0, 1, 3, 2},
                {3, 1, 2, 0},
        };


        adjSwapNeighbor.init(smallSol);
        for (long long unsigned int i = 0; i < adjSwapNeighbor.getMaxKey(); i++) {
            SOL tmp = smallSol;
            adjSwapNeighbor.setKey(i);
            adjSwapNeighbor(tmp);

            bool checkResultMove = true;
            for (long long unsigned int k = 0; k < tmp.size() && checkResultMove; k++) {
                if (tmp[k] != expected[i][k])
                    checkResultMove = false;
            }
            ASSERT_TRUE(checkResultMove);
        }
    }

    TEST_F(AdjSwapNeighborTest, TestMoveBackOperator) {
      adjSwapNeighbor.setKey(3);
      adjSwapNeighbor(permutationSolution);
      adjSwapNeighbor.do_move_back(permutationSolution);
      unsigned int expected[] = {0,1,2,3,4,5,6,7,8,9};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], permutationSolution[i]);
    }

    TEST_F(AdjSwapNeighborTest, TestThrowWhenMoveBackBeforeMoveOperator) {
      adjSwapNeighbor.setKey(3);
      ASSERT_THROW(adjSwapNeighbor.do_move_back(permutationSolution), std::runtime_error);
    }

    TEST_F(AdjSwapNeighborTest, TestTheNeighborhoodSize) {
      ASSERT_EQ(permutationSolution.size(), adjSwapNeighbor.getMaxKey());
    }
}


#endif //MH_BUILDER_ADJSWAPNEIGHBORTEST_H
