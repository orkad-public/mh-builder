/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DOUBLE_BRIDGE_NEIGHBOR_TEST_H
#define MH_BUILDER_DOUBLE_BRIDGE_NEIGHBOR_TEST_H

#include "representation/permutation/PermutationSolution.h"
#include "representation/permutation/neighborhood/neighbor/DoubleBridgeNeighbor.h"
#include "gtest/gtest.h"

namespace test {

    class DoubleBridgeNeighborTest : public ::testing::Test {
    protected:

        typedef representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> SOL;

        DoubleBridgeNeighborTest() : permutationSolution(10) {};

        ~DoubleBridgeNeighborTest() override = default;

        void SetUp() override {
          doubleBridgeNeighbor.init(permutationSolution);
        }

        void TearDown() override {}

        SOL permutationSolution;
        representation::permutation::neighborhood::neighbor::DoubleBridgeNeighbor<SOL> doubleBridgeNeighbor;
    };

    TEST_F(DoubleBridgeNeighborTest, TestExhaustiveSolOperator) {
      SOL smallSol(5);  // generates : { 0, 1 , 2, 3, 4}
      unsigned int expected[][5] = {
              {0,3,4,2,1},
              {0,4,2,3,1},
              {0,4,3,1,2},
              {0,1,3,3,2}
      };

      doubleBridgeNeighbor.init(smallSol);
      for (long long unsigned int i = 0; i < doubleBridgeNeighbor.getMaxKey(); i++) {
          SOL tmp = smallSol;
          doubleBridgeNeighbor.setKey(i);
          doubleBridgeNeighbor(tmp);

          bool checkResultMove = true;
          for (long long unsigned int k = 0; k < tmp.size() && checkResultMove; k++) {
              if (tmp[k] != expected[i][k])
                  checkResultMove = false;
          }
          // ASSERT_TRUE(checkResultMove);
      }
    }

    TEST_F(DoubleBridgeNeighborTest, TestExhaustiveSolOperatorTwo) {
      SOL smallSol(6);
      unsigned int expected[][6] = {
              { 0,3,4,5,2,1},
              { 0,4,5,2,3,1},
              { 0,5,2,3,4,1},
              { 0,4,5,3,1,2},
              { 0,5,3,4,1,2},
              { 0,5,4,1,2,3},
              { 0,1,4,5,3,2},
              { 0,1,5,3,4,2},
              { 0,1,5,4,2,3},
              { 0,1,2,5,4,3}
      };

      
      doubleBridgeNeighbor.init(smallSol);
      for (long long unsigned int i = 0; i < doubleBridgeNeighbor.getMaxKey(); i++) {
          SOL tmp = smallSol;
          doubleBridgeNeighbor.setKey(i);
          doubleBridgeNeighbor(tmp);
          bool checkResultMove = true;
          for (long long unsigned int k = 0; k < tmp.size() && checkResultMove; k++) {
              if (tmp[k] != expected[i][k])
                  checkResultMove = false;
          }
          ASSERT_TRUE(checkResultMove);
      }
    }

    TEST_F(DoubleBridgeNeighborTest, TestMoveBackOperator) {
        doubleBridgeNeighbor.setKey(3);
        doubleBridgeNeighbor(permutationSolution);
        doubleBridgeNeighbor.do_move_back(permutationSolution);
      unsigned int expected[] = {0,1,2,3,4,5,6,7,8,9};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], permutationSolution[i]);
    }

    TEST_F(DoubleBridgeNeighborTest, TestThrowWhenMoveBackBeforeMoveOperator) {
        doubleBridgeNeighbor.setKey(3);
      ASSERT_THROW(doubleBridgeNeighbor.do_move_back(permutationSolution), std::runtime_error);
    }
}

#endif //MH_BUILDER_DOUBLE_BRIDGE_NEIGHBOR_TEST_H
