/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SHIFTWITHOUTSWAPNEIGHBORTEST_H
#define MH_BUILDER_SHIFTWITHOUTSWAPNEIGHBORTEST_H

#include "representation/permutation/PermutationSolution.h"
#include "representation/permutation/neighborhood/neighbor/ShiftWithoutSwapNeighbor.h"
#include "gtest/gtest.h"

namespace test {

    class ShiftWithoutSwapNeighborTest : public ::testing::Test {
    protected:

        typedef representation::permutation::PermutationSolution<core::fitness::FitnessMax<double>> SOL;

        ShiftWithoutSwapNeighborTest() : permutationSolution(10) {};

        ~ShiftWithoutSwapNeighborTest() override = default;

        void SetUp() override {
          shiftWithoutSwapNeighbor.init(permutationSolution);
        }

        void TearDown() override {}

        SOL permutationSolution;
        representation::permutation::neighborhood::neighbor::ShiftWithoutSwapNeighbor<SOL> shiftWithoutSwapNeighbor;
    };

    TEST_F(ShiftWithoutSwapNeighborTest, TestExhaustiveSolOperator) {
      SOL smallSol(3);
      unsigned int expected[][3] = {
              {1, 2, 0},
              {2, 0, 1}
      };

      bool verification[] = {false, false};
      shiftWithoutSwapNeighbor.init(smallSol);
      for (long long unsigned int i = 0; i < shiftWithoutSwapNeighbor.getMaxKey(); i++) {
        SOL tmp = smallSol;
        shiftWithoutSwapNeighbor.setKey(i);
        shiftWithoutSwapNeighbor(tmp);
        bool change = false;
        for (long long unsigned int j = 0; j < shiftWithoutSwapNeighbor.getMaxKey() && !change; j++) {
          if (!verification[j]) {
            bool v1 = true;
            for (long long unsigned int k = 0; k < tmp.size() && v1; k++) {
              if (tmp[k] != expected[j][k])
                v1 = false;
            }

            if (v1) {
              verification[j] = true;
              change = true;
            }
          }
        }

        ASSERT_TRUE(change);

      }

    }

    TEST_F(ShiftWithoutSwapNeighborTest, TestExhaustiveSolOperatorTwo) {
      SOL smallSol(4);
      unsigned int expected[][4] = {
              {1,2,0,3},
              {1,2,3,0},
              {0,2,3,1},
              {2,0,1,3},
              {3,0,1,2},
              {0,3,1,2},
      };

      bool verification[] = {false, false, false, false, false, false};
      shiftWithoutSwapNeighbor.init(smallSol);
      for (long long unsigned int i = 0; i < shiftWithoutSwapNeighbor.getMaxKey(); i++) {
        SOL tmp = smallSol;
        shiftWithoutSwapNeighbor.setKey(i);
        shiftWithoutSwapNeighbor(tmp);
        bool change = false;
        for (long long unsigned int j = 0; j < shiftWithoutSwapNeighbor.getMaxKey() && !change; j++) {
          if ( !verification[j] ) {
            bool v1 = true;
            for (long long unsigned int k = 0; k < tmp.size() && v1; k++) {
              if ( tmp[k] != expected[j][k] )
                v1 = false;
            }

            if ( v1 ) {
              verification[j] = true;
              change = true;
            }
          }
        }
        ASSERT_TRUE(change);
      }

      for (long long unsigned int i = 0; i < shiftWithoutSwapNeighbor.getMaxKey(); ++i)
        ASSERT_TRUE(verification[i]);
    }

    TEST_F(ShiftWithoutSwapNeighborTest, TestExhaustiveSolOperatorThree) {
      SOL smallSol(5);
      unsigned int expected[][5] = {
              {1,2,0,3,4},
              {1,2,3,0,4},
              {1,2,3,4,0},
              {0,2,3,1,4},
              {0,2,3,4,1},
              {2,0,1,3,4},
              {0,1,3,4,2},
              {3,0,1,2,4},
              {0,3,1,2,4},
              {4,0,1,2,3},
              {0,4,1,2,3},
              {0,1,4,2,3}
      };

      bool verification[] = {false, false, false, false, false, false, false, false, false, false, false, false};
      shiftWithoutSwapNeighbor.init(smallSol);
      for (long long unsigned int i = 0; i < shiftWithoutSwapNeighbor.getMaxKey(); i++) {
        SOL tmp = smallSol;
        shiftWithoutSwapNeighbor.setKey(i);
        shiftWithoutSwapNeighbor(tmp);
        bool change = false;
        for (long long unsigned int j = 0; j < shiftWithoutSwapNeighbor.getMaxKey() && !change; j++) {
          if ( !verification[j] ) {
            bool v1 = true;
            for (long long unsigned int k = 0; k < tmp.size() && v1; k++) {
              if ( tmp[k] != expected[j][k] )
                v1 = false;
            }

            if ( v1 ) {
              verification[j] = true;
              change = true;
            }
          }
        }
        ASSERT_TRUE(change);
      }

      for (long long unsigned int i = 0; i < shiftWithoutSwapNeighbor.getMaxKey(); ++i)
        ASSERT_TRUE(verification[i]);
    }

    TEST_F(ShiftWithoutSwapNeighborTest, TestMoveBackOperator) {
      shiftWithoutSwapNeighbor.setKey(3);
      shiftWithoutSwapNeighbor(permutationSolution);
      shiftWithoutSwapNeighbor.do_move_back(permutationSolution);
      unsigned int expected[] = {0,1,2,3,4,5,6,7,8,9};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], permutationSolution[i]);
    }

    TEST_F(ShiftWithoutSwapNeighborTest, TestThrowWhenMoveBackBeforeMoveOperator) {
      shiftWithoutSwapNeighbor.setKey(3);
      ASSERT_THROW(shiftWithoutSwapNeighbor.do_move_back(permutationSolution), std::runtime_error);
    }
}

#endif //MH_BUILDER_SHIFTWITHOUTSWAPNEIGHBORTEST_H
