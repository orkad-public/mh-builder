/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULESETSOLUTIONTEST_H
#define MH_BUILDER_RULESETSOLUTIONTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/ModifTermNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"
using namespace representation::rulemining ;
#include "gtest/gtest.h"

namespace test {
    class RulesetSolutionTest : public ::testing::Test {
    protected:
        RulesetSolutionTest() :
          // Entre 47 et 57ms de parsage d'instance
          rulemining("../../instances/rulemining/tictactoe/tictactoe.desc", "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
          topLeft(0, data::OP_EQUALS, 0), // topLeft = x
          topMiddle(1, data::OP_EQUALS, 0), // topMiddle = x
          topRight(2, data::OP_EQUALS, 0), // topRight = x
          middleLeft(3, data::OP_EQUALS, 0), // middleLeft = x
          middleMiddle(4, data::OP_EQUALS, 2), // middleMiddle = x
          middleRight(5, data::OP_EQUALS, 2), // middleRight = x
          bottomLeft(6, data::OP_EQUALS, 0), // bottomLeft = x
          bottomMiddle(7, data::OP_EQUALS, 1), // bottomMiddle = x
          bottomRight(8, data::OP_EQUALS, 1) // bottomRight =
          {};

        typedef core::fitness::FitnessMax<double> FIT;

        ~RulesetSolutionTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining rulemining;
        data::Term topLeft; // topLeft = x
        data::Term topMiddle; // topMiddle = x
        data::Term topRight; // topRight = x
        data::Term middleLeft; // middleLeft = x
        data::Term middleMiddle; // middleMiddle = x
        data::Term middleRight; // middleRight = x
        data::Term bottomLeft; // bottomLeft = x
        data::Term bottomMiddle; // bottomMiddle = x
        data::Term bottomRight; // bottomRight = x
    };

    TEST_F(RulesetSolutionTest, TestEvalTerm) {
      //Test on first individual : {x, x, x} {x, o, o} {b, o, b}
      ASSERT_EQ(topLeft.eval(rulemining.getIndividuals()[0]), 1);
      ASSERT_EQ(topMiddle.eval(rulemining.getIndividuals()[0]), 1);
      ASSERT_EQ(topRight.eval(rulemining.getIndividuals()[0]), 1);
      ASSERT_EQ(middleLeft.eval(rulemining.getIndividuals()[0]), 1);
      ASSERT_EQ(middleMiddle.eval(rulemining.getIndividuals()[0]), 0);
      ASSERT_EQ(middleRight.eval(rulemining.getIndividuals()[0]), 0);
      ASSERT_EQ(bottomLeft.eval(rulemining.getIndividuals()[0]), 0);
      ASSERT_EQ(bottomMiddle.eval(rulemining.getIndividuals()[0]), 0);
      ASSERT_EQ(bottomRight.eval(rulemining.getIndividuals()[0]), 0);
    }

    TEST_F(RulesetSolutionTest, TestEvalRule) {
      std::vector<data::Term> topTerms({topLeft, topMiddle, topRight});
        data::Rule<FIT> rule(topTerms);

      //Test rule on individual 0 : {x, x, x} {x, o, o} {b, o, b}
      ASSERT_EQ(rule.eval(rulemining.getIndividuals()[0]), 1);
      //Test rule 1 on individual 58 : {x, x, o} {x, x,o} {o, o, x}
      ASSERT_EQ(rule.eval(rulemining.getIndividuals()[58]), 0);
      //Test rule 1 on individual 605 : {o, o, b} {x, o, x} {x, o, x}
      ASSERT_EQ(rule.eval(rulemining.getIndividuals()[605]), 0);
    }

    TEST_F(RulesetSolutionTest, TestEvalRuleset) {
      std::vector<data::Term> topTerms({topLeft, topMiddle, topRight});
      std::vector<data::Term> middleTerms({middleLeft, middleMiddle, middleRight});
      std::vector<data::Term> bottomTerms({bottomLeft, bottomMiddle, bottomRight});
      std::vector<data::Term> col1Terms({topLeft, middleLeft, bottomLeft});
      std::vector<data::Term> col2Terms({topMiddle, middleMiddle, bottomMiddle});
      std::vector<data::Term> col3Terms({topRight, middleRight, bottomRight});
      std::vector<data::Term> diagToRightTerms({topLeft, middleMiddle, bottomRight});
      std::vector<data::Term> diagToLeftTerms({topRight, middleMiddle, bottomLeft});

        data::Rule<FIT> rule1(topTerms);
        data::Rule<FIT> rule2(middleTerms);
        data::Rule<FIT> rule3(bottomTerms);
        data::Rule<FIT> rule4(col1Terms);
        data::Rule<FIT> rule5(col2Terms);
        data::Rule<FIT> rule6(col3Terms);
        data::Rule<FIT> rule7(diagToLeftTerms);
        data::Rule<FIT> rule8(diagToRightTerms);

      RulesetSolution<core::fitness::FitnessMax<double>> sol;
      sol.push_back(rule1);
      sol.push_back(rule2);
      sol.push_back(rule3);
      sol.push_back(rule4);
      sol.push_back(rule5);
      sol.push_back(rule6);
      sol.push_back(rule7);
      sol.push_back(rule8);

      //Test rule on individual 0 : {x, x, x} {x, o, o} {b, o, b}
      ASSERT_EQ(sol.eval(rulemining.getIndividuals()[0]), 1);
      //Test rule 1 on individual 58 : {x, x, o} {x, x,o} {o, o, x}
      ASSERT_EQ(sol.eval(rulemining.getIndividuals()[58]), 1);
      //Test rule 1 on individual 605 : {o, o, b} {x, o, x} {x, o, x}
      ASSERT_EQ(sol.eval(rulemining.getIndividuals()[605]), 0);
    }
}

#endif //MH_BUILDER_RULESETSOLUTIONTEST_H
