/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULEMININGEVALTEST_H
#define MH_BUILDER_RULEMININGEVALTEST_H


#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/ModifTermNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"
using namespace representation::rulemining ;


#include "gtest/gtest.h"

namespace test {
    class RuleminingEvalTest : public ::testing::Test {
    protected:
        RuleminingEvalTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
                topLeft(0, data::OP_EQUALS, 0), // topLeft = x
                topMiddle(1, data::OP_EQUALS, 0), // topMiddle = x
                topRight(2, data::OP_EQUALS, 0), // topRight = x
                middleLeft(3, data::OP_EQUALS, 0), // middleLeft = x
                middleMiddle(4, data::OP_EQUALS, 2), // middleMiddle = x
                middleRight(5, data::OP_EQUALS, 2), // middleRight = x
                bottomLeft(6, data::OP_EQUALS, 0), // bottomLeft = x
                bottomMiddle(7, data::OP_EQUALS, 1), // bottomMiddle = x
                bottomRight(8, data::OP_EQUALS, 1), // bottomRight =
                rulesetEval(rulemining)
        {};

        typedef core::fitness::FitnessMax<double> FIT;

        ~RuleminingEvalTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining rulemining;
        data::Term topLeft; // topLeft = x
        data::Term topMiddle; // topMiddle = x
        data::Term topRight; // topRight = x
        data::Term middleLeft; // middleLeft = x
        data::Term middleMiddle; // middleMiddle = x
        data::Term middleRight; // middleRight = x
        data::Term bottomLeft; // bottomLeft = x
        data::Term bottomMiddle; // bottomMiddle = x
        data::Term bottomRight; // bottomRight = x
        RulesetEval<RulesetSolution<core::fitness::FitnessMax<double>>> rulesetEval;
    };

    TEST_F(RuleminingEvalTest, TestEval) {
      std::vector<data::Term> topTerms({topLeft, topMiddle, topRight});
      std::vector<data::Term> middleTerms({middleLeft, middleMiddle, middleRight});
      std::vector<data::Term> bottomTerms({bottomLeft, bottomMiddle, bottomRight});
      std::vector<data::Term> col1Terms({topLeft, middleLeft, bottomLeft});
      std::vector<data::Term> col2Terms({topMiddle, middleMiddle, bottomMiddle});
      std::vector<data::Term> col3Terms({topRight, middleRight, bottomRight});
      std::vector<data::Term> diagToRightTerms({topLeft, middleMiddle, bottomRight});
      std::vector<data::Term> diagToLeftTerms({topRight, middleMiddle, bottomLeft});

        data::Rule<FIT> rule1(topTerms);
        data::Rule<FIT> rule2(middleTerms);
        data::Rule<FIT> rule3(bottomTerms);
        data::Rule<FIT> rule4(col1Terms);
        data::Rule<FIT> rule5(col2Terms);
        data::Rule<FIT> rule6(col3Terms);
        data::Rule<FIT> rule7(diagToLeftTerms);
        data::Rule<FIT> rule8(diagToRightTerms);

      RulesetSolution<core::fitness::FitnessMax<double>> sol;
      sol.push_back(rule1);
      sol.push_back(rule2);
      sol.push_back(rule3);
      sol.push_back(rule4);
      sol.push_back(rule5);
      sol.push_back(rule6);
      sol.push_back(rule7);
      sol.push_back(rule8);

      rulesetEval(sol);
      ASSERT_EQ(sol.fitness().objectives()[0], 1); // Sensitivity
      ASSERT_EQ(sol.fitness().objectives()[1], 1); // Confidence
      ASSERT_EQ(sol.fitness().objectives()[2], -24); // Nb Terms
    }

}

#endif //MH_BUILDER_RULEMININGEVALTEST_H
