/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULESETNEIGHBORHOODTEST_H
#define MH_BUILDER_RULESETNEIGHBORHOODTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/ModifTermNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"
using namespace representation::rulemining ;

#include "gtest/gtest.h"

namespace test {
    class RulesetNeighborhoodTest : public ::testing::Test {
    protected:
        typedef core::fitness::FitnessMax<double> FIT;
        typedef data::Rule<FIT> RULE;
        typedef RulesetSolution<FIT> SOLUTION;

        RulesetNeighborhoodTest() :
                ruleminingTictactoe("../../instances/rulemining/tictactoe/tictactoe.desc",
                                    "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
                ruleminingEcoli1d("../../instances/rulemining/ecoli1d/ecoli1d.desc",
                                  "../../instances/rulemining/ecoli1d/ecoli1d.individuals.1.training") {};

        ~RulesetNeighborhoodTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining ruleminingTictactoe;
        Rulemining ruleminingEcoli1d;
    };

    TEST_F(RulesetNeighborhoodTest, TicTacToeRulesetNeighborhoodSizeTest) {

      representation::rulemining::neighborhood::RulesetNeighborhood<SOLUTION> rulesetNeighborhood(ruleminingTictactoe);
      std::vector<RULE> rules_init;
      rules_init.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      SOLUTION solution(rules_init);
      rulesetNeighborhood.init(solution);

      std::vector<SOLUTION> rulesets;
      // RULESET NEIGHBORS
      // 27 new ADD RULE
      std::vector<RULE> rules_to_add;
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topLeft = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topLeft = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topMiddle = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topMiddle = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topMiddle = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topRight = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topRight = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("topRight = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleLeft = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleLeft = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleLeft = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleMiddle = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleMiddle = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleMiddle = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleRight = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleRight = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("middleRight = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomLeft = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomLeft = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomLeft = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomMiddle = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomMiddle = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomMiddle = b"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomRight = x"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomRight = o"));
      rules_to_add.emplace_back(ruleminingTictactoe.parseTerms("bottomRight = b"));
      for (const auto& rule: rules_to_add) {
        SOLUTION sol(rules_init);
        sol.push_back(rule);
        rulesets.push_back(sol);
      }
      // DROP Solution
      SOLUTION empty;
      rulesets.push_back(empty);
      // Other rules neighbor
      std::vector<RULE> rules;
      // 18 ADD MODFI
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topMiddle = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topMiddle = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topMiddle = b"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topRight = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topRight = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topRight = b"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleMiddle = x"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleMiddle = o"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleMiddle = b"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleRight = x"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleRight = o"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleRight = b"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomMiddle = x"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomMiddle = o"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomMiddle = b"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomRight = x"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomRight = o"));
      rules.emplace_back(
              ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomRight = b"));
      // 3 DROP MODIF
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("middleLeft = x, bottomLeft = x"));
      // 6 MODIF MODIF
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = o, middleLeft = x, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = b, middleLeft = x, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = o, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = b, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = b"));


      for (auto &rule: rules) {
        std::vector<RULE> vectorToAdd;
        vectorToAdd.push_back(rule);
        rulesets.emplace_back(SOLUTION(vectorToAdd));
      }


      std::vector<int> founded(rulesets.size());
      for (unsigned long long int i = 0; i < rulesets.size(); i++) founded[i] = 0;

      //Init move
      rulesetNeighborhood(solution);
      for (unsigned long long int i = 0; i < rulesets.size(); i++) {
        if (rulesets[i] == solution)
          founded[i] = 1;
      }
      rulesetNeighborhood.move_back(solution);

      // Other moves
      do {
        rulesetNeighborhood.next();
        rulesetNeighborhood(solution);
        for (unsigned long long int i = 0; i < rulesets.size(); i++) {
          if (rulesets[i] == solution) {
            ASSERT_FALSE(founded[i]); // Only one
            founded[i] = 1;
          }
        }
        rulesetNeighborhood.move_back(solution);

      } while (rulesetNeighborhood.hasNextNeighbor());

      ASSERT_EQ(static_cast<unsigned long long int>(55), rulesetNeighborhood.size());
      for (unsigned long long int i = 0; i < rulesets.size(); i++) {
        ASSERT_TRUE(founded[i]);
      }
    }
}
#endif //MH_BUILDER_RULESETNEIGHBORHOODTEST_H
