/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ADDTERMNEIGHBORTEST_H
#define MH_BUILDER_ADDTERMNEIGHBORTEST_H


#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/AddTermNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"


#include "gtest/gtest.h"

namespace test {
    class AddTermNeighborTest : public ::testing::Test {
    protected:
        AddTermNeighborTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
                topLeft(0, representation::rulemining::data::OP_EQUALS, 0), // topLeft = x
                topMiddle(1, representation::rulemining::data::OP_EQUALS, 0), // topMiddle = x
                topRight(2, representation::rulemining::data::OP_EQUALS, 0), // topRight = x
                middleLeft(3, representation::rulemining::data::OP_EQUALS, 0), // middleLeft = x
                middleMiddle(4, representation::rulemining::data::OP_EQUALS, 2), // middleMiddle = x
                middleRight(5, representation::rulemining::data::OP_EQUALS, 2), // middleRight = x
                bottomLeft(6, representation::rulemining::data::OP_EQUALS, 0), // bottomLeft = x
                bottomMiddle(7, representation::rulemining::data::OP_EQUALS, 1), // bottomMiddle = x
                bottomRight(8, representation::rulemining::data::OP_EQUALS, 1) // bottomRight =
        {};

        typedef core::fitness::FitnessMax<double> FIT;
        typedef representation::rulemining::data::Rule<FIT> RULE;

        ~AddTermNeighborTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        representation::rulemining::Rulemining rulemining;
        representation::rulemining::data::Term topLeft; // topLeft = x
        representation::rulemining::data::Term topMiddle; // topMiddle = x
        representation::rulemining::data::Term topRight; // topRight = x
        representation::rulemining::data::Term middleLeft; // middleLeft = x
        representation::rulemining::data::Term middleMiddle; // middleMiddle = x
        representation::rulemining::data::Term middleRight; // middleRight = x
        representation::rulemining::data::Term bottomLeft; // bottomLeft = x
        representation::rulemining::data::Term bottomMiddle; // bottomMiddle = x
        representation::rulemining::data::Term bottomRight; // bottomRight = x
    };

    TEST_F(AddTermNeighborTest, AddTermTest) {
      RULE rule1;
      RULE rule2;
      representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addTermNeighbor(topLeft);
      addTermNeighbor.operator()(rule1);
      rule2.push_back(topLeft);
      ASSERT_EQ(rule1, rule2);
    }

    TEST_F(AddTermNeighborTest, AddTermAlreadyExistInRuleThrowExceptionTest) {
      RULE rule1;
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addTermNeighbor(topLeft);
      addTermNeighbor(rule1);
      ASSERT_THROW(addTermNeighbor(rule1), std::runtime_error);
    }

    TEST_F(AddTermNeighborTest, AddTermInAttribueIdOrderTest) {
      RULE rule1;
      RULE rule2;
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addTopLeft(topLeft);
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addMiddleLeft(middleLeft);
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addBottomRight(bottomRight);
      addMiddleLeft.operator()(rule1);
      addTopLeft.operator()(rule1);
      addBottomRight.operator()(rule1);
      rule2.push_back(topLeft);
      rule2.push_back(bottomRight);
      rule2.push_back(middleLeft);
      ASSERT_EQ(rule1, rule2);
    }

    TEST_F(AddTermNeighborTest, AddTermMoveBackTest) {
      RULE rule1;
      RULE rule2;
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addTopLeft(topLeft);
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addMiddleLeft(middleLeft);
        representation::rulemining::neighborhood::neighbor::AddTermNeighbor<RULE> addBottomRight(bottomRight);
      addMiddleLeft.operator()(rule1);
      addTopLeft.operator()(rule1);
      addTopLeft.do_move_back(rule1);
      addBottomRight.operator()(rule1);
      rule2.push_back(bottomRight);
      rule2.push_back(middleLeft);
      ASSERT_EQ(rule1, rule2);
    }

}

#endif //MH_BUILDER_ADDTERMNEIGHBORTEST_H
