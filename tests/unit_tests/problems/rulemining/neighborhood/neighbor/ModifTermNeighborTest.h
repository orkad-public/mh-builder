/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MODIFTERMNEIGHBORTEST_H
#define MH_BUILDER_MODIFTERMNEIGHBORTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/ModifTermNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"
using namespace representation::rulemining ;
#include "gtest/gtest.h"

namespace test {
    class ModifTermNeighborTest : public ::testing::Test {
    protected:
        ModifTermNeighborTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
                topLeft(0, data::OP_EQUALS, 0), // topLeft = x
                topMiddle(1, data::OP_EQUALS, 0), // topMiddle = x
                topRight(2, data::OP_EQUALS, 0), // topRight = x
                middleLeft(3, data::OP_EQUALS, 0), // middleLeft = x
                middleMiddle(4, data::OP_EQUALS, 2), // middleMiddle = x
                middleRight(5, data::OP_EQUALS, 2), // middleRight = x
                bottomLeft(6, data::OP_EQUALS, 0), // bottomLeft = x
                bottomMiddle(7, data::OP_EQUALS, 1), // bottomMiddle = x
                bottomRight(8, data::OP_EQUALS, 1) // bottomRight = x
        {};

        typedef core::fitness::FitnessMax<double> FIT;
        typedef data::Rule<FIT> RULE;

        ~ModifTermNeighborTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining rulemining;
        data::Term topLeft; // topLeft = x
        data::Term topMiddle; // topMiddle = x
        data::Term topRight; // topRight = x
        data::Term middleLeft; // middleLeft = x
        data::Term middleMiddle; // middleMiddle = x
        data::Term middleRight; // middleRight = x
        data::Term bottomLeft; // bottomLeft = x
        data::Term bottomMiddle; // bottomMiddle = x
        data::Term bottomRight; // bottomRight = x
    };

    TEST_F(ModifTermNeighborTest, ModifTermTest) {
      RULE rule1;
      RULE rule2;

      rule1.push_back(topLeft);
        data::Term topLeftO = data::Term(0, data::OP_EQUALS, 1);
      representation::rulemining::neighborhood::neighbor::ModifTermNeighbor<RULE> modifTermNeighbor(topLeftO);
      modifTermNeighbor(rule1);
      rule2.push_back(topLeftO);
      ASSERT_EQ(rule1, rule2);
    }

    TEST_F(ModifTermNeighborTest, ModifTermNotExistInRuleThrowExceptionTest) {
      RULE rule1;
        data::Term topLeftO = data::Term(0, data::OP_EQUALS, 1);
        representation::rulemining::neighborhood::neighbor::ModifTermNeighbor<RULE> modifTermNeighbor(topLeftO);
      ASSERT_THROW(modifTermNeighbor(rule1), std::runtime_error);
    }

    TEST_F(ModifTermNeighborTest, ModifTermNeighborTest) {
      RULE rule1;
      RULE rule2;
      rule1.push_back(topLeft);
      rule1.push_back(middleLeft);
      rule1.push_back(bottomRight);

        data::Term topLeftO = data::Term(0, data::OP_EQUALS, 1);
        representation::rulemining::neighborhood::neighbor::ModifTermNeighbor<RULE> modifTermNeighbor(topLeftO);
      modifTermNeighbor(rule1);
      modifTermNeighbor.do_move_back(rule1);
      rule2.push_back(topLeft);
      rule2.push_back(bottomRight);
      rule2.push_back(middleLeft);
      ASSERT_EQ(rule1, rule2);
    }

}

#endif //MH_BUILDER_MODIFTERMNEIGHBORTEST_H
