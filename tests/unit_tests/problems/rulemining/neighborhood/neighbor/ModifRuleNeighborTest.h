/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MODIFRULENEIGHBORTEST_H
#define MH_BUILDER_MODIFRULENEIGHBORTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/ModifRuleNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"
using namespace representation::rulemining ;
#include "gtest/gtest.h"

namespace test {
    class ModifRuleNeighborTest : public ::testing::Test {
    protected:
        ModifRuleNeighborTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training")
        {};

        typedef core::fitness::FitnessMax<double> FIT;
        typedef data::Rule<FIT> RULE;
        typedef RulesetSolution<FIT> RULESET;

        ~ModifRuleNeighborTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining rulemining;
    };

    TEST_F(ModifRuleNeighborTest, ModifRuleTest) {
      RULESET ruleset1;
      RULE rule1(rulemining.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      RULE rule2(rulemining.parseTerms("topMiddle = x, middleMiddle = x, bottomMiddle = x"));
      RULE rule2modif(rulemining.parseTerms("topMiddle = x, middleMiddle = b, bottomMiddle = x"));
      RULE rule3(rulemining.parseTerms("topRight = x, middleRight = x, bottomRight = x"));

      data::Term middleMiddleO = data::Term(4, data::OP_EQUALS, 1);
      representation::rulemining::neighborhood::neighbor::ModifTermNeighbor<RULE> modifTermNeighbor(middleMiddleO);

      ruleset1.push_back(rule1);
      ruleset1.push_back(rule2);
      ruleset1.push_back(rule3);

      RULESET ruleset2;
      ruleset2.push_back(rule1);
      ruleset2.push_back(rule2modif);
      ruleset2.push_back(rule3);

        representation::rulemining::neighborhood::neighbor::ModifRuleNeighbor<RULESET> modifRuleNeighbor(&modifTermNeighbor, 1);
      modifRuleNeighbor(ruleset1);

      ASSERT_EQ(ruleset1, ruleset2);
    }

   TEST_F(ModifRuleNeighborTest, ModifRuleMoveBackTest) {
      RULESET ruleset1;
      RULE rule1(rulemining.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      RULE rule2(rulemining.parseTerms("topMiddle = x, middleMiddle = x, bottomMiddle = x"));
      RULE rule3(rulemining.parseTerms("topRight = x, middleRight = x, bottomRight = x"));

      data::Term middleMiddleO = data::Term(4, data::OP_EQUALS, 1);
        representation::rulemining::neighborhood::neighbor::ModifTermNeighbor<RULE> modifTermNeighbor(middleMiddleO);

      ruleset1.push_back(rule1);
      ruleset1.push_back(rule2);
      ruleset1.push_back(rule3);

      RULESET ruleset2;
      ruleset2.push_back(rule1);
      ruleset2.push_back(rule2);
      ruleset2.push_back(rule3);

        representation::rulemining::neighborhood::neighbor::ModifRuleNeighbor<RULESET> modifRuleNeighbor(&modifTermNeighbor, 1);
      modifRuleNeighbor(ruleset1);
      modifRuleNeighbor.do_move_back(ruleset1);
      ASSERT_EQ(ruleset1, ruleset2);
    }

}


#endif //MH_BUILDER_MODIFRULENEIGHBORTEST_H
