/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_DROPRULENEIGHBORTEST_H
#define MH_BUILDER_DROPRULENEIGHBORTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/DropRuleNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"

using namespace representation::rulemining ;
#include "gtest/gtest.h"

namespace test {
    class DropRuleNeighborTest : public ::testing::Test {
    protected:
        DropRuleNeighborTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training")
        {};

        typedef core::fitness::FitnessMax<double> FIT;
        typedef data::Rule<FIT> RULE;
        typedef RulesetSolution<FIT> RULESET;

        ~DropRuleNeighborTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining rulemining;
    };

    TEST_F(DropRuleNeighborTest, DropRuleTest) {
      RULESET ruleset1;
      RULE rule1(rulemining.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      ruleset1.push_back(rule1);
      RULE ruleToDrop(rulemining.parseTerms("topMiddle = x, middleMiddle = x, bottomMiddle = x"));
      ruleset1.push_back(ruleToDrop);
      RULE rule2(rulemining.parseTerms("topRight = x, middleRight = x, bottomRight = x"));
      ruleset1.push_back(rule2);

      RULESET ruleset2;
      ruleset2.push_back(rule1);
      ruleset2.push_back(rule2);

      representation::rulemining::neighborhood::neighbor::DropRuleNeighbor<RULESET> dropRuleNeighbor(1);
      dropRuleNeighbor(ruleset1);

      ASSERT_EQ(ruleset1, ruleset2);
    }

    TEST_F(DropRuleNeighborTest, DropRuleMoveBackTest) {
      RULESET ruleset1;
      RULE rule1(rulemining.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      ruleset1.push_back(rule1);
      RULE rule2(rulemining.parseTerms("topMiddle = x, middleMiddle = x, bottomMiddle = x"));
      ruleset1.push_back(rule2);
      RULE rule3(rulemining.parseTerms("topRight = x, middleRight = x, bottomRight = x"));
      ruleset1.push_back(rule3);

      RULESET ruleset2;
      ruleset2.push_back(rule1);
      ruleset2.push_back(rule2);
      ruleset2.push_back(rule3);

        representation::rulemining::neighborhood::neighbor::DropRuleNeighbor<RULESET> dropRuleNeighbor(1);
      dropRuleNeighbor(ruleset1);
      dropRuleNeighbor.do_move_back(ruleset1);

      ASSERT_EQ(ruleset1, ruleset2);
    }

}

#endif //MH_BUILDER_DROPRULENEIGHBORTEST_H
