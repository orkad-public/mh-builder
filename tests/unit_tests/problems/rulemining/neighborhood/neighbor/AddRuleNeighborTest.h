/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ADDRULENEIGHBORTEST_H
#define MH_BUILDER_ADDRULENEIGHBORTEST_H


#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/AddRuleNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"

#include "opt/manySolutions/explorer/AllExplorer.h"
#include "opt/manySolutions/selection/SelectAll.h"

#include "opt/manySolutions/localsearch/ParetoLocalSearch.h"
#include "gtest/gtest.h"

namespace test {
    class AddRuleNeighborTest : public ::testing::Test {
    protected:
        AddRuleNeighborTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training")
        {};

        typedef core::fitness::FitnessMax<double> FIT;
        typedef representation::rulemining::data::Rule<FIT> RULE;
        typedef representation::rulemining::RulesetSolution<FIT> RULESET;

        ~AddRuleNeighborTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        representation::rulemining::Rulemining rulemining;
    };

    TEST_F(AddRuleNeighborTest, AddRuleTest) {
      RULESET ruleset1;
      RULE ruleToAdd(rulemining.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      RULESET ruleset2;
        representation::rulemining::neighborhood::neighbor::AddRuleNeighbor<RULESET> addRuleNeighbor(ruleToAdd);
      addRuleNeighbor(ruleset1);
      ruleset2.push_back(ruleToAdd);
      ASSERT_EQ(ruleset1, ruleset2);
    }

    TEST_F(AddRuleNeighborTest, AddRuleMoveBackTest) {
      RULESET ruleset1;
      RULE ruleToAdd1(rulemining.parseTerms("topLeft = x, middleLeft = o, bottomLeft = b"));
      RULE ruleToAdd2(rulemining.parseTerms("topMiddle = o, middleMiddle = b, bottomMiddle = x"));
      RULE ruleToAdd3(rulemining.parseTerms("topRight = b, middleRight = x, bottomRight = o"));
      RULESET ruleset2;

        representation::rulemining::neighborhood::neighbor::AddRuleNeighbor<RULESET> addRuleNeighbor1(ruleToAdd1);
      addRuleNeighbor1(ruleset1);
        representation::rulemining::neighborhood::neighbor::AddRuleNeighbor<RULESET> addRuleNeighbor2(ruleToAdd2);
      addRuleNeighbor2(ruleset1);
      addRuleNeighbor2.do_move_back(ruleset1);
        representation::rulemining::neighborhood::neighbor::AddRuleNeighbor<RULESET> addRuleNeighbor3(ruleToAdd3);
      addRuleNeighbor3(ruleset1);

      ruleset2.push_back(ruleToAdd1);
      ruleset2.push_back(ruleToAdd3);
      ASSERT_EQ(ruleset1, ruleset2);
    }

}

#endif //MH_BUILDER_ADDRULENEIGHBORTEST_H
