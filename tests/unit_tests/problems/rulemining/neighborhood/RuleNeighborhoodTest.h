/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULENEIGHBORHOODTEST_H
#define MH_BUILDER_RULENEIGHBORHOODTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/ModifTermNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"
using namespace representation::rulemining ;

#include "gtest/gtest.h"

namespace test {
    class RuleNeighborhoodTest : public ::testing::Test {
    protected:
        typedef data::Rule<core::fitness::FitnessMax<double>> RULE;

        RuleNeighborhoodTest() :
                ruleminingTictactoe("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
                ruleminingEcoli1d("../../instances/rulemining/ecoli1d/ecoli1d.desc",
                                    "../../instances/rulemining/ecoli1d/ecoli1d.individuals.1.training") {};

        ~RuleNeighborhoodTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        Rulemining ruleminingTictactoe;
        Rulemining ruleminingEcoli1d;
    };


    TEST_F(RuleNeighborhoodTest, TicTacToeRuleNeighborhoodSizeTest) {

      representation::rulemining::neighborhood::RuleNeighborhood<RULE> ruleNeighborhood(ruleminingTictactoe);
      RULE rule(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x"));
      ruleNeighborhood.init(rule);
      // 18 ADD
      std::vector<RULE> rules;
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topMiddle = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topMiddle = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topMiddle = b"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topRight = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topRight = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, topRight = b"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleMiddle = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleMiddle = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleMiddle = b"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleRight = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleRight = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, middleRight = b"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomMiddle = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomMiddle = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomMiddle = b"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomRight = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomRight = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = x, bottomRight = b"));
      // 3 DROP
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("middleLeft = x, bottomLeft = x"));
      // 6 MODIF
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = o, middleLeft = x, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = b, middleLeft = x, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = o, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = b, bottomLeft = x"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = o"));
      rules.emplace_back(ruleminingTictactoe.parseTerms("topLeft = x, middleLeft = x, bottomLeft = b"));

      std::vector<int> founded(rules.size());
      for (unsigned long long int i = 0; i < rules.size(); i++) founded[i] = 0;

      //Init move
      ruleNeighborhood(rule);
      for (unsigned long long int i = 0; i < rules.size(); i++) {
        if ( rules[i] == rule)
          founded[i] = 1;
      }
      ruleNeighborhood.move_back(rule);

      // Other moves
      do {
        ruleNeighborhood.next();
        ruleNeighborhood(rule);
        for (unsigned long long int i = 0; i < rules.size(); i++) {
          if ( rules[i] == rule) {
            ASSERT_FALSE(founded[i]); // Only one
            founded[i] = 1;
          }

        }
        ruleNeighborhood.move_back(rule);

      } while ( ruleNeighborhood.hasNextNeighbor() );

      ASSERT_EQ(static_cast<unsigned long long int>(27), ruleNeighborhood.size());
      for (unsigned long long int i = 0; i < rules.size(); i++)
        ASSERT_TRUE(founded[i]);
    }


    TEST_F(RuleNeighborhoodTest, Ecoli1dRuleNeighborhoodSizeTest) {

        representation::rulemining::neighborhood::RuleNeighborhood<RULE> ruleNeighborhood(ruleminingEcoli1d);
      RULE rule(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 > '(-inf-0.099]'"));

      ruleNeighborhood.init(rule);
      // 90 ADDS + 3 DROPS + 9 MODIFS = 40 Neighbors

      std::vector<RULE> rules;
      // SOME ADDS
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 > '(-inf-0.099]', mcg > '(-inf-0.089]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 > '(-inf-0.099]', lip < '(0.532-inf)'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 > '(-inf-0.099]', alm1 = '(-inf-0.127]'"));
      // 3 DROPS
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("aac < '(0.352-0.44]', alm2 > '(-inf-0.099]'"));
      // 9 MODIFS
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg > '(-inf-0.55]', aac < '(0.352-0.44]', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(-inf-0.55]', aac < '(0.352-0.44]', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac = '(0.264-0.352]', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.264-0.352]', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.44-0.528]', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac > '(0.176-0.264]', alm2 > '(-inf-0.099]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 = '(0.099-0.198]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 > '(0.099-0.198]'"));
      rules.emplace_back(ruleminingEcoli1d.parseTerms("chg = '(0.55-inf)', aac < '(0.352-0.44]', alm2 < '(0.198-0.297]'"));

      std::vector<int> founded(rules.size());
      for (unsigned long long int i = 0; i < rules.size(); i++) founded[i] = 0;

      //Init move
      ruleNeighborhood(rule);

      for (unsigned long long int i = 0; i < rules.size(); i++) {
        if ( rules[i] == rule)
          founded[i] = 1;
      }
      ruleNeighborhood.move_back(rule);

      // Other moves
      do {
        ruleNeighborhood.next();
        ruleNeighborhood(rule);
        for (unsigned long long int i = 0; i < rules.size(); i++) {
          if ( rules[i] == rule) {
            ASSERT_FALSE(founded[i]); // Only one
            founded[i] = 1;
          }

        }
        ruleNeighborhood.move_back(rule);

      } while ( ruleNeighborhood.hasNextNeighbor() );

      ASSERT_EQ(static_cast<unsigned long long int>(99), ruleNeighborhood.size());
      for (unsigned long long int i = 0; i < rules.size(); i++) {
        ASSERT_TRUE(founded[i]);
      }

    }

}


#endif //MH_BUILDER_RULENEIGHBORHOODTEST_H
