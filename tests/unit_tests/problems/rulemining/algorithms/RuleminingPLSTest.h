/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_RULEMININGPLSTEST_H
#define MH_BUILDER_RULEMININGPLSTEST_H

#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/RulesetSolution.h"
#include "representation/rulemining/algorithm/InitRuleset.h"
#include "representation/rulemining/data/Rule.h"
#include "representation/rulemining/neighborhood/neighbor/AddRuleNeighbor.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"

#include "opt/manySolutions/explorer/AllExplorer.h"
#include "opt/manySolutions/selection/SelectAll.h"

#include "opt/manySolutions/localsearch/ParetoLocalSearch.h"

namespace test {
    class RuleminingPLSTest : public ::testing::Test {
    protected:
        typedef core::fitness::FitnessMax<double> FIT;
        typedef representation::rulemining::data::Rule<FIT> RULE;
        typedef representation::rulemining::RulesetSolution<FIT> SOLUTION;
        typedef core::archive::NonDominatedArchive<SOLUTION> ARCHIVE;

        RuleminingPLSTest() :
                rulemining("../../instances/rulemining/tictactoe/tictactoe.desc",
                           "../../instances/rulemining/tictactoe/tictactoe.individuals.1.training"),
                rulesetEval(rulemining)
        {};

        ~RuleminingPLSTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        representation::rulemining::Rulemining rulemining;
        representation::rulemining::RulesetEval<SOLUTION> rulesetEval;
    };

    TEST_F(RuleminingPLSTest, DMLS_star_star_Test) {
      SOLUTION sol;
      ARCHIVE archive;
        representation::rulemining::algorithm::InitRuleset initRuleset(rulemining, rulesetEval);

      //DMLS (*,*)
        representation::rulemining::neighborhood::neighbor::AddRuleNeighbor<SOLUTION> dummyNeighbor((representation::rulemining::data::Rule<FIT>()));
        representation::rulemining::neighborhood::RulesetNeighborhood<SOLUTION> rulesetNeighborhood(rulemining);
      opt::manySolutions::explorer::AllExplorer<ARCHIVE> allExplorer(rulesetNeighborhood, rulesetEval);
        opt::manySolutions::selection::SelectAll<ARCHIVE> selectAll;
      opt::manySolutions::localsearch::ParetoLocalSearch<ARCHIVE> paretoLocalSearch(selectAll, allExplorer);

      // INIT ARCHIVE
      for (int j = 0; j < 100; j++) {
        initRuleset(sol);
        rulesetEval(sol);
        archive(sol);
      }
      paretoLocalSearch(archive);
      // Found best Solution
      rulesetEval.setRuleminingEvalType(representation::rulemining::FMEASURE_ONLY);
      for(auto &solution : archive) {
        rulesetEval(solution);
      }
      archive.update();
      ASSERT_EQ(archive.size(), static_cast<unsigned long long int>(1));
      ASSERT_EQ(archive[0].fitness().objectives()[0], static_cast<unsigned long long int>(1));
    }

}


#endif //MH_BUILDER_RULEMININGPLSTEST_H
