/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FLIPNEIGHBORTEST_H
#define MH_BUILDER_FLIPNEIGHBORTEST_H

#include "representation/bitstring/BitstringSolution.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"

#include "gtest/gtest.h"

namespace test {

    class FlipNeighborTest : public ::testing::Test {
    protected:

        typedef representation::bitstring::BitstringSolution<core::fitness::FitnessMax<double>> SOL;

        FlipNeighborTest() : bitstringSolution(10) { this->bitstringSolution.reset() ; };

        ~FlipNeighborTest() override = default;

        void SetUp() override {
          flipNeighbor.init(bitstringSolution);
        }

        void TearDown() override {}

        SOL bitstringSolution;
        representation::bitstring::neighborhood::neighbor::FlipNeighbor<SOL> flipNeighbor;
    };

    TEST_F(FlipNeighborTest, TestOperator) {
      flipNeighbor.setKey(3);
      flipNeighbor(bitstringSolution);
      unsigned int expected[] = {0,0,0,1,0,0,0,0,0,0};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], bitstringSolution[i]);
    }

    TEST_F(FlipNeighborTest, TestMoveBackOperator) {
      flipNeighbor.setKey(3);
      flipNeighbor(bitstringSolution);
      flipNeighbor.do_move_back(bitstringSolution);
      unsigned int expected[] = {0,0,0,0,0,0,0,0,0,0};
      for (int i = 0; i < 10; ++i)
        ASSERT_EQ(expected[i], bitstringSolution[i]);
    }

    TEST_F(FlipNeighborTest, TestThrowWhenMoveBackBeforeMoveOperator) {
      flipNeighbor.setKey(3);
      ASSERT_THROW(flipNeighbor.do_move_back(bitstringSolution), std::runtime_error);
    }

    TEST_F(FlipNeighborTest, TestTheNeighborhoodSize) {
      ASSERT_EQ(bitstringSolution.size(), flipNeighbor.getMaxKey());
    }
}

#endif //MH_BUILDER_FLIPNEIGHBORTEST_H
