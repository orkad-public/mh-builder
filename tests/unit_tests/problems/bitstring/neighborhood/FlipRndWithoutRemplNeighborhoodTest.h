/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FLIPRNDWITHOUTREMPLNEIGHBORHOODTEST_H
#define MH_BUILDER_FLIPRNDWITHOUTREMPLNEIGHBORHOODTEST_H

#include "representation/bitstring/BitstringSolution.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "opt/singleSolution/neighborhood/RandomNeighborhood.h"
namespace test {
    class FlipRndWithoutRemplNeighborhoodTest : public ::testing::Test {
    protected:

        typedef representation::bitstring::BitstringSolution<core::fitness::FitnessMax<double>> SOL;

        FlipRndWithoutRemplNeighborhoodTest() : bitstringSolution(10), rndWithoutRemplNeighborhood(flipNeighbor) { this->bitstringSolution.reset() ;};

        ~FlipRndWithoutRemplNeighborhoodTest() override = default;

        void SetUp() override {
          rndWithoutRemplNeighborhood.init(bitstringSolution);
        }

        void TearDown() override {}

        SOL bitstringSolution;
        representation::bitstring::neighborhood::neighbor::FlipNeighbor<SOL> flipNeighbor;
        opt::singleSolution::neighborhood::RandomNeighborhood<SOL> rndWithoutRemplNeighborhood;
    };

    TEST_F(FlipRndWithoutRemplNeighborhoodTest, TestIfCanNextOnly10Times) {
      for (unsigned long long int i = 0; i < bitstringSolution.size() - 1; ++i) {
        ASSERT_NO_THROW(rndWithoutRemplNeighborhood.next());
      }
      ASSERT_ANY_THROW(rndWithoutRemplNeighborhood.next());
    }

    TEST_F(FlipRndWithoutRemplNeighborhoodTest, TestExplorationOfAllNeighborhood) {
      for (unsigned long long int i = 0; i < bitstringSolution.size() - 1; ++i) {
        ASSERT_NO_THROW(flipNeighbor.do_move(bitstringSolution));
        ASSERT_NO_THROW(rndWithoutRemplNeighborhood.next());
      }
      ASSERT_NO_THROW(flipNeighbor.do_move(bitstringSolution));
      unsigned long long int expected[] = {1,1,1,1,1,1,1,1,1,1};
      for (unsigned long long int i = 0; i < bitstringSolution.size(); ++i) {
        ASSERT_EQ(expected[i], bitstringSolution[i]);
      }
    }
}

#endif //MH_BUILDER_FLIPRNDWITHOUTREMPLNEIGHBORHOODTEST_H
