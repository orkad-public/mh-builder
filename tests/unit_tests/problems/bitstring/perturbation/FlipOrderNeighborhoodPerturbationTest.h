/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_FLIPORDERNEIGHBORHOODPERTURBATIONTEST_H
#define MH_BUILDER_FLIPORDERNEIGHBORHOODPERTURBATIONTEST_H
#include "representation/bitstring/BitstringSolution.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "opt/singleSolution/perturbation/NeighborhoodPerturbation.h"

namespace test {
    class FlipOrderNeighborhoodPerturbationTest : public ::testing::Test {
    protected:

        typedef representation::bitstring::BitstringSolution<core::fitness::FitnessMax<double>> SOL;

        FlipOrderNeighborhoodPerturbationTest() : bitstringSolution(10), orderNeighborhood(flipNeighbor), neighborhoodPerturbation(orderNeighborhood) {
            this->bitstringSolution.reset() ;
        };

        ~FlipOrderNeighborhoodPerturbationTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        SOL bitstringSolution;
        representation::bitstring::neighborhood::neighbor::FlipNeighbor<SOL> flipNeighbor;
        mock::OrderNeighborhoodMock<SOL> orderNeighborhood;
        opt::singleSolution::perturbation::NeighborhoodPerturbation<SOL> neighborhoodPerturbation;
    };

    TEST_F(FlipOrderNeighborhoodPerturbationTest, TestPerturbation) {
      unsigned long long int expected[] = {1,0,0,0,0,0,0,0,0,0};
      neighborhoodPerturbation(bitstringSolution);
      for (unsigned long long int i = 0; i < bitstringSolution.size() - 1; ++i) {
        ASSERT_EQ(expected[i], bitstringSolution[i]);
      }
    }

    TEST_F(FlipOrderNeighborhoodPerturbationTest, Test2Perturbations) {
      unsigned long long int expected[] = {1,1,0,0,0,0,0,0,0,0};
      neighborhoodPerturbation.setStrength(2);
      neighborhoodPerturbation(bitstringSolution);
      for (unsigned long long int i = 0; i < bitstringSolution.size() - 1; ++i) {
        ASSERT_EQ(expected[i], bitstringSolution[i]);
      }
    }
}

#endif //MH_BUILDER_FLIPORDERNEIGHBORHOODPERTURBATIONTEST_H
