/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KSPHILLCLIMBINGTEST_H
#define MH_BUILDER_KSPHILLCLIMBINGTEST_H

#include "representation/bitstring/problems/ksp/KSP.h"
#include "representation/bitstring/problems/ksp/KSPEval.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"
#include "opt/singleSolution/neighborhood/explorer/FirstImprNeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/explorer/BestImprNeighborhoodExplorer.h"
#include "opt/singleSolution/neighborhood/explorer/WorstImprNeighborhoodExplorer.h"
#include "opt/singleSolution/localsearch/HillClimbing.h"


namespace test {
    class KSPHillClimbingTest : public ::testing::Test {
    protected:

        KSPHillClimbingTest() : ksp("../../instances/ksp/low-dimensional/f1_l-d_kp_10_269"),
                                               kspSol(ksp.getN()), kspEval(ksp), indexNeighborhood(flipNeighbor),
                                               firstImprNeighborhoodExplorer(indexNeighborhood, kspEval),
                                               bestImprNeighborhoodExplorer(indexNeighborhood, kspEval),
                                               worstImprNeighborhoodExplorer(indexNeighborhood, kspEval),
                                               hillClimbing(firstImprNeighborhoodExplorer, kspEval)
                                               { this->kspSol.reset();
                                               }

        ~KSPHillClimbingTest() override = default;

        void SetUp() override {
          kspEval(kspSol);
          hillClimbing.init(kspSol);
        }

        void TearDown() override {}

        representation::bitstring::problems::ksp::KSP ksp;
        representation::bitstring::problems::ksp::KSPSol kspSol;
        representation::bitstring::problems::ksp::KSPEval kspEval;
        representation::bitstring::neighborhood::neighbor::FlipNeighbor<representation::bitstring::problems::ksp::KSPSol> flipNeighbor;
        opt::singleSolution::neighborhood::IndexNeighborhood<representation::bitstring::problems::ksp::KSPSol> indexNeighborhood;
        opt::singleSolution::neighborhood::explorer::FirstImprNeighborhoodExplorer<representation::bitstring::problems::ksp::KSPSol> firstImprNeighborhoodExplorer;
        opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<representation::bitstring::problems::ksp::KSPSol> bestImprNeighborhoodExplorer;
        opt::singleSolution::neighborhood::explorer::WorstImprNeighborhoodExplorer<representation::bitstring::problems::ksp::KSPSol> worstImprNeighborhoodExplorer;
        opt::singleSolution::localsearch::HillClimbing<representation::bitstring::problems::ksp::KSPSol> hillClimbing;

    };

    TEST_F(KSPHillClimbingTest, HillClimbingFirstImpr) {
      int fit = kspSol.fitness()[0];
      hillClimbing(kspSol);
      fit += 208;
      ASSERT_EQ(fit, kspSol.fitness()[0]);
    }

    TEST_F(KSPHillClimbingTest, HillClimbingBestImpr) {
      int fit = kspSol.fitness()[0];
      hillClimbing.setNeighborhoodExplorer(bestImprNeighborhoodExplorer); //Change explorer
      hillClimbing(kspSol);
      fit += 288;
      ASSERT_EQ(fit, kspSol.fitness()[0]);
    }

    TEST_F(KSPHillClimbingTest, HillClimbingWorstImpr) {
      int fit = kspSol.fitness()[0];
      hillClimbing.setNeighborhoodExplorer(worstImprNeighborhoodExplorer); //Change explorer
      hillClimbing(kspSol);
      fit += 135;
      ASSERT_EQ(fit, kspSol.fitness()[0]);
    }
}

#endif //MH_BUILDER_KSPHILLCLIMBINGTEST_H
