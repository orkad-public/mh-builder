/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KSPSIMPLEILSTEST_H
#define MH_BUILDER_KSPSIMPLEILSTEST_H

#include "representation/bitstring/problems/ksp/KSP.h"
#include "representation/bitstring/problems/ksp/KSPEval.h"

using namespace representation::bitstring ;
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"

#include "opt/singleSolution/localsearch/IteratedLocalSearch.h"
#include "opt/singleSolution/perturbation/NeighborhoodPerturbation.h"
#include "opt/criterion/IterCriterion.h"

namespace test {
    class KSPSimpleILSTest : public ::testing::Test {
    protected:

        KSPSimpleILSTest() : ksp("../../instances/ksp/low-dimensional/f1_l-d_kp_10_269"),
                                kspSol(ksp.getN()), kspEval(ksp), indexNeighborhood(flipNeighbor),
                                firstImprNeighborhoodExplorer(indexNeighborhood, kspEval),
                                hillClimbing(firstImprNeighborhoodExplorer, kspEval),
                                indexNeighborhoodMock(flipNeighbor),
                                neighborhoodPerturbation(indexNeighborhoodMock, &kspEval, 6),
                                iterCriterion(1),
                                ils( neighborhoodPerturbation, hillClimbing, iterCriterion)
        { this->kspSol.reset();}

        ~KSPSimpleILSTest() override = default;

        void SetUp() override {
          kspEval(kspSol);
        }

        void TearDown() override {}

        problems::ksp::KSP ksp;
        problems::ksp::KSPSol kspSol;
        problems::ksp::KSPEval kspEval;
        neighborhood::neighbor::FlipNeighbor<problems::ksp::KSPSol> flipNeighbor;
        opt::singleSolution::neighborhood::IndexNeighborhood<problems::ksp::KSPSol> indexNeighborhood;
        opt::singleSolution::neighborhood::explorer::FirstImprNeighborhoodExplorer<problems::ksp::KSPSol> firstImprNeighborhoodExplorer;
        opt::singleSolution::localsearch::HillClimbing<problems::ksp::KSPSol> hillClimbing;
        mock::OrderNeighborhoodMock<problems::ksp::KSPSol> indexNeighborhoodMock;
        opt::singleSolution::perturbation::NeighborhoodPerturbation<problems::ksp::KSPSol> neighborhoodPerturbation;
        opt::criterion::IterCriterion<problems::ksp::KSPSol> iterCriterion;
        opt::singleSolution::localsearch::IteratedLocalSearch<problems::ksp::KSPSol> ils;
    };

    TEST_F(KSPSimpleILSTest, SimpleILS) {
      int fit = kspSol.fitness()[0];
      ils(kspSol);
      fit += 208;
      ASSERT_EQ(fit, kspSol.fitness()[0]);
    }

    TEST_F(KSPSimpleILSTest, SimpleILS3Iteration) {
      int fit = kspSol.fitness()[0];
      iterCriterion.setMaxIter(2);
      ils(kspSol);
      fit += 208; // No Impr
      ASSERT_EQ(fit, kspSol.fitness()[0]);
    }

}

#endif //MH_BUILDER_SIMPLEILSTEST_H
