/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KSPEVALTEST_H
#define MH_BUILDER_KSPEVALTEST_H

#include "representation/bitstring/problems/ksp/KSP.h"
#include "representation/bitstring/problems/ksp/KSPEval.h"

using namespace representation::bitstring;
namespace test {

    class KSPEvalTest : public ::testing::Test {
    protected:
        KSPEvalTest() : ksp("../../instances/ksp/low-dimensional/f1_l-d_kp_10_269"), kspSol(ksp.getN()), kspEval(ksp) {
            this->kspSol.reset();
        }

        ~KSPEvalTest() override = default;

        void SetUp() override {}

        void TearDown() override {}

        problems::ksp::KSP ksp;
        problems::ksp::KSPSol kspSol;
        problems::ksp::KSPEval kspEval;
    };

    TEST_F(KSPEvalTest, TestDefaultOperator) {
        kspSol.updateElement(1, !kspSol[1]);
        kspSol.updateElement(3, !kspSol[3]);
        ASSERT_FALSE(kspSol.valid());
        kspEval(kspSol);
        ASSERT_TRUE(kspSol.valid());
        ASSERT_EQ(15, kspSol.fitness().operator[](0));
        ASSERT_EQ(36, kspSol.fitness().getConstraint(0));
    }

    TEST_F(KSPEvalTest, TestMaximumCapacity) {
        ASSERT_FALSE(kspSol.valid());
        kspSol.updateElement(0, !kspSol[0]);
        kspEval(kspSol);
        ASSERT_TRUE(kspSol. valid()
        );
        kspSol.updateElement(1, !kspSol[1]);
        kspEval(kspSol);
        ASSERT_TRUE(kspSol.valid());
        kspSol.updateElement(2, !kspSol[2]);
        kspEval(kspSol);
        ASSERT_TRUE(kspSol.valid());
        kspSol.updateElement(3, !kspSol[3]);
        kspEval(kspSol);
        ASSERT_TRUE(kspSol.valid());
        kspSol.updateElement(4, !kspSol[4]);
        kspEval(kspSol);
        ASSERT_TRUE(kspSol.valid());
        kspSol.updateElement(5, !kspSol[5]);
        kspEval(kspSol);
        ASSERT_FALSE(kspSol.valid());
    }

//    TEST_F(KSPEvalTest, TestFlipIncr) {
//      kspSol.updateElement(4, ! kspSol[4]);
//      ASSERT_EQ(4, kspEval.flip(kspSol, 0, 4));
//      ASSERT_EQ(23, kspSol.getWeight());
//      kspSol.updateElement(4, ! kspSol[4]);
//      ASSERT_EQ(0, kspEval.flip(kspSol, 4, 4));
//      ASSERT_EQ(0, kspSol.getWeight());
//    }

}

#endif //MH_BUILDER_KSPEVALTEST_H
