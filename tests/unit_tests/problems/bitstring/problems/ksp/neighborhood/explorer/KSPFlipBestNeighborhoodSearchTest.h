/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_KSPFLIPBESTNEIGHBORHOODSEARCHTEST_H
#define MH_BUILDER_KSPFLIPBESTNEIGHBORHOODSEARCHTEST_H

#include "representation/bitstring/problems/ksp/KSP.h"
#include "representation/bitstring/problems/ksp/KSPEval.h"

using namespace representation::bitstring ;
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"
#include "opt/singleSolution/neighborhood/explorer/BestImprNeighborhoodExplorer.h"


namespace test {
    class KSPFlipBestNeighborhoodSearchTest : public ::testing::Test {
    protected:

        KSPFlipBestNeighborhoodSearchTest() : ksp("../../instances/ksp/low-dimensional/f1_l-d_kp_10_269"),
                                              kspSol(ksp.getN()), kspEval(ksp), indexNeighborhood(flipNeighbor),
                                              bestImprNeighborhoodExplorer(indexNeighborhood, kspEval) {
            this->kspSol.reset() ;
        }

        ~KSPFlipBestNeighborhoodSearchTest() override = default;

        void SetUp() override {
          kspEval(kspSol);
          indexNeighborhood.init(kspSol);
        }

        void TearDown() override {}

        problems::ksp::KSP ksp;
        problems::ksp::KSPSol kspSol;
        problems::ksp::KSPEval kspEval;
        neighborhood::neighbor::FlipNeighbor<problems::ksp::KSPSol> flipNeighbor;
        opt::singleSolution::neighborhood::IndexNeighborhood<problems::ksp::KSPSol> indexNeighborhood;
        opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<problems::ksp::KSPSol> bestImprNeighborhoodExplorer;
    };

    TEST_F(KSPFlipBestNeighborhoodSearchTest, TestAllFirstImpr) {
      int fit = kspSol.fitness()[0];
      bestImprNeighborhoodExplorer(kspSol);
      fit += 87;
      ASSERT_EQ(fit, kspSol.fitness()[0]);

      bestImprNeighborhoodExplorer(kspSol);
      fit += 85;
      ASSERT_EQ(fit, kspSol.fitness()[0]);

      bestImprNeighborhoodExplorer(kspSol);
      fit += 61;
      ASSERT_EQ(fit, kspSol.fitness()[0]);

      bestImprNeighborhoodExplorer(kspSol);
      fit += 55;
      ASSERT_EQ(fit, kspSol.fitness()[0]);

      bestImprNeighborhoodExplorer(kspSol); // No Change
      ASSERT_EQ(fit, kspSol.fitness()[0]);
    }
}

#endif //MH_BUILDER_KSPFLIPBESTNEIGHBORHOODSEARCHTEST_H
