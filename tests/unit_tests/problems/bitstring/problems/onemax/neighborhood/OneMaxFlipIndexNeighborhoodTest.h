/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ONEMAXFLIPINDEXNEIGHBORHOODTEST_H
#define MH_BUILDER_ONEMAXFLIPINDEXNEIGHBORHOODTEST_H

#include "unit_tests/tests.h"

#include "representation/bitstring/problems/onemax/OneMaxEval.h"
#include "representation/bitstring/BitstringSolution.h"
#include "core/fitness/FitnessMax.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"
#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"

using namespace representation::bitstring ;

namespace test {
    class OneMaxFlipIndexNeighborhoodTest : public ::testing::Test {
    protected:

        OneMaxFlipIndexNeighborhoodTest() : oneMaxSol(10), indexNeighborhood(flipNeighbor) {
            this->oneMaxSol.reset();
        };

        ~OneMaxFlipIndexNeighborhoodTest() override = default;

        void SetUp() override {
          eval(oneMaxSol);
          indexNeighborhood.init(oneMaxSol);
        }

        void TearDown() override {}

        typedef BitstringSolution<core::fitness::FitnessMax<unsigned long long int>> OneMaxSol ;
        OneMaxSol  oneMaxSol;
        problems::onemax::OneMaxEval<OneMaxSol> evalOneMax;

        representation::bitstring::neighborhood::neighbor::FlipNeighbor<OneMaxSol> flipNeighbor;
        opt::singleSolution::neighborhood::IndexNeighborhood<OneMaxSol> indexNeighborhood;
        problems::onemax::OneMaxEval<OneMaxSol> eval;
    };

    TEST_F(OneMaxFlipIndexNeighborhoodTest, TestOneMoveAndMoveBack) {
      unsigned int fit = oneMaxSol.fitness()[0];
      indexNeighborhood.operator()(oneMaxSol, eval);
      ASSERT_EQ(fit + 1, oneMaxSol.fitness()[0]);
      indexNeighborhood.move_back(oneMaxSol);
      ASSERT_EQ(fit, oneMaxSol.fitness()[0]);
    }
}
#endif //MH_BUILDER_ONEMAXFLIPINDEXNEIGHBORHOODTEST_H
