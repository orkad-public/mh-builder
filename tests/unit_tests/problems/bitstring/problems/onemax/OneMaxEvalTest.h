/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ONEMAXEVALTEST_H
#define MH_BUILDER_ONEMAXEVALTEST_H

#include "representation/bitstring/problems/onemax/OneMaxEval.h"
#include "representation/bitstring/BitstringSolution.h"
#include "core/fitness/FitnessMax.h"

using namespace representation::bitstring ;

namespace test {
    class OneMaxEvalTest : public ::testing::Test {
    protected:
        OneMaxEvalTest() : oneMaxSol(10) { this->oneMaxSol.reset() ;};

        ~OneMaxEvalTest() override = default;

        void SetUp() override {}

        void TearDown() override {}
        typedef BitstringSolution<core::fitness::FitnessMax<unsigned long long int>> OneMaxSol ;
        OneMaxSol  oneMaxSol;
        problems::onemax::OneMaxEval<OneMaxSol> evalOneMax;
    };

    TEST_F(OneMaxEvalTest, TestDefaultOperator) {
      oneMaxSol.updateElement(1,! oneMaxSol[1]) ;
    oneMaxSol.updateElement(2, !oneMaxSol[2]) ;
    oneMaxSol.updateElement(3, !oneMaxSol[3]) ;

      ASSERT_FALSE(oneMaxSol.valid());
      evalOneMax(oneMaxSol);
      ASSERT_TRUE(oneMaxSol.valid());
      ASSERT_EQ((unsigned int)(3), oneMaxSol.fitness().operator[](0));
    }

    TEST_F(OneMaxEvalTest, TestCount) {
      ASSERT_EQ(0, evalOneMax.count(oneMaxSol));
oneMaxSol.updateElement(2,! oneMaxSol[2]) ;
oneMaxSol.updateElement(4, !oneMaxSol[4]) ;
      ASSERT_EQ(2, evalOneMax.count(oneMaxSol));
oneMaxSol.updateElement(4,! oneMaxSol[4]) ;
      ASSERT_EQ(1, evalOneMax.count(oneMaxSol));
    }

    TEST_F(OneMaxEvalTest, TestFlip) {
oneMaxSol.updateElement(4,! oneMaxSol[4]) ;
      ASSERT_EQ(1, evalOneMax.flip(oneMaxSol, 0, 4));
oneMaxSol.updateElement(4,! oneMaxSol[4]) ;
      ASSERT_EQ(0, evalOneMax.flip(oneMaxSol, 1, 4));
    }
}

#endif //MH_BUILDER_ONEMAXEVALTEST_H
