/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BITSTRINGSOLUTIONTEST_H
#define MH_BUILDER_BITSTRINGSOLUTIONTEST_H


#include "representation/bitstring/BitstringSolution.h"

#include "gtest/gtest.h"


namespace test {

    class BitstringSolutionTest : public ::testing::Test {
    protected:
        BitstringSolutionTest() : bitstringSolution(10) { this->bitstringSolution.reset(); };

        ~BitstringSolutionTest() override = default;

        void SetUp() override {}

        void TearDown() override {}

        representation::bitstring::BitstringSolution <core::fitness::FitnessMax<double>> bitstringSolution;
    };

    TEST_F(BitstringSolutionTest, TestConstructor) {
        for (int i = 0; i < 10; ++i)
            ASSERT_EQ((unsigned int) (0), bitstringSolution[i]);
    }

    TEST_F(BitstringSolutionTest, TestFlip) {
        bitstringSolution.updateElement(3, !bitstringSolution[3]);
        bitstringSolution.updateElement(4, !bitstringSolution[4]);
        unsigned int expected[] = {0, 0, 0, 1, 1, 0, 0, 0, 0, 0};
        for (int i = 0; i < 10; ++i)
            ASSERT_EQ(expected[i], bitstringSolution[i]);
        bitstringSolution.updateElement(3, !bitstringSolution[3]);
        bitstringSolution.updateElement(4, !bitstringSolution[4]);
        for (int i = 0; i < 10; ++i)
            ASSERT_EQ((unsigned int) (0), bitstringSolution[i]);
    }
}

#endif //MH_BUILDER_BITSTRINGSOLUTIONTEST_H
