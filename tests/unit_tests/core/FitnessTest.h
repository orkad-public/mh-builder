/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef EASYMETA_FITNESS_TEST_H
#define EASYMETA_FITNESS_TEST_H

#include "core/fitness/FitnessMin.h"
#include "core/fitness/FitnessMax.h"

namespace test {
    class FitnessTest : public ::testing::Test {
    protected:
        FitnessTest() = default;

        ~FitnessTest() override {}

        void SetUp() override {}

        void TearDown() override {}

    };

    TEST_F(FitnessTest, TestWhenFitnessMinCompareMonoObj) {
      core::fitness::FitnessMin<double> f1, f2;
      f1.validate();
      f2.validate();
      f1[0] = 5;
      f2[0] = 10;
      ASSERT_FALSE(f1 == f2);
      ASSERT_TRUE(f1 > f2);
      ASSERT_FALSE(f1 < f2);
      ASSERT_TRUE(f2 < f1);
      ASSERT_FALSE(f2 > f1);
      ASSERT_TRUE(f1 == f1);
    }

    TEST_F(FitnessTest, TestWhenFitnessMaxCompareMonoObj) {
        core::fitness::FitnessMax<double> f1, f2;
      f1.validate();
      f2.validate();
      f1[0] = 5;
      f2[0] = 10;
      ASSERT_FALSE(f1 == f2);
      ASSERT_FALSE(f1 > f2);
      ASSERT_TRUE(f1 < f2);
      ASSERT_FALSE(f2 < f1);
      ASSERT_TRUE(f2 > f1);
      ASSERT_TRUE(f1 == f1);
    }

    TEST_F(FitnessTest, TestWhenFitnessMinCompareBiObj) {
        core::fitness::FitnessMin<double> f1, f2, f3, f4, f5;
      f1.validate();
      f2.validate();
      f3.validate();
      f4.validate();
      f5.validate();
      f1.objectives({5, 10});
      f2.objectives({10, 5});
      f3.objectives({15, 15});
      f4.objectives({5, 8});
      f5.objectives({5, 12});
      ASSERT_FALSE(f1 == f2);
      ASSERT_FALSE(f1 > f2); //F1 ne domine pas F2
      ASSERT_FALSE(f2 < f1); //F2 ne domine pas F1
      ASSERT_FALSE(f1 < f2); //F2 ne domine pas F1
      ASSERT_FALSE(f2 > f1); //F1 ne domine pas F2
      ASSERT_TRUE(f1 > f3); //F1 domine F3
      ASSERT_TRUE(f3 < f1); //F3 est dominé par F1
      ASSERT_FALSE(f1 < f3); //F1 n'est pas dominé par F3
      ASSERT_FALSE(f3 > f1); //F3 ne domine pas F1

      ASSERT_TRUE(f4 > f1); // F4 domine F1
      ASSERT_TRUE(f1 < f4); // F1 est dominé par F4
      ASSERT_FALSE(f4 < f1); // F4 n'est pas dominé F1
      ASSERT_FALSE(f1 > f4); // F1 ne domine pas F4

      ASSERT_FALSE(f5 > f1); // F5 ne domine pas F1
      ASSERT_FALSE(f1 < f5); // F1 n'est pas dominé par F5
      ASSERT_TRUE(f5 < f1); // F5 est dominé par F1
      ASSERT_TRUE(f1 > f5); // F1 domine F5
    }

    TEST_F(FitnessTest, TestWhenFitnessMaxCompareBiObj) {
        core::fitness::FitnessMax<double> f1, f2, f3, f4, f5;
      f1.validate();
      f2.validate();
      f3.validate();
      f4.validate();
      f5.validate();
      f1.objectives({5, 10});
      f2.objectives({10, 5});
      f3.objectives({15, 15});
      f4.objectives({5, 12});
      f5.objectives({5, 8});
      ASSERT_FALSE(f1 == f2);
      ASSERT_FALSE(f1 > f2); //F1 ne domine pas F2
      ASSERT_FALSE(f2 < f1); //F2 ne domine pas F1
      ASSERT_FALSE(f1 < f2); //F2 ne domine pas F1
      ASSERT_FALSE(f2 > f1); //F1 ne domine pas F2
      ASSERT_FALSE(f1 > f3); //F1 domine F3
      ASSERT_FALSE(f3 < f1); //F3 est dominé par F1
      ASSERT_TRUE(f1 < f3); //F1 est dominé par F3
      ASSERT_TRUE(f3 > f1); //F3 domine  F1

      ASSERT_TRUE(f4 > f1); // F4 domine F1
      ASSERT_TRUE(f1 < f4); // F1 est dominé par F4
      ASSERT_FALSE(f4 < f1); // F4 n'est pas dominé F1
      ASSERT_FALSE(f1 > f4); // F1 ne domine pas F4

      ASSERT_FALSE(f5 > f1); // F5 ne domine pas F1
      ASSERT_FALSE(f1 < f5); // F1 n'est pas dominé par F5
      ASSERT_TRUE(f5 < f1); // F5 est dominé par F1
      ASSERT_TRUE(f1 > f5); // F1 domine F5
    }

    TEST_F(FitnessTest, TestWhenFitnessMinLexicoCompareBiObj) {
        core::fitness::FitnessMin<double> f1, f2, f3;
      f1.validate();
      f2.validate();
      f3.validate();
      f1.objectives({5, 10});
      f2.objectives({10, 5});
      f3.objectives({5, 15});
      ASSERT_TRUE(f1.lexicoComp(f2));
      ASSERT_FALSE(f2.lexicoComp(f1));
      ASSERT_TRUE(f1.lexicoComp(f3));
      ASSERT_FALSE(f3.lexicoComp(f1));
    }
}

#endif //EASYMETA_FITNESS_TEST_H
