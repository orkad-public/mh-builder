/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ALLACCEPTEDBOUNDEDARCHIVETEST_H
#define MH_BUILDER_ALLACCEPTEDBOUNDEDARCHIVETEST_H
#include "core/archive/BoundedArchive.h"

namespace test {
    class AllAcceptedBoundedArchiveTest : public ArchiveTest {
    protected:
        AllAcceptedBoundedArchiveTest() = default;
        ~AllAcceptedBoundedArchiveTest() override = default;

        core::Archive<SOLUTION>* createArchive() override {

          auto *allAcceptedArchive = new core::archive::AllAcceptedArchive<SOLUTION>();
          return new core::archive::BoundedArchive<SOLUTION, core::archive::AllAcceptedArchive<SOLUTION>>(allAcceptedArchive, 3);
        }

        typedef core::fitness::FitnessMax<> FIT;
        typedef core::Solution<FIT> SOLUTION;

    };

    TEST_F(AllAcceptedBoundedArchiveTest, TestAllAcceptedBoundedArchive) {
      FIT fit;
      fit.validate();
      SOLUTION sol;

      fit.objectives({5, 10});
      sol.fitness(fit);
      archive->operator()(sol);
      ASSERT_EQ(static_cast<unsigned long long int>(1), archive->size());

      fit.objectives({10, 5});
      sol.fitness(fit);
      archive->operator()(sol);
      ASSERT_EQ(static_cast<unsigned long long int>(2), archive->size());

      fit.objectives({7, 6}); // Non dominated
      sol.fitness(fit);
      archive->operator()(sol);
      ASSERT_EQ(static_cast<unsigned long long int>(3), archive->size());

      fit.objectives({3, 7}); // Dominated by (5, 10)
      sol.fitness(fit);
      archive->operator()(sol);
      ASSERT_EQ(static_cast<unsigned long long int>(3), archive->size());

      fit.objectives({7, 12}); // Dominate (5, 10) and (7, 6)
      sol.fitness(fit);
      archive->operator()(sol);
      ASSERT_EQ(static_cast<unsigned long long int>(3), archive->size());
    }
}

#endif //MH_BUILDER_ALLACCEPTEDBOUNDEDARCHIVETEST_H
