/**
 * Created by Cyprien Borée (cyprien.boree@tuta.io) on July 9th 2021.
 * **/
#ifndef HYPER_OCTREE_TEST_H
#define HYPER_OCTREE_TEST_H
#include <vector>
#include <optional>
#include "core/archive/HyperOctree.h"

namespace test{
    
    class HyperOctreeTest : public ::testing::Test {
        protected:
        HyperOctreeTest() = default;

        ~HyperOctreeTest() override {}

        void SetUp() override {}

        void TearDown() override {}
    };

    TEST_F(HyperOctreeTest,TestHasChildren){
        core::archive::HyperOctree<unsigned> tree_1d(0,1);
        ASSERT_FALSE(tree_1d.hasChildren());
        tree_1d.emplace(0,10);
        tree_1d.emplace(1,4);
        ASSERT_TRUE(tree_1d.hasChildren());
    }

    TEST_F(HyperOctreeTest,TestEmplace){
        core::archive::HyperOctree<unsigned> tree_1d(0,1);
        tree_1d.emplace(0,10);
        tree_1d.emplace(1,4);
        unsigned child_found = 0;
        auto tree_1d_children = tree_1d.getChildren();
        for(unsigned i = 0; i < tree_1d.size(); ++i){
            if(tree_1d_children[i])
                child_found++;
        }
        ASSERT_EQ(child_found, (unsigned)2);
        ASSERT_TRUE(tree_1d_children[0].has_value());
        ASSERT_TRUE(tree_1d_children[1].has_value());
    }

    TEST_F(HyperOctreeTest,TestThrowRuntimeExceptionWhenEmplacing){
        core::archive::HyperOctree<unsigned> tree_1d(0,1);
        try{
            tree_1d.emplace(2,0);
            FAIL(); // function should throw an exception and cannot reach this point.
        }catch(std::runtime_error const& err){
            EXPECT_EQ(err.what(),std::string("Node can't exceed 2 children."));
        }

        core::archive::HyperOctree<unsigned> tree_2d(0,2);
        try{
            tree_2d.emplace(4,0);
            FAIL(); // function should throw an exception and cannot reach this point.
        }catch(std::runtime_error const& err){
            EXPECT_EQ(err.what(),std::string("Node can't exceed 4 children."));
        }

        core::archive::HyperOctree<unsigned> tree_3d(0,3);
        try{
            tree_3d.emplace(8,0);
            FAIL(); // function should throw an exception and cannot reach this point.
        }catch(std::runtime_error const& err){
            EXPECT_EQ(err.what(),std::string("Node can't exceed 8 children."));
        }
    }

    TEST_F(HyperOctreeTest, TestRemoveChildByIndex){
        core::archive::HyperOctree<unsigned> tree_2d(0,2);
        tree_2d.emplace(0,0);
        tree_2d.emplace(1,0);
        tree_2d.emplace(2,4);
        auto tree_2d_children = tree_2d.getChildren();
        ASSERT_TRUE(tree_2d_children[1].has_value());
        tree_2d.erase_at(1);
        tree_2d_children = tree_2d.getChildren();
        ASSERT_FALSE(tree_2d_children[1].has_value());
    }

}
#endif