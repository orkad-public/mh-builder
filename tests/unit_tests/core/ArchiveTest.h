/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ARCHIVE_TEST_H
#define MH_BUILDER_ARCHIVE_TEST_H

#include "core/fitness/FitnessMax.h"
#include "core/Archive.h"
#include "core/Solution.h"

namespace test {
    class ArchiveTest : public ::testing::Test {
    protected:

        typedef core::fitness::FitnessMax<> FIT;
        typedef core::Solution<FIT> SOLUTION;

        virtual core::Archive<SOLUTION>* createArchive() = 0;

        void SetUp() override {
          archive = createArchive();
        }

        core::Archive<SOLUTION> *archive;
    };
}

#endif //MH_BUILDER_ARCHIVE_TEST_H
