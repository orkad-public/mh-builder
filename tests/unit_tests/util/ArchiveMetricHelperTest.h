/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ARCHIVEMETRICHELPERTEST_H
#define MH_BUILDER_ARCHIVEMETRICHELPERTEST_H

namespace test {
    class ArchiveMetricHelperTest : public ::testing::Test {
    protected:
        ArchiveMetricHelperTest() = default;

        ~ArchiveMetricHelperTest() override = default;

        void SetUp() override {
          FIT fit;
          fit.validate();
          SOLUTION sol;

          fit.objectives({10, 1});
          sol.fitness(fit);
          nonDominatedArchive(sol);
          fit.objectives({9, 2});
          sol.fitness(fit);
          sol.flag(1);
          nonDominatedArchive(sol);
          fit.objectives({8, 3});
          sol.fitness(fit);
          sol.flag(1);
          nonDominatedArchive(sol);
          fit.objectives({7, 4});
          sol.fitness(fit);
          nonDominatedArchive(sol);
          fit.objectives({6, 5});
          sol.fitness(fit);
          nonDominatedArchive(sol);
        }

        void TearDown() override {}

        typedef core::fitness::FitnessMax<> FIT;
        typedef core::Solution <FIT> SOLUTION;
        typedef core::archive::NonDominatedArchive <SOLUTION> ARCHIVE;
        ARCHIVE nonDominatedArchive;
    };

    TEST_F(ArchiveMetricHelperTest, TestComputeBounds) {
      util::ArchiveMetricHelper<ARCHIVE> archiveMetricHelper;
      archiveMetricHelper.computeBounds(nonDominatedArchive);
      ASSERT_EQ(static_cast<unsigned long long int>(6), archiveMetricHelper.getBounds()[0].first);
      ASSERT_EQ(static_cast<unsigned long long int>(10), archiveMetricHelper.getBounds()[0].second);
      ASSERT_EQ(static_cast<unsigned long long int>(1), archiveMetricHelper.getBounds()[1].first);
      ASSERT_EQ(static_cast<unsigned long long int>(5), archiveMetricHelper.getBounds()[1].second);
    }

    TEST_F(ArchiveMetricHelperTest, TestNormalizeArchive) {
      util::ArchiveMetricHelper<ARCHIVE> archiveMetricHelper;
      archiveMetricHelper.computeNormalizedFront(nonDominatedArchive);
      ASSERT_EQ(static_cast<unsigned long long int>(6), archiveMetricHelper.getBounds()[0].first);
      ASSERT_EQ(static_cast<unsigned long long int>(10), archiveMetricHelper.getBounds()[0].second);
      ASSERT_EQ(static_cast<unsigned long long int>(1), archiveMetricHelper.getBounds()[1].first);
      ASSERT_EQ(static_cast<unsigned long long int>(5), archiveMetricHelper.getBounds()[1].second);
    }

}

#endif //MH_BUILDER_ARCHIVEMETRICHELPERTEST_H
