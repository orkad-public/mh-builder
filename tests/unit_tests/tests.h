/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef EASYMETA_TESTS_H
#define EASYMETA_TESTS_H



#include "gtest/gtest.h"
#include "gmock/gmock.h"

//MOCKS

#include "mocks/core/MockFitness.h"
#include "mocks/core/MockSolution.h"
#include "mocks/core/MockArchive.h"
#include "mocks/core/MockEval.h"
#include "mocks/opt/singleSolution/neighborhood/OrderNeighborhoodMock.h"
#include "mocks/opt/singleSolution/neighborhood/MockNeighborhood.h"
#include "mocks/opt/singleSolution/neighborhood/MockMonoObjectiveNeighborhood.h"

//TESTS
#include "unit_tests/core/FitnessTest.h"
#include "unit_tests/core/ArchiveTest.h"
#include "unit_tests/core/archive/AllAcceptedArchiveTest.h"
#include "unit_tests/core/archive/AllAcceptedBoundedArchiveTest.h"
#include "unit_tests/core/archive/NonDominatedArchiveTest.h"
// #include "unit_tests/core/archive/HyperOctreeTest.h"

#include "unit_tests/opt/criterion/IterCriterionTest.h"




#include "unit_tests/opt/manySolutions/selection/SelectTest.h"
#include "unit_tests/opt/manySolutions/selection/SelectAllTest.h"
#include "unit_tests/opt/manySolutions/selection/SelectNewestTest.h"
#include "unit_tests/opt/manySolutions/selection/SelectOldestTest.h"

#include "unit_tests/opt/manySolutions/explorer/ExplorerTest.h"
#include "unit_tests/opt/manySolutions/explorer/AllExplorerTest.h"
#include "unit_tests/opt/manySolutions/explorer/ImpExplorerTest.h"
#include "unit_tests/opt/manySolutions/explorer/ImpNDomExplorerTest.h"
#include "unit_tests/opt/manySolutions/explorer/FirstImpExplorerTest.h"
#include "unit_tests/opt/manySolutions/explorer/NDomExplorerTest.h"

#include "unit_tests/opt/singleSolution/neighborhood/explorer/NeighborhoodExplorerTest.h"
#include "unit_tests/opt/singleSolution/neighborhood/explorer/FirstImprNeighborhoodExplorerTest.h"
#include "unit_tests/opt/singleSolution/neighborhood/explorer/WorstImprNeighborhoodExplorerTest.h"
#include "unit_tests/opt/singleSolution/neighborhood/explorer/BestImprNeighborhoodExplorerTest.h"



#include "unit_tests/opt/manySolutions/metrics/MaximisationObjectiveHypervolumeMetricTest.h"
#include "unit_tests/opt/manySolutions/metrics/MinimisationObjectiveHypervolumeMetricTest.h"
#include "unit_tests/opt/manySolutions/metrics/BiObjectiveSpreadMetricTest.h"

#include "unit_tests/opt/memory/TabuListTest.h"

#include "unit_tests/problems/bitstring/BitstringSolutionTest.h"
#include "unit_tests/problems/bitstring/neighborhood/neighbor/FlipNeighborTest.h"
#include "unit_tests/problems/bitstring/neighborhood/FlipIndexNeighborhoodTest.h"
#include "unit_tests/problems/bitstring/neighborhood/FlipRndWithoutRemplNeighborhoodTest.h"
#include "unit_tests/problems/bitstring/perturbation/FlipOrderNeighborhoodPerturbationTest.h"

#include "unit_tests/problems/bitstring/problems/onemax/OneMaxEvalTest.h"
#include "unit_tests/problems/bitstring/problems/onemax/neighborhood/OneMaxFlipIndexNeighborhoodTest.h"
#include "unit_tests/problems/bitstring/problems/onemax/neighborhood/explorer/OneMaxFlipFirstneighborhoodSearchTest.h"

#include "unit_tests/problems/bitstring/problems/ksp/KSPEvalTest.h"
#include "unit_tests/problems/bitstring/problems/ksp/neighborhood/KSPFlipIndexNeighborhoodTest.h"
#include "unit_tests/problems/bitstring/problems/ksp/neighborhood/explorer/KSPFlipFirstNeighborhoodSearchTest.h"
#include "unit_tests/problems/bitstring/problems/ksp/neighborhood/explorer/KSPFlipBestNeighborhoodSearchTest.h"
#include "unit_tests/problems/bitstring/problems/ksp/neighborhood/explorer/KSPFlipWorstNeighborhoodSearchTest.h"
#include "unit_tests/problems/bitstring/problems/ksp/localsearch/KSPHillClimbingTest.h"
#include "unit_tests/problems/bitstring/problems/ksp/localsearch/KSPSimpleILSTest.h"

#include "unit_tests/problems/permutation/PermutationSolutionTest.h"

#include "unit_tests/problems/permutation/neighborhood/neighbor/AdjSwapNeighborTest.h"
#include "unit_tests/problems/permutation/neighborhood/neighbor/ShiftNeighborTest.h"
#include "unit_tests/problems/permutation/neighborhood/neighbor/ShiftWithoutSwapNeighborTest.h"
#include "unit_tests/problems/permutation/neighborhood/neighbor/SwapNeighborTest.h"
#include "unit_tests/problems/permutation/neighborhood/neighbor/TwoOptNeighborTest.h"
#include "unit_tests/problems/permutation/neighborhood/neighbor/DoubleBridgeNeighborTest.h"
#include "unit_tests/problems/permutation/neighborhood/ShiftIndexNeighborhoodTest.h"

#include "unit_tests/problems/permutation/fsp/FSPEvalTest.h"
#include "unit_tests/problems/permutation/fsp/algo/NEHTest.h"

#include "unit_tests/problems/rulemining/RulesetSolutionTest.h"
#include "unit_tests/problems/rulemining/RuleminingEvalTest.h"
#include "unit_tests/problems/rulemining/neighborhood/neighbor/AddTermNeighborTest.h"
#include "unit_tests/problems/rulemining/neighborhood/neighbor/DropTermNeighborTest.h"
#include "unit_tests/problems/rulemining/neighborhood/neighbor/ModifTermNeighborTest.h"
#include "unit_tests/problems/rulemining/neighborhood/RuleNeighborhoodTest.h"
#include "unit_tests/problems/rulemining/neighborhood/neighbor/AddRuleNeighborTest.h"
#include "unit_tests/problems/rulemining/neighborhood/neighbor/DropRuleNeighborTest.h"
#include "unit_tests/problems/rulemining/neighborhood/neighbor/ModifRuleNeighborTest.h"
#include "unit_tests/problems/rulemining/neighborhood/RulesetNeighborhoodTest.h"
#include "unit_tests/problems/rulemining/algorithms/RuleminingPLSTest.h"

#include "unit_tests/util/ArchiveMetricHelperTest.h"

#include "unit_tests/opt/manySolutions/geneticAlgorithm/GATest.h"
#include "unit_tests/opt/manySolutions/geneticAlgorithm/SelectionTest.h"
#include "unit_tests/opt/manySolutions/geneticAlgorithm/RandomSelectionTest.h"
#include "unit_tests/opt/manySolutions/geneticAlgorithm/CrossoverTest.h"
#include "unit_tests/opt/manySolutions/geneticAlgorithm/OnePointCrossoverTest.h"
#include "unit_tests/opt/manySolutions/geneticAlgorithm/OXCrossoverTest.h"
#include "unit_tests/opt/manySolutions/geneticAlgorithm/NSGA2Test.h"

#endif //EASYMETA_TESTS_H
