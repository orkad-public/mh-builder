/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_TABULISTTEST_H
#define MH_BUILDER_TABULISTTEST_H
#include "opt/memory/TabuList.h"


namespace test {
    class TabuListTest : public ::testing::Test {
    protected:
        TabuListTest() : tabuList(3, 5) {};

        ~TabuListTest() override = default;

        void SetUp() override {
        }

        void TearDown() override {}

        opt::memory::TabuList<unsigned long long int> tabuList;
    };

    TEST_F(TabuListTest, SimpleTabuListTest) {
      unsigned long long int a = 1, b = 2;
      tabuList(a);
      ASSERT_TRUE(tabuList.isTabu(a));
      ASSERT_FALSE(tabuList.isTabu(b));
    }

    TEST_F(TabuListTest, TabuRemplaceWhenFull) {
      unsigned long long int a = 1, b = 2, c = 3, d = 4;
      tabuList(a);
      tabuList(b);
      tabuList(c);
      tabuList(d);
      ASSERT_FALSE(tabuList.isTabu(a));
      ASSERT_TRUE(tabuList.isTabu(b));
      ASSERT_TRUE(tabuList.isTabu(c));
      ASSERT_TRUE(tabuList.isTabu(d));
    }

    TEST_F(TabuListTest, NoTabuAfterSomeIter) {
      unsigned long long int a = 1;
      tabuList(a);
      for (int i = 0; i < 5; ++i) {
        ASSERT_TRUE(tabuList.isTabu(a));
        tabuList.update();
      }
      ASSERT_FALSE(tabuList.isTabu(a));
    }

}


#endif //MH_BUILDER_TABULISTTEST_H
