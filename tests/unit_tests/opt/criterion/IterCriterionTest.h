/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ITERCRITERIONTEST_H
#define MH_BUILDER_ITERCRITERIONTEST_H

#include "opt/criterion/IterCriterion.h"
#include "core/criterion/CriterionAnd.h"
#include "core/criterion/CriterionOr.h"

namespace test {
    class IterCriterionTest : public ::testing::Test {
    protected:
        IterCriterionTest() : iterCriterion1(10), iterCriterion2(20) {};

        ~IterCriterionTest() override = default;

        void SetUp() override {
          iterCriterion1.init();
          iterCriterion2.init();
        }

        void TearDown() override {}

        opt::criterion::IterCriterion<int> iterCriterion1, iterCriterion2;
    };

    TEST_F(IterCriterionTest, NormalUseCase) {
      ASSERT_EQ(static_cast<unsigned long long int>(0), iterCriterion1.getCurrentIter());
      for(unsigned long long int i = 0; i < 10; i++) {
        ASSERT_TRUE(iterCriterion1());
        iterCriterion1.update();
        ASSERT_EQ(i + 1, iterCriterion1.getCurrentIter());
      }
      ASSERT_FALSE(iterCriterion1());
    }

    TEST_F(IterCriterionTest, UseCaseWithInitDuringTheExecution) {
      for(unsigned long long int i = 0; i < 10; i++)
        iterCriterion1.update();
      iterCriterion1.init();
      ASSERT_TRUE(iterCriterion1());
      for(unsigned long long int i = 0; i < 10; i++) {
        ASSERT_TRUE(iterCriterion1());
        iterCriterion1.update();
        ASSERT_EQ(i + 1, iterCriterion1.getCurrentIter());
      }
      ASSERT_FALSE(iterCriterion1());
    }

    TEST_F(IterCriterionTest, CombinationAndIter) {
      ASSERT_EQ(static_cast<unsigned long long int>(0), iterCriterion1.getCurrentIter());
      ASSERT_EQ(static_cast<unsigned long long int>(0), iterCriterion2.getCurrentIter());

      core::criterion::CriterionAnd<int> criterionAnd(iterCriterion1, iterCriterion2);

      for(unsigned long long int i = 0; i < 10; i++) {
        ASSERT_TRUE(criterionAnd());
        criterionAnd.update();
        ASSERT_EQ(i + 1, iterCriterion1.getCurrentIter());
        ASSERT_EQ(i + 1, iterCriterion2.getCurrentIter());
      }
      ASSERT_FALSE(criterionAnd());
    }

    TEST_F(IterCriterionTest, CombinationOrIter) {
      ASSERT_EQ(static_cast<unsigned long long int>(0), iterCriterion1.getCurrentIter());
      ASSERT_EQ(static_cast<unsigned long long int>(0), iterCriterion2.getCurrentIter());

      core::criterion::CriterionOr<int> criterionOr(iterCriterion1, iterCriterion2);

      for(unsigned long long int i = 0; i < 20; i++) {
        ASSERT_TRUE(criterionOr());
        criterionOr.update();
        ASSERT_EQ(i + 1, iterCriterion1.getCurrentIter());
        ASSERT_EQ(i + 1, iterCriterion2.getCurrentIter());
      }
      ASSERT_FALSE(criterionOr());
    }
}

#endif //MH_BUILDER_ITERCRITERIONTEST_H
