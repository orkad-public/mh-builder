/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BEST_IMPR_NEIGHBORHOOD_EXPLORER_TEST_H
#define MH_BUILDER_BEST_IMPR_NEIGHBORHOOD_EXPLORER_TEST_H

#include "opt/singleSolution/neighborhood/explorer/BestImprNeighborhoodExplorer.h"
#include "NeighborhoodExplorerTest.h"

namespace test {
    class BestImprNeighborhoodExplorerTest : public NeighborhoodExplorerTest {
    protected:
        BestImprNeighborhoodExplorerTest() = default;

        ~BestImprNeighborhoodExplorerTest() override = default;

        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOLUTION> *createExplorer() override {
            return new opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<SOLUTION>(
                    this->neighborhood, this->eval);
        }
    };

    TEST_F(BestImprNeighborhoodExplorerTest, TestBestImprovedSolution) {

        explorer->operator()(initSolution);
        ASSERT_EQ(initSolution.fitness()[0],12) ;
        ASSERT_EQ(explorer->getNeighborhood()->hasNextNeighbor(),false) ;
    }

}


#endif //MH_BUILDER_BEST_IMPR_NEIGHBORHOOD_EXPLORER_TEST_H
