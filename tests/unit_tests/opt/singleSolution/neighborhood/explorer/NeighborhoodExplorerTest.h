/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NEIGHBORHOOD_EXPLORER_TEST_H
#define MH_BUILDER_NEIGHBORHOOD_EXPLORER_TEST_H

#include "opt/singleSolution/neighborhood/explorer/NeighborhoodExplorer.h"


namespace test {
    class NeighborhoodExplorerTest : public ::testing::Test {
    protected:
        typedef MockFitness FIT;
        typedef MockSolution SOLUTION;


        NeighborhoodExplorerTest() = default;

        virtual ~NeighborhoodExplorerTest() override = default;

        void SetUp() override {
          explorer = createExplorer();
          fit.validate();
          fit.objectives({5});
          initSolution.fitness(fit);
        }

        virtual opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOLUTION>* createExplorer() = 0;

        void TearDown() override {}

        MockMonoObjectiveNeighborhood neighborhood;
        MockEval eval;
        FIT fit;
        MockSolution initSolution;
        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOLUTION> *explorer{};
    };
}

#endif //MH_BUILDER_NEIGHBORHOOD_EXPLORER_TEST_H
