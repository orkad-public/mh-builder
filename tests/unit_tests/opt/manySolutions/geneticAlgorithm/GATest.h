/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_TEST_H
#define MH_BUILDER_GA_TEST_H

#include "util/RNGHelper.h"
#include "core/fitness/FitnessMin.h"

#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "core/archive/AllAcceptedArchive.h"

using namespace representation::permutation::problems::tsp ;
namespace test {
    class GATest : public ::testing::Test {
    protected:
        typedef core::fitness::FitnessMin<double> FIT;
        typedef TSPSol<FIT> SOL;
        typedef TSPEval<SOL> EVAL;
        typedef core::archive::AllAcceptedArchive<SOL> POPULATION;

        /**
         * default constructor
         */
        GATest() = default;
        /**
         * default destructor
         */

        virtual ~GATest() override = default;

        virtual void SetUp() override {
            util::RNGHelper::get()->reseed(120);
            std::string ficName = "../../instances/tsp/tsp_test/att48.txt";
            TSP tsp_instance(ficName);
            this->eval = new EVAL(tsp_instance);

            randomInit(this->population,50, tsp_instance.getNumberOfCities(), *this->eval);
            randomInit(this->populationOddSize,3, tsp_instance.getNumberOfCities(), *this->eval);
        }


        void TearDown() override {}

        static void randomInit(POPULATION & pop, unsigned long long int nbIndividuals, unsigned long long int sizeIndividuals, EVAL &eval) {
            for(unsigned long long int i=0; i<nbIndividuals;++i){
                SOL solution(sizeIndividuals);
                solution.shuffle() ; eval(solution);
                pop(solution);
            }
        }

        EVAL *eval ;
        POPULATION population;
        POPULATION populationOddSize ;
    };
}

#endif //MH_BUILDER_GA_TEST_H
