/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_OX_CROSSOVER_TEST_H
#define MH_BUILDER_GA_OX_CROSSOVER_TEST_H
#include <vector>


#include "opt/manySolutions/geneticAlgorithm/crossover/OXCrossover.h"

namespace test {
    class OXCrossoverTest : public CrossoverTest {
    protected:
        OXCrossoverTest() = default;
        ~OXCrossoverTest() override = default;

        virtual opt::manySolutions::geneticAlgorithm::crossover::Crossover<POPULATION>* createCrossover() override {
          return new opt::manySolutions::geneticAlgorithm::crossover::OXCrossover<POPULATION>(0.5, *this->eval);
        }
    };



    TEST_F(OXCrossoverTest, TestOXCrossover) {
        tsp::TSPSol<fitness::FitnessMin<>> sol1(10) ; // { 0, 1, ..., 9}
        tsp::TSPSol<fitness::FitnessMin<>> sol2(10) ;
        vector<unsigned long long int> v = {9, 8, 7, 6, 5, 4, 3, 2, 1 , 0};
        sol2.replace(v) ;
        auto pair = this->crossover->operator()(sol1, sol2) ;
        // random generated position : 0 len :8
        vector<unsigned long long int> expectedChild1 = {9, 1, 2, 3, 4, 5, 6, 7, 8, 0};
        vector<unsigned long long int> expectedChild2 = {0, 8, 7, 6, 5, 4, 3, 2, 1, 9} ;
        for (unsigned long long int i = 0 ; i < 10; i++) {
            ASSERT_EQ(pair.first[i], expectedChild1[i]) ;
            ASSERT_EQ(pair.second[i], expectedChild2[i]) ;
        }
    }

}

#endif //MH_BUILDER_GA_OX_CROSSOVER_TEST_H
