/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_RANDOM_SELECTION_TEST_H
#define MH_BUILDER_GA_RANDOM_SELECTION_TEST_H


#include "SelectionTest.h"
#include "opt/manySolutions/geneticAlgorithm/selection/RandomSelection.h"

namespace test {
    class RandomSelectionTest : public SelectionTest {
    protected:
        RandomSelectionTest() = default;
        ~RandomSelectionTest() override = default;

        virtual opt::manySolutions::geneticAlgorithm::selection::Selection<POPULATION>* createSelection() override {
          return new opt::manySolutions::geneticAlgorithm::selection::RandomSelection<POPULATION>(12);
        }
    };

    TEST_F(RandomSelectionTest, TestSelectionContent) {
      this->selection->operator()(this->population) ;
      ASSERT_EQ(this->selection->getSelectSize(), 12);
      // checks if selected individuals are coming from initial population
      for (auto sol : this->selection->getSelectedPopulation()) {
          auto res = std::find(this->population.begin(), this->population.begin(), sol);
          ASSERT_TRUE(res != this->population.end());
      }
    }
    TEST_F(RandomSelectionTest, TestSelectionResize) {
        this->selection->operator()(this->population) ;
        this->selection->setSelectSize(8);
        ASSERT_EQ(this->selection->getSelectSize(),8);
        ASSERT_EQ(this->selection->getSelectedPopulation().size(),8) ;
    }


}

#endif //MH_BUILDER_GA_RANDOM_SELECTION_TEST_H