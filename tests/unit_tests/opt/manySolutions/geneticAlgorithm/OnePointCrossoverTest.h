/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_ONE_POINT_CROSSOVER_TEST_H
#define MH_BUILDER_GA_ONE_POINT_CROSSOVER_TEST_H
#include <vector>


#include "opt/manySolutions/geneticAlgorithm/crossover/OnePointCrossover.h"

namespace test {
    class OnePointCrossoverTest : public CrossoverTest {
    protected:
        OnePointCrossoverTest() = default;
        ~OnePointCrossoverTest() override = default;

        virtual opt::manySolutions::geneticAlgorithm::crossover::Crossover<POPULATION>* createCrossover() override {
          return new opt::manySolutions::geneticAlgorithm::crossover::OnePointCrossover<POPULATION>(0.5, *this->eval);
        }
    };

    TEST_F(OnePointCrossoverTest, TestCrossoverSizePairResult) {
      this->crossover->operator()(this->population) ;
      ASSERT_EQ(this->population.size(), this->crossover->getOffspring().size());
    }
    TEST_F(OnePointCrossoverTest, TestCrossoverOddSizeResult) {
        this->crossover->operator()(this->populationOddSize) ;
        ASSERT_EQ(this->populationOddSize.size(), this->crossover->getOffspring().size());
        ASSERT_EQ(this->populationOddSize[populationOddSize.size()-1],this->crossover->getOffspring()[this->crossover->getOffspring().size()-1]) ;
    }

    TEST_F(OnePointCrossoverTest, TestCrossoverOnePoint) {
        tsp::TSPSol<fitness::FitnessMin<>> sol1(10) ; // { 0, 1, ..., 9}
        tsp::TSPSol<fitness::FitnessMin<>> sol2(10) ;
        vector<unsigned long long int> v = {9, 8, 7, 6, 5, 4, 3, 2, 1 , 0};
        sol2.replace(v) ;
        auto pair = this->crossover->operator()(sol1, sol2) ;
        // cross point generated should be 7
        vector<unsigned long long int> expectedChild1 = {0, 1, 2, 3, 4, 5, 6, 2, 1, 0};
        vector<unsigned long long int> expectedChild2 = {9, 8, 7, 6, 5, 4, 3, 7, 8, 9} ;
        for (unsigned long long int i = 0 ; i < 10; i++) {
            ASSERT_EQ(pair.first[i], expectedChild1[i]) ;
            ASSERT_EQ(pair.second[i], expectedChild2[i]) ;
        }
    }

}

#endif //MH_BUILDER_GA_ONE_POINT_CROSSOVER_TEST_H
