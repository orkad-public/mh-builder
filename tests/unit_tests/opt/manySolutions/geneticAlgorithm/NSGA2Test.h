/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_NSGA2_TEST_H
#define MH_BUILDER_NSGA2_TEST_H


#include "util/RNGHelper.h"
#include "core/fitness/FitnessMin.h"
#include "opt/criterion/TimeCriterion.h"

#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/problems/tsp/TSPEvalBiObjective.h"
#include "core/archive/AllAcceptedArchive.h"

#include "opt/manySolutions/geneticAlgorithm/crossover/OXCrossover.h"
#include "opt/manySolutions/geneticAlgorithm/mutation/Mutation.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Solution.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Algorithm.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Eval.h"

#include "representation/permutation/PermutationSolution.h"

using namespace representation::permutation::problems::tsp ;
namespace test {
    class NSGA2Test : public ::testing::Test {
    protected:
        typedef core::fitness::FitnessMin<double> FIT;
        typedef TSPSol<FIT> SOL;
        typedef TSPEvalBiObjective<SOL> EVAL;

        typedef opt::manySolutions::geneticAlgorithm::NSGA2_Solution<SOL> NSGA2_SOL;
        typedef core::archive::AllAcceptedArchive<NSGA2_SOL> POP;
        typedef opt::manySolutions::geneticAlgorithm::crossover::OXCrossover<POP> CROSSOVER;
        typedef opt::manySolutions::geneticAlgorithm::mutation::Mutation<POP> MUTATION;
        typedef opt::criterion::TimeCriterion<POP> STOP;
        typedef opt::manySolutions::geneticAlgorithm::NSGA2_Algorithm<POP> GA;

        /**
         * default constructor
         */
        NSGA2Test() = default;

        /**
         * default destructor
         */

        virtual ~NSGA2Test() override = default;

        virtual void SetUp() override {
            util::RNGHelper::get()->reseed(120);

            std::vector<std::vector<double>> distanceMatrix = {
                    {0,  10, 15, 20},
                    {10, 0,  35, 25},
                    {15, 35, 0,  30},
                    {20, 25, 30, 0}
            };
            std::vector<std::vector<double>> timeMatrix = {{
                                                                   {0, 1, 3, 4},
                                                                   {1, 0, 5, 2},
                                                                   {3, 5, 0, 6},
                                                                   {4, 2, 6, 0}
                                                           }};
            TSP tsp1(distanceMatrix);
            TSP tsp2(timeMatrix);
            TSPEval<SOL> tsp1eval(tsp1);
            TSPEval<SOL> tsp2eval(tsp2);

            EVAL eval(tsp1eval, tsp2eval);
            NSGA2_Eval<SOL> nsga2_eval(eval);


            pop1.clear();
            unsigned long long int values[4][4] = {
                    {0, 1, 2, 3},
                    {0, 2, 1, 3},
                    {0, 3, 1, 2},
                    {0, 2, 3, 1}
            };
            for (int i = 0; i < 4; i++) {
                SOL sol;
                for (int j = 0; j < 4; j++) {
                    sol.push_back(values[i][j]);
                }
                NSGA2_SOL decorator(sol);
                nsga2_eval(decorator);
                pop1(decorator);
            }

            std::string ficName1 = "../../instances/tsp/tsp_test/cgC50_20_01.txt";
            std::string ficName2 = "../../instances/tsp/tsp_test/cgC50_20_02.txt";
            TSP tsp_instance1(ficName1);
            TSP tsp_instance2(ficName2);

            TSPEval<SOL> tsp1eval_2(tsp_instance1);
            TSPEval<SOL> tsp2eval_2(tsp_instance2);
            EVAL eval_2(tsp1eval_2, tsp2eval_2);

            NSGA2_Eval<SOL> nsga2_eval2(eval_2);

            pop2.clear();

            // init population of 100 individuals
            for (unsigned long long int i = 0; i < 100; ++i) {
                SOL solution(tsp_instance1.getNumberOfCities());
                solution.shuffle();
                NSGA2_SOL decorator(solution);
                nsga2_eval2(decorator);
                pop2(decorator);
            }
        }


        void TearDown() override {}

        POP pop1;
        POP pop2;
    };

    TEST_F(NSGA2Test, FastNonDominatedSortTest1) {

        unsigned long long int expected_size_fronts[] = {1, 2, 1};
        unsigned long long int expected_sol_front0[] = {0, 2, 3, 1};
        unsigned long long int expected_sol_front2[] = {0, 1, 2, 3};
        unsigned long long int f_num = 0;
        auto fronts = NSGA2_SOL::fastNonDominatedSort(this->pop1);
        for (auto &f: fronts) {
            ASSERT_EQ(f.size(), expected_size_fronts[f_num]);
            if (f_num == 0) {
                for (auto i = 0; i < 4; i++)
                    ASSERT_EQ(expected_sol_front0[i],
                              this->pop1[f[0]][i]);
            }
            if (f_num ==2 ) {
                for (auto i = 0;i < 4; i++) ASSERT_EQ(expected_sol_front2[i],this->pop1[f[0]][i]);
            }
            f_num++;
        }
    }

    TEST_F(NSGA2Test, FastNonDominatedSortTest2) {
        unsigned long long int f_num = 0;
        auto popSize = this->pop2.size();
        unsigned long long int size = 0;
        auto fronts = NSGA2_SOL::fastNonDominatedSort(this->pop2);
        for (auto &f: fronts) {
            size+=f.size();
            for (auto index_solution: f ) {
                ASSERT_LT(index_solution, popSize); // f is an index of an element of pop
                ASSERT_EQ(this->pop2[index_solution].getRank(), f_num);
            }
            f_num++;
        }
        ASSERT_EQ(size, popSize);
    }

    TEST_F(NSGA2Test, CrowdingDistance1) {
        auto fronts = NSGA2_SOL::fastNonDominatedSort(this->pop1);
        for (auto &f: fronts) {
            NSGA2_SOL::crowdingDistanceAssignment(this->pop1,f);
            for (auto ind: f)
                ASSERT_EQ(this->pop1[ind].getCrowdingDistance(), std::numeric_limits<double>::infinity());
        }
    }

    TEST_F(NSGA2Test, CrowdingDistance2) {
        auto fronts = NSGA2_SOL::fastNonDominatedSort(this->pop2);
        for (auto &f: fronts) {
            ASSERT_GE(f.size(), 1);
            NSGA2_SOL::crowdingDistanceAssignment(pop2, f);
            auto counterInfinity = 0;
            for (auto ind: f)
                if (pop2[ind].getCrowdingDistance()==std::numeric_limits<double>::infinity())
                counterInfinity++;
            ASSERT_EQ(counterInfinity, std::min<unsigned long long int>(2, f.size()));
        }
    }

    TEST_F(NSGA2Test, COMPLETE_TEST) {
        const int points[14][2] = {{1, 2},
                           {1, 4},
                           {2, 2},
                           {4, 1},
                           {2, 5},
                           {3, 4},
                           {3, 3},
                           {4, 6},
                           {4, 4},
                           {4, 3},
                           {5, 5},
                           {5, 2},
                           {6, 3},
                           {6, 1}};
        core::archive::AllAcceptedArchive<NSGA2_Solution<representation::permutation::PermutationSolution<MockFitness>>> pop3;
        for (auto i = 0; i < 14; i++ ) {
            MockFitness fit;
            fit.validate();
            fit.objectives({points[i],points[i]+2});
            representation::permutation::PermutationSolution<MockFitness> s;
            s.fitness(fit);
            NSGA2_Solution<representation::permutation::PermutationSolution<MockFitness>> sol2(s);
            pop3(sol2); // add to archive
        }
        auto fronts = NSGA2_Solution<representation::permutation::PermutationSolution<MockFitness>>::fastNonDominatedSort(pop3);
        for (auto &f: fronts)
            NSGA2_Solution<representation::permutation::PermutationSolution<MockFitness>>::crowdingDistanceAssignment(pop3, f);
        const double expectedResults[14][2] = {
            {5, std::numeric_limits<double>::infinity()}, {3, std::numeric_limits<double>::infinity()},
            {4, std::numeric_limits<double>::infinity()}, {3, std::numeric_limits<double>::infinity()},
            {1, std::numeric_limits<double>::infinity()}, {2, std::numeric_limits<double>::infinity()},
            {3, 2}, {0, std::numeric_limits<double>::infinity()},
            {1, 1.5}, {2, std::numeric_limits<double>::infinity()},
            {0, 2}, {1, 1.25},
            {0, std::numeric_limits<double>::infinity()}, {1, std::numeric_limits<double>::infinity()}
        };
        for (auto i = 0;i < 14; i++ ) {
            ASSERT_EQ(pop3[i].getRank(), expectedResults[i][0]);
            ASSERT_EQ(pop3[i].getCrowdingDistance(), expectedResults[i][1]);
        }
    }
}

#endif //MH_BUILDER_NSGA2_TEST_H
