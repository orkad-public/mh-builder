/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_GA_SELECTION_TEST_H
#define MH_BUILDER_GA_SELECTION_TEST_H


#include "GATest.h"
#include "opt/manySolutions/geneticAlgorithm/selection/Selection.h"

using namespace representation::permutation::problems::tsp ;
namespace test {
    class SelectionTest : public GATest {
    protected:
        typedef core::fitness::FitnessMin<double> FIT;
        typedef TSPSol<FIT> SOL;
        typedef TSPEval<SOL> EVAL;
        typedef core::archive::AllAcceptedArchive<SOL> POPULATION;

        /**
         * default constructor
         */
        SelectionTest() = default;
        /**
         * default destructor
         */

        virtual ~SelectionTest() override = default;

        void SetUp() override {
            this->GATest::SetUp() ;
            this->selection = this->createSelection();
        }

        virtual opt::manySolutions::geneticAlgorithm::selection::Selection<POPULATION>* createSelection() = 0;

        void TearDown() override {}

        opt::manySolutions::geneticAlgorithm::selection::Selection<POPULATION> *selection{};

    };
}

#endif //MH_BUILDER_GA_SELECTION_TEST_H
