/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_EXPLORERTEST_H
#define MH_BUILDER_EXPLORERTEST_H

#include "opt/manySolutions/explorer/Explorer.h"
namespace test {
    class ExplorerTest : public ::testing::Test {
    protected:
        typedef MockFitness FIT;
        typedef MockSolution SOLUTION;
        typedef MockArchive ARCHIVE;

        ExplorerTest() = default;

        ~ExplorerTest() override = default;

        void SetUp() override {
          explorer = createExplorer();
        }

        virtual opt::manySolutions::explorer::Explorer<ARCHIVE>* createExplorer() = 0;

        void TearDown() override {}

        ARCHIVE archive;
        MockNeighborhood neighborhood;
        MockEval eval;
        MockSolution solution;
        opt::manySolutions::explorer::Explorer<ARCHIVE> *explorer{};
    };
}

#endif //MH_BUILDER_EXPLORERTEST_H
