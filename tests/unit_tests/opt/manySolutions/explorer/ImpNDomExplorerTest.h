/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_IMPNDOMEXPLORERTEST_H
#define MH_BUILDER_IMPNDOMEXPLORERTEST_H
#include <vector>

#include "opt/manySolutions/explorer/ImpNDomExplorer.h"
#include "opt/manySolutions/explorer/Explorer.h"

namespace test {
    class ImpNDomExplorerTest : public ExplorerTest {
    protected:
        ImpNDomExplorerTest() = default;
        ~ImpNDomExplorerTest() override = default;

        opt::manySolutions::explorer::Explorer<ARCHIVE> * createExplorer() override {
          return new opt::manySolutions::explorer::ImpNDomExplorer<ARCHIVE>(this->neighborhood, this->eval, 2);
        }
    };

    TEST_F(ImpNDomExplorerTest, TestImpNDomExplorerWithSolRef) {
      FIT fit;
      fit.validate();
      fit.objectives({6, 6});
      solution.fitness(fit);
      explorer->operator()(solution,archive);
      std::vector<unsigned long long int> expectedSolution = {0,1,2,3,4,5,6};
      ASSERT_EQ(expectedSolution.size(), archive.size());
      for(unsigned long long int i = 0; i < archive.size(); i++) {
        bool found = false;
        for (unsigned long long int j = 0; j < archive.size() && !found; j++) {
          found = found || neighborhood.getSolution(expectedSolution[i]) == archive[j];
        }
        ASSERT_TRUE(found);
      }
    }

    TEST_F(ImpNDomExplorerTest, TestImpNDomExplorerWithArchRef) {
      ARCHIVE archive_ref;
      FIT fit;
      fit.validate();
      fit.objectives({4, 8});
      solution.fitness(fit);
      archive_ref(solution);
      fit.objectives({7,3});
      solution.fitness(fit);
      archive_ref(solution);

      explorer->operator()(solution,archive_ref, archive);
      std::vector<unsigned long long int> expectedSolution = {0,1,2,3};
      ASSERT_EQ(expectedSolution.size(), archive.size());
      for(unsigned long long int i = 0; i < archive.size(); i++) {
        bool found = false;
        for (unsigned long long int j = 0; j < archive.size() && !found; j++) {
          found = found || neighborhood.getSolution(expectedSolution[i]) == archive[j];
        }
      ASSERT_TRUE(found);
      }
    }
}
#endif //MH_BUILDER_IMPNDOMEXPLORERTEST_H
