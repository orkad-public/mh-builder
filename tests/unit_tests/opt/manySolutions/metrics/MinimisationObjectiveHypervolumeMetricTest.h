/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MINIMISATIONOBJECTIVEHYPERVOLUMEMETRICTEST_H
#define MH_BUILDER_MINIMISATIONOBJECTIVEHYPERVOLUMEMETRICTEST_H

#include "gtest/gtest.h"
#include "core/Solution.h"
#include "core/fitness/FitnessMin.h"
#include "core/archive/NonDominatedArchive.h"
#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/MultiObjectiveHypervolumeMetric.h"

namespace test {
    class MinimisationObjectiveHypervolumeMetricTest : public ::testing::Test {
    protected:
        MinimisationObjectiveHypervolumeMetricTest() = default;

        ~MinimisationObjectiveHypervolumeMetricTest() override = default;

        void SetUp() override {
          FIT fit;
          fit.validate();
          SOLUTION sol;

          fit.objectives({10, 1});
          sol.fitness(fit);
          nonDominatedArchive(sol);
          fit.objectives({8, 2});
          sol.fitness(fit);
          sol.flag(1);
          nonDominatedArchive(sol);
          fit.objectives({5, 3});
          sol.fitness(fit);
          sol.flag(1);
          nonDominatedArchive(sol);
          fit.objectives({3, 4});
          sol.fitness(fit);
          nonDominatedArchive(sol);
          fit.objectives({2, 6});
          sol.fitness(fit);
          nonDominatedArchive(sol);
        }

        void TearDown() override {}

        typedef core::fitness::FitnessMin<> FIT;
        typedef core::Solution <FIT> SOLUTION;
        typedef core::archive::NonDominatedArchive <SOLUTION> ARCHIVE;
        ARCHIVE nonDominatedArchive;
    };

    TEST_F(MinimisationObjectiveHypervolumeMetricTest, TestBiObjectiveHypervolume) {
      opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<ARCHIVE> biObjectiveHypervolumeMetric;
      ASSERT_DOUBLE_EQ(static_cast<double>(0.525), biObjectiveHypervolumeMetric(nonDominatedArchive));
    }

    TEST_F(MinimisationObjectiveHypervolumeMetricTest, TestMultiObjectiveHypervolume1) {
        opt::manySolutions::metrics::MultiObjectiveHypervolumeMetric<ARCHIVE> multiObjectiveHypervolumeMetric;
        ASSERT_DOUBLE_EQ(static_cast<double>(0.525), multiObjectiveHypervolumeMetric(nonDominatedArchive));
    }

    TEST_F(MinimisationObjectiveHypervolumeMetricTest, TestBiObjectiveHypervolumeWithMulti) {
        nonDominatedArchive.clear();
        FIT fit; fit.validate();
        SOLUTION sol;
        fit.objectives({5, 3}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({2, 6}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({4, 5}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({6, 2}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({7, 1}); sol.fitness(fit); nonDominatedArchive(sol);
        nonDominatedArchive(sol);
        opt::manySolutions::metrics::MultiObjectiveHypervolumeMetric<ARCHIVE> multiObjectiveHypervolumeMetric;
        ASSERT_DOUBLE_EQ(0.32, multiObjectiveHypervolumeMetric(nonDominatedArchive));
    }
    TEST_F(MinimisationObjectiveHypervolumeMetricTest, TestBiObjectiveHypervolumeWithBi) {
        nonDominatedArchive.clear();
        FIT fit; fit.validate();
        SOLUTION sol;
        fit.objectives({5, 3}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({2, 6}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({4, 5}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({6, 2}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({7, 1}); sol.fitness(fit); nonDominatedArchive(sol);
        nonDominatedArchive(sol);
        opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<ARCHIVE> biObjectiveHypervolumeMetric;
        ASSERT_DOUBLE_EQ(0.32, biObjectiveHypervolumeMetric(nonDominatedArchive));
    }

    TEST_F(MinimisationObjectiveHypervolumeMetricTest, TestBiObjectiveHypervolumeWithMultiFixedBounds) {
        nonDominatedArchive.clear();
        FIT fit; fit.validate();
        SOLUTION sol;
        fit.objectives({5, 3}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({2, 6}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({4, 5}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({6, 2}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({7, 1}); sol.fitness(fit); nonDominatedArchive(sol);
        nonDominatedArchive(sol);
        std::vector<std::pair<double, double>> bounds ;
        bounds.push_back({0,10}) ; bounds.push_back({0,10})  ;
        opt::manySolutions::metrics::MultiObjectiveHypervolumeMetric<ARCHIVE> multiObjectiveHypervolumeMetric;
        ASSERT_DOUBLE_EQ(0.55, multiObjectiveHypervolumeMetric(nonDominatedArchive,bounds));
    }
    TEST_F(MinimisationObjectiveHypervolumeMetricTest, TestBiObjectiveHypervolumeWithBiFixedBounds) {
        nonDominatedArchive.clear();
        FIT fit; fit.validate();
        SOLUTION sol;
        fit.objectives({5, 3}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({2, 6}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({4, 5}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({6, 2}); sol.fitness(fit); nonDominatedArchive(sol);
        fit.objectives({7, 1}); sol.fitness(fit); nonDominatedArchive(sol);
        nonDominatedArchive(sol);
        std::vector<std::pair<double, double>> bounds ;
        bounds.push_back({0,10}) ; bounds.push_back({0,10})  ;
        opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<ARCHIVE> biObjectiveHypervolumeMetric;
        ASSERT_DOUBLE_EQ(0.55, biObjectiveHypervolumeMetric(nonDominatedArchive,bounds));
    }

}

#endif //MH_BUILDER_MINIMISATIONOBJECTIVEHYPERVOLUMEMETRICTEST_H
