/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_BIOBJECTIVESPREADMETRICTEST_H
#define MH_BUILDER_BIOBJECTIVESPREADMETRICTEST_H
#include "core/archive/NonDominatedArchive.h"
#include "opt/manySolutions/metrics/BiObjectiveSpreadMetric.h"

namespace test {
    class BiObjectiveSpreadMetricTest : public ::testing::Test {
    protected:
        BiObjectiveSpreadMetricTest() = default;

        ~BiObjectiveSpreadMetricTest() override = default;

        void SetUp() override {
          FIT fit;
          fit.validate();
          SOLUTION sol;

          fit.objectives({10, 1});
          sol.fitness(fit);
          nonDominatedArchive(sol);
          fit.objectives({8, 2});
          sol.fitness(fit);
          sol.flag(1);
          nonDominatedArchive(sol);
          fit.objectives({7, 4});
          sol.fitness(fit);
          sol.flag(1);
          nonDominatedArchive(sol);
          fit.objectives({5, 5});
          sol.fitness(fit);
          nonDominatedArchive(sol);
          fit.objectives({2, 6});
          sol.fitness(fit);
          nonDominatedArchive(sol);
        }

        void TearDown() override {}

        typedef core::fitness::FitnessMax<> FIT;
        typedef core::Solution <FIT> SOLUTION;
        typedef core::archive::NonDominatedArchive <SOLUTION> ARCHIVE;
        ARCHIVE nonDominatedArchive;
    };

    TEST_F(BiObjectiveSpreadMetricTest, TestSpread) {
      opt::manySolutions::metrics::BiObjectiveSpreadMetric<ARCHIVE> biObjectiveSpreadMetric;
      // So many round with hand computation
      ASSERT_NEAR(0.1372708509, biObjectiveSpreadMetric(nonDominatedArchive), 0.00001);
    }

}

#endif //MH_BUILDER_BIOBJECTIVESPREADMETRICTEST_H
