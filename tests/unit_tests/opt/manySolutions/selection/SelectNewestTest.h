/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_SELECTNEWESTTEST_H
#define MH_BUILDER_SELECTNEWESTTEST_H
#include "opt/manySolutions/selection/SelectNewest.h"
#include "opt/manySolutions/selection/Select.h"
namespace test {
    class SelectNewestTest : public SelectTest {
    protected:
        SelectNewestTest() = default;
        ~SelectNewestTest() override = default;

        opt::manySolutions::selection::Select<ARCHIVE> * createSelect() override {
          return new opt::manySolutions::selection::SelectNewest<ARCHIVE>(2);
        }

    };

   TEST_F(SelectNewestTest, TestSelectAll) {
      select->setSelectOnlyUnvisited(false);
      (*select)(archive);
      std::vector<unsigned long long int> expected = {3,4};
      ASSERT_EQ(select->size(), static_cast<long long unsigned int>(2));
      for(unsigned long long int i = 0; i < select->size(); i++) {
        ASSERT_EQ(expected[i], (*select)[i]);
      }
    }

    TEST_F(SelectNewestTest, TestSelectAllUnvisited) {
      (*select)(archive);
      std::vector<unsigned long long int> expected = {3,4};
      ASSERT_EQ(select->size(), static_cast<long long unsigned int>(2));
      for(unsigned long long int i = 0; i < select->size(); i++) {
        ASSERT_EQ(expected[i], (*select)[i]);
      }
    }
}

#endif //MH_BUILDER_SELECTNEWESTTEST_H
