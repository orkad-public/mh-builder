/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_ORDERNEIGHBORHOODMOCK_H
#define MH_BUILDER_ORDERNEIGHBORHOODMOCK_H


#include "opt/singleSolution/neighborhood/IndexNeighborhood.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

namespace mock {
    template <typename SOL>
    class OrderNeighborhoodMock : public opt::singleSolution::neighborhood::IndexNeighborhood<SOL> {
    public:

        explicit OrderNeighborhoodMock(opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> &_neighbor) : opt::singleSolution::neighborhood::IndexNeighborhood<SOL>(_neighbor) {}

        void init(const SOL &sol) override {
            opt::singleSolution::neighborhood::IndexNeighborhood<SOL>::init(sol);
            this->getIndexNeighbor()->setKey(kmock++ % this->getIndexNeighbor()->getMaxKey()); //To avoid the same re-init for test
        }

    protected:
        unsigned long long int kmock = 0;
    };
}

#endif //MH_BUILDER_ORDERNEIGHBORHOODMOCK_H
