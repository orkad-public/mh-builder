/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#ifndef MH_BUILDER_MOCKNEIGHBORHOOD_H
#define MH_BUILDER_MOCKNEIGHBORHOOD_H

#include<vector>
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/Eval.h"

namespace test {
    class MockNeighborhood : public opt::singleSolution::neighborhood::Neighborhood<MockSolution> {
    public:

        MockNeighborhood() : solutions(10) {
          MockSolution sol;
          MockFitness fit;

          fit.objectives({10, 1});
          sol.fitness(fit);
          solutions[0] = sol;

          fit.objectives({9, 1});
          sol.fitness(fit);
          solutions[1] = sol;

          fit.objectives({8, 4});
          sol.fitness(fit);
          solutions[2] = sol;

          fit.objectives({9, 8});
          sol.fitness(fit);
          solutions[3] = sol;

          fit.objectives({5, 10});
          sol.fitness(fit);
          solutions[4] = sol;

          fit.objectives({3, 9});
          sol.fitness(fit);
          solutions[5] = sol;

          fit.objectives({7, 7});
          sol.fitness(fit);
          solutions[6] = sol;

          fit.objectives({8, 7});
          sol.fitness(fit);
          solutions[7] = sol;

          fit.objectives({5, 9});
          sol.fitness(fit);
          solutions[8] = sol;

          fit.objectives({10, 4});
          sol.fitness(fit);
          solutions[9] = sol;

          index = 0;
        }

        void init([[maybe_unused]] const MockSolution &_sol) override {}

        void operator()(MockSolution &_sol) override {
          current = _sol;
          _sol = solutions[index];
        }

        void operator()(MockSolution &_sol, core::Eval<MockSolution> &_eval __attribute__((unused))) override {
          current = _sol;
          _sol = solutions[index];
        }

        void move_back(MockSolution &_sol) override {
          _sol = current;
        }

        void next() override {
          index++;
        }

        bool hasNextNeighbor() override {
          return index < 9;
        }

        MockSolution getSolution(unsigned long long int i) {
          return solutions[i];
        }

    protected:
        std::vector<MockSolution> solutions;
        unsigned long long int index;
        MockSolution current;
    };
}

#endif //MH_BUILDER_MOCKNEIGHBORHOOD_H
