/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Asuncion Gomez and additional contributors (see Authors)
****************************************************************************************/

#include "core/fitness/FitnessMin.h"
#include "representation/permutation/problems/fsp/FSP.h"
#include "representation/permutation/problems/fsp/FSPEval.h"
#include "representation/permutation/problems/fsp/algo/NEH.h"

using namespace representation::permutation::problems::fsp;

#include "util/RNGHelper.h"

#include "opt/factory/singleSolution/NeighborhoodFactory.h"
#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/factory/singleSolution/NeighborhoodExplorerFactory.h"
#include "opt/factory/singleSolution/PerturbationFactory.h"
#include "opt/factory/criterion/CriterionFactory.h"

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "core/Perturbation.h"
#include "core/Criterion.h"

#include "opt/singleSolution/localsearch/coolingSchedule/SimpleCoolingSchedule.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"
#include "opt/singleSolution/localsearch/SimulatedAnnealing.h"

#include "core/adaptive/TimeWindow.h"
#include "core/adaptive/ChangeIfSlowImprovement.h"

#include "util/TimerHelper.h"

typedef core::fitness::FitnessMin<double, double> FIT;
typedef FSPSol<FIT> SOL;
typedef FSPEval<SOL> EVAL;



opt::singleSolution::localsearch::SimulatedAnnealing<SOL> *
createAlgorithm(std::string neighbor_type, std::string neighborhood_type, std::string neighborhood_explorer,
                EVAL &eval, SOL &sol,
                std::string perturbation_strategy, unsigned long long int perturbation_strength,
                unsigned long long int sa_k,
                std::string criterion_strategy, unsigned long long int criterion_length);



int main(void) {

    util::RNGHelper::get()->reseed(117);
    auto instance= "../../instances/fsp/500_20_10.txt" ;

    FSP fsp_instance(instance);
    EVAL eval(fsp_instance);
    SOL sol(fsp_instance.getN());
    //----------- INITIAL SOLUTION VIA NEH -------------------//
    algo::NEH<SOL> neh(fsp_instance, eval);
    neh(sol);
    std::cout << "INIT SOL With NEH " << std::endl;
    std::cout << sol << std::endl;

    SOL sol2 = sol ; // duplicate solution
    SOL sol3 = sol ; // duplicate solution

    opt::singleSolution::localsearch::SimulatedAnnealing<SOL> *sa;

    //------------ STEP 1 -------------------------------------//
    //-------Configuration Algorithm --------------------------//
    std::string neighbor_type="swap" ;// available choices : "shift", "adj", "shift_without_adj", "swap", "2opt", "Incr2opt"
    std::string neighborhood_type = "rnd"; // available choices : "rnd", "order"
    std::string neighborhood_explorer = "first";// available choices :  "best", "first", "worst-improve"
    std::string criterion_strategy = "time";  // "iter", "time", "eval"
    unsigned long long int criterion_length = 10000 ;// milliseconds

    std::string perturbation_strategy = "neighborhood";
    unsigned long long int perturbation_strength = 3;
    unsigned long long int sa_k = 10000;

    for (auto run =1 ; run<=4 ; run++) {
        std::cout << "Run "<<run << ": neighbor_type='" << neighbor_type
                << "' during " << (criterion_length / 1000) << " seconds" << std::endl;
        sa = createAlgorithm(neighbor_type, neighborhood_type, neighborhood_explorer,
                             eval, sol,
                             perturbation_strategy, perturbation_strength,
                             sa_k,
                             criterion_strategy, criterion_length);
        sa->operator()(sol);
        eval(sol);
        std::cout << sol << std::endl;
        (neighbor_type == "swap") ? neighbor_type="shift" : neighbor_type="swap" ;
    }


    std::cout << "*************** Adaptive Part ************" << std::endl;


    // Step 1 : creation of the algorithm (once time)
    neighbor_type = "swap";
    sa = createAlgorithm(neighbor_type, neighborhood_type, neighborhood_explorer,
                         eval, sol2,
                         perturbation_strategy, perturbation_strength,
                         sa_k,
                         criterion_strategy, 4*criterion_length);


    // Step 2 : define functions that will modify the configuration of the algorithm during the process
    // remark : any box of the algorithm is updatable dynamically, here illustrated with neighbor boxes
    //util::TimerHelper timer2 ;
    representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL> shiftNeighbor;
    std::function<void(opt::singleSolution::localsearch::SimulatedAnnealing<SOL>&)> setToShift = [&](opt::singleSolution::localsearch::SimulatedAnnealing<SOL> &_sa) {
        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *ne ;
        ne = dynamic_cast<opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *>(_sa.getAlgorithm()) ;
        ne->getNeighborhood()->setNeighbor(shiftNeighbor);
        std::cout << "setToShift"  <<std::endl ;
    };

    representation::permutation::neighborhood::neighbor::SwapNeighbor<SOL> swapNeighbor;
    std::function<void(opt::singleSolution::localsearch::SimulatedAnnealing<SOL>&)> setToSwap = [&](opt::singleSolution::localsearch::SimulatedAnnealing<SOL> &_sa) {
        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *ne ;
        ne = dynamic_cast<opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *>(_sa.getAlgorithm()) ;
        ne->getNeighborhood()->setNeighbor(shiftNeighbor);
        std::cout << "setToSwap"  << std::endl ;
    };

    // Step 3 : store the functions in a vector in the right order and fix a time where these functions are called
    std::vector<std::function<void(opt::singleSolution::localsearch::SimulatedAnnealing<SOL>&)>> vec_functions = {setToSwap,setToShift, setToSwap,setToShift};

    std::vector<long long int> durations ;
    for (unsigned long long int i = 1 ; i<=vec_functions.size();i++) durations.push_back(criterion_length) ;

    // Step 4, encapsulation of the algorithm to the adaptive time windows
    core::adaptive::TimeWindow<SOL,opt::singleSolution::localsearch::SimulatedAnnealing<SOL> > timeWindow(*sa, vec_functions, durations);

    std::cout << "INITIAL SOLUTION AFTER NEH" << std::endl;
    std::cout << sol2 << std::endl;

    std::cout << "using TimeWindow strategy during " << vec_functions.size()*criterion_length << std::endl;
    std::cout << "every " << criterion_length <<" seconds, alternate the neighbor operators: swap and shift" << std::endl;

    timeWindow(sol2);


    std::cout << "FINAL SOLUTION" << std::endl;
    eval(sol2);
    std::cout << sol2 << std::endl;

    /****************************************************************/
    /* Using EvalSwitchWindow adaptive strategy */
    /****************************************************************/

    // Step 1 : creation of the algorithm (once time)
    neighbor_type = "swap";
    sa = createAlgorithm(neighbor_type, neighborhood_type, neighborhood_explorer,
                         eval, sol3,
                         perturbation_strategy, perturbation_strength,
                         sa_k,
                         criterion_strategy, 4*criterion_length);


    // Step 2 : define functions that will modify the configuration of the algorithm during the process
    // remark : any box of the algorithm is updatable dynamically, here illustrated with neighbor boxes
    util::TimerHelper timer2 ;
    //representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL> shiftNeighbor;
    std::function<void(opt::singleSolution::localsearch::SimulatedAnnealing<SOL>&)> setToShift2 = [&](opt::singleSolution::localsearch::SimulatedAnnealing<SOL> &_sa) {
        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *ne ;
        ne = dynamic_cast<opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *>(_sa.getAlgorithm()) ;
        ne->getNeighborhood()->setNeighbor(shiftNeighbor);
        std::cout << "setToShift"  <<std::endl ;
    };

    //representation::permutation::neighborhood::neighbor::SwapNeighbor<SOL> swapNeighbor;
    std::function<void(opt::singleSolution::localsearch::SimulatedAnnealing<SOL>&)> setToSwap2 = [&](opt::singleSolution::localsearch::SimulatedAnnealing<SOL> &_sa) {
        opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *ne ;
        ne = dynamic_cast<opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *>(_sa.getAlgorithm()) ;
        ne->getNeighborhood()->setNeighbor(shiftNeighbor);
        std::cout << "setToSwap"  << std::endl ;
    };

    // Step 3 : store the functions in a vector in the right order and fix a time where these functions are called
    std::vector<std::function<void(opt::singleSolution::localsearch::SimulatedAnnealing<SOL>&)>> vec_functions2 = {setToSwap,setToShift};

    // Step 4, encapsulation of the algorithm to the adaptive time windows
    core::adaptive::ChangeIfSlowImprovement<SOL,opt::singleSolution::localsearch::SimulatedAnnealing<SOL> > adaptive_sa(*sa, vec_functions2, 4, 0.02, 10,eval);
    std::cout << "using ChangeIfSlowImprovementWindow strategy during " << vec_functions.size()*criterion_length << std::endl;
    std::cout << "every 4 seconds, alternate the neighbor operators: swap and shift if the fitness improves less than 0.02 %" << std::endl;
    std::cout << "repeat 10 times, so 40 seconds for the complete run "<< std::endl;

    std::cout << "INITIAL SOLUTION AFTER NEH" << std::endl;
    std::cout << sol3 << std::endl;


    adaptive_sa(sol3);


    std::cout << "FINAL SOLUTION" << std::endl;
    eval(sol3);
    std::cout << sol3 << std::endl;

    return 0;
}


opt::singleSolution::localsearch::SimulatedAnnealing<SOL> *
createAlgorithm(std::string neighbor_type, std::string neighborhood_type, std::string neighborhood_explorer,
                EVAL &eval, SOL &sol,
                std::string perturbation_strategy, unsigned long long int perturbation_strength,
                unsigned long long int sa_k,
                std::string criterion_strategy, unsigned long long int criterion_length) {
    //----------- NEIGHBOR --------------------------//
    opt::factory::singleSolution::NeighborFactory<SOL, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL>> neighborFactory;
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> *neighbor = neighborFactory.create(neighbor_type);

    //----------- NEIGHBORHOOD ------------------------//
    opt::factory::singleSolution::NeighborhoodFactory<SOL> neighborhoodFactory;
    opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood = neighborhoodFactory.create(neighborhood_type,
                                                                                                    *neighbor);

    //----------- NEIGHBORHOOD EXPLORER ---------------//
    opt::factory::singleSolution::NeighborhoodExplorerFactory<SOL, EVAL> neighborhoodExplorerFactory;
    opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *neighborhoodExplorer = neighborhoodExplorerFactory.create(
            neighborhood_explorer, *neighborhood, eval);

    //---------- PERTURBATION ------------------------//
    opt::factory::singleSolution::PerturbationFactory<SOL, EVAL> perturbationFactory;
    core::Perturbation<SOL> *perturbation = perturbationFactory.create(perturbation_strategy, *neighborhood, &eval,
                                                                       perturbation_strength);

    //---------- STOP CRITERION ---------------------//
    opt::factory::criterion::CriterionFactory<SOL, EVAL> criterionFactory;
    core::Criterion<SOL> *criterion = criterionFactory.create(criterion_strategy, criterion_length, eval);


    //neighborhoodExplorer->setCriterion(*criterion) ;
    //neighbor->setCriterion(*criterion) ;
    //--------- LOCAL SEARCH ----------------------//
    opt::singleSolution::localsearch::coolingSchedule::SimpleCoolingSchedule<SOL> *simpleCoolingSchedule;

    simpleCoolingSchedule = new opt::singleSolution::localsearch::coolingSchedule::SimpleCoolingSchedule<SOL>(
            sol.fitness().scalar(), 100, 0.99, sa_k);

    return new opt::singleSolution::localsearch::SimulatedAnnealing<SOL>(*neighborhoodExplorer, *simpleCoolingSchedule,
                                                                         *criterion, *perturbation);
}


