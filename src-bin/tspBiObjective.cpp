/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Asuncion Gomez and additional contributors (see Authors)
****************************************************************************************/

#include <chrono>

#include "util/ParserHelper.h"
#include "util/RNGHelper.h"
#include "util/TimerHelper.h"


#include "opt/criterion/TimeCriterion.h"
#include "opt/eval/AggregationNormalizedEval.h"


#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/problems/tsp/TSPEvalBiObjective.h"
#include "representation/permutation/problems/tsp/algo/SimpleGreedy.h"
#include "representation/permutation/problems/tsp/algo/TwoGreedyImpr.h"
#include "representation/permutation/problems/fsp/algo/IteratedGreedyMakespan.h"

#include "representation/permutation/neighborhood/neighbor/DoubleBridgeNeighbor.h"
#include "representation/permutation/algorithm/RandomInit.h"

using namespace representation::permutation::problems::tsp ;

#include "opt/factory/singleSolution/NeighborhoodFactory.h"
#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/factory/singleSolution/NeighborhoodExplorerFactory.h"
#include "opt/factory/singleSolution/PerturbationFactory.h"
#include "opt/factory/criterion/CriterionFactory.h"

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "opt/singleSolution/neighborhood/RandomNeighborhood.h"
#include "opt/singleSolution/perturbation/NeighborhoodPerturbation.h"
#include "core/Perturbation.h"
#include "core/Criterion.h"
#include "core/fitness/FitnessMin.h"
#include "core/archive/NonDominatedArchive.h"
#include "core/archive/BoundedArchive.h"

#include "opt/manySolutions/selection/SelectAll.h"
#include "opt/manySolutions/selection/SelectRand.h"
#include "opt/manySolutions/selection/SelectNewest.h"
#include "opt/manySolutions/selection/SelectOldest.h"

#include "opt/manySolutions/explorer/Explorer.h"
#include "opt/manySolutions/explorer/ImpExplorer.h"
#include "opt/manySolutions/explorer/ImpNDomExplorer.h"
#include "opt/manySolutions/explorer/NDomExplorer.h"

#include "opt/manySolutions/localsearch/ParetoLocalSearch.h"
#include "opt/manySolutions/localsearch/IteratedParetoLocalSearch.h"

#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/BiObjectiveSpreadMetric.h"

#include "opt/singleSolution/localsearch/HillClimbing.h"
#include "opt/singleSolution/localsearch/coolingSchedule/SimpleCoolingSchedule.h"
#include "opt/singleSolution/localsearch/IteratedLocalSearch.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"
#include "opt/singleSolution/localsearch/SimulatedAnnealing.h"

#include "opt/perturbation/RestartPerturbation.h"
#include "opt/manySolutions/perturbation/KickPerturbation.h"

#include "representation/permutation/localsearch/IterativeImprovementInsertion.h"



#include "representation/permutation/localsearch/IterativeImprovementInsertion.h"

int main(int argc, char *argv[]){
  typedef core::fitness::FitnessMin<double, double> FIT;
  typedef TSPSol<FIT> SOL;

  typedef TSPEvalBiObjective<SOL> EVAL;
  typedef core::archive::NonDominatedArchive<SOL> UNBOUNDED_ARCHIVE;
  typedef core::archive::BoundedArchive<SOL, UNBOUNDED_ARCHIVE> ARCHIVE;

  util::ParserHelper parserHelper;

  parserHelper.setAs<unsigned long long int>("--seed", 1)->setImplicit(time(nullptr));
  parserHelper.setAs<std::string>("--instance1", "../../instances/tsp/tsp_test/cgC50_10_01.txt");
  parserHelper.setAs<std::string>("--instance2", "../../instances/tsp/tsp_test/cgC50_10_02.txt");
  parserHelper.setAs<std::string>("--select_strategy", "all");  //Tested: all, one, 1, 10
  parserHelper.setAs<std::string>("--explorer_strategy", "imp"); //Tested: imp, imp-ndom, ndom
  parserHelper.setAs<std::string>("--perturbation_strategy", "kick"); //Tested: restart, kick, kick-all 
  parserHelper.setAs<unsigned long long int>("--kick_size", 10); //Tested: 10
  parserHelper.setAs<unsigned long long int>("--explor_size", 1); //Tested: 1, 10
  parserHelper.setAs<unsigned long long int>("--perturbation_strength", 3); //Tested: 3, 10
  parserHelper.setAs<unsigned long long int>("--archive_size", 1000); // -1 for unbounded //Tested: 1000
  parserHelper.setAs<bool>("--moparamils", false)->setImplicit(true);
  parserHelper.setAs<bool>("--verbose", true)->setImplicit(true);

  parserHelper.parse(argc, argv);

  auto seed = parserHelper.getAs<unsigned long long int>("--seed");
  auto instance1 = parserHelper.getAs<std::string>("--instance1");
  auto instance2 = parserHelper.getAs<std::string>("--instance2");
  auto select_strategy = parserHelper.getAs<std::string>("--select_strategy");
  auto explorer_strategy = parserHelper.getAs<std::string>("--explorer_strategy");
  auto perturbation_strategy = parserHelper.getAs<std::string>("--perturbation_strategy");
  auto perturbation_strength = parserHelper.getAs<unsigned long long int>("--perturbation_strength");
  auto kick_size = parserHelper.getAs<unsigned long long int>("--kick_size");
  auto archive_size = parserHelper.getAs<unsigned long long int>("--archive_size");
  auto explor_size = parserHelper.getAs<unsigned long long int>("--explor_size");
  auto moparamils_output = parserHelper.getAs<bool>("--moparamils");
  auto verbose = parserHelper.getAs<bool>("--verbose");
  util::RNGHelper::get()->reseed(seed);

  TSP tsp_instance1(instance1);
  TSP tsp_instance2(instance2);
  
  TSPEval<SOL> tsp1(tsp_instance1);
  TSPEval<SOL> tsp2(tsp_instance2);


  auto runtime = static_cast<int>(100 * tsp_instance1.getNumberOfCities() * 0.9); // n*0.9 //1000
  auto runtime_init = static_cast<int>(runtime * 0.05); // 5%
  opt::criterion::TimeCriterion<ARCHIVE> required_time(runtime);
  opt::criterion::TimeCriterion<SOL> required_time_init(runtime_init);

  if (verbose){
    std::cout << "Instance loaded: " << std::endl;
    std::cout << "   " << instance1 << std::endl;
    std::cout << "   " << instance2 << std::endl;
    
    std::cout << "Number of cities: " << tsp_instance1.getNumberOfCities() << std::endl;
    std::cout << "Runtime: " << runtime/1000 << " seconds" << std::endl;
    std::cout << "Runtime initialisation: " << runtime_init/1000 << " seconds" << std::endl;
  }

  EVAL eval(tsp1, tsp2);
  opt::eval::AggregationNormalizedEval<SOL> aggregationNormalizedEval(eval, eval.getMinMax());
  aggregationNormalizedEval.init({0.5,0.5});

  representation::permutation::neighborhood::neighbor::DoubleBridgeNeighbor<SOL> doubleBridgeNeighbor;
  opt::singleSolution::neighborhood::RandomNeighborhood<SOL> rndWithoutRemplNeighborhood(doubleBridgeNeighbor);
  opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood = &rndWithoutRemplNeighborhood;

  // CHOSE SELECT STRATEGY
  opt::manySolutions::selection::SelectAll<ARCHIVE> *select;
  if (select_strategy == "all")
    select = new opt::manySolutions::selection::SelectAll<ARCHIVE>();
  else if (select_strategy == "one")
    select = new opt::manySolutions::selection::SelectRand<ARCHIVE>(1);
  else if (util::StringHelper::isNumber(select_strategy))
    select = new opt::manySolutions::selection::SelectRand<ARCHIVE>(std::stoi(select_strategy));
  else if (select_strategy == "newest")
    select = new opt::manySolutions::selection::SelectNewest<ARCHIVE>(1);
  else if (select_strategy == "oldest")
    select = new opt::manySolutions::selection::SelectOldest<ARCHIVE>(1);
  else
    throw std::runtime_error(R"(Selection strategy unknown. Choose between ["all", "one"] or an unsigned integer)");

  // CHOOSE EXPLORER STRATEGY
  opt::manySolutions::explorer::Explorer<ARCHIVE> *explorer;
  if ( explorer_strategy == "all" )
    explorer = new opt::manySolutions::explorer::NDomExplorer<ARCHIVE>(*neighborhood, eval);
  else if ( explorer_strategy == "all-imp" )
    explorer = new opt::manySolutions::explorer::ImpExplorer<ARCHIVE>(*neighborhood, eval);
  else if ( explorer_strategy == "first" )
    explorer = new opt::manySolutions::explorer::ImpNDomExplorer<ARCHIVE>(*neighborhood, eval);
  else if ( explorer_strategy == "imp" )
    explorer = new opt::manySolutions::explorer::ImpExplorer<ARCHIVE>(*neighborhood, eval, explor_size);
  else if ( explorer_strategy == "imp-ndom" )
    explorer = new opt::manySolutions::explorer::ImpNDomExplorer<ARCHIVE>(*neighborhood, eval, explor_size);
  else if ( explorer_strategy == "ndom" )
    explorer = new opt::manySolutions::explorer::NDomExplorer<ARCHIVE>(*neighborhood, eval, explor_size);
  else
    throw std::runtime_error(R"(Exploration strategy unknown. Choose between ["all", "all-imp", "first", "imp", "imp-ndom", "ndom"])");

  // CHOOSE PERTURBATION STRATEGY
  core::Perturbation<ARCHIVE> *perturbation;
  opt::singleSolution::perturbation::NeighborhoodPerturbation<SOL> neighborhoodPerturbation(*neighborhood, &eval, perturbation_strength);

  if ( perturbation_strategy == "restart" ) {
    representation::permutation::algorithm::RandomInit<SOL> randomInit(&eval);
    opt::perturbation::RestartPerturbation<SOL> restartPerturbation(randomInit);
    perturbation = new opt::manySolutions::perturbation::KickPerturbation<ARCHIVE>(restartPerturbation, kick_size);
  }
  else if ( perturbation_strategy == "kick" )
    perturbation = new opt::manySolutions::perturbation::KickPerturbation<ARCHIVE>(neighborhoodPerturbation, kick_size);
  else if ( perturbation_strategy == "kick-all" )
    perturbation = new opt::manySolutions::perturbation::KickPerturbation<ARCHIVE>(neighborhoodPerturbation);
  else
    throw std::runtime_error(R"(Perturbation strategy unknown. Choose between ["restart", "kick", "kick-all"])");
  
  //INITIALISATION
  opt::factory::singleSolution::NeighborhoodExplorerFactory<SOL, EVAL> neighborhoodExplorerFactory;
  opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *neighborhoodExplorer = neighborhoodExplorerFactory.create("first", *neighborhood, eval);

  opt::singleSolution::localsearch::HillClimbing<SOL> hillclimbing(*neighborhoodExplorer, aggregationNormalizedEval);
  algo::TwoGreedyImpr<ARCHIVE, SOL> twoGreedy(tsp_instance1, tsp_instance2, hillclimbing, aggregationNormalizedEval, required_time_init);
  
  core::archive::NonDominatedArchive<SOL> nonDominatedArchive;
  ARCHIVE archive(&nonDominatedArchive, archive_size);
  
  twoGreedy(archive);

  if (verbose) { 
    std::cout << "Population of the archive initialized: " << std::endl;
    std::cout << archive << std::endl;
  }
  
  // ALGORITHM
  opt::manySolutions::localsearch::ParetoLocalSearch<ARCHIVE, UNBOUNDED_ARCHIVE> paretoLocalSearch(*select, *explorer);
  opt::manySolutions::localsearch::IteratedParetoLocalSearch<ARCHIVE> iteratedParetoLocalSearch(*perturbation, paretoLocalSearch, required_time);

  util::TimerHelper timerHelper;

  timerHelper.start();
  if (verbose) std::cout << "Pareto Iterated Local Search started ..." << std::endl;
  iteratedParetoLocalSearch(archive);
  timerHelper.update();
  if (verbose) std::cout << "Pareto Iterated Local Search done" << std::endl;

  if ( !moparamils_output ) {
    std::cout << "FINAL ARCHIVE :" << std::endl;
    archive.sort();
    std::cout << archive << std::endl;
  }

  opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<ARCHIVE> biObjectiveHypervolumeMetric;
  opt::manySolutions::metrics::BiObjectiveSpreadMetric<ARCHIVE> biObjectiveSpreadMetric;
  
  double hypervolume = biObjectiveHypervolumeMetric(archive);
  double spread = biObjectiveSpreadMetric(archive);
  std::cout << "Result: SUCCESS, " << 1;
  std::cout << ", [" << hypervolume << ", " << spread << "], " << seed << std::endl;
  
  return 0;

}
