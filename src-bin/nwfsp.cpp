/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#include "core/fitness/FitnessMin.h"
#include "representation/permutation/problems/fsp/FSP.h"
#include "representation/permutation/problems/fsp/NoWaitFSP.h"
#include "representation/permutation/problems/fsp/NoWaitFSPEval.h"
#include "representation/permutation/problems/fsp/algo/NEH.h"

#include "opt/criterion/EvalCriterion.h"

using namespace representation::permutation::problems::fsp;

#include "util/RNGHelper.h"

#include "opt/singleSolution/localsearch/HillClimbing.h"
#include "representation/permutation/neighborhood/neighbor/ShiftNeighbor.h"
#include "opt/singleSolution/neighborhood/RandomNeighborhood.h"
#include "opt/singleSolution/neighborhood/explorer/BestImprNeighborhoodExplorer.h"
#include "opt/criterion/TimeCriterion.h"


int main(void) {


    util::RNGHelper::get()->reseed(1);

    // tag::part1[]
    /**************************************************************************/
    /* Part 1 : creation of required objects                                  */
    /**************************************************************************/
    typedef core::fitness::FitnessMin<double, double> FIT; // <1>
    typedef FSPSol<FIT> SOL;
    typedef NoWaitFSPEval<SOL> EVAL;

    NoWaitFSP fsp_instance("../../instances/fsp/020_05_01.txt");  // the problem object <2>
    EVAL eval(fsp_instance);  // the evaluation object, requires the problem instance
    SOL sol(fsp_instance.getN());  // <4> creation of the solution object
    // end::part1[]

    // tag::part2[]
    /**************************************************************************/
    /* Part 2 : first resolution of the problem                               */
    /* by using the heuristics NEH                                            */
    /**************************************************************************/

    algo::NEH<SOL> neh(fsp_instance, eval); //  <1>
    neh(sol); // <2>
    std::cout << "Solution provided by the NEH heuristic" << std::endl; // display results
    std::cout << sol << std::endl;

    std::cout << "Number of evaluations: "  << eval.getEvalCounter() << std::endl ;
    // end::part2[]


    // tag::part3And4[]
    /****************************************************************************/
    /* Part 3 : optimisation of the problem                                     */
    /* by using the hill climbing metaheuristics                                */
    /****************************************************************************/
    representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL> shiftNeighbor; //<1>
    opt::singleSolution::neighborhood::RandomNeighborhood<SOL> randomNeighborhood(shiftNeighbor);
    opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<SOL> best(randomNeighborhood, eval);
    opt::singleSolution::localsearch::HillClimbing<SOL> hillclimbing(best, eval);

    /******************************************************************************/
    /* Part 4 : running metaheuristics                                            */
    /******************************************************************************/

    std::cout << "RUNNING HillClimbing LOCAL SEARCH" << std::endl;
    hillclimbing(sol); // <2>


    std::cout << "Solution provided by the HillClimbing metaheuristic" << std::endl; // display results
    std::cout << sol << std::endl;

    std::cout << "Number of evaluations: "  << eval.getEvalCounter() << std::endl ;
    // end::part3And4[]

    /****************************************************************************/
    /* begin Section : tutorial of stop criterion                               */
    /****************************************************************************/

    std::cout << "************************************************" << std::endl;
    std::cout << " Begin section : tutorial of stop criterion (SC)" << std::endl ;
    std::cout << "************************************************" << std::endl;

    // creation of objects for tutorial:
    // tag::stopCriterionPart1[]
    NoWaitFSP fsp_instanceSC("../../instances/fsp/020_05_01.txt");  // the problem object
    EVAL evalSC(fsp_instanceSC);  // the evaluation object, requires the problem instance
    SOL solSC(fsp_instance.getN());  //  creation of the solution object
    opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<SOL> bestSC(randomNeighborhood, evalSC);

    opt::singleSolution::localsearch::HillClimbing<SOL> hillclimbingSC(bestSC, evalSC);

    opt::criterion::EvalCriterion<SOL> stop500evaluations(500,evalSC) ;  // <1>
    opt::criterion::TimeCriterion<SOL> stopMilliSeconds(3000) ;
    // end::stopCriterionPart1[]

    //  tag::stopCriterionPart2[]
    hillclimbingSC.setCriterion(stop500evaluations); // <1>
    std::cout << "Start running HillClimbing LOCAL SEARCH with max evaluation : 500" << std::endl;
    hillclimbingSC(solSC); // <2>
    // end::stopCriterionPart2[]

    std::cout << "Solution provided by the HillClimbing metaheuristic" << std::endl; // display results
    std::cout << solSC << std::endl;

    std::cout << "Number of evaluations: "  << evalSC.getEvalCounter() << std::endl ;

    //  tag::stopCriterionPart3[]
    hillclimbingSC.setCriterion(stopMilliSeconds); // <1>
    std::cout << "Continue the execution of  HillClimbing LOCAL SEARCH during 3 seconds" << std::endl;
    hillclimbingSC(solSC); // <2>
    // end::stopCriterionPart3[]

    std::cout << "Solution provided by the HillClimbing metaheuristic" << std::endl; // display results
    std::cout << solSC << std::endl;

    std::cout << "Number of evaluations: "  << evalSC.getEvalCounter() << std::endl ;

    std::cout << "************************************************" << std::endl;
    std::cout << " End section : tutorial of stop criterion (SC)" << std::endl ;
    std::cout << "************************************************" << std::endl;
    return 0;
}


