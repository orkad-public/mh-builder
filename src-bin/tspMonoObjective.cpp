/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Asuncion Gomez and additional contributors (see Authors)
****************************************************************************************/

#include <chrono>

#include "util/ParserHelper.h"
#include "util/RNGHelper.h"


#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"

#include "representation/permutation/problems/fsp/algo/IteratedGreedyMakespan.h"

using namespace representation::permutation::problems::tsp ;

#include "opt/factory/singleSolution/NeighborhoodFactory.h"
#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/factory/singleSolution/NeighborhoodExplorerFactory.h"
#include "opt/factory/singleSolution/PerturbationFactory.h"
#include "opt/factory/criterion/CriterionFactory.h"

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "core/Perturbation.h"
#include "core/Criterion.h"
#include "core/fitness/FitnessMin.h"

#include "opt/singleSolution/localsearch/HillClimbing.h"

#include "opt/singleSolution/localsearch/IteratedLocalSearch.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"


#include "representation/permutation/localsearch/IterativeImprovementInsertion.h"

int main(int argc, char *argv[]){
  typedef core::fitness::FitnessMin<double> FIT;
  typedef TSPSol<FIT> SOL;
  typedef TSPEval<SOL> EVAL;

  util::ParserHelper parserHelper;

  parserHelper.setAs<unsigned long long int>("--seed", 1)->setImplicit(time(nullptr));
  parserHelper.setAs<std::string>("--instance", "../../instances/tsp/tsp_test/rat783.tsp");
  //parserHelper.setAs<std::string>("--instance", "../../instances/tsp/tsp_test/small.txt");
  parserHelper.setAs<std::string>("--localsearch_algorithm", "hc"); // "hc", "ils", "ig", "iii"
  parserHelper.setAs<std::string>("--neighbor", "2opt"); // "shift", "adj", "shift_without_adj", "swap", "2opt", "doubleBridge"
  parserHelper.setAs<std::string>("--neighborhood", "rnd"); // "rnd", "order"
  parserHelper.setAs<std::string>("--neighborhood_explorer", "first"); // "best", "first", "worst-improve"
  parserHelper.setAs<std::string>("--perturbation_strategy", "neighborhood"); // "neighborhood"
  parserHelper.setAs<unsigned long long int>("--perturbation_strength", 3);
  parserHelper.setAs<std::string>("--criterion_strategy", "time"); // "iter", "time"
  parserHelper.setAs<unsigned long long int>("--criterion_length", 30000);

  parserHelper.parse(argc, argv);

  auto seed = parserHelper.getAs<unsigned long long int>("--seed");
  auto instance = parserHelper.getAs<std::string>("--instance");
  auto localsearch_algorithm = parserHelper.getAs<std::string>("--localsearch_algorithm");
  auto neighbor_type = parserHelper.getAs<std::string>("--neighbor");
  auto neighborhood_type = parserHelper.getAs<std::string>("--neighborhood");
  auto neighborhood_explorer = parserHelper.getAs<std::string>("--neighborhood_explorer");
  auto perturbation_strategy = parserHelper.getAs<std::string>("--perturbation_strategy");
  auto perturbation_strength = parserHelper.getAs<unsigned long long int>("--perturbation_strength");
  auto criterion_strategy = parserHelper.getAs<std::string>("--criterion_strategy");
  auto criterion_length = parserHelper.getAs<unsigned long long int>("--criterion_length");

  util::RNGHelper::get()->reseed(seed);

  TSP tsp_instance(instance);
  EVAL eval(tsp_instance);


  //----------- INITIAL SOLUTION -------------------//
  SOL sol(tsp_instance.getNumberOfCities());



    //----------- NEIGHBOR --------------------------//
    opt::factory::singleSolution::NeighborFactory<SOL, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL>> neighborFactory;
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> *neighbor = neighborFactory.create(neighbor_type);

    //----------- NEIGHBORHOOD ------------------------//
    opt::factory::singleSolution::NeighborhoodFactory<SOL> neighborhoodFactory;
    opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood = neighborhoodFactory.create(neighborhood_type, *neighbor);

    //----------- NEIGHBORHOOD EXPLORER ---------------//
    opt::factory::singleSolution::NeighborhoodExplorerFactory<SOL, EVAL> neighborhoodExplorerFactory;
    opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *neighborhoodExplorer = neighborhoodExplorerFactory.create(neighborhood_explorer, *neighborhood, eval);

    //---------- PERTURBATION ------------------------//
    opt::factory::singleSolution::PerturbationFactory<SOL, EVAL> perturbationFactory;
    core::Perturbation<SOL> *perturbation = perturbationFactory.create(perturbation_strategy, *neighborhood, &eval, perturbation_strength);


    //---------- STOP CRITERION ---------------------//
    opt::factory::criterion::CriterionFactory<SOL, EVAL> criterionFactory;
    core::Criterion<SOL> *criterion = criterionFactory.create(criterion_strategy, criterion_length, eval);

  //--------- LOCAL SEARCH ----------------------//
  core::Algorithm<SOL> *localSearch;
    opt::singleSolution::localsearch::HillClimbing<SOL> hillclimbing(*neighborhoodExplorer, eval);
    representation::permutation::localsearch::IterativeImprovementInsertion<SOL> iterativeImprovementInsertion(eval, *criterion);
  if ( localsearch_algorithm == "hc" ) {
    localSearch = &hillclimbing;
    hillclimbing.setCriterion(*criterion) ;
  } else if ( localsearch_algorithm == "ils" ) {
    localSearch = new opt::singleSolution::localsearch::IteratedLocalSearch<SOL>(*perturbation, hillclimbing, *criterion);
  } else if ( localsearch_algorithm == "ig") {
    localSearch = new representation::permutation::problems::fsp::algo::IteratedGreedyMakespan<SOL>(eval, *criterion, iterativeImprovementInsertion);
  } else if ( localsearch_algorithm == "iii") {
    localSearch = &iterativeImprovementInsertion;
  } else {
    throw std::runtime_error(R"(Local search algorithm unknown. Choose between ["hc", "ils", "ig", "iii"])");
  }

  sol.shuffle();
  eval(sol);
  std::cout << "START " << sol.fitness() << std::endl;
 /* neighbor->init(sol);
  for (int i = 0; i < neighbor->getMaxKey(); i++) {
    neighbor->setKey(i);
    neighbor->do_move(sol);
    eval(sol);
    std::cout << sol << std::endl;
    neighbor->do_move_back(sol);
  }*/

 std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
/*
  int total_count = 0;
  int idle_count = 0;
  FIT fitness = sol.fitness();
  neighbor->init(sol);
  while (total_count < 100000 || idle_count * 2 < total_count) {
    //neighborhood->init(sol);
    //neighborhood->operator()(sol, eval);
    neighbor->setKey(core::RNGHelper::get()->random(neighbor->getMaxKey()));
    neighbor->do_move(sol, eval);
    if ( fitness < sol.fitness() ) {
      fitness = sol.fitness();
      idle_count = 0;
    } else {
      neighbor->do_move_back(sol);
      total_count++;
      idle_count++;
    }
  }



  //*/

  (*localSearch)(sol);
  std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();

  eval(sol);
  std::cout << "FINAL " << sol.fitness() << std::endl;
  std::cout << "TPS = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " milliseconds" << std::endl;

  return 0;

}
