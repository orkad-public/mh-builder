/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#include <vector>
#include "util/ParserHelper.h"
#include "util/RNGHelper.h"

#include "core/adaptive/TimeWindow.h"


#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/neighborhood/neighbor/ShiftWithoutSwapNeighbor.h"


using namespace representation::permutation::problems::tsp ;

#include "opt/factory/singleSolution/NeighborhoodFactory.h"
#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/factory/singleSolution/NeighborhoodExplorerFactory.h"
#include "opt/factory/criterion/CriterionFactory.h"

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/fitness/FitnessMin.h"

#include "opt/manySolutions/localsearch/IteratedParetoLocalSearch.h"
#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"

#include "opt/singleSolution/localsearch/HillClimbing.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"

typedef core::fitness::FitnessMin<double> FIT;
typedef TSPSol<FIT> SOL;
typedef TSPEval<SOL> EVAL;

opt::singleSolution::localsearch::HillClimbing<SOL> *
createAlgorithm(std::string neighbor_type, std::string neighborhood_type, std::string neighborhood_explorer,
                EVAL &eval,
        std::string criterion_strategy, unsigned long long int criterion_length) ;

int main(void) {


  // std::string instance="../../instances/tsp/tsp_test/rat783.tsp" ;
  std::string instance="../../instances/tsp/tsp_test/gC400_10.txt" ;

  std::string neighbor_type = "swap" ; // "shift", "adj", "shift_without_adj", "swap"
  std::string neighborhood_type = "rnd" ;//  "rnd", "order"
  std::string neighborhood_explorer = "first";// available choices :  "best", "first", "worst-improve"
  std::string criterion_strategy = "time";  // "iter", "time", "eval"
  unsigned long long int criterion_length = 5000 ;// milliseconds

  util::RNGHelper::get()->reseed(3);

  TSP tsp_instance(instance);
  EVAL eval(tsp_instance);


  //----------- INITIAL SOLUTION -------------------//
  SOL sol(tsp_instance.getNumberOfCities());

    //--------- LOCAL SEARCH ----------------------//

  // Chosen algorithm : HillClimbing
  std::cout << "HillClimbing algorithm" << std::endl;
  std::cout << "Part 1/3 : swap operator in a random neighborhood, explorer:first" << std::endl ;


    opt::singleSolution::localsearch::HillClimbing<SOL> *algo =
            createAlgorithm(neighbor_type, neighborhood_type, neighborhood_explorer,eval,
    criterion_strategy, 2*criterion_length) ;

  // define functions that will modify the configuration of the algorithm during the process
  // remark : any box of the algorithm is updatable dynamically
  // first change : substitute the explorer box (from first to best)


    std::function<void(opt::singleSolution::localsearch::HillClimbing<SOL> &)> set2BestImprovement = [&](
            opt::singleSolution::localsearch::HillClimbing<SOL> &_hillclimbing) {
        std::cout << "set2BestImprovement" << std::endl;
        opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<SOL> *bestImprovement =
                new opt::singleSolution::neighborhood::explorer::BestImprNeighborhoodExplorer<SOL>(
                        *(algo->getNeighborhoodExplorer()->getNeighborhood()),
                        eval);

        _hillclimbing.setNeighborhoodExplorer(*bestImprovement);
    };

    std::function<void(opt::singleSolution::localsearch::HillClimbing<SOL> &)> set2Shift = [&](
            opt::singleSolution::localsearch::HillClimbing<SOL> &_hillclimbing) {
        std::cout << "setNeighborOperatorToShift" << std::endl;
        representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL> *shiftNeighbor = new representation::permutation::neighborhood::neighbor::ShiftNeighbor<SOL>();
        _hillclimbing.getNeighborhoodExplorer()->getNeighborhood()->setNeighbor(*shiftNeighbor);
    };



  //  store the functions in a vector in the right order and fix a time where these functions are called
  std::vector<std::function<void(opt::singleSolution::localsearch::HillClimbing<SOL>&)>> functions = {set2BestImprovement,set2Shift};

  //  fix the durations
  std::vector<long long int> durations ;
  for (unsigned long long int i = 1 ; i<=functions.size();i++) durations.push_back(criterion_length) ;

  // encapsulation of the algorithm with the adaptive timewindow object
  core::adaptive::TimeWindow<SOL, opt::singleSolution::localsearch::HillClimbing<SOL>> timeWindow(*algo, functions, durations);

  std::cout << "ADAPTIVE VERSION" << std::endl ;
  std::cout << "INITIAL SOLUTION" << std::endl;
  eval(sol);
  std::cout << sol << std::endl;
  SOL sol2 = sol ;// duplicate solution



  timeWindow(sol);

  std::cout << "FINAL SOLUTION" << std::endl;
  std::cout << sol << std::endl;



  return 0;

}


opt::singleSolution::localsearch::HillClimbing<SOL> *
createAlgorithm(std::string neighbor_type, std::string neighborhood_type, std::string neighborhood_explorer,
                EVAL &eval,
                std::string criterion_strategy, unsigned long long int criterion_length) {
    //----------- NEIGHBOR --------------------------//
    opt::factory::singleSolution::NeighborFactory<SOL, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL>> neighborFactory;
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> *neighbor = neighborFactory.create(neighbor_type);

    //----------- NEIGHBORHOOD ------------------------//
    opt::factory::singleSolution::NeighborhoodFactory<SOL> neighborhoodFactory;
    opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood = neighborhoodFactory.create(neighborhood_type,
                                                                                                    *neighbor);

    //----------- NEIGHBORHOOD EXPLORER ---------------//
    opt::factory::singleSolution::NeighborhoodExplorerFactory<SOL, EVAL> neighborhoodExplorerFactory;
    opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *neighborhoodExplorer = neighborhoodExplorerFactory.create(
            neighborhood_explorer, *neighborhood, eval);
    //---------- STOP CRITERION ---------------------//
    opt::factory::criterion::CriterionFactory<SOL, EVAL> criterionFactory;
    core::Criterion<SOL> *criterion = criterionFactory.create(criterion_strategy, criterion_length, eval);
    opt::singleSolution::localsearch::HillClimbing<SOL> *algo = new opt::singleSolution::localsearch::HillClimbing<SOL>(*neighborhoodExplorer, eval);
    algo->setCriterion(*criterion) ;
    return algo ;
}