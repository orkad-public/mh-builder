/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/



#include "util/PlotHelper.h"
#include "util/RNGHelper.h"
#include "opt/eval/AggregationNormalizedEval.h"


#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/problems/tsp/TSPEvalBiObjective.h"


using namespace representation::permutation::problems::tsp ;

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "core/fitness/FitnessMin.h"
#include "core/archive/NonDominatedArchive.h"
#include "core/archive/BoundedArchive.h"
#include "core/archive/AllAcceptedArchive.h"

#include "opt/manySolutions/localsearch/IteratedParetoLocalSearch.h"
#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/BiObjectiveSpreadMetric.h"


int main(void) {
  typedef core::fitness::FitnessMin<double, double> FIT;
  typedef TSPSol<FIT> SOL;

  typedef TSPEvalBiObjective<SOL> EVAL;
  typedef core::archive::AllAcceptedArchive<SOL> ARCHIVE;
  typedef core::archive::NonDominatedArchive<SOL> PARETO;
  typedef core::archive::BoundedArchive<SOL,PARETO> BOUNDED_ARCHIVE;  // first parameter ?

  TSP tsp_instance1(11, 1);
  TSP tsp_instance2(11, 2);

  TSPEval<SOL> tsp1(tsp_instance1);
  TSPEval<SOL> tsp2(tsp_instance2);

  EVAL eval(tsp1, tsp2);
  ARCHIVE archive;

  SOL sol(tsp_instance1.getNumberOfCities());

  do {
    eval(sol);
    archive(sol);
  } while (std::next_permutation(sol.begin()+1, sol.end()));

  util::RNGHelper::get()->shuffle(archive.begin(), archive.end());

  PARETO pareto1, pareto2;
  for (unsigned long long int i = 0; i < archive.size() / 2; ++i) {
    pareto1(archive[i]);
  }
  for(unsigned long long int i = archive.size() / 2; i < archive.size(); i++) {
    pareto2(archive[i]);
  }

  opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<PARETO> biObjectiveHypervolumeMetric1;
  opt::manySolutions::metrics::BiObjectiveSpreadMetric<PARETO> biObjectiveSpreadMetric1;

    opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<BOUNDED_ARCHIVE> biObjectiveHypervolumeMetric2;
    opt::manySolutions::metrics::BiObjectiveSpreadMetric<BOUNDED_ARCHIVE> biObjectiveSpreadMetric2;

//  plt::plot_arch_biobj(archive);
//  plt::plot_arch_biobj(pareto);
  plt::scatter_arch_biobj(pareto1, 100, "F1");
  plt::scatter_arch_biobj(pareto2, 80, "F2");

  PARETO pareto;
  pareto(pareto1);
  pareto(pareto2);
  plt::scatter_arch_biobj(pareto, 60, "MF 1+2");

  PARETO pareto3;
  BOUNDED_ARCHIVE boundedArchive(&pareto3, 10);
  boundedArchive(pareto1);
  boundedArchive(pareto2);
  plt::scatter_arch_biobj(boundedArchive, 40, "MBF 1 -> 2");

  PARETO pareto4;
  BOUNDED_ARCHIVE boundedArchive2(&pareto4, 10);
  boundedArchive2(pareto2);
  boundedArchive2(pareto1);
  plt::scatter_arch_biobj(boundedArchive2, 20, "MBF 2 -> 1");

  std::cout << "SANS BORNE COMMUNE" << std::endl;
  std::cout << "F1 [" << biObjectiveHypervolumeMetric1(pareto1) << ", " << biObjectiveSpreadMetric1(pareto1) << "], " << std::endl;
  std::cout << "F2 [" << biObjectiveHypervolumeMetric1(pareto2) << ", " << biObjectiveSpreadMetric1(pareto2) << "], " << std::endl;
  std::cout << "MF1+2 [" << biObjectiveHypervolumeMetric1(pareto) << ", " << biObjectiveSpreadMetric1(pareto) << "], " << std::endl;
  std::cout << "MBF 1->2 [" << biObjectiveHypervolumeMetric2(boundedArchive) << ", " << biObjectiveSpreadMetric2(boundedArchive) << "], " << std::endl;
  std::cout << "MBF 1->2 [" << biObjectiveHypervolumeMetric2(boundedArchive2) << ", " << biObjectiveSpreadMetric2(boundedArchive2) << "], " << std::endl;

  std::cout << "AVEC BORNE COMMUNE" << std::endl;
  std::cout << "F1 [" << biObjectiveHypervolumeMetric1(pareto1, eval.getMinMax()) << ", " << biObjectiveSpreadMetric1(pareto1, eval.getMinMax()) << "], " << std::endl;
  std::cout << "F2 [" << biObjectiveHypervolumeMetric1(pareto2, eval.getMinMax()) << ", " << biObjectiveSpreadMetric1(pareto2, eval.getMinMax()) << "], " << std::endl;
  std::cout << "MF1+2 [" << biObjectiveHypervolumeMetric1(pareto, eval.getMinMax()) << ", " << biObjectiveSpreadMetric1(pareto, eval.getMinMax()) << "], " << std::endl;
  std::cout << "MBF 1->2 [" << biObjectiveHypervolumeMetric2(boundedArchive, eval.getMinMax()) << ", " << biObjectiveSpreadMetric2(boundedArchive, eval.getMinMax()) << "], " << std::endl;
  std::cout << "MBF 2->1 [" << biObjectiveHypervolumeMetric2(boundedArchive2, eval.getMinMax()) << ", " << biObjectiveSpreadMetric2(boundedArchive2, eval.getMinMax()) << "], " << std::endl;

  matplotlibcpp::legend();
  matplotlibcpp::show();


}
