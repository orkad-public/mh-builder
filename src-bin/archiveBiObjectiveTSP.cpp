/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptative metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Cyprien Borée and additional contributors (see Authors)
****************************************************************************************/

#include <chrono>
#include <iostream>
#include <fstream>
#include <string>

#include "core/fitness/FitnessMin.h"
#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEvalBiObjective.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "core/archive/NonDominatedArchive.h"
#include "core/archive/BoundedArchive.h"
#include "core/archive/BoundedDiversityArchive.h"

#include "util/RNGHelper.h"
#include "util/ParserHelper.h"

#include "opt/criterion/TimeCriterion.h"

#include "opt/manySolutions/selection/SelectAll.h"
#include "opt/manySolutions/explorer/Explorer.h"
#include "opt/manySolutions/explorer/NDomExplorer.h"

#include "opt/singleSolution/neighborhood/Neighborhood.h"
#include "opt/singleSolution/neighborhood/RandomNeighborhood.h"


#include "core/archive/HammingDiversityCriterion.h"
#include "core/archive/JaccardDiversityCriterion.h"
#include "core/archive/ArchiveDiversityCriterion.h"
#include "core/archive/AGADiversityCriterion.h"
#include "core/archive/LAHCDiversityCriterion.h"

#include "opt/manySolutions/localsearch/ParetoLocalSearch.h"
#include "opt/manySolutions/localsearch/IteratedParetoLocalSearch.h"

#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/BiObjectiveSpreadMetric.h"

#include "representation/permutation/neighborhood/neighbor/TwoOptNeighbor.h"

/*          TYPEDEFs                    */
using FIT = core::fitness::FitnessMin<double, double>;
using SOL = representation::permutation::problems::tsp::TSPSol<FIT>;

using EVAL = representation::permutation::problems::tsp::TSPEvalBiObjective<SOL>;
using NON_DOMINATED_ARCHIVE = core::archive::NonDominatedArchive<SOL>;
using BOUNDED_ARCHIVE = core::archive::BoundedArchive<SOL, NON_DOMINATED_ARCHIVE>;
using BOUNDED_DIVERSITY_ARCHIVE = core::archive::BoundedDiversityArchive<SOL, NON_DOMINATED_ARCHIVE>;

/*          DEFAULT PARAMETER VALUES    */
const unsigned long long int INITIAL_SOLUTIONS = 1'000'000;
const unsigned long long int ARCHIVE_SIZE = 100;
const long long int TIME_MAX = 60000; // milliseconds
const uint16_t AGA_DIV_SIZE = 8;
const string DIV_ALG = "AGA";
const string FIRST_INSTANCE = "../../instances/tsp/bi-obj-tsp/random_100_1.tsp";
const string SECOND_INSTANCE = "../../instances/tsp/bi-obj-tsp/random_100_2.tsp";

/*          EXAMPLES            */
/*  Run PLS on archive of size 50, from 10 random initial solutions for 20000ms:
    ./archiveBiObjectiveTSP --time_max=20000 --archive_size=50 --init_size=10 --diversity_algorithm AGA
*/
int main(int argc, char* argv[]) {
    /*          PARSER DEFINITION           */
    util::ParserHelper parserHelper;
    auto now = std::chrono::system_clock::now();
    auto now_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
    auto value = now_ms.time_since_epoch();
    unsigned long long duration = value.count();
    parserHelper.setAs<unsigned long long int>("--seed", 1)->setImplicit(duration);
    parserHelper.setAs<unsigned long long int>("--init_size", INITIAL_SOLUTIONS);
    parserHelper.setAs<unsigned int>("--archive_size", ARCHIVE_SIZE);
    parserHelper.setAs<long long int>("--time_max", TIME_MAX);
    parserHelper.setAs<bool>("--benchmark",false)->setImplicit(true);
    parserHelper.setAs<unsigned int>("--benchmark_time", TIME_MAX);
    parserHelper.setAs<unsigned long long int>("--benchmark_init_solutions", INITIAL_SOLUTIONS);
    parserHelper.setAs<unsigned int>("--benchmark_archive_size", ARCHIVE_SIZE);
    parserHelper.setAs<unsigned>("--benchmark_runs", 2);
    parserHelper.setAs<string>("--diversity_algorithm", DIV_ALG);
    parserHelper.setAs<unsigned int>("--aga_div_size", AGA_DIV_SIZE);
    parserHelper.setAs<string>("--instance1", FIRST_INSTANCE);
    parserHelper.setAs<string>("--instance2", SECOND_INSTANCE);
    parserHelper.parse(argc, argv);

    auto seed = parserHelper.getAs<unsigned long long int>("--seed");
    auto init_size = parserHelper.getAs<unsigned long long int>("--init_size");
    auto archive_size = parserHelper.getAs<unsigned int>("--archive_size");
    auto time_max = parserHelper.getAs<long long int>("--time_max");
    auto diversity_algorithm = parserHelper.getAs<string>("--diversity_algorithm");
    auto aga_div_size = parserHelper.getAs<unsigned int>("--aga_div_size");
    auto first_instance = parserHelper.getAs<string>("--instance1");
    auto second_instance = parserHelper.getAs<string>("--instance2");

    util::RNGHelper::get()->reseed(static_cast<int>(seed));
    representation::permutation::problems::tsp::TSP tsp_instance1(first_instance);
    representation::permutation::problems::tsp::TSP tsp_instance2(second_instance);
    representation::permutation::problems::tsp::TSPEval<SOL> tsp1(tsp_instance1);
    representation::permutation::problems::tsp::TSPEval<SOL> tsp2(tsp_instance2);

    EVAL eval(tsp1, tsp2);

    // Selecting Archiving algorithm
    core::archive::ArchiveDiversityCriterion<SOL>* ADC = nullptr;
    if (diversity_algorithm.compare("hamming") == 0) {
        ADC = new core::archive::HammingDiversityCriterion<SOL>();
    }
    if (diversity_algorithm.compare("jaccard") == 0) {
        ADC = new core::archive::JaccardDiversityCriterion<SOL>();
    }
    if (diversity_algorithm.compare("AGA") == 0) {
        ADC = new core::archive::AGADiversityCriterion<SOL>(static_cast<unsigned short>(aga_div_size));
    }
    if (diversity_algorithm.compare("LAHC") == 0) {
        ADC = new core::archive::LAHCDiversityCriterion<SOL>();
    }
    BOUNDED_DIVERSITY_ARCHIVE bounded_archive(new NON_DOMINATED_ARCHIVE(), archive_size, ADC);

    // Fill archive with random solutions.
    for (unsigned i = 0; i < init_size; ++i) {
        SOL tmp_sol(tsp_instance1.getNumberOfCities());
        tmp_sol.shuffle();
        eval(tmp_sol);
        bounded_archive(tmp_sol);
    }
    // Initial archive.
    std::cout << "Initial archive contains " << bounded_archive.size() << " solutions: " << std::endl;
    std::cout << bounded_archive << std::endl;
    std::cout << endl;

    opt::criterion::TimeCriterion<BOUNDED_DIVERSITY_ARCHIVE> time_criterion(time_max);
    auto *select = new opt::manySolutions::selection::SelectAll<BOUNDED_DIVERSITY_ARCHIVE>();
    representation::permutation::neighborhood::neighbor::TwoOptNeighbor<SOL> twoOptNeighbor;
    opt::singleSolution::neighborhood::RandomNeighborhood<SOL> randomNeighborhood(twoOptNeighbor);
    opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood = &randomNeighborhood;
    opt::manySolutions::explorer::Explorer<BOUNDED_DIVERSITY_ARCHIVE> *explorer = new opt::manySolutions::explorer::NDomExplorer<BOUNDED_DIVERSITY_ARCHIVE>(*neighborhood, eval);

    // Use NON_DOMINATED_ARCHIVE as a template parameter to prevent deadlock situation.
    opt::manySolutions::localsearch::ParetoLocalSearch<BOUNDED_DIVERSITY_ARCHIVE, NON_DOMINATED_ARCHIVE> paretoLocalSearch(*select, *explorer);
    time_criterion.init();
    paretoLocalSearch(bounded_archive, time_criterion);
    std::cout << "Final archive contains " << bounded_archive.size() << " solutions : " << std::endl;
    std::cout << bounded_archive << std::endl;
    opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<BOUNDED_DIVERSITY_ARCHIVE> biObjectiveHypervolumeMetric;
    opt::manySolutions::metrics::BiObjectiveSpreadMetric<BOUNDED_DIVERSITY_ARCHIVE> biObjectiveSpreadMetric;
    std::cout << "HYPERVOLUME = " << biObjectiveHypervolumeMetric(bounded_archive) << std::endl;
    std::cout << "SPREAD = " << biObjectiveSpreadMetric(bounded_archive) << std::endl;
    return 0;
}




