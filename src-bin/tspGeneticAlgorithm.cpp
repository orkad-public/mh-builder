/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/

#include "util/RNGHelper.h"
#include "core/fitness/FitnessMin.h"

#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/problems/tsp/algo/SimpleGreedy.h"
#include "core/archive/AllAcceptedArchive.h"

#include "opt/manySolutions/geneticAlgorithm/selection/TournamentSelection.h"
#include "opt/manySolutions/geneticAlgorithm/crossover/OXCrossover.h"
#include "opt/manySolutions/geneticAlgorithm/replacement/ElitismReplacement.h"
#include "opt/criterion/TimeCriterion.h"
#include "opt/checkpoint/ArchiveCheckpoint.h"
#include "opt/manySolutions/geneticAlgorithm/GeneticAlgorithm.h"


#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "../external/mathplotlib/mathplotlib.h"


using namespace representation::permutation::problems::tsp ;
using namespace opt::manySolutions::geneticAlgorithm ;


int main(void) {
    typedef core::fitness::FitnessMin<double> FIT;
    typedef TSPSol<FIT> SOL;
    typedef TSPEval<SOL> EVAL;
    typedef core::archive::AllAcceptedArchive<SOL> POPULATION;
    typedef selection::TournamentSelection<POPULATION> SELECTION ;
    typedef crossover::OXCrossover<POPULATION> CROSSOVER ;
    typedef replacement::ElitismReplacement<POPULATION> REPLACEMENT ;
    typedef mutation::Mutation<POPULATION> MUTATION ;
    typedef opt::criterion::TimeCriterion<POPULATION> STOP ;
    typedef GeneticAlgorithm<POPULATION> GA ;

    std::cout << "*****************************************************************************" << std::endl ;
    std::cout << "Example of a TSP optimisation problem mono objective with a genetic algorithm" << std::endl ;
    std::cout << "*****************************************************************************" << std::endl ;



    util::RNGHelper::get()->reseed(120);
    std::string ficName = "../../instances/tsp/tsp_test/rat783.tsp";
    std::cout << "Process file :" + ficName << std::endl;

    TSP tsp_instance(ficName);
    EVAL eval(tsp_instance);


    //----------- INITIAL SOLUTION -------------------//
    SOL sol(tsp_instance.getNumberOfCities());

    representation::permutation::problems::tsp::algo::SimpleGreedy<SOL> sg_algorithm =
            representation::permutation::problems::tsp::algo::SimpleGreedy<SOL>(tsp_instance);

    sg_algorithm(sol);
    eval(sol);
    std::cout << "result of simple greedy algorithm " << sol << std::endl;



    POPULATION population;
    // init population of 50 individuals
    for(unsigned long long int i=0; i<50;++i){
        SOL solution(tsp_instance.getNumberOfCities());
        solution.shuffle() ; eval(solution);
        population(solution);
    }


    population(sol) ; // inserting the solution found by the greedy algorithm
    population.sort();
    std::cout << "the init population (sorted):" << std::endl << population << std::endl ;



    STOP stopGen(5000) ; // using TimeCriterion with 5 seconds

    SELECTION sel(5, population.size()*.8);
    CROSSOVER crossover(0.8, eval) ;
    // neighbor operator for mutation step
    opt::factory::singleSolution::NeighborFactory<SOL, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL>> neighborFactory;
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> *neighbor = neighborFactory.create("2opt");
    // mutation operator
    MUTATION mutation(0.15, eval,*neighbor);

    REPLACEMENT replace ;
    opt::checkpoint::ArchiveCheckpoint<POPULATION> archiveCheckpoint;

    GA ga(&sel, &crossover, &mutation, &replace, &stopGen) ;
    ga.setCheckpoint(archiveCheckpoint);
    ga(population) ; // run the genetic algorithm


    population.sort();
    std::cout << "the population after the genetic algorithm:" << std::endl << population << std::endl ;

    std::cout << "And the best solution is: " << population[0].fitness() << std::endl ;
    std::cout << "Number of generations : " << ga.getNumberOfGenerations() << std::endl ;

        matplotlibcpp::named_plot("best Obj (t)", archiveCheckpoint.getTimeValues(), archiveCheckpoint.getValues(0));
        matplotlibcpp::named_plot("best Obj (0->t)", archiveCheckpoint.getTimeValues(), archiveCheckpoint.getBestValues(0));
        matplotlibcpp::xlim(0, 5000);
        matplotlibcpp::title("Genetic Algorithm");
        matplotlibcpp::xlabel("Time (milliseconds)");
        matplotlibcpp::ylabel("fitness objective");
        matplotlibcpp::legend();
        matplotlibcpp::show();

}
