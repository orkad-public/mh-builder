/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/


#include "util/RNGHelper.h"
#include "../external/mathplotlib/mathplotlib.h"

#include "core/fitness/FitnessMin.h"
#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/problems/tsp/TSPEvalBiObjective.h"

#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"

#include "opt/manySolutions/geneticAlgorithm/crossover/OXCrossover.h"
#include "opt/manySolutions/geneticAlgorithm/mutation/Mutation.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Solution.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Algorithm.h"
#include "opt/manySolutions/geneticAlgorithm/NSGA2_Eval.h"
#include "opt/criterion/TimeCriterion.h"
#include "core/archive/AllAcceptedArchive.h"
#include "opt/checkpoint/ArchiveCheckpoint.h"



using namespace representation::permutation::problems::tsp;

#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/MultiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/BiObjectiveSpreadMetric.h"


int main() {
    typedef core::fitness::FitnessMin<double, double> FIT;
    typedef TSPSol<FIT> SOL;
    typedef TSPEvalBiObjective<SOL> EVAL;


    typedef opt::manySolutions::geneticAlgorithm::NSGA2_Solution<SOL> NSGA2_SOL;
    typedef core::archive::AllAcceptedArchive<NSGA2_SOL> POP;
    typedef opt::manySolutions::geneticAlgorithm::crossover::OXCrossover<POP> CROSSOVER;
    typedef opt::manySolutions::geneticAlgorithm::mutation::Mutation<POP> MUTATION;
    typedef opt::criterion::TimeCriterion<POP> STOP;
    typedef opt::manySolutions::geneticAlgorithm::NSGA2_Algorithm<POP> GA;

    std::cout << "***********************************************************************************" << std::endl ;
    std::cout << "Example of a TSP optimisation problem bi objective with a NSGA-II genetic algorithm" << std::endl ;
    std::cout << "***********************************************************************************" << std::endl ;
    auto seed = 200;

    util::RNGHelper::get()->reseed(seed);

    TSP tsp_instance1("../../instances/tsp/tsp_test/cgC50_10_01.txt");
    TSP tsp_instance2("../../instances/tsp/tsp_test/cgC50_10_02.txt");

    TSPEval<SOL> tsp1eval(tsp_instance1);
    TSPEval<SOL> tsp2eval(tsp_instance2);
    EVAL eval(tsp1eval, tsp2eval);

    NSGA2_Eval<SOL> nsga2_eval(eval);

    POP population;
    population.clear();

    // init population of 200 individuals
    for(unsigned long long int i=0; i<200;++i){
        SOL solution(tsp_instance1.getNumberOfCities());
        solution.shuffle() ;
        NSGA2_SOL decorator(solution) ; nsga2_eval(decorator);
        population(decorator);
    }

    std::cout << "Population of the archive initialized: " << std::endl;
    std::cout << population << std::endl;

    // components for NSGA2 algorithm

    STOP stopGen(5000);
    CROSSOVER crossover(0.8, nsga2_eval);
    // neighbor operator for mutation step
    opt::factory::singleSolution::NeighborFactory<NSGA2_SOL, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<NSGA2_SOL>> neighborFactory;
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<NSGA2_SOL> *neighbor = neighborFactory.create("2opt");
    MUTATION mutation(0.15, nsga2_eval, *neighbor);
    opt::checkpoint::ArchiveCheckpoint<POP> archiveCheckpoint(500); // checkpoints every 500 milliseconds

    GA ga(
            &crossover,
            &mutation,
            nsga2_eval,
            &stopGen);
    ga.setCheckpoint(archiveCheckpoint);
    ga(population); // run the genetic algorithm



    std::cout << "FINAL ARCHIVE :" << std::endl;
    population.sort();
    std::cout << population << std::endl;
    opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<POP> biObjectiveHypervolumeMetric;
    opt::manySolutions::metrics::BiObjectiveSpreadMetric<POP> biObjectiveSpreadMetric;

    double hypervolume = biObjectiveHypervolumeMetric(population);
    double spread = biObjectiveSpreadMetric(population);
    std::cout << "Result: SUCCESS, " << 1;
    std::cout << ", [" << hypervolume << ", " << spread << "], " << seed << std::endl;


    /** begin section in order to see the evolution of the hypervolume for all checkpoints. **/
    std::vector<std::pair<double, double>> bounds ;
    // step 1 : fixed bounds
    bounds.push_back({archiveCheckpoint.getCheckpoints()[0][0].fitness()[0],
                      archiveCheckpoint.getCheckpoints()[0][0].fitness()[0]}) ;
    bounds.push_back({bounds[0].first,bounds[0].second})  ; // init bounds with values of the first sol of first archive
    for (auto archive : archiveCheckpoint.getCheckpoints()) {
        for (auto nObj=0 ; nObj < 2 ; nObj++)
            for (auto sol: archive) {
                bounds[nObj].first = std::min(bounds[nObj].first, sol.fitness()[nObj]);
                bounds[nObj].second = std::max(bounds[nObj].second, sol.fitness()[nObj]);
            }
    }


    // step 2 : compute hypervolume
    std::vector<double> hypervolumes ;
    opt::manySolutions::metrics::MultiObjectiveHypervolumeMetric<POP> multiObjectiveHypervolumeMetric ;
    for (auto archive : archiveCheckpoint.getCheckpoints()) {
        hypervolumes.push_back (multiObjectiveHypervolumeMetric(archive,bounds));
    }
    matplotlibcpp::figure(1) ;
    matplotlibcpp::named_plot("Hypervolume (t)", archiveCheckpoint.getTimeValues(), hypervolumes);
    matplotlibcpp::title("Genetic Algorithm");
    matplotlibcpp::xlabel("Time (milliseconds)");
    matplotlibcpp::ylabel("hypervolume");
    matplotlibcpp::legend();


    matplotlibcpp::figure(2) ;
    matplotlibcpp::named_plot("Best Obj 1 (t)", archiveCheckpoint.getTimeValues(), archiveCheckpoint.getValues(0));
    matplotlibcpp::named_plot("Best Obj 2 (t)", archiveCheckpoint.getTimeValues(), archiveCheckpoint.getValues(1));
    matplotlibcpp::named_plot("Best Obj 1 (t0 -> t)", archiveCheckpoint.getTimeValues(), archiveCheckpoint.getBestValues(0));
    matplotlibcpp::named_plot("Best Obj 2 (t0 -> t)", archiveCheckpoint.getTimeValues(), archiveCheckpoint.getBestValues(1));
    matplotlibcpp::xlim(0, 5000);
    matplotlibcpp::title("Genetic Algorithm");
    matplotlibcpp::xlabel("Time (milliseconds)");
    matplotlibcpp::ylabel("objective values");
    matplotlibcpp::legend();
    matplotlibcpp::show(); // show both figures
    return 0;

}
