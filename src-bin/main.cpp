/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include <string>
#include <iostream>

#include "core/fitness/FitnessMax.h"
#include "core/archive/NonDominatedArchive.h"
#include "core/Perturbation.h"
#include "representation/rulemining/RulesetEval.h"
#include "representation/rulemining/archive/CustomBoundedArchive.h"
#include "util/ParserHelper.h"
#include "util/TimerHelper.h"

#include "representation/rulemining/Rulemining.h"
#include "representation/rulemining/neighborhood/RulesetNeighborhood.h"

#include "opt/manySolutions/selection/SelectAll.h"
#include "opt/manySolutions/selection/SelectRand.h"
#include "opt/manySolutions/selection/SelectNewest.h"
#include "opt/manySolutions/selection/SelectOldest.h"

#include "opt/manySolutions/explorer/Explorer.h"
#include "opt/manySolutions/explorer/FirstImpNDomExplorer.h"
#include "opt/manySolutions/explorer/ImpExplorer.h"
#include "opt/manySolutions/explorer/ImpNDomExplorer.h"
#include "opt/manySolutions/explorer/NDomExplorer.h"

#include "representation/rulemining/algorithm/InitPopulationRuleset.h"
#include "opt/singleSolution/perturbation/NeighborhoodPerturbation.h"
#include "opt/perturbation/RestartPerturbation.h"
#include "opt/manySolutions/perturbation/KickPerturbation.h"
#include "opt/criterion/TimeCriterion.h"
#include "opt/criterion/KeyboardHitCriterion.h"

#include "opt/manySolutions/localsearch/ParetoLocalSearch.h"
#include "opt/manySolutions/localsearch/IteratedParetoLocalSearch.h"

#include "opt/manySolutions/metrics/BiObjectiveHypervolumeMetric.h"
#include "opt/manySolutions/metrics/BiObjectiveSpreadMetric.h"

using namespace representation::rulemining;


/* Example of command:
 * ./bin/MH-Builder --seed=1 --instance_desc=../instances/rulemining/habermand/habrmand.desc 
 * --instance_training=../instances/rulemining/habermand/habermand.individuals.1.training 
 * --instance_test=../instances/rulemining/habermand/habermand.individuals.1.test 
 * --select_strategy=all --explorer_strategy=all --init_size=200 --max_rules=20 --archive_size=500
 */

void optional_help_option(int argc,char *argv[]) ;

void optional_help_option(int argc,char *argv[]) {
    if (argc !=2) return ;
    string option(argv[1]) ;
    if (option !="--help" && option != "-h") return ;
    cout << "Usage: " << argv[0] << " [OPTIONS]" << endl;
    cout << "Available command line options are:" << endl ;
    cout << " -h, --help\t\t\t\tdisplay this message" << endl ;
    cout << " --instance_desc=<FILE>\t\t\tset description file of the dataset" <<endl;
    cout << " --instance_training=<FILE>\t\t\tset file dataset for training phase" << endl;
    cout << " --instance_test=<FILE>\t\t\tset file dataset for test phase" << endl ;
    cout << " --seed=<NUMBER>\t\t\tset a seed according to given NUMBER (mersenne twister)" << endl;
    cout << "        (default: 1)" << endl ;
    cout << " --select_strategy=<CHOICE>\t\tset a select strategy" << endl ;
    cout << "        Available strategies are: all (default), one, newest, oldest, or a positive number" << endl ;
    cout << " --explorer_strategy=<CHOICE>\t\tset a explorer strategy" << endl ;
    cout << "        Available strategies are: all (default), all-imp, first, imp, imp-ndom and ndom" << endl ;
    cout << " --explor_size=<NUMBER>\t\t\tset exploration size for explorer (imp, imp-ndom and ndom)" << endl ;
    cout << "        (default: 1)"<<endl;
    cout << " --perturbation_strategy=<CHOICE>\tset a perturbation strategy" << endl ;
    cout << "        Available strategies are: restart (default), kick, kick-all" << endl ;
    cout << " --perturbation_strength=<NUMBER>\tset perturbation strength for neighborhood" << endl ;
    cout << "        (default: 3)"<<endl;
    cout << " --kick_size=<NUMBER>\t\t\tset kick size for for perturbation strategy : kick" << endl ;
    cout << "        (default: 3)"<<endl;
    cout << " --init_size=<NUMBER>\t\t\tset init size for a population ruleset" << endl ;
    cout << "        (default: 100)"<<endl;
    cout << " --archive_size=<NUMBER>\t\tset size for the archive" << endl ;
    cout << "        (default: 500), -1 if unbounded"<<endl;
    cout << " --max_rules=<NUMBER>\t\t\tset max rules" << endl ;
    cout << "        (default: 10)"<<endl;
    cout << " --objectives=<NUMBER>\t\tset objectives for evaluation ruleset" << endl ;
    cout << "        1: fmeasure, 2: gmean, 3: sensitivity and confidence," << endl ;
    cout << "        4: sensitivity,confidence and nb terms (default), "<< endl;
    cout << "        5: sensitivity, confidence, nb terms and specificity, 6: all" << endl;
    cout << " --required_time=<NUMBER>\t\tset time stop criterion in milliseconds" << endl ;
    cout << "        (default: -1 : no stop criterion)"<<endl;
    cout << " --moparamils=<BOOLEAN>\t\t\tset display mode for multi objectives" << endl ;
    cout << "        (default: false)"<<endl;
    cout << " --verbose=<BOOLEAN>\t\t\tset mode verbose" << endl ;
    cout << "        (default: true)"<<endl;
    exit(0) ;
}

int main(int argc, char *argv[]) {
    optional_help_option(argc,argv) ;
    //TYPE DEFINITION
    typedef core::fitness::FitnessMax<double> FIT;
    typedef representation::rulemining::RulesetSolution<FIT> SOL;
    typedef representation::rulemining::RulesetEval<SOL> EVAL;
    typedef core::archive::NonDominatedArchive<SOL> UNBOUNDED_ARCHIVE;
    typedef representation::rulemining::archive::CustomBoundedArchive<SOL, UNBOUNDED_ARCHIVE> ARCHIVE;

    util::ParserHelper parserHelper;

    parserHelper.setAs<unsigned long long int>("--seed", 1)->setImplicit(time(nullptr));
    parserHelper.setAs<std::string>("--instance_desc", "../../instances/rulemining/ecoli1d/ecoli1d.desc");
    parserHelper.setAs<std::string>("--instance_training",
                                    "../../instances/rulemining/ecoli1d/ecoli1d.individuals.1.training");
    parserHelper.setAs<std::string>("--instance_test", "../../instances/rulemining/ecoli1d/ecoli1d.individuals.1.test");
    parserHelper.setAs<std::string>("--select_strategy", "all");
    parserHelper.setAs<std::string>("--explorer_strategy", "all");
    parserHelper.setAs<std::string>("--perturbation_strategy", "restart");
    parserHelper.setAs<unsigned long long int>("--init_size", 100);
    parserHelper.setAs<unsigned long long int>("--kick_size", 3);
    parserHelper.setAs<unsigned long long int>("--explor_size", 1);
    parserHelper.setAs<unsigned long long int>("--perturbation_strength", 3);
    parserHelper.setAs<unsigned long long int>("--archive_size", 500); // -1 for unbounded
    parserHelper.setAs<unsigned long long int>("--max_rules", 10);
    parserHelper.setAs<unsigned long long int>("--objectives", 4);
    parserHelper.setAs<long long int>("--required_time", -1); // time in ms, -1 for natural stop criterion
    parserHelper.setAs<bool>("--moparamils", false);
    parserHelper.setAs<bool>("--verbose", true)->setImplicit(true);

    parserHelper.parse(argc, argv);
    auto seed = parserHelper.getAs<unsigned long long int>("--seed");
    auto desc = parserHelper.getAs<std::string>("--instance_desc");
    auto training = parserHelper.getAs<std::string>("--instance_training");
    auto test = parserHelper.getAs<std::string>("--instance_test");
    auto select_strategy = parserHelper.getAs<std::string>("--select_strategy");
    auto explorer_strategy = parserHelper.getAs<std::string>("--explorer_strategy");
    auto perturbation_strategy = parserHelper.getAs<std::string>("--perturbation_strategy");
    auto perturbation_strength = parserHelper.getAs<unsigned long long int>("--perturbation_strength");
    auto kick_size = parserHelper.getAs<unsigned long long int>("--kick_size");
    auto archive_size = parserHelper.getAs<unsigned long long int>("--archive_size");
    auto init_size = parserHelper.getAs<unsigned long long int>("--init_size");
    auto explor_size = parserHelper.getAs<unsigned long long int>("--explor_size");
    auto max_rules = parserHelper.getAs<unsigned long long int>("--max_rules");
    auto objectives = parserHelper.getAs<unsigned long long int>("--objectives");
    auto required_time = parserHelper.getAs<long long int>("--required_time");
    auto moparamils_output = parserHelper.getAs<bool>("--moparamils");
    auto verbose = parserHelper.getAs<bool>("--verbose");

    Rulemining ruleminingInstanceTraining(desc, training);

    Rulemining ruleminingInstanceTest(desc, test);

    if (verbose) std::cout << "Instance loaded" << std::endl;
    util::RNGHelper::get()->reseed(seed);

    SOL sol;

    if (objectives < 1 && objectives > 6)
        throw std::runtime_error(R"(objectives option must be a number in 1..6)");
    RuleminingEvalType val = static_cast<RuleminingEvalType>(objectives);
    EVAL eval(ruleminingInstanceTraining,val);
    EVAL evalTest(ruleminingInstanceTest,val);


    neighborhood::RulesetNeighborhood<SOL> rulesetNeighborhood(ruleminingInstanceTraining, max_rules);

    // CHOSE SELECT STRATEGY
    opt::manySolutions::selection::SelectAll<ARCHIVE> *select;
    if (select_strategy == "all")
        select = new opt::manySolutions::selection::SelectAll<ARCHIVE>();
    else if (select_strategy == "one")
        select = new opt::manySolutions::selection::SelectRand<ARCHIVE>(1);
    else if (util::StringHelper::isNumber(select_strategy))
        select = new opt::manySolutions::selection::SelectRand<ARCHIVE>(std::stoi(select_strategy));
    else if (select_strategy == "newest")
        select = new opt::manySolutions::selection::SelectNewest<ARCHIVE>(1);
    else if (select_strategy == "oldest")
        select = new opt::manySolutions::selection::SelectOldest<ARCHIVE>(1);
    else
        throw std::runtime_error(R"(Selection strategy unknown. Choose between ["all", "one", "newest", "oldest"] or an unsigned integer)");

    // CHOOSE EXPLORER STRATEGY

    opt::manySolutions::explorer::Explorer<ARCHIVE> *explorer;
    if (explorer_strategy == "all")
        explorer = new opt::manySolutions::explorer::NDomExplorer<ARCHIVE>(rulesetNeighborhood, eval);
    else if (explorer_strategy == "all-imp")
        explorer = new opt::manySolutions::explorer::ImpExplorer<ARCHIVE>(rulesetNeighborhood, eval);
    else if (explorer_strategy == "first")
        explorer = new opt::manySolutions::explorer::ImpNDomExplorer<ARCHIVE>(rulesetNeighborhood, eval);
    else if (explorer_strategy == "imp")
        explorer = new opt::manySolutions::explorer::ImpExplorer<ARCHIVE>(rulesetNeighborhood, eval, explor_size);
    else if (explorer_strategy == "imp-ndom")
        explorer = new opt::manySolutions::explorer::ImpNDomExplorer<ARCHIVE>(rulesetNeighborhood, eval, explor_size);
    else if (explorer_strategy == "ndom")
        explorer = new opt::manySolutions::explorer::NDomExplorer<ARCHIVE>(rulesetNeighborhood, eval, explor_size);
    else
        throw std::runtime_error(
                R"(Exploration strategy unknown. Choose between ["all", "all-imp", "first", "imp", "imp-ndom", "ndom"])");

    // INIT ARCHIVE

    core::archive::NonDominatedArchive<SOL> nonDominatedArchive;
    ARCHIVE archive(&nonDominatedArchive, archive_size);
    algorithm::InitPopulationRuleset<ARCHIVE> initPop(ruleminingInstanceTraining, eval, init_size);

    // CHOOSE PERTURBATION STRATEGY
    core::Perturbation<ARCHIVE> *perturbation;
    opt::singleSolution::perturbation::NeighborhoodPerturbation<SOL> neighborhoodPerturbation(rulesetNeighborhood,
                                                                                              &eval,
                                                                                              perturbation_strength);
    if (perturbation_strategy == "restart")
        perturbation = new opt::perturbation::RestartPerturbation<ARCHIVE>(initPop);
    else if (perturbation_strategy == "kick")
        perturbation = new opt::manySolutions::perturbation::KickPerturbation<ARCHIVE>(neighborhoodPerturbation,
                                                                                       kick_size);
    else if (perturbation_strategy == "kick-all")
        perturbation = new opt::manySolutions::perturbation::KickPerturbation<ARCHIVE>(neighborhoodPerturbation);
    else
        throw std::runtime_error(R"(Perturbation strategy unknown. Choose between ["restart", "kick", "kick-all"])");


    //


    initPop(archive);


    if (verbose) std::cout << "Population of the archive initialized" << std::endl;

    // STOP Criterion
    opt::criterion::TimeCriterion<ARCHIVE> timeCriterion(required_time);
    /* core::Criterion<ARCHIVE> *stopCriterion ;

    if (required_time==-1)
        stopCriterion= new opt::criterion::KeyboardHitCriterion<ARCHIVE>() ;
    else
        stopCriterion= new opt::criterion::TimeCriterion<ARCHIVE>(required_time);
    */
    // ALGORITHM
    opt::manySolutions::localsearch::ParetoLocalSearch<ARCHIVE, UNBOUNDED_ARCHIVE> paretoLocalSearch(*select,
                                                                                                     *explorer);
    //paretoLocalSearch.setCriterion(*stopCriterion) ;
    opt::perturbation::RestartPerturbation<ARCHIVE> restartPerturbation(initPop);
    opt::manySolutions::localsearch::IteratedParetoLocalSearch<ARCHIVE> iteratedParetoLocalSearch(*perturbation,
                                                                                                  paretoLocalSearch,
                                                                                                  timeCriterion);

    util::TimerHelper timerHelper;

    timerHelper.start();
    if (required_time == -1) {
        if (verbose) std::cout << "Pareto Local Search started ..." << std::endl;
        paretoLocalSearch(archive);
    } else {
        if (verbose) std::cout << "Pareto Iterated Local Search started ..." << std::endl;
        iteratedParetoLocalSearch(archive);
    }
    timerHelper.update();
    if (verbose) std::cout << "Pareto Iterated Local Search done" << std::endl;

    if (!moparamils_output) {
        std::cout << "FINAL ARCHIVE :" << std::endl;
        archive.sort();

        for (const auto &i: archive) {
            ruleminingInstanceTraining.printSolution(std::cout, i, true, false);
            std::cout << std::endl;
        }

        // TO COMPUTE METRICS, NEED TO BE IN TWO OBJECTIVE
        eval.setRuleminingEvalType(SENSITIVITY_AND_CONFIDENCE);
        for (auto &i: archive) {
            eval(i);
        }

        archive.update();

        opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<ARCHIVE> biObjectiveHypervolumeMetric;
        opt::manySolutions::metrics::BiObjectiveSpreadMetric<ARCHIVE> biObjectiveSpreadMetric;

        std::cout << "TOTAL TIME = " << timerHelper.elapsedTime() << " ms " << std::endl;
        std::cout << "NB EVAL = " << eval.getNumberOfEvaluation() << " evaluations " << std::endl;
        std::cout << "NB SOLUTIONS = " << archive.size() << " solutions" << std::endl;
        std::cout << "HYPERVOLUME = " << biObjectiveHypervolumeMetric(archive) << std::endl;
        std::cout << "SPREAD = " << biObjectiveSpreadMetric(archive) << std::endl;
        std::cout << std::endl;

        eval.setRuleminingEvalType(FMEASURE_ONLY);
        for (auto &i: archive) {
            eval(i);
        }
        archive.update();

        eval.setRuleminingEvalType(ALL_METRICS);
        eval(archive[0]);
        std::cout << "TRAINING F-MEASURE : " << archive[0].fitness().objectives()[0] << std::endl;
        std::cout << "TRAINING G-MEAN : " << archive[0].fitness().objectives()[1] << std::endl;
        std::cout << "TRAINING SENSITIVITY : " << archive[0].fitness().objectives()[2] << std::endl;
        std::cout << "TRAINING CONFIDENCE : " << archive[0].fitness().objectives()[3] << std::endl;
        std::cout << "TRAINING SPECIFICITY : " << archive[0].fitness().objectives()[4] << std::endl;
        std::cout << "TRAINING MCC : " << archive[0].fitness().objectives()[5] << std::endl;
        std::cout << "TRAINING TP : " << archive[0].fitness().objectives()[6] << std::endl;
        std::cout << "TRAINING FN : " << archive[0].fitness().objectives()[7] << std::endl;
        std::cout << "TRAINING FP : " << archive[0].fitness().objectives()[8] << std::endl;
        std::cout << "TRAINING TN : " << archive[0].fitness().objectives()[9] << std::endl;

        std::cout << std::endl;

        evalTest.setRuleminingEvalType(ALL_METRICS);
        evalTest(archive[0]);
        std::cout << "TEST F-MEASURE : " << archive[0].fitness().objectives()[0] << std::endl;
        std::cout << "TEST G-MEAN : " << archive[0].fitness().objectives()[1] << std::endl;
        std::cout << "TEST SENSITIVITY : " << archive[0].fitness().objectives()[2] << std::endl;
        std::cout << "TEST CONFIDENCE : " << archive[0].fitness().objectives()[3] << std::endl;
        std::cout << "TEST SPECIFICITY : " << archive[0].fitness().objectives()[4] << std::endl;
        std::cout << "TEST MCC : " << archive[0].fitness().objectives()[5] << std::endl;
        std::cout << "TEST TP : " << archive[0].fitness().objectives()[6] << std::endl;
        std::cout << "TEST FN : " << archive[0].fitness().objectives()[7] << std::endl;
        std::cout << "TEST FP : " << archive[0].fitness().objectives()[8] << std::endl;
        std::cout << "TEST TN : " << archive[0].fitness().objectives()[9] << std::endl;
        std::cout << std::endl;

        std::cout << "OUTPUT RULESET : " << std::endl;
        ruleminingInstanceTraining.printSolution(std::cout, archive[0], false);
    } else {
        // TO COMPUTE METRICS, NEED TO BE IN TWO OBJECTIVE
        eval.setRuleminingEvalType(SENSITIVITY_AND_CONFIDENCE);
        for (auto &i: archive) {
            eval(i);
        }

        opt::manySolutions::metrics::BiObjectiveHypervolumeMetric<ARCHIVE> biObjectiveHypervolumeMetric;
        opt::manySolutions::metrics::BiObjectiveSpreadMetric<ARCHIVE> biObjectiveSpreadMetric;

        double hypervolume = biObjectiveHypervolumeMetric(archive);
        double spread = biObjectiveSpreadMetric(archive);
        std::cout << "Result: SUCCESS, " << 1;
        std::cout << ", [" << hypervolume << ", " << spread << "], " << seed << std::endl;
    }

    delete select;
    delete explorer;
    return 0;
}
