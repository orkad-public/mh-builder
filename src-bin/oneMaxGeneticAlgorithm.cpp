/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics      *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/



#include "util/RNGHelper.h"
#include "core/fitness/FitnessMax.h"

#include "representation/bitstring/BitstringSolution.h"
#include "representation/bitstring/problems/onemax/OneMaxEval.h"
#include "core/archive/AllAcceptedArchive.h"
#include "opt/manySolutions/geneticAlgorithm/selection/FitnessProportionateSelection.h"
#include "opt/manySolutions/geneticAlgorithm/crossover/OnePointCrossover.h"
#include "opt/manySolutions/geneticAlgorithm/replacement/ElitismReplacement.h"
#include "opt/criterion/ArchiveEvalCriterion.h"
#include "opt/checkpoint/ArchiveCheckpoint.h"
#include "opt/manySolutions/geneticAlgorithm/GeneticAlgorithm.h"

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "representation/bitstring/neighborhood/neighbor/FlipNeighbor.h"


using namespace opt::manySolutions::geneticAlgorithm ;


int main() {
    typedef core::fitness::FitnessMax<unsigned  int, unsigned int> FIT;
    typedef representation::bitstring::BitstringSolution<FIT> SOL;
    typedef representation::bitstring::problems::onemax::OneMaxEval<SOL> EVAL;
    typedef core::archive::AllAcceptedArchive<SOL> POPULATION;
    typedef selection::FitnessProportionateSelection<POPULATION> SELECTION ;
    typedef crossover::OnePointCrossover<POPULATION> CROSSOVER ;
    typedef replacement::ElitismReplacement<POPULATION> REPLACEMENT ;
    typedef mutation::Mutation<POPULATION> MUTATION ;
    typedef opt::criterion::ArchiveEvalCriterion<POPULATION,SOL> STOP ;
    typedef GeneticAlgorithm<POPULATION> GA ;

    std::cout << "********************************************************************************" << std::endl ;
    std::cout << "Example of a OneMax optimisation problem mono objective with a genetic algorithm" << std::endl ;
    std::cout << "********************************************************************************" << std::endl ;

    EVAL eval ;

    STOP stopEval(3000,eval) ; // using EvalCriterion with 10000 evaluations
    SELECTION sel(40);
    CROSSOVER crossover(1, eval) ;
    // neighbor operator for mutation step
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> *neighbor = new representation::bitstring::neighborhood::neighbor::FlipNeighbor<SOL>() ;
    MUTATION mutation(1, eval,*neighbor);
    REPLACEMENT replace ;

    GA ga(&sel, &crossover, &mutation, &replace, &stopEval) ;
    for (int run= 1 ; run<=10; run ++) {
        util::RNGHelper::get()->reseed(10 * run);


        POPULATION population;
        // init population of 50 individuals
        for(unsigned long long int i=0; i<50;++i){
            SOL solution(512);
            solution.shuffle() ;
            eval(solution);
            population(solution);
        }

        ga(population); // run the genetic algorithm


        population.sort();
        auto cumulativeFitness = population[0].fitness()[0] ;
        for (unsigned long long int i=1 ; i< population.size();i++) cumulativeFitness += population[i].fitness()[0];
        std::cout << "run" << run << ": Best fitness: " << population[0].fitness()
                  << ", Worst fitness: " << population[population.size() - 1].fitness()
                  << ", Average fitness: " << (cumulativeFitness / population.size()) << std::endl;
    }

}
