/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Lucien Mousin and additional contributors (see Authors)
****************************************************************************************/

#include "../../external/mathplotlib/mathplotlib.h"
#include "opt/factory/singleSolution/NeighborhoodFactory.h"
#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/factory/singleSolution/NeighborhoodExplorerFactory.h"
#include "opt/factory/singleSolution/PerturbationFactory.h"
#include "opt/factory/criterion/CriterionFactory.h"

#include "core/fitness/FitnessMin.h"
#include "representation/permutation/problems/fsp/FSP.h"
#include "representation/permutation/problems/fsp/FSPEval.h"
#include "representation/permutation/problems/fsp/algo/NEH.h"
#include "representation/permutation/problems/fsp/algo/IteratedGreedyMakespan.h"

using namespace representation::permutation::problems::fsp ;

#include "util/ParserHelper.h"
#include "util/RNGHelper.h"

#include "opt/factory/singleSolution/NeighborhoodFactory.h"
#include "opt/factory/singleSolution/NeighborFactory.h"
#include "opt/factory/singleSolution/NeighborhoodExplorerFactory.h"
#include "opt/factory/singleSolution/PerturbationFactory.h"
#include "opt/factory/criterion/CriterionFactory.h"

#include "opt/singleSolution/neighborhood/neighbor/IndexNeighbor.h"
#include "core/Perturbation.h"
#include "core/Criterion.h"

#include "opt/singleSolution/neighborhood/explorer/TabuNeighborhoodExplorer.h"

#include "opt/singleSolution/localsearch/HillClimbing.h"
#include "opt/singleSolution/localsearch/coolingSchedule/SimpleCoolingSchedule.h"
#include "opt/singleSolution/localsearch/IteratedLocalSearch.h"
#include "opt/singleSolution/localsearch/LocalSearch.h"
#include "opt/singleSolution/localsearch/SimulatedAnnealing.h"

#include "opt/checkpoint/SolutionCheckpoint.h"

#include "representation/permutation/localsearch/IterativeImprovementInsertion.h"

namespace plt = matplotlibcpp;

int main(int argc, char *argv[]) {
  typedef core::fitness::FitnessMin<double, double> FIT;
  typedef FSPSol<FIT> SOL;
  typedef FSPEval<SOL> EVAL;

  util::ParserHelper parserHelper;

  parserHelper.setAs<unsigned long long int>("--seed", 1)->setImplicit(/*time(nullptr)*/ 1);
  parserHelper.setAs<std::string>("--instance", "../../instances/fsp/020_10_01.txt");
  parserHelper.setAs<std::string>("--localsearch_algorithm", "ils"); // "hc", "ils", "ig", "iii", "ts", "sa"
  parserHelper.setAs<unsigned long long int>("--sa_k", 10000);
  parserHelper.setAs<std::string>("--neighbor", "shift"); // "shift", "adj", "shift_without_adj", "swap"
  parserHelper.setAs<std::string>("--neighborhood", "rnd"); // "rnd", "order"
  parserHelper.setAs<std::string>("--neighborhood_explorer", "first"); // "best", "first", "worst-improve", "random"
  parserHelper.setAs<std::string>("--perturbation_strategy", "neighborhood"); // "neighborhood"
  parserHelper.setAs<unsigned long long int>("--perturbation_strength", 3);
  parserHelper.setAs<std::string>("--criterion_strategy", "time"); // "iter", "time"
  parserHelper.setAs<unsigned long long int>("--criterion_length", 1000);
  parserHelper.setAs<unsigned long long int>("--tabulist_size", 20);
  parserHelper.setAs<unsigned long long int>("--tabulist_iter", 20);

  parserHelper.parse(argc, argv);

  auto seed = parserHelper.getAs<unsigned long long int>("--seed");
  auto instance = parserHelper.getAs<std::string>("--instance");
  auto localsearch_algorithm = parserHelper.getAs<std::string>("--localsearch_algorithm");
  // auto sa_k = parserHelper.getAs<unsigned long long int>("--sa_k");
  auto neighbor_type = parserHelper.getAs<std::string>("--neighbor");
  auto neighborhood_type = parserHelper.getAs<std::string>("--neighborhood");
  auto neighborhood_explorer = parserHelper.getAs<std::string>("--neighborhood_explorer");
  auto perturbation_strategy = parserHelper.getAs<std::string>("--perturbation_strategy");
  auto perturbation_strength = parserHelper.getAs<unsigned long long int>("--perturbation_strength");
  auto criterion_strategy = parserHelper.getAs<std::string>("--criterion_strategy");
  auto criterion_length = parserHelper.getAs<unsigned long long int>("--criterion_length");
  auto tabulist_size = parserHelper.getAs<unsigned long long int>("--tabulist_size");
  auto tabulist_iter = parserHelper.getAs<unsigned long long int>("--tabulist_iter");


  util::RNGHelper::get()->reseed(seed);

  FSP fsp_instance(instance);
  EVAL eval(fsp_instance);
  //----------- INITIAL SOLUTION -------------------//
  SOL sol(fsp_instance.getN());
  algo::NEH<SOL> neh(fsp_instance, eval);
  neh(sol);
  std::cout << "INIT SOL" << std::endl;
  std::cout << sol << std::endl;

    //----------- NEIGHBOR --------------------------//
    opt::factory::singleSolution::NeighborFactory<SOL, opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL>> neighborFactory;
    opt::singleSolution::neighborhood::neighbor::IndexNeighbor<SOL> *neighbor = neighborFactory.create(neighbor_type);

    //----------- NEIGHBORHOOD ------------------------//
    opt::factory::singleSolution::NeighborhoodFactory<SOL> neighborhoodFactory;
    opt::singleSolution::neighborhood::Neighborhood<SOL> *neighborhood = neighborhoodFactory.create(neighborhood_type, *neighbor);

    //----------- NEIGHBORHOOD EXEPLORER ---------------//
    opt::factory::singleSolution::NeighborhoodExplorerFactory<SOL, EVAL> neighborhoodExplorerFactory;
    opt::singleSolution::neighborhood::explorer::NeighborhoodExplorer<SOL> *neighborhoodExplorer = neighborhoodExplorerFactory.create(neighborhood_explorer, *neighborhood, eval);
    opt::singleSolution::neighborhood::explorer::TabuNeighborhoodExplorer<SOL> tabuNeighborhoodExplorer(*neighborhoodExplorer, eval, tabulist_size, tabulist_iter);

    //---------- PERTURBATION ------------------------//
    opt::factory::singleSolution::PerturbationFactory<SOL, EVAL> perturbationFactory;
    core::Perturbation<SOL> *perturbation = perturbationFactory.create(perturbation_strategy, *neighborhood, &eval, perturbation_strength);

    //---------- STOP CRITERION ---------------------//
    opt::factory::criterion::CriterionFactory<SOL, EVAL> criterionFactory;
    core::Criterion<SOL> *criterion = criterionFactory.create(criterion_strategy, criterion_length, eval);

    //--------- LOCAL SEARCH ----------------------//
    // core::Algorithm<SOL> *localSearch;
    opt::checkpoint::SolutionCheckpoint<SOL> solutionCheckpoint;
    opt::singleSolution::localsearch::HillClimbing<SOL> hillclimbing(*neighborhoodExplorer, eval);
    representation::permutation::localsearch::IterativeImprovementInsertion<SOL> iterativeImprovementInsertion(eval, *criterion);

    opt::singleSolution::localsearch::coolingSchedule::SimpleCoolingSchedule<SOL> simpleCoolingSchedule(sol.fitness().scalar(), 100, 0.99, 1000);

  SOL tmp = sol;
  hillclimbing.setCheckpoint(solutionCheckpoint);
  std::cout << "RUNNING HILLCLIMBING" << std::endl;
  solutionCheckpoint.init(tmp);
  solutionCheckpoint(tmp);
  util::RNGHelper::get()->reseed(seed);
  hillclimbing.init(tmp);
  hillclimbing(tmp);
  plt::named_plot("Hillclimbing", solutionCheckpoint.getTimeValues(), solutionCheckpoint.getValues());
  std::cout << tmp << std::endl;

  tmp = sol;
    opt::singleSolution::localsearch::IteratedLocalSearch<SOL> ils(*perturbation, hillclimbing, *criterion);
  ils.setCheckpoint(solutionCheckpoint);
  std::cout << "RUNNING ILS" << std::endl;
  solutionCheckpoint.init(tmp);
  solutionCheckpoint(tmp);
  util::RNGHelper::get()->reseed(seed);
  ils.init(tmp);
  ils(tmp);
  plt::named_plot("ILS", solutionCheckpoint.getTimeValues(), solutionCheckpoint.getValues());
  std::cout << tmp << std::endl;

  tmp = sol;
  solutionCheckpoint.init(tmp);
  iterativeImprovementInsertion.setCheckpoint(solutionCheckpoint);
  algo::IteratedGreedyMakespan<SOL> ig(eval, *criterion, iterativeImprovementInsertion);
  ig.setCheckpoint(solutionCheckpoint);
  std::cout << "RUNNING IG" << std::endl;
  solutionCheckpoint.init(tmp);
  solutionCheckpoint(tmp);
  util::RNGHelper::get()->reseed(seed);
  ig.init(tmp);
  ig(tmp);
  plt::named_plot("IG", solutionCheckpoint.getTimeValues(), solutionCheckpoint.getValues());
  std::cout << tmp << std::endl;

  tmp = sol;
  solutionCheckpoint.init(tmp);
  iterativeImprovementInsertion.setCheckpoint(solutionCheckpoint);
  opt::singleSolution::localsearch::LocalSearch<SOL> ts(tabuNeighborhoodExplorer, eval, *criterion);
  ts.setCheckpoint(solutionCheckpoint);
  std::cout << "RUNNING TS" << std::endl;
  solutionCheckpoint.init(tmp);
  solutionCheckpoint(sol);
  util::RNGHelper::get()->reseed(seed);
  ts.init(tmp);
  ts(tmp);
  plt::named_plot("TS", solutionCheckpoint.getTimeValues(), solutionCheckpoint.getValues());
  std::cout << tmp << std::endl;

/*
    if ( localsearch_algorithm == "hc" ) {
      localSearch = &hillclimbing;
    } else if ( localsearch_algorithm == "ils" ) {
      hillclimbing.setCheckpoint(solutionCheckpoint);
      localSearch = new opt::IteratedLocalSearch<SOL>(*perturbation, hillclimbing, *criterion);
    } else if ( localsearch_algorithm == "ig") {
      localSearch = new fsp::IteratedGreedyMakespan<SOL>(eval, *criterion, iterativeImprovementInsertion);
    } else if ( localsearch_algorithm == "iii") {
      localSearch = &iterativeImprovementInsertion;
    } else if ( localsearch_algorithm == "ts" ) {
      localSearch = new opt::LocalSearch<SOL>(tabuNeighborhoodExplorer, eval, *criterion);
    } else if ( localsearch_algorithm == "sa") {
      localSearch = new opt::SimulatedAnnealing<SOL>(*neighborhoodExplorer, simpleCoolingSchedule, sa_k, *criterion);
    } else {
      throw std::runtime_error(R"(Local search algorithm unknown. Choose between ["hc", "ils", "ig", "iii", "ts", "sa"])");
    }

    (*localSearch).setCheckpoint(solutionCheckpoint);
    solutionCheckpoint(sol);

    std::cout << "RUNNING LOCAL SEARCH" << std::endl;
    (*localSearch)(sol);*/
  /*
  eval(sol);
  std::cout << sol << std::endl;*/

  plt::xlim(0, 1050);
  plt::title("FSP Local Search");
  plt::legend();
  plt::show();


  return 0;
}
