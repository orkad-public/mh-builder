/***************************************************************************************
*                 MH-Builder,   a framework for designing adaptive metaheuristics    *
*                               for single and multi-objective optimization.           *
*                    (c) 2019 University of Lille, CNRS                                *
*                                                                                      *
* This program is free software; you can redistribute it and/or modify it              *
* under the terms of the GNU General Public License as published by                    *
* the Free Software Foundation; either version 3 of the License, or (at                *
* your option) any later version.                                                      *
*                                                                                      *
* This program is distributed in the hope that it will be useful, but WITHOUT          *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License                 *
* for more details.                                                                    *
*                                                                                      *
* You should have received a copy of the GNU General Public License                    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.                 *
****************************************************************************************
         Authors: Olivier Caron and additional contributors (see Authors)
****************************************************************************************/


#include "util/ParserHelper.h"
#include "util/RNGHelper.h"
#include "core/fitness/FitnessMin.h"

#include "representation/permutation/problems/tsp/TSP.h"
#include "representation/permutation/problems/tsp/TSPEval.h"
#include "representation/permutation/problems/tsp/algo/SimpleGreedy.h"

using namespace representation::permutation::problems::tsp ;

int main(void) {
    typedef core::fitness::FitnessMin<double> FIT;
    typedef TSPSol<FIT> SOL;
    typedef TSPEval<SOL> EVAL;


    util::RNGHelper::get()->reseed(12);
    std::string ficName ;
    for(int i=1; i<=10; i++) {
        if (i < 10) ficName = "../../instances/tsp/tsp_test/gC50_0" + std::to_string(i) + ".txt";
        else ficName = "../../instances/tsp/tsp_test/gc50_" + std::to_string(i) + ".txt";
        std::cout << "Process file :"+ficName << std::endl ;
        TSP tsp_instance(ficName);
        EVAL eval(tsp_instance);


        //----------- INITIAL SOLUTION -------------------//
        SOL sol(tsp_instance.getNumberOfCities());

        representation::permutation::problems::tsp::algo::SimpleGreedy<SOL> sg_algorithm =
                representation::permutation::problems::tsp::algo::SimpleGreedy<SOL>(tsp_instance);

        sg_algorithm(sol);
        eval(sol);
        std::cout  << sol << std::endl;
    }

}